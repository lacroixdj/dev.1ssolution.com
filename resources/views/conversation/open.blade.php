
<!-- /.Mensaje Area -->
<div class="box box-primary direct-chat direct-chat-primary" style="border: 1px solid #d2d6de;">
    <div class="box-header with-border">
        <h3 class="box-title">Messages</h3>
            <div class="box-tools pull-right">
                <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    <!-- Conversations are loaded here -->
        <div class="direct-chat-messages">
        <!-- Message. Default to the left -->
        
                      
        </div>
        <!--/.direct-chat-messages-->
    </div>
    <!-- /.box-body -->    
    <div class="box-footer">
        <form action="#" method="post">
            <div class="input-group">
                <input type="text" id="input-message" name="message" placeholder="Type Message ..." class="form-control">
                <span class="input-group-btn">
                    <button type="button" id="send-message" class="btn btn-primary btn-flat">Send</button>
                </span>
            </div>
        </form>
    </div>
    <!-- /.box-footer-->
</div>  
    <!-- /.End Mensaje Area -->  

<script id="chat_message_template" type="text/template">
    <div class="direct-chat-msg">
        <!-- message-info -->
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-left"></span>
            <span class="direct-chat-timestamp pull-right"></span>
        </div>
        <!-- end message-info -->

        <!-- message img -->
        <img class="direct-chat-img" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="Message User Image">
        <!-- end message img -->

        <!-- message text -->
        <div class="direct-chat-text">
        </div>
        <!-- end message text -->
    </div>
</script>

<script id="chat_message_template_right" type="text/template">
    <div class="direct-chat-msg right">
        
        <!-- message-info -->
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-right"></span>
            <span class="direct-chat-timestamp pull-left"></span>
        </div>
        <!-- end message-info -->
        
        <!-- message img -->
        <img class="direct-chat-img" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="Message User Image">
        <!-- end message img -->

        <!-- message text -->
        <div class="direct-chat-text">
        </div>
        <!-- end message text -->
    </div>
</script>


<!-- Pusher lib (It loads from the web, because it doesn't make sense to use a local version of the lib --pusher won't work without internet connection ) -->
<script src="//js.pusher.com/3.0/pusher.min.js"></script>
<script>
  $(function(){  
    
    // Send on enter/return key
    function checkSend(e) {
    
        if (e.keyCode === 13) {
    
            return sendMessage();
    
        }
    }

    
    // Handle the send button being clicked
    function sendMessage() {
    
        var messageText = $('#input-message').val();
    
        if(messageText.length < 2) {
    
            return false;
        }

        // Build POST data and make AJAX request
        var data = {
        
            text: messageText,

            socketId: pusher.connection.socket_id,

            //chatchannel:

        };
        
        $.post('/conversation/message', data).success(sendMessageSuccess);

        // Ensure the normal browser event doesn't take place
        return false;
    }

    
    // Handle the success callback
    function sendMessageSuccess() {
    
        $('#input-message').val('')
    
        console.log('message sent successfully');
    }

    
    // Build the UI for a new message and add to the DOM
    function addMessage(data) {
    
        console.log(data);
        // Create element from template and set values    
        var el = createMessageEl(data.user_id);
    
        el.find('.direct-chat-text').html(data.text);

        el.find('.direct-chat-name').text(data.username);

        //el.find('.direct-chat-img').attr('src', data.avatar)
        
        // Utility to build nicely formatted time
        //el.find('.direct-chat-timestamp').text(strftime('%H:%M:%S %P', new Date(data.timestamp)));
        el.find('.direct-chat-timestamp').text(data.timestamp);
        
        var messages = $('.direct-chat-messages');
        
        messages.append(el)
        
        // Make sure the incoming message is shown
        messages.scrollTop(messages[0].scrollHeight);
    }

    // Creates an activity element from the template
    function createMessageEl(user_id) {
        var text = false;

        if(user_id=={{ auth()->user()->id }}) text = $('#chat_message_template_right').text();
        else text = $('#chat_message_template').text();

        var el = $(text);
        return el;
    }

    
    var pusher = new Pusher('{{env("PUSHER_KEY")}}', {
        
        authEndpoint: '/conversation/auth',
        
        auth: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
        }
    });

    // Added Pusher logging
    Pusher.log = function(msg) {
    
        console.log(msg);
    
    };
    
    var channel = false;    
    
    function open_conversation (){

        // Ensure CSRF token is sent with AJAX requests
        $.ajaxSetup({

            headers: {
        
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        
        });

        
        var rfp_id = {{ $rfp_id }} ;

        var workorder_id =  {{ isset($workorder_id) ? $workorder_id : 0 }};

        var vendor_id = {{ $vendor_id }};
        
        var backend_url = "{{ url('/conversation/open') }} ";
        
        $.ajax({

            method: "POST",

            url: backend_url,

            dataType: 'json',

            data: {

                _token : "{{ csrf_token() }}",

                rfpId: rfp_id,

                workorderId: workorder_id,

                vendorId: vendor_id,
            }

        }).done(function(data) {

            console.log(data.chatChannel);  

            $.ajaxSetup({

                data: {
        
                    channel: data.chatChannel
                }
        
            });
            
            channel = pusher.subscribe(data.chatChannel);
            
            channel.bind('new-message', addMessage);   

            loadMessages(data.messages);           
            
        });          

    };

    
    function loadMessages(messages){

        if(!empty(messages)) {

            messages.forEach(function(item, index){

                data = {

                    user_id: item.user_id,

                    text: item.text,

                    timestamp: item.created_at,

                    username: item.user.name+' '+item.user.lastname

                }    
                
                addMessage(data) ;

            });

        }
    }





    // send button click handling
    $('#send-message').click(sendMessage);
    
    // input message on enter key press handling
    $('#input-message').keypress(checkSend);
    
    // initialize channel subscription and auth methods against the backend
    open_conversation();
});
    

</script>

