@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Conversation {{ $conversation->id }}
        <a href="{{ url('conversation/' . $conversation->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Conversation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['conversation', $conversation->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Conversation',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $conversation->id }}</td>
                </tr>
                <tr><th> Client Id </th><td> {{ $conversation->client_id }} </td></tr><tr><th> Vendor Id </th><td> {{ $conversation->vendor_id }} </td></tr><tr><th> Rfp Id </th><td> {{ $conversation->rfp_id }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
