@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Edit Conversation {{ $conversation->id }}</h1>

    {!! Form::model($conversation, [
        'method' => 'PATCH',
        'url' => ['/conversation', $conversation->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('client_id') ? 'has-error' : ''}}">
                {!! Form::label('client_id', 'Client Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('client_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('client_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('vendor_id') ? 'has-error' : ''}}">
                {!! Form::label('vendor_id', 'Vendor Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('vendor_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('vendor_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('rfp_id') ? 'has-error' : ''}}">
                {!! Form::label('rfp_id', 'Rfp Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('rfp_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('rfp_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('workorder_id') ? 'has-error' : ''}}">
                {!! Form::label('workorder_id', 'Workorder Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('workorder_id', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('workorder_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection