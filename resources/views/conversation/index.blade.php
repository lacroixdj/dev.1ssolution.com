@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Conversation <a href="{{ url('/conversation/create') }}" class="btn btn-primary btn-xs" title="Add New Conversation"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> Client Id </th><th> Vendor Id </th><th> Rfp Id </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($conversation as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->client_id }}</td><td>{{ $item->vendor_id }}</td><td>{{ $item->rfp_id }}</td>
                    <td>
                        <a href="{{ url('/conversation/' . $item->id) }}" class="btn btn-success btn-xs" title="View Conversation"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/conversation/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Conversation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/conversation', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Conversation" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Conversation',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $conversation->render() !!} </div>
    </div>

</div>
@endsection
