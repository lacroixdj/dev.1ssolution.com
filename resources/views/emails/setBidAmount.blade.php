<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
        <meta name="csrf-token" content="TOKEN">
  <title>One Seamless Solution | Welcome!</title> <!-- the <title> tag shows on email notifications on Android 4.4. -->
  @include('emails.css')
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#f4f4f4" style="margin:0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#f4f4f4" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;"><tr><td>

	<!-- Hidden Preheader Text : BEGIN -->
	<div style="display:none; visibility:hidden; opacity:0; color:transparent; height:0; width:0;line-height:0; overflow:hidden;mso-hide: all;">
	<!--	Visually hidden preheader text. -->
	</div>
	<!-- Hidden Preheader Text : END -->

  <!-- Logo Left, Nav Right, 100% Nav Bar : BEGIN -->
  <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" bgcolor="#0f395f" style="text-align: center;">
    <tr>
      <td>
        <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="margin: auto;" class="email-container">
          <tr>
            <td height="10" style="font-size: 0; line-height: 0;">&nbsp;</td>
          </tr>
          <tr>
            <td class="hh-force-col-center" valign="middle" style="text-align: left;">
              <a href="{{ url('/') }}">
                  <img src="{{  $img_url  }}/assets/img/1ssolution-logo-header-blanco.png" alt="alt text" height="40" width="200" border="0">
              </a>
            </td>
            <td class="hh-force-col-center" valign="middle" style="padding: 10px 0;text-align: right;">
<!--
              <a href="" style="color: #ffffff;font-family: sans-serif;">Link 1</a>&nbsp;&nbsp;
              <a href="" style="color: #ffffff;font-family: sans-serif;">Link 2</a>&nbsp;&nbsp;
              <a href="" style="color: #ffffff;font-family: sans-serif;">Link 3</a>
-->
            </td>
          </tr>
          <tr>
            <td height="10" style="font-size: 0; line-height: 0;">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- Logo Left, Nav Right, 100% Nav Bar : END -->
 
  <!-- Email Container : BEGIN -->
  <!-- This table wraps the whole body email within it's width (600px), sets the background color (white) and border (thin, gray, solid) -->
    <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border: 1px solid #e5e5e5;margin: auto;" class="email-container">
  
      <!-- Single Fluid Image, No Crop : BEGIN -->
      <tr>
        <td>
          <img src="{{  $img_url  }}/assets/img/bidamount-regular.png" align="center" height="64" width="64" style="display: block; margin: 30px auto 0px;" class="col-3-img-l center">
        </td>
      </tr>
      <!-- Single Fluid Image, No Crop : END -->
    
      <!-- Full Width, Fluid Column : BEGIN -->
      <tr>
        <td style="border-bottom: 1px solid #e5e5e5;">
          <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td style="padding: 30px; font-family: sans-serif; font-size: 16px; line-height: 22px; color: #444444;">
              
              <h3><br>Hi {{$clientName}},</h3> 

                  <h1>A bid has been submitted</h1>
                  <h4>{{$serviceDescription}}</h4>
                 
                  <p>{{$serviceDetails}}</p>             
                  <p> Request Code: <b>#{{$id}}</b></p>
                  <p> Vendor: <b>{{$vendorName}}</b></p>
                  <p> Phone: <b>{{$vendorPhone}}</b></p>
                  <p> Email: <b>{{$vendorEmail}}</b></p>
	                
              </td>
            </tr>
            <tr>
              <td class="green-btn" style="-webkit-border-radius: 0px; -moz-border-radius: 0px; border-radius: 0px; text-align: center; padding: 5px 30px; color: #fff;">
                <a href="{{ $bid_url }}"  
                        style="color: #ffffff; 
                        font-family: sans-serif; 
                        font-size: 15px; 
                        line-height: 15px; 
                        text-align: center; 
                        text-decoration: none; 
                        display: block; 
                        padding: 15px 20px; 
                        border: 1px solid #4cae4c;
                        -webkit-border-radius: 0px; 
                        -moz-border-radius: 0px; 
                        border-radius: 0px;
                        background-position: top left; 
                        background-repeat: repeat-x;
                        background-color: #5cb85c;">
                  <b><!--[if mso]>&nbsp;<![endif]-->View this bid<!--[if mso]>&nbsp;<![endif]--></b>
                </a>
              </td>
            </tr>
            <tr>
                <td style="padding: 5px 30px; font-family: sans-serif; font-size: 14px; line-height: 22px; color: #444444;">
                    <p align="left" style="text-align:left"> 
                        <ul> 
                            <li>Is not the button working properly? You can try to copy and paste this url on your browser: <a href="{{ $bid_url }}">{{ $bid_url }}</a><br></li>
                            <li>Do you still have problems? Do not hesitate to contact our support team:  <a href="mailto:support@1ssolution.com">support@1ssolution.com</a></li>
                            
                        </ul>                        
                    </p>
                    
                    <br>                    
                    <p style="font-size: 16px;">
                        Thanks for your time, regards:<br>
                        <br>
                        <b>--<i> The One Seamless Solution's team.</i></b>
                    </p> 
                </td>
            </tr>
          </table>
        </td>
      </tr>
      <!-- Full Width, Fluid Column : END -->

  </table>
  <!-- Email Container : END -->
  
  <!-- Footer : BEGIN -->
  <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" class="email-container">
    <tr>
      <td class="azul" style="text-align: center;padding: 10px;font-family: sans-serif; font-size: 12px; line-height: 18px;color: #0f395f;">
        One Seamless Solution &bull; [Address] US &bull; <span class="mobile_link">(123) 456-7890</span><br><br>
        
        <a href="#" class="titulo-enlace-footer" style="font-weight:bold">Privacy Policy</a>  &bull;  <a href="#" class="titulo-enlace-footer" style="font-weight:bold">Terms and Conditions</a>
        
      </td>
    </tr>
  </table>
  <!-- Footer : END -->

</td></tr></table>
</body>
</html>