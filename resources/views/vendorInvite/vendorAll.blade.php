@extends('layouts.master')
   
<!-- /.Page Title -->           
@section('title', 'Vendor Invite - All')
    
@section('content')
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
     
        <h1>All Vendors<small></small></h1>
      
        <ol class="breadcrumb">

            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="#">Vendors</li>

            <li class="active">View All</li>

        </ol>
        
    </section>
	
    <section class="content">
       
        <!-- /.Search -->  
        <div class="row">
            
            {{ Form::open(['url'=>'/vendors/viewall/', 'id'=>'search-form', 'method'=>'get', 'class'=>'search']) }}
            
            <div class="col-xs-12 col-sm-12  col-md-8 col-md-offset-2 margin-t-10">
                
                <div class="form-group ui-widget">
                      
                    <div class="input-group margin">

                        {{ Form::text('filter', $name, ['id'=>'q', 'placeholder'=>'Search for vendors...', 'class'=>'form-control autocomplete ui-autocomplete-input' ]) }}

                        <span class="input-group-btn">

                            {{ Form::submit('Search', array('class'=>'btn btn-info btn-flat'))}}

                        </span>

                    </div>
                      
                </div>  
            
                <div class="col-xs-12 col-sm-12  col-md-6  margin-b-20">

                     {{ Form::select('industry_id', Array("null"=>'All industries...')+ $industries, $industry_id,
                       [ 

                         'id' => 'industry_id',

                         'class' => 'form-control',

                      ]) 
                    }}

                </div>

                <div class="col-xs-12 col-sm-12  col-md-6  margin-b-20">

                    {{ Form::select('short_id', Array("null"=>'Short by...')+ $categories, $short_id,
                       [ 

                         'id' => 'short_id',

                         'class' => 'form-control',

                      ]) 
                    }}

                </div>
            
            </div>
            
            {{ Form::close() }}
        
        </div><!-- /.End Row -->  
        
        <!-- /.invite -->  
        <div class="row margin-b-20">
            
            <div class="col-md-12">
            
                <h4 class="text-center">You know any vendor that is not in <b>One Seamless Solution</b>, invite him!</h4>
            
                <div class="text-center">
            
                    <button href="" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#inviteVendor"> Invite vendor</button>
                
                </div>
            
            </div>
        
        </div><!-- /.End Row -->  
        
        <!-- /.vendors cards -->  
        <div class="row">
            
            <div class="col-md-12">
               
                <div class="row">

                        @foreach ($vendors as $vendor)

                        <div class="col-sm-6 col-md-3 ">

                          <!-- Widget: user widget style 1 -->
                          <div class="box box-widget widget-user front">

                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            @if (in_array($vendor->id, $vendorInFavotites))
                            <div id="vendor-panel-{{$vendor->id}}" class="widget-user-header vendors-favorite-header bg-success">
                            @else
                            <div id="vendor-panel-{{$vendor->id}}" class="widget-user-header vendors-favorite-header bg-primary">
                            @endif  

                              <div class="vendor-more-info pull-right margin-r-5" data-toggle="tooltip" data-placement="left" title="More Info.">
                                           
                                    <button class="vendor_info" value="{{$vendor->id}}"><i class="fa fa-info-circle fa-2x"></i></button>
                                            
                              </div>     
                                 
                              <h3 class="widget-user-username">{{ str_limit($vendor->name, 18) }}</h3>
                               
                              <input name="input-rate" class="rating rating-loading rating-disabled" data-show-clear="false" value="{{ $vendor->rate }}" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs">
                               
                              <h5 class="widget-user-desc">{{$vendor->industry->name}}</h5>    
                                
                            </div>        

                            <div class="widget-user-fav-image">

                              <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">

                            </div>

                            <div class="box-footer">

                               <div class="row">

                                <div class="col-sm-12">

                                    <p><i class="fa fa-user"></i> <b id="contactName">{{$vendor->user->name}} {{$vendor->user->lastname}}</b></p>

                                    <p><i class="fa fa-envelope"></i> <b id="contactPhone">{{ $vendor->email }}</b></p>

                                  <!-- /.description-block -->

                                </div>

                                <br class="small">

                                <!-- /.col -->
                                <div class="col-sm-12">

                                    @if (in_array($vendor->id, $vendorInFavotites))

                                        {{ Form::open(['url'=>'/vendors/removefavorite/', 'id'=>'vendor-detach-'.$vendor->id, 'method'=>'POST', 'role' => 'form']) }}
                                             <!-- /.Campo oculto con el id del vendor -->  
                                            {{ Form::hidden('sincValue', $vendor->id) }}

                                            {{ Form::submit('The vendor is in favorites', array('id'=>$vendor->id,'class'=>'btn btn-block btn-success  btn-flat', 'disabled'))}}

                                        {{ Form::close() }}

                                    @else

                                        {{ Form::open(['url'=>'/vendors/setfavorite/', 'id'=>'vendor-attach-'.$vendor->id, 'method'=>'POST', 'role' => 'form']) }}

                                            <!-- /.Campo oculto con el id del vendor -->  
                                            {{ Form::hidden('sincValue', $vendor->id) }}

                                            {{ Form::submit('Add this vendor to favorites', array('id'=>$vendor->id, 'class'=>'btn btn-block btn-primary btn-flat'))}}

                                         {{ Form::close() }}

                                    @endif    

                                </div>
                                <!-- /.col -->

                              </div>
                              <!-- /.row -->

                            </div>

                          </div>
                          <!-- /.widget-user -->

                        </div>
                    
                    @endforeach

                    </div>

                <!-- /.pagination --> 
                <div class="text-center">

                    {{-- $vendors->links() --}}
                    
                    {{ $vendors->appends(request()->input())->links() }}

                </div> 
              
            
                 
            </div>
            
            </div><!-- /.end col -->  
        
        </div><!-- /.End Row -->  
    
    </section>
    <!-- /.content -->
    
    <!-- /.Modal to invite vendor -->  
    <div class="modal fade" id="inviteVendor">
      
        <div class="modal-dialog">
        
          <div class="modal-content">
          
            <div class="modal-header">
            
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
                <h4 class="modal-title">Vendor invite to <b>One Seamless Solution</b></h4>
          
            </div>
          
            {{ Form::open([
                    'url'=>'/vendors/invite/', 
                    'id'=>'invite-form', 
                    'method'=>'POST', 
                    'class'=>'search', 
                    'data-parsley-validate' => '', 
                    'data-parsley-trigger' => 'focusout',
                    'role' => 'form']) 
            }}
          
            <div class="modal-body">
                  <p>Please complete the fields that are below.</p> 
                  
                  <p>Once completed, you will be sent an invitation to register on our platform of services</p> 
                  
                <div class="box-body"> 
                    
                    <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                      <label for="workorderId"><i class="fa fa-user"></i> Contact name</label>
                      <input type="text" name="name" class="form-control" value="" required>
                       {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                    <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                      <label for="workorderId"><i class="fa fa-envelope"></i> Email Address</label>
                      <input type="email" name="email" class="form-control" value="" required>
                       {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                    <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                      <label for="workorderId"><i class="fa fa-phone"></i> Phone number</label>
                      <input type="text" id="phone" name="phone" placeholder="(000) 000-0000" data-mask="(000)000-0000" placeholder="" data-parsley-minlength="13" value=""  class="form-control">
                       {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>

                    
                </div>
            </div>
          
            <div class="modal-footer">
               
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                
                <button type="button" id="btnUpdate" class="btn btn-primary btn-flat">Send Invitation!</button>
            
            </div>
            
            {{ Form::close() }}
            
        </div>
          <!-- /.modal-content -->
      
        </div>
        <!-- /.modal-dialog -->
    
    </div>
    <!-- /.modal -->
    <!-- /.End Modal -->  
 
<!--</div> /.End content-wrapper   -->
@endsection
  
  
<!-- /.Page Custom JavaScript -->    
@section('pageCustomJavaScript')

<script>

    //loop to ajax XHR submit form.
    @foreach ($vendors as $vendor)
    
        @if (in_array($vendor->id, $vendorInFavotites))      

            XHRFormListener('#vendor-detach-{{$vendor->id}}'); 
        
        @else
            
            XHRFormListener('#vendor-attach-{{$vendor->id}}');
            
        @endif

    @endforeach
    //End Loop
        
    //Parsley vaidate 
    
    //$('#invite-form').parsley();

    //submit vendor invite
    
    XHRFormListener('#invite-form');
    
    $("#btnUpdate").on("click", function (e) {
            
          
        if ($('#invite-form').parsley().validate()){                

                $("#invite-form").submit();

                $('#inviteVendor').modal('hide');

            }
            
    });
    
    //
    
    $('#inviteVendor').on('shown.bs.modal', function() {

        $(':input','#invite-form')
             .not(':button, :submit, :reset, :hidden, [readonly]')
             .val('')
             .removeAttr('checked')
             .removeAttr('selected');
        
        $('#invite-form').parsley();


    });


    var id;

    $("form input[type=submit]").click(function(event) {
        
        
        if(this.id != ''){
            
            event.preventDefault();

            id = this.id;

            console.log(id);

            $("#"+this.id).val('The vendor is in favorites');

            $("#"+this.id).prop('disabled',true);//.prop('disabled', true);

            //Change the color of header vendor card
            if ( $("#vendor-panel-"+this.id).hasClass('bg-primary') ){
                
                document.getElementById(this.id).classList.toggle('btn-success');
                
                $("#vendor-panel-"+this.id).removeClass('bg-primary');

                $("#vendor-panel-"+this.id).addClass('bg-success');
            }

            //submit this form 

            $("#vendor-attach-"+this.id).submit();
        }
        
    });
    
    //On error volver a poner el car como estaba 
    
    $('#modal_error').on('hidden.bs.modal', function() {
        
        
        $("#"+id).removeClass( "btn-success" ).addClass( "btn-primary" );
        
        //change the buttom text

        $("#"+id).val('Add this vendor to favorites');

        $("#"+id).prop('disabled',false);//.prop('disabled', true);

        //Change the color of header vendor card
        
        if ( $("#vendor-panel-"+id).hasClass('bg-success') ){

            $("#vendor-panel-"+id).removeClass('bg-success');

            $("#vendor-panel-"+id).addClass('bg-primary');
        }
        
        
    });
    

    function markFavorite(){

    }
        
    //Funciones de busqueda 
    $(function()
    {
         
        $( "#q" ).autocomplete({
          
            source: "{{ url('/autocomplete/vendors/') }}",
          
            minLength: 3,
          
            select: function(event, ui) {
            
                $('#q').val(ui.item.value);
          
            }
        
        });
    
    });

    $( "#q" ).autocomplete({
      
        open: function( event, ui ) {
          
            console.log('test autocomplete');
      
        }
    
    });

    $('#industry_id').change(function(){
        
        console.log('Change Industry');
        
        $("#search-form").submit();
        
    });    
    
    $('#short_id').change(function(){
        
        console.log('Change short');
        
        $("#search-form").submit();
        
    });

    
    // Vendor More Info
    // --------------------------------------------------------------------------
    //
    // --------------------------------------------------------------------------

    $(".vendor_info").click(function(){
        
        console.log(this.value);
        
        findVendorInfo(this.value, "{{{ csrf_token() }}}" );    
                   
    });

   

            

</script>
@endsection
    