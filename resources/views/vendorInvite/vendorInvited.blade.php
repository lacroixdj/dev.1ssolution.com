@extends('layouts.master')
   
<!-- /.Page Title -->           
@section('title', 'Vendor Invite - All')
    
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
     
        <h1>Vendors Invitations<small></small></h1>
      
        <ol class="breadcrumb">

            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="#">Vendors</li>

            <li class="active">Invitations</li>

        </ol>
        
    </section>
	
    <section class="content">
        <!-- /.search space -->  
        <div class="row">
           
            {{ Form::open(['url'=>'/vendors/invitations/', 'id'=>'search-form', 'method'=>'get', 'class'=>'search']) }}
            
            <div class="col-xs-12 col-sm-12  col-md-8 col-md-offset-2 margin-t-10">
                
                <div class="form-group ui-widget">
                      
                    <div class="input-group margin">

                        {{ Form::text('filter', $name, ['id'=>'q', 'placeholder'=>'Search for invitations...', 'class'=>'form-control autocomplete ui-autocomplete-input' ]) }}

                        <span class="input-group-btn">

                            {{ Form::submit('Search', array('class'=>'btn btn-info btn-flat')) }}

                        </span>

                    </div>
                      
                </div>  
            
                <div class="col-xs-12 col-sm-12  col-md-6  margin-b-20">

                    {{  Form::select('filter_id', Array("null"=>'All invites...')+ $filterInvitesStatus, $filter_id,
                       [ 

                         'id' => 'industry_id',

                         'class' => 'form-control',

                      ]) 
                    }}

                </div>

                <div class="col-xs-12 col-sm-12  col-md-6  margin-b-20">

                    {{  Form::select('short_id', Array("null"=>'Short by...')+ $shorInvitesDate, $short_id,
                       [ 

                         'id' => 'short_id',

                         'class' => 'form-control',

                      ]) 
                    }}

                </div>
            
            </div>
            
        {{  Form::close() }}
        
        </div><!-- /.End Row -->  
        
        
          <!-- /.invite -->  
        <div class="row margin-b-20">
            
            <div class="col-md-12">
            
                <h4 class="text-center">You know any vendor that is not in <b>One Seamless Solution</b>, invite him!</h4>
            
                <div class="text-center">
            
                    <button href="" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#inviteVendor"> Invite vendor</button>
                
                </div>
            
            </div>
        
        </div><!-- /.End Row --> 
        
        <!-- /.invitation space --> 
        @if(!$vendorsIvitations->isEmpty())
        <div class="row"> 
        
           @foreach ($vendorsIvitations as $vendorsIvitation)
                <div class="col-sm-6 col-md-6 col-lg-3">
                     <!-- /.aqui el loop de invitacione  -->  
                     <div class="box box-widget widget-user-2 widget-user-invitation">

                        @if ($vendorsIvitation->status == 'sent')
                        <div class="widget-user-header bg-gray">

                            <div class="widget-user-image">

                                <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg-pending.png') }}" alt="User Avatar">

                            </div>
                         
                         @else
                         <div class="widget-user-header bg-green">

                            <div class="widget-user-image">

                                <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">

                            </div>
                         @endif
                          <!-- /.widget-user-image -->

                          <h3 class="widget-user-username">{{ str_limit($vendorsIvitation->name, $limit = 23, $end = '...') }}</h3>

                          <h5 class="widget-user-desc">{{$vendorsIvitation->email}}</h5>
                        
                        </div>
                        @if ($vendorsIvitation->status == 'sent')
                        <div class="box-footer bg-gray">

                            <div class="col-md-6 p-info">

                                <p class="">Sent:<b>  {{$vendorsIvitation->created_at->format('m/d/Y')}} </b></p>

                            </div>

                            <div class="col-md-6 p-info">

                                <p class="">Status:<b>  {{ucfirst(trans($vendorsIvitation->status))}}</b></p>

                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                {{ Form::button('Pending request', array('id'=>'test','class'=>'btn btn-block btn-gray  btn-flat', 'value'=>'', 'disabled' ))}} 
                            </div>

                        </div>
                        @else
                        <div class="box-footer  color-palette">

                            <div class="col-md-6 p-info">

                                <p class="">Sent:<b>  {{$vendorsIvitation->created_at->format('m/d/Y')}} </b></p>

                            </div>

                            <div class="col-md-6 p-info">

                                <p class="">Status:<b>  {{ucfirst(trans($vendorsIvitation->status))}}</b></p>

                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                {{-- Form::button('Add to favorites', array('id'=>'test','class'=>'btn btn-block btn-default btn-flat', 'data-toggle'=>'modal', 'data-target'=>'#modalConfirm', 'value'=>'' )) --}} 


                                {{ Form::open(['url'=>'/vendors/invtofavorite/', 'id'=>'vendor-attach-'.$vendorsIvitation->id, 'method'=>'POST', 'role' => 'form']) }}
                                 <!-- /.Campo oculto con el id del vendor -->  
                                    {{ Form::hidden('sincValue', $vendorsIvitation->email) }}
                                    {{ Form::hidden('sincStatus', $vendorsIvitation->status) }}

                                    {{ Form::submit('Add to vendors favorites', array('id'=>$vendorsIvitation->id,'class'=>'btn btn-block btn-primary btn-flat', 'value'=>$vendorsIvitation->id ))}}   

                                {{Form::close() }}

                            </div>

                        </div>
                        @endif


                    </div>

                </div><!-- /.End col -->  
                @endforeach
        </div>   
        @endif
        
        @if($vendorsIvitations->isEmpty())         
        <div class="row">
            
            <div class="col-md-12">

                <h1 class="text-center"><i class="fa fa-exclamation-circle"></i> No invites results for this search...</h1>
                <h4 class="text-center"><a data-toggle="modal" data-target="#inviteVendor">You can invite one in this section</a></h4>

            </div>
            
        </div>
        @else
        <div class="row">
            <!-- /.pagination --> 
            <div class="col-md-12">

                <div class="text-center">
                   
                    {{ $vendorsIvitations->appends(request()->input())->links() }}
                
                </div>

            </div>
        </div> 
        @endif
    
    </section>
    <!-- /.content -->
   
    <!-- /.Modal to invite vendor -->  
    <div class="modal fade" id="inviteVendor">
      
        <div class="modal-dialog">
        
          <div class="modal-content">
          
            <div class="modal-header">
            
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
                <h4 class="modal-title">Vendor invite to <b>One Seamless Solution</b></h4>
          
            </div>
          
            {{ Form::open([
                    'url'=>'/vendors/invite/', 
                    'id'=>'invite-form', 
                    'method'=>'POST', 
                    'class'=>'search', 
                    'data-parsley-validate' => '', 
                    'data-parsley-trigger' => 'focusout',
                    'role' => 'form']) 
            }}
          
            <div class="modal-body">
                  <p>Please complete the fields that are below.</p> 
                  
                  <p>Once completed, you will be sent an invitation to register on our platform of services</p> 
                  
                <div class="box-body"> 
                    
                    <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                      <label for="workorderId"><i class="fa fa-user"></i> Contact name</label>
                      <input type="text" name="name" class="form-control" value="" required>
                       {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                    <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                      <label for="workorderId"><i class="fa fa-envelope"></i> Email Address</label>
                      <input type="email" name="email" class="form-control" value="" required>
                       {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                    <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                      <label for="workorderId"><i class="fa fa-phone"></i> Phone number</label>
                      <input type="text" id="phone" name="phone" placeholder="(000) 000-0000" data-mask="(000)000-0000" placeholder="" data-parsley-minlength="13" value=""  class="form-control">
                       {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>

                    
                </div>
            </div>
          
            <div class="modal-footer">
               
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                
                <button type="button" id="btnInvite" class="btn btn-primary btn-flat">Send Invitation!</button>
            
            </div>
            
            {{ Form::close() }}
            
        </div>
          <!-- /.modal-content -->
      
        </div>
        <!-- /.modal-dialog -->
    
    </div>
    <!-- /.modal -->
    <!-- /.End Modal -->   
   
</div><!-- /.End content-wrapper --> 
@endsection
  

  
  
  
<!-- /.Page Custom JavaScript -->    
@section('pageCustomJavaScript')
<script>
    
    var id_vendor;

    //Vendor invitation attach

    @foreach ($vendorsIvitations as $vendorsIvitation)
    
        @if($vendorsIvitation->status == 'joined')
    
            XHRFormListener('#vendor-attach-{{$vendorsIvitation->id}}');

        @endif
    
    @endforeach
    
    
    function reloadPage(){
        
        console.log('reload');
        
        location.reload();

    }
    
    //End vendor invitation attach
        
    //submit vendor invite
    
    XHRFormListener('#invite-form');
    
    $("#btnInvite").on("click", function (e) {
            
          
        if ($('#invite-form').parsley().validate()){                

                $("#invite-form").submit();

                $('#inviteVendor').modal('hide');

            }
            
    });

    //Funciones de busqueda 
    $(function()
    {
         
        $( "#q" ).autocomplete({
          
            source: "{{ url('/autocomplete/invites/') }}",
          
            minLength: 3,
          
            select: function(event, ui) {
            
                $('#q').val(ui.item.value);
          
            }
        
        });
    
    });

    $( "#q" ).autocomplete({
      
        open: function( event, ui ) {
          
            console.log('test autocomplete');
      
        }
    
    });

    $('#industry_id').change(function(){
        
        console.log('Change Industry');
        
        $("#search-form").submit();
        
    });    
    
    $('#short_id').change(function(){
        
        console.log('Change short');
        
        $("#search-form").submit();
        
    });


</script>
@endsection
    