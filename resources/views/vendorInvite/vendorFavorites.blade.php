@extends('layouts.master')
   
<!-- /.Page Title -->           
@section('title', 'Vendor Invite - All')
    
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
     
        <h1>Vendors Favorites<small></small></h1>
      
        <ol class="breadcrumb">

            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="#">Vendors</li>

            <li class="active">Favorites</li>

        </ol>
        
    </section>
	
    <section class="content">
        <!-- /.search space -->  
        <div class="row">
            
            {{ Form::open(['url'=>'/vendors/favorites/', 'id'=>'search-form', 'method'=>'get', 'class'=>'search']) }}
            
            <div class="col-xs-12 col-sm-12  col-md-8 col-md-offset-2 margin-t-10">
                
                <div class="form-group ui-widget">
                      
                    <div class="input-group margin">

                        {{ Form::text('filter', $name, ['id'=>'q', 'placeholder'=>'Search for vendors...', 'class'=>'form-control autocomplete ui-autocomplete-input' ]) }}

                        <span class="input-group-btn">

                            {{ Form::submit('Search', array('class'=>'btn btn-info btn-flat'))}}

                        </span>

                    </div>
                      
                </div>  
            
                <div class="col-xs-12 col-sm-12  col-md-6  margin-b-20">

                     {{ Form::select('industry_id', Array("null"=>'All industries...')+ $industries, $industry_id,
                       [ 

                         'id' => 'industry_id',

                         'class' => 'form-control',

                      ]) 
                    }}

                </div>

                <div class="col-xs-12 col-sm-12  col-md-6  margin-b-20">

                    {{ Form::select('short_id', Array("null"=>'Short by...')+ $categories, $short_id,
                       [ 

                         'id' => 'short_id',

                         'class' => 'form-control',

                      ]) 
                    }}

                </div>
            
            </div>
            
        {{ Form::close() }}
        
        </div><!-- /.End Row -->  
        
        <!-- /.vendors cards -->
        @if(!$vendors->isEmpty())    
        <div class="row">
            
            <!-- /.vendors cards -->  
            <div class="col-md-12">
               
                <div>            
                    
                    @foreach ($vendors as $vendor)

                    <div id="vendor-panel-{{$vendor->id}}" class="col-sm-6 col-md-3">

                      <!-- Widget: user widget style 1 -->
                      <div class="box box-widget widget-user front">

                        <div class="widget-user-header vendors-favorite-header bg-success">
                          
                          <div class="vendor-more-info pull-right margin-r-5" data-toggle="tooltip" data-placement="left" title="More Info.">
                                           
                                <button class="vendor_info" value="{{$vendor->id}}"><i class="fa fa-info-circle fa-2x"></i></button>
                                            
                          </div>

                          <h3 class="widget-user-username">{{ str_limit($vendor->name, 18) }}</h3>

                          
                          <input name="input-rate" class="rating rating-loading rating-disabled" data-show-clear="false" value="{{ $vendor->rate }}" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs"> 
                          
                          <h5 class="widget-user-desc">{{$vendor->industry->name}}</h5>
                          
                        </div>

                        <div class="widget-user-fav-image">

                          <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">

                        </div>

                        <div class="box-footer">

                           <div class="row">

                            <div class="col-sm-12">
                            
                                                            
                                <p><i class="fa fa-user"></i> <b id="contactName">{{$vendor->user->name}} {{$vendor->user->lastname}}</b></p>
                                
                                <p><i class="fa fa-envelope"></i> <b id="contactPhone">{{ $vendor->email }}</b></p>

                              <!-- /.description-block -->

                            </div>

                            <br class="small">

                            <!-- /.col -->
                            <div class="col-sm-12">

                                @if (in_array($vendor->id, $vendorInFavotites))

                                    {{ Form::open(['url'=>'/vendors/removefavorite/', 'id'=>'vendor-detach-'.$vendor->id, 'method'=>'POST', 'role' => 'form']) }}
                                        
                                        {{ Form::hidden('sincValue', $vendor->id) }}

                                        {{ Form::button('Remove from favorites', array('id'=>$vendor->id,'class'=>'btn btn-block btn-success  btn-flat', 'data-toggle'=>'modal', 'data-target'=>'#modalConfirm', 'value'=>$vendor->id ))}}   

                                    {{ Form::close() }}

                                @endif

                            </div>
                            <!-- /.col -->

                          </div>
                          <!-- /.row -->

                        </div>

                      </div>
                      <!-- /.widget-user -->

                    </div>
                    @endforeach

                </div>

               
              
            </div>
                 
        </div>
        @endif
        
        @if($vendors->isEmpty())         
        <div class="row">
            
            <div class="col-md-12">

                <h1 class="text-center"><i class="fa fa-exclamation-circle"></i> No vendors results for this search...</h1>
                <h4 class="text-center"><a href="/vendors/viewall">You can add one in this section</a></h4>

            </div>
            
        </div>
        @else
        <div class="row">
            <!-- /.pagination --> 
            <div class="col-md-12">

                <div class="text-center">

                    {{-- $vendors->links() --}}
                    
                    {{ $vendors->appends(request()->input())->links() }}

                </div>

            </div>
        </div> 
        @endif
    
    </section>
    <!-- /.content -->
    
    <!-- /.Modal to detach vendor -->  
    <div class="modal fade" id="modalConfirm">
      
        <div class="modal-dialog">
        
          <div class="modal-content">
          
            <div class="modal-header">
            
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
                <h4 class="modal-title">Remove vendor to favorite</h4>
          
            </div>
          
            {{ Form::open(['url'=>'/vendors/removefavorite/', 'id'=>'vendor-detach', 'method'=>'POST', 'role' => 'form']) }}
          
            <div class="modal-body">
                  
                  <p class="text-center">You want to remove the seller from your list of favorites?</p> 
                  
                  <input name="sincValue" type="hidden" id="sincValue" class="form-control">
                  
            </div>
          
            <div class="modal-footer">
               
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                
                <button type="button" id="btnUpdate" class="btn btn-success btn-flat">Yes, remove.</button>
            
            </div>
            
            {{ Form::close() }}
            
        </div>
        <!-- /.modal-content -->
      
      </div>
      <!-- /.modal-dialog -->
    
    </div>
    <!-- /.End Modal -->  
 
</div><!-- /.End content-wrapper --> 
@endsection
  
  
<!-- /.Page Custom JavaScript -->    
@section('pageCustomJavaScript')
<script>
    
    var id_vendor;

    XHRFormListener('#vendor-detach');


    //Vendor invitation attach

    @foreach ($vendorsIvitations as $vendorsIvitation)
    
        @if($vendorsIvitation->status == 'joined')
    
            XHRFormListener('#vendor-attach-{{$vendorsIvitation->id}}');

        @endif
    
    @endforeach
    
    
    function reloadPage(){
        
        console.log('reload');
        
        location.reload();

    }
    
    //End vendor invitation attach


    $("#btnUpdate").on("click", function (e) {
            
            console.log('submit');
        
            $("#vendor-detach").submit();

            $('#modalConfirm').modal('hide');
        
            //delay to destroy the vendor
        
            $("#vendor-panel-"+id_vendor).delay(500).fadeOut("slow", function(){
              $(this).remove();
              // alert( $('.background-blackout').length );
              //console.log( $('.background-blackout').length );
            });

    });
        
    $('.btn').click(function() {

        if($(this).attr("value") !== undefined){

            id_vendor = $(this).attr("value");

            console.log('asignar el valor en el campo hidden');

            document.getElementById("sincValue").value = $(this).attr("value");
        }
    });


    var id;

    $("form input[type=button]").click(function() {

        id = this.id;
        
        console.log(id);
        
    });

    function testing(){

        document.getElementById(this.id).classList.toggle('btn-success');

        //change the buttom text

        $("#"+id).val('The vendor is in favorites');

        $("#"+id).prop('disabled',true);//.prop('disabled', true);

        //Change the color of header vendor card
        if ( $("#vendor-panel-"+id).hasClass('bg-primary') ){

            $("#vendor-panel-"+id).removeClass('bg-primary');

            $("#vendor-panel-"+id).addClass('bg-success');
        }
    }
        
    //Funciones de busqueda 
    $(function()
    {
         
        $( "#q" ).autocomplete({
          
            source: "{{ url('/autocomplete/vendors/') }}",
          
            minLength: 3,
          
            select: function(event, ui) {
            
                $('#q').val(ui.item.value);
          
            }
        
        });
    
    });

    $( "#q" ).autocomplete({
      
        open: function( event, ui ) {
          
            console.log('test autocomplete');
      
        }
    
    });

    $('#industry_id').change(function(){
        
        console.log('Change Industry');
        
        $("#search-form").submit();
        
    });    
    
    $('#short_id').change(function(){
        
        console.log('Change short');
        
        $("#search-form").submit();
        
    });

    // Vendor More Info
    // --------------------------------------------------------------------------
    //
    // --------------------------------------------------------------------------

    $(".vendor_info").click(function(){
        
        console.log(this.value);
        
        findVendorInfo(this.value, "{{{ csrf_token() }}}" ); 
                        
    });


</script>
@endsection
    