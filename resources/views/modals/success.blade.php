
<!-- /. Modal success -->
<div class="modal fade" id="modal_success" tabindex="-1" role="dialog" aria-labelledby="modal_success_title">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_success_title" style="color:#222222"></h4>
      </div>
      
      <div class="modal-body">
        <h3 id="modal_success_message" class="text-center"  style="color:#222222"></h3>
        <h4 id="modal_success_text" class="text-center" style="color:#222222"></h4>
      </div>
      
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-flat btn-primary btn-lg" data-dismiss="modal">Ok </button>
      </div>
    
    </div>
  </div>
</div> 
<!-- /Modal success -->