   <!-- Modal for Support -->
    <div class="modal fade" id="supportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h3>How can we help?</h3>
          </div>
          <!-- form start -->
          {!! Form::open( 
            [
              'method' => 'POST',
              'url' => ['/support/store'],
              'id' => 'support_create',
              'data-parsley-validate' => '', 
              'data-parsley-trigger' => 'focusout',
              'role' => 'form'
            ]) 
          !!}
          <div class="modal-body">
          @if(isset(Auth::User()->email))
            <!-- Support Form -->
              <div class="box box-primary">
                <div class="box-header with-border">
                        <h3 class="box-title">Support Form</h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                        <div class="form-group {{ $errors->has('supportEmail') ? 'has-error' : ''}}">
                          <label for="emailUser"><i class="fa fa-envelope"></i> User Email</label>
                          <input type="email" name="supportEmail" class="form-control" value="{{ Auth::User()->email }}" readonly>
                           {!! $errors->first('supportEmail', '<p class="help-block">:message</p>') !!}
                        </div>
                    
                        <div class="form-group {{ $errors->has('supportCategory') ? 'has-error' : ''}}">
                            <label for="exampleInputEmail1"><i class="fa fa-plus-square"></i> For faster response, select a category:  </label>
                            <select class="form-control" name="supportCategory" id="supportCategory" required>
                                <option value="">Select item</option>
                            </select>
                            {!! $errors->first('supportCategory', '<p class="help-block">:message</p>') !!}
                        </div>
                    
                        <div class="form-group {{ $errors->has('supportDetail') ? 'has-error' : ''}}">
                            <label><i class="fa fa-plus-square"></i> Please share additional details</label>
                            <textarea class="form-control" name="supportDetail" rows="3" placeholder="Dtails here..." required></textarea>
                            {!! $errors->first('supportDetail', '<p class="help-block">:message</p>') !!}
                        </div>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->

          </div>
            @else
            <script>window.location.href = "/logout";</script>
            @endif
          <div class="modal-footer">
              <div class=" pull-right">
                    <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success btn-flat" id="btnUpdate">Submit</button>
              </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    <!-- End Modal for Support -->
    
    <script>
        
        $(document).ready(function() { 
            
            XHRFormListener('#support_create');
            
            //TODO: Pasar esta sentencia al script global
            var category_url = '{{ url('json/categories/support_category') }}';
            $.ajax({
                method: "GET",
                url: category_url,
                dataType: 'json'
            }).done(function(data) {
                //Populating  Category combo
                $.each( data, function( key, value ) {                    
                    $('#supportCategory').append($("<option />").val(key).text(value));
                });
            });
         
        });
    
        $('#supportModal').on('shown.bs.modal', function() {

            $('#support_create').parsley();

            $(':input','#support_create')
                 .not(':button, :submit, :reset, :hidden, [readonly]')
                 .val('')
                 .removeAttr('checked')
                 .removeAttr('selected');

        });

        $( "#support_create" ).submit(function( event ) {
            
            event.preventDefault();
            
            if ($('#support_create').parsley().validate()){
                
                $('#supportModal').modal('hide');
    
            }else{
            
            }  
            
        });
        
        $('#supportModal').on('hidden.bs.modal', function() {
            
            $('#support_create').parsley().reset();
        
        });
        
        $("#btnUpdate").on("click", function (e) {
            
            if ($('#support_create').parsley().validate()){
                
                $("#support_create").submit();
            
                $('#supportModal').modal('hide');
    
            }else{
            
            } 
            
        });
    </script>