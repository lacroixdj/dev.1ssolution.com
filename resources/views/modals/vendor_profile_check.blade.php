
<!-- vendor_profile_check modal -->
<div class="modal fade" id="modalChecking" tabindex="-1" role="dialog" aria-labelledby="modalCheckingLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalCheckingLabel" style="color:#222222">Account Verification</h4>
      </div>
      <div class="modal-body">
<!--      <img src="img/1ssolution-logo.png" class="img-responsive img-center"/> -->
          <h3 class="text-left" style="color:#222222">
              <i class="fa fa-check" style="color:green"> </i>&nbsp; Welcome to <b>One Seamless Solution</b> marketplace.
      </h3>
          <br>
        <h4 class="text-justify" style="color:#222222">In <b>One Seamless Solution</b> we know that trust between us and our users is the most important thing, we care about the information quality published in our platform.<br><br>

        Therefore your company profile will go through a verification process and then you will be contacted via email by our team.<br><br>

        Many thanks for your time.</h4>
      </div>
      <div class="modal-footer text-right">
           <a class="btn btn-primary btn-flat" href="{{ url('/logout') }}">Logout</a>
           <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
<!-- vendor_profile_check modal -->

<script type="text/javascript">
    jQuery(document).ready(function () {
            @if(session('haslogged')!=1 && session('user_type')=='vendor')          
                $('#modalChecking').modal({
                  keyboard: false,
                  backdrop: 'static'
                });

                $('#modalChecking').modal('show');                  
                {{ session(['haslogged' => Auth::User()->haslogged]) }}
                
              @endif
            
    });
            
</script>