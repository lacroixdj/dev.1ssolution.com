<!-- Modal error -->
<div class="modal modal-danger fade" id="modal_error" tabindex="-1" role="dialog" aria-labelledby="modal_error_title">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_error_title"></h4>
      </div>

      <div class="modal-body">
        <h3 id="modal_error_message" class="text-center" ></h3>
        <ul id="modal_error_list"></ul>        
      </div>

      <div class="modal-footer text-center">
        <button type="button" class="btn btn-flat btn-lg btn-danger" data-dismiss="modal">Ok </button>
      </div>

    </div>
  </div>
</div>
<!-- /Modal error -->