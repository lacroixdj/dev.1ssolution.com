 <!-- Modal for Terms -->
    <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Terms of Use</h4>
          </div>
          <div class="modal-body">
            <div class="pre-scrollable padding-l-r-30">
                <p style="text-align: justify; margin-left:10px;">
                    These Vendor Master Terms and Conditions (.Vendor Master Terms and Conditions. or .Vendor MTC.) are entered into by and between One Seamless Solution! Inc. (.One Seamless Solution.) and Vendor as of the Start Date. Except for terms defined in the body hereof, capitalized terms used herein are defined in Section 14 of these Vendor Master Terms and Conditions.1. SCOPE OF AGREEMENT. The Agreement sets forth the terms and conditions under which Vendor will provide Services. All SOWs, Change Orders, SLAs, PLSSs, and POs will be governed by the Vendor MTC. Any One Seamless Solution Company will be entitled to purchase Services under the Agreement by submitting a PO and/or executing an SOW, as applicable. Notwithstanding any independent references to .One Seamless Solution. herein, Affiliates will have the rights of One Seamless Solution under the Agreement to order or purchase Services and to enforce all rights and obligations with respect to any such order or purchase; provided, however, that One Seamless Solution and Affiliates will be independently responsible for their respective purchase and/or use of Services.
                </p>
                <h4>2. SERVICES</h4>
                <p style="text-align: justify; margin-left:10px;">
                    <b>2.1</b> Ordering.  One Seamless Solution will issue a PO for Services. Vendor agrees not to provide any Service unless One Seamless Solution issues a PO for such Services. One Seamless Solution makes no representations to Vendor as to the frequency of POs or the scope of Services that may be ordered. Any modifications to a PO must be in writing and pre-approved by One Seamless Solution in writing.
                </p>
                <p style="text-align: justify; margin-left:10px;">
                    <b>2.2</b> Price Listed Services. If Vendor offers any Price Listed Services, Vendor will provide One Seamless Solution written notice of such Price Listed Services, and Vendor and One Seamless Solution may enter into a PLSS. The prices indicated on a PLSS will remain fixed throughout the Term and will not be subject to increase, unless the Parties execute an updated PLSS. For Services ordered pursuant to a PLSS, Vendor will deliver the ordered Services on or before the delivery date set forth in the applicable PO, or if no delivery date is set forth in the PO, then within ten (10) days of the PO date.
                </p>
                <p style="text-align: justify; margin-left:10px;">
                    <b>2.3 </b>Statements of Work. Upon One Seamless Solution's request for Services that are not Price Listed, One Seamless Solution will issue a PO or the Parties will sign an SOW, as applicable.
                </p>
                <p style="text-align: justify; margin-left:10px;">
                    <b>2.4</b> Service Level Agreement. The Parties may adopt an SLA as specified in an SOW, PO or at any time by written agreement. Once an SLA is adopted, the SLA cannot be amended, terminated or revoked except by a written amendment to the Agreement signed by the Parties. Unless otherwise stated in the SLA, Vendor will inform One Seamless Solution, within one (1) business day, of any failure to meet any SLA. Vendor will issue One Seamless Solution service level credits, if any, set forth in the applicable SLA (.SLA Credits.). One Seamless Solution may apply any SLA Credits to any charges otherwise payable to Vendor by One Seamless Solution.
                </p>
                <p style="text-align: justify; margin-left:10px;">
                    <b>2.5</b> Staffing. (a) Personnel. Vendor agrees: (i) to assign Personnel qualified to perform the Services; (ii) to maintain sufficient staffing levels to ensure Services are performed within the time frames specified in the Agreement and at the performance levels in the SLAs; and (iii) Personnel will be supervised and controlled by Vendor. In addition, Vendor is solely responsible for: (1) the acts and/or omissions of Personnel; (2) payment of all Personnel compensation, including all legal and contractual benefits; (3) withholding any and all appropriate taxes; and (4) complying with any federal, state or local employment Laws as well as any other employer duties and oment and any Documentation; and (xiv) it and its Subcontractor(s) will take all necessary precautions to prevent injury to any person or damage to any property while performing Services.
                </p>
                <p style="text-align: justify; margin-left:10px;">
                    <b>8.2</b> Remedies. If the Services do not comply with Section 8.1(viii), above, in addition to any other remedies One Seamless Solution may have, Vendor will in the following order (at Vendor.s sole cost and expense): (a) procure for the One Seamless Solution Companies the right to continue using the affected Services; (b) if applicable, replace the affected Services with conforming and/or non-infringing Services at no cost to One Seamless Solution; (c) modify the affected Services so that such Services conform or become non-infringing without detracting from their functionality or performance; or (d) if the foregoing alternatives are not commercially available, within fifteen (15) days, refund to the One Seamless Solution Companies all fees paid to Vendor for such non-conforming or infringing Services.
                </p>
                <b>9. INDEMNIFICATION</b>
                <p style="text-align: justify;">    
                    Vendor will indemnify, defend and hold harmless the One Seamless Solution Entities from all costs and claims asserted by a third party, whether actual or alleged, that arise out of or in connection with the Services, including: (a) personal injury, death, or property damage; (b) theft; (c) negligent or intentional misconduct; (d) Vendor.s breach of the Agreement or a violation of applicable Law; and (e) payments to any Personnel and/or Subcontractor(s) (collectively .Claim(s).). One Seamless Solution will (at Vendor.s sole expense) reasonably cooperate to facilitate the settlement or defense of such Claim. Vendor is solely responsible for defending any Claim against a One Seamless Solution Entity, subject to such One Seamless Solution Entity.s right to participate with counsel of its own choosing at its own expense, and for payment of all judgments, settlements, damages, losses, liabilities, costs, and expenses, including reasonable attorneys. fees, resulting from all Claims against a One Seamless Solution Entity; provided however, that Vendor will not agree to any settlement that imposes any obligation or liability on a One Seamless Solution Entity without such One Seamless Solution Entity.s prior express written consent.
                </p>
        <!--        <p style="text-align: justify;"s>-->
                    <h4>10. INSURANCE</h4>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>10.1 </b>Coverage. 
                             Vendor will obtain and keep in effect, at Vendor.s expense, insurance coverage as follows: (a) commercial general liability insurance, on an occurrence policy form, with policy limits equal to or greater than $1,000,000 per occurrence (combined single limit) and $2,000,000 in the aggregate, covering operations by or on behalf of Vendor, including coverage for: (i) premises and operations, (ii) products and completed operations, (iii) contractual liability, (iv) property damage (including completed operations), and (v) personal injury liability/advertising injury; (b) fidelity and commercial crime insurance, naming One Seamless Solution! Inc. as a Loss Payee, with policy limits equal to or greater than $250,000; (c) professional liability, errors and omissions and cyber liability insurance, on a claims made policy form, with policy limits equal to or greater than $3,000,000 annual aggregate; coverage to include the following: defamation, copyright infringement, trademark, and trade dress, loss of data, invasion of privacy, network security liability, theft, unauthorized disclosure, alteration, corruption, destruction or deletion of information stored or processed on a computer system, the failure to prevent the transmission of malicious code or malware (intentional or unintentional), remediation expenses, and infringement upon materials used in the project or provision of services (d) automobile liability insurance, including coverage for all owned, hired and non-owned automobiles with policy limits equal to or greater than $1,000,000 combined single limit each accident for bodily injury and property damage; (e) workers' compensation as required by Laws and employer's liability insurance with policy equal or greater than $1,000,000 combined single limit for each accident for bodily injury by accident, and $1,000,000 each employee for bodily injury by disease policy limit and $1,000,000 policy limit by disease; (f) umbrella liability insurance, on an occurrence policy form, with policy limits of $5,000,000 per occurrence and in the aggregate; and (g) such other liability insurance coverage and policy limits, as may be requested by One Seamless Solution
                        </p>

                        <p style="text-align: justify; margin-left:10px;">
                            <b>10.2</b> Certificates and Policies of Insurance. A Certificate of Insurance evidencing all required coverage, limits and endorsements must be furnished to One Seamless Solution before Vendor provides any Services, and annually thereafter upon expiration of policies, throughout the Term of the Agreement. Vendor will endeavor to provide One Seamless Solution thirty (30) days' written notice prior to cancellation or intent not to renew any insurance coverage(s) required to be maintained by the Agreement. All insurance policies will be written by financially viable companies rated by A. M. Best as A-VII or better and duly licensed and authorized to do business in the state, province or territory in which Vendor is located. One Seamless Solution shall be named as an additional insured under Vendor.s policies for commercial general liability insurance and liability waiver of subrogation shall apply in favor of One Seamless Solution, with respect to the workers' compensation policy.  Vendor's liability policies shall be primary and non-contributory with any insurance policy maintained by One Seamless Solution. Policy deductibles shall not be more than $100,000, and Vendor will notify One Seamless Solution of the amount of any applicable deductibles. Vendor agrees to indemnify and hold One Seamless Solution harmless for covered losses and/or occurrences that fall within any applicable deductible if such losses are Vendor's responsibility under the Agreement. Certificates of insurance will be mailed to: One Seamless Solution! Inc., Procurement Department, 701 First Avenue, Sunnyvale, CA 94089.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>10.3</b> Continuation of Insurance. Vendor will keep all liability insurance coverage required by the Agreement in effect for at least four (4) years after the expiration or termination of the Agreement.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>10.4</b> Obligations. In no event will the insurance coverage, deductible, self-insured retention or limits of any insurance maintained by Vendor under the Agreement, or the lack or unavailability oforted, resold, shipped, or diverted directly or indirectly any regulated material, including technical data, to any country for which the U.S. Government, any agency thereof, or any other sovereign government, requires an export license or other governmental approval without first obtaining such license or approval. Vendor will provide One Seamless Solution with all information that may be required to comply with all export laws, including applicable export control classification numbers, and documentation substantiating U.S. and foreign regulatory approvals. Under the U.S. Export Administration Regulations, transfers of certain export-controlled software and technology to foreign nationals are treated as exports to the foreign nationals. home countries (otherwise known as .deemed exports.) and may require export licenses. Vendor will comply with U.S export controls regulating deemed exports, will obtain all export licenses that may be required before releasing export-controlled software and technology to its foreign national personnel, and ensure that none of its personnel working under this Agreement are identified on U.S. Government export exclusion lists.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.11</b> Equal Employment. One Seamless Solution may be considered a federal contractor or subcontractor, as defined by federal law. If One Seamless Solution meets the definition of a federal contractor or subcontractor, One Seamless Solution and Vendor agree that, as applicable, they shall abide by the requirements of 41 CFR 60-1.4(a), 41 CFR 60-300.5(a) and 41 CFR 60-741.5(a) and that these laws are incorporated herein by reference. These regulations prohibit discrimination against qualified individuals based on their status as protected veterans or individuals with disabilities, and prohibit discrimination against all individuals based on their race, color, religion, sex, sexual orientation, gender identity, or national origin. These regulations require that covered prime contractors and subcontractors take affirmative action to employ and advance in employment individuals without regard to race, color, religion, sex, sexual orientation, gender identity, national origin, protected veteran status or disability. One Seamless Solution and Vendor also agree that, as applicable, they will abide by the requirements of Executive Order 13496 (29 CFR 471, Appendix A to Subpart A), relating to the notice of employee rights under federal labor laws.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.12</b> Force Majeure. A Party will be excused from a delay in performing, or a failure to perform, its obligations under the Agreement to the extent such delay or failure is caused by the occurrence of any contingency beyond the reasonable control, and without any fault, of such Party, which contingencies include acts of God, war, riot, power failures, fires, and floods (referred to as a .Force Majeure Event.). In such event, the performance times will be extended for a period of time equivalent to the time lost due to the Force Majeure Event. In order to avail itself of the relief provided in this Section 13.12, the affected Party must act with due diligence to remedy the cause of, or to mitigate or overcome, such delay or failure. For purposes of this Section 13.12, due diligence will, require Vendor to maintain a contingency and disaster recovery plan for the continuation of business so that despite any disruption in Vendor.s ability to fulfill its Services obligations from any particular location or through the efforts of any particular individuals, Vendor will be able to fulfill its Services obligations from an alternative/backup location.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.13</b> Governing Law; Jurisdiction; and Venue. The Agreement and any dispute relating thereto will be governed by the laws of the State of California, without regard to conflict/choice of law principles. The United Nations Convention on Contracts for the International Sale of Goods does not apply to the Agreement. Vendor agrees to submit to the exclusive jurisdiction of the state and federal courts located in Los Angeles or Santa Clara County, California. Any claim against One Seamless Solution will be adjudicated on an individual basis and will not be consolidated in any proceeding with any claim or controversy of any other party.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.14</b> Entire Agreement and Amendments. The Agreement constitutes the entire agreement between One Seamless Solution and Vendor and supersedes any and all other agreements and understandings between One Seamless Solution and Vendor, whether oral or written, with respect to the subject matter hereof. In the event of a conflict between the provisions of the Vendor MTC, and any PO, SOW, SLA, PLSS, or Change Order, the Vendor MTC will control. Notwithstanding the foregoing, a PO, SOW or Change Order may amend Sections 3, 4, or 10.1 of the Vendor MTC only if the amended terms contained in such a PO, SOW or Change Order: (i) apply only to the individual PO, SOW or Change Order and not to any other PO, SOW or Change Order, and (ii) specifically identifies the provision(s) of the Vendor MTC they amend. The terms and conditions on Vendor.s invoice, quotation or other document will not be binding and will not supersede, supplement, or modify the Agreement. One Seamless Solution may change the Vendor MTC at any time by posting such on the applicable One Seamless Solution Company Website or by email, and such revised Vendor MTC will supersede and replace the earlier Vendor MTC. Any provision of Services after such revision will be deemed to be acceptance by Vendor of the revised Vendor MTC.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.15</b> Cumulative Remedies. The rights and remedies of any One Seamless Solution Company under the Agreement are not exclusive and may be exercised alternatively or cumulatively, with any other rights and remedies available under the Agreement or in law or equity.
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.16</b> Contract Acceptance; Counterparts. Any PO, SOW, SLA, PLSS, and/or Change Order requiring signatures may be executed in counterparts, each of which will be deemed an original, but all of which together will constitute one and the same instrument. Vendor.s online acceptance of any or all portions of the Agreement will be deemed an execution for purposes of the preceding sentence. Vendor will not have the right to object to the manner (i.e., online acceptance, electronic signatures, fax, or scanned images of sig
                        </p>
                        <p style="text-align: justify; margin-left:10px;">
                            <b>13.17</b> Captions and Construction. Captions in the Agreement are for convenience only and will not affect the interpretation or construction of the Agreement. As used in the Agreement, (a) .days. mean calendar days unless otherwise stated, (b) .include. and .including. mean .including, without limitation,. and (c) .will,. .shall,. and .must. are deemed to be equivalent and denote a mandatory obligation or prohibition, as applicable.
                        </p>


                    <h4>14. DEFINITIONS</h4>
                    <p style="text-align: justify;">
                        All definitions will apply both to their singular and plural forms, as the context may require. In addition to those definitions set forth elsewhere in the Agreement, the following capitalized terms have the meanings set forth below:
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.1</b> .Affiliate. means an entity that directly or indirectly controls, is controlled by, or is under common control with One Seamless Solution.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.2</b> .Agreement. means collectively: (a) these Vendor MTC; (b) the Vendor Master Data Template; (c) SOW(s), if any; (d) Price Listed Services Schedule(s), if any; (e) Change Order(s), if any; (f) PO(s); and (g) any SLA(s).
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.3</b> .Anti-Spam Policy. means One Seamless Solution's anti-spam policy terms located at:https://policies.yahoo.com/us/en/yahoo/terms/vendor/antispam/index.html, which are incorporated herein by this reference and may be updated from time to time.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.4</b> .Defect. means any failure of the Services, in whole or in part, to comply with the Acceptance Criteria or perform as contemplated by the Agreement.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.5</b> .Deliverables. mean all developments, discoveries, inventions, products, product formulae, software, drawings, procedures, processes, Specifications, reports, notes, documents, information, plans, reports, compilations of data, and other materials made, conceived, reduced to practice or developed by Vendor alone or with others, and Improvements to any of the foregoing, which (a) are identified as Deliverables in PO or an SOW, (b) are created using One Seamless Solution Property, or (c) incorporate One Seamless Solution Property.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.6</b> .Documentation. means any written or electronic support materials/documents relating to the use or operation of the Services that Vendor generally makes available with the Services, including user manuals, software support materials, guides, how-to information, data sheets, promotional materials, and/or any other information regarding how to enable or use the Services.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.7</b> .Improvements. means collectively, all enhancements, additions, modifications, extensions, updates, new versions, translations, improvements, and derivative works.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.8</b> .Information System. means: (a) any information or communications system, including net-services, computer systems, data networks, software applications, broadband/satellite/wireless communications systems, and voicemail; and (b) the means of access to such systems, including all authentication methods.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.9</b> .Intellectual Property. or .Intellectual Property Rights. means all: (a) trade secrets; (b) patents and patent applications; (c) trademarks and trademark applications; (d) service marks and service mark applications; (e) trade names; (f) Internet domain names; (g) copyrights and copyright applications; (h) moral rights; (i) database rights; (j) design rights; (k) rights in know-how; (l) rights in inventions (whether patentable or not); (m) renewals or extensions of subsections 14.9(a) through (l); (n) goodwill associated with subsections 14.9(a) through (h), and (o) all other equivalent rights anywhere in the world.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.10</b> .Laws. means all applicable laws, statutes, directives, ordinances, treaties, contracts, or regulations.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.11</b> .Party. means either One Seamless Solution or Vendor, as applicable, and .Parties. means One Seamless Solution and Vendor. For any PO issued by a One Seamless Solution Company other than One Seamless Solution or an SOW between Vendor and a One Seamless Solution Company other than One Seamless Solution, references to One Seamless Solution through the Agreement will mean the applicable One Seamless Solution Company.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.12</b> .Personal Information. or .PII. or .Personally Identifiable Information. means any information about a User that: (a) can be used to identify, contact or locate a specific individual; (b) can be used in conjunction with other personal or identifying information to identify or locate a specific individual, including, for example, a persistent identifier, such as a customer number held in a .cookie. or processor serial number; or (c) is defined as .personal information. by applicable Laws relating to the collection, use, storage and/or disclosure of information about an identifiable individual.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.13</b> .Personnel. means all workers employed, contracted, or used by Vendor in connection with the Agreement, including employees, agents, independent contractors, temporary personnel, day laborers, and other individuals/entities.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.14</b> .Price Listed. or .Price Listed Services. means Services offered at a fixed price inclusive of all costs and fees.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.15</b> .Price Listed Services Schedule. or .PLSS. means a mutually agreed upon and executed document that identifies Price Listed Services, the agreed upon prices for Price Listed Services.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.16</b> .Project Manager. means the One Seamless Solution employee or consultant who is the primary contact person for the applicable PO or SOW (as identified therein).
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.17</b> .Purchase Order. or .PO. means a document issued by One Seamless Solution authorizing the purchase of Services.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.18</b> .Service Level Agreement(s). or .SLA(s). means the performance metrics including technical requirements, measurement periods, SLA Credits, and such other performance criteria that govern the Services.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.19</b> .Services. means the services and Deliverables, if any, to be provided by Vendor as described in the Agreement. Notwithstanding any independent reference to Deliverables herein, Deliverables are included within the meaning of Services.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.20</b> .Site. means the buildings and related premises owned, operated, used, or leased by any One Seamless Solution Company, including those designated in the applicable PO or SOW as a location at which Services will be performed.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.21</b> .Site Access Policies. mean the policies that Vendor must follow when on a Site, including security, facility, equipment, conduct, and safety policies, as updated from time to time.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.22</b> .Specifications. means any criteria, including plans, drawings, data, or performance requirements, that must be satisfied for acceptance of Services under an applicable PO or SOW.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.23</b> .Start Date. means the earlier of the first delivery of any Services, Vendor.s signature on a VMDT, Effective Date of an SOW, or the date of the PO issued by One Seamless Solution.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.24</b> .Statement(s) of Work. or .SOW(s). means a fully executed document referencing these Vendor Master Terms and Conditions and outlining the nature and scope of Services, which may include: the project plan, Specifications, delivery dates, performance milestones, Deliverables, fees, payment schedule, Project Managers, Vendor Property Disclosure, if applicable, and such other pertinent information to the Services and/or the Agreement.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.25</b> .Subcontractor. means a third party to which Vendor delegates any portion of its obligations, subject to Section 2.5 (b), above.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.26</b> .Travel Policy. means One Seamless Solution's travel policy terms, which will be provided to Vendor and are incorporated herein by this reference.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.27</b> .United States Safe Harbor Program. means the United States Department of Commerce Safe Harbor framework, and as used herein United States Safe Harbor Program will be included in the meaning of Laws.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.28</b> .User. means any actual or prospective user (including advertisers and content providers) of a One Seamless Solution Company.s products and/or services, and any officer, director, employee, agent, contractor, and representative of a One Seamless Solution Company.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.29</b> .Vendor. means the vendor executing the VMDT, PO, SOW, SLA, or PLSS.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.30</b> .Vendor Master Data Template. or .VMDT. means the Vendor signed document that identifies the Vendor.s information, including: address, telephone, facsimile, federal employer ID and such other information as set forth therein.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.31</b> .Vendor Property Disclosure. means the Vendor signed document that identifies any Vendor Property that will be used to provide the Services.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.32</b> .One Seamless Solution Company. means One Seamless Solution or an Affiliate, and .One Seamless Solution Companies. means One Seamless Solution and Affiliates. A One Seamless Solution Company also includes any divested business operations or Affiliate for a term not to exceed twelve (12) months from the date of divestiture.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.33</b> .One Seamless Solution Company Website(s). means all web pages owned, operated, authorized, or hosted by or for a One Seamless Solution Company.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.34</b> .One Seamless Solution Data. means all data and information provided by, relating to, of or concerning any One Seamless Solution Company and/or its Users, that is/was obtained by, disclosed to or otherwise made available to Vendor, including Personal Information, systems procedures, processes, employment practices, sales costs, profits, pricing methods, organization/employee lists, finances, product information, inventions, designs, methodologies, Information Systems, Intellectual Property, all Deliverables and interim work product created by or on behalf of Vendor, all survey responses, feedback and reports, and all data and information of such a nature that a reasonable person would believe to be confidential or proprietary.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.35</b> .One Seamless Solution Entities. means the One Seamless Solution Companies and their officers, directors, consultants, contractors, agents, attorneys, and employees.
                    </p>
                    <p style="text-align: justify; margin-left:10px;">
                        <b>14.36</b> .One Seamless Solution Property. means, collectively, any and all One Seamless Solution Confidential Information, One Seamless Solution Data, One Seamless Solution Marks, Deliverables, One Seamless Solution's Information Systems and all property, equipment, and proprietary information and materials provided by a One Seamless Solution Company to, or otherwise obtained by, Vendor, or existing at any Site(s), as well as all derivatives of the foregoing.
                    </p>
                    <p style="text-align: center;"> * * *</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal for Terms -->
    