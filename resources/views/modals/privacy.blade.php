<!-- Modal for Privacy -->
    <div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
          </div>
          <div class="modal-body">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eos perferendis aperiam accusantium recusandae illo incidunt nam, aspernatur impedit repudiandae. Impedit quam consequuntur, veniam vero cupiditate eligendi ipsam iste quae.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eos perferendis aperiam accusantium recusandae illo incidunt nam, aspernatur impedit repudiandae. Impedit quam consequuntur, veniam vero cupiditate eligendi ipsam iste quae.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eos perferendis aperiam accusantium recusandae illo incidunt nam, aspernatur impedit repudiandae. Impedit quam consequuntur, veniam vero cupiditate eligendi ipsam iste quae.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eos perferendis aperiam accusantium recusandae illo incidunt nam, aspernatur impedit repudiandae. Impedit quam consequuntur, veniam vero cupiditate eligendi ipsam iste quae.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eos perferendis aperiam accusantium recusandae illo incidunt nam, aspernatur impedit repudiandae. Impedit quam consequuntur, veniam vero cupiditate eligendi ipsam iste quae.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal for Privacy -->