<!-- /.Modal more info vendor  -->  
    
    <div class="modal fade" id="modalVendorInfo" tabindex="-1" role="dialog" aria-labelledby="modalVendorLabel">
        <div class="modal-dialog" role="document">
            <div class="">
                  <div class="">
                    <div class="box box-widget widget-user" id="modalVendorInfo" tabindex="-1" role="dialog" aria-labelledby="modalVendorLabel">
                    <button type="button" class="close margin-r-5" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div id="vendor-card" class="widget-user-header info-user-header">       
                    <div class="col-xs-12 col-sm-8">
                        <h3 class="widget-user-username" id="companyNameInfo"></h3>
                        <h4 class="widget-user-desc" id="companyIndustryInfo"></h4>
                    </div>
                    <div class="hidden-xs col-sm-4">
                        <div class="pull-right">
                             <input  id="vendorRate" name="input-rate" class="rating rating-loading rating-disabled pull-right" data-show-clear="false" value="" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs">
                        </div>
                    </div>

                    </div>
                    <div class="hidden-xs widget-user-image">
                      <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="">
                        <div class="col-sm-6">
                          <div class="">
                           <!-- <p>Contact Name: <b id="contactName"></b></p> -->
                            <p>Phone: <b id="vendorContactPhoneInfo"></b></p>
                            <p>Email: <b id="contactEmailInfo"></b></p>
                            <p>Web: <b id="contactWebInfo"></b></p>
                            <p>Metropolitan Area: <b id="CompanyMetroAreaInfo"></b></p>
                            <p>Attends Emergency: <b id="CompanyEmergenciesInfo"></b></p>
                            @if(env('APP_DEBUG')==true)
                                <p style="color:red;"><b> Vendor User Email (For Testing purpose):</b> <b id="vendorUserEmailInfo"></b></p>
                            @endif
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 border-left">
                          <div class="">
                            <p>Address: <b id="companyAddressInfo"></b></p>
                            <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                            <p>Zip: <b id="companyZipInfo"></b></p>
                            <p>City: <b id="companyCityInfo"></b></p>
                            <p>State: <b id="companyStateInfo"></b></p>
                            <p>Country: <b id="companyCountryInfo"></b></p>                   
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                      <div class="">
                        <div class="col-sm-12">
                          <address>
                            <strong><li class="fa fa-plus-square"></li> Services</strong>
                            <p id="servicesListInfo"></p>
                            <strong><li class="fa fa-plus-square"></li> Atributes</strong>
                            <p id="attributesListInfo"></p>
                            <strong><li class="fa fa-map-marker"></li> Vendor location</strong><br>
                            <div id="examples">
                                <a href="#"></a>
                            </div>
                            <br>
                            <input type="hidden" id="geocomplete">
                            <div id="geocomplete"  class="map_canvas_vendorlist"></div>
                            <div id="errorVendorInfoMap"></div>
                          </address>
                          <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->  
                    </div>
                  </div>
                  </div> <!-- /.end modal body -->     
            </div>
        </div>
    </div>

<!-- /.End Modal more vendor info -->   