<!-- /.Modal more info vendor  -->  
    
    <div class="modal fade" id="modalVendorInfo" tabindex="-1" role="dialog" aria-labelledby="modalVendorLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header info-user-header bg-primary">
            <div class="col-xs-8">
                <h3 class="widget-user-username" id="companyNameInfo"></h3>
                <h4 class="widget-user-desc" id="companyIndustryInfo"></h4>
            </div>
            <div class="col-xs-4">
                <input  id="vendorRate" name="input-rate" class="pull-right rating rating-loading rating-disabled" data-show-clear="false" value="" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs">
            </div>
              
              
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-6">
                  <div class="">
                   <!-- <p>Contact Name: <b id="contactName"></b></p> -->
                    <p>Phone: <b id="vendorContactPhoneInfo"></b></p>
                    <p>Email: <b id="contactEmailInfo"></b></p>
                    <p>Web: <b id="contactWebInfo"></b></p>
                    <p>Metropolitan Area: <b id="CompanyMetroAreaInfo"></b></p>
                    <p>Attends Emergency: <b id="CompanyEmergenciesInfo"></b></p>
                    @if(env('APP_DEBUG')==true)
                        <p style="color:red;"><b> Vendor User Email (For Testing purpose):</b> <b id="vendorUserEmailInfo"></b></p>
                    @endif
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6 border-left">
                  <div class="">
                    <p>Address: <b id="companyAddressInfo"></b></p>
                    <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                    <p>Zip: <b id="companyZipInfo"></b></p>
                    <p>City: <b id="companyCityInfo"></b></p>
                    <p>State: <b id="companyStateInfo"></b></p>
                    <p>Country: <b id="companyCountryInfo"></b></p>                   
                   
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-sm-12">
                  <address>
                    <strong><li class="fa fa-plus-square"></li> Services</strong>
                    <p id="servicesListInfo"></p>
                    <strong><li class="fa fa-plus-square"></li> Atributes</strong>
                    <p id="attributesListInfo"></p>
                    <strong><li class="fa fa-map-marker"></li> Map location</strong><br>
                    <div id="examples">
                        <a href="#"></a>
                    </div>
                    <br>
                    <input type="hidden" id="geocomplete">
                    <div id="geocomplete"  class="map_canvas_vendorlist"></div>
<!--                    <img src="{{ URL::asset('assets/img/error-map.png') }}" alt="" class="img-responsive img-map-error">-->
                    <div id="errorVendorInfoMap"></div>
                  </address>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->  
            </div>
          </div>
          </div><!-- /.end modal body -->  
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

<!-- /.End Modal more vendor info -->   