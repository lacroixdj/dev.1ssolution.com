 <!-- /.Modal more info client  -->  
    <div class="modal fade" id="modalClientInfo" tabindex="-1" role="dialog" aria-labelledby="modalClientLabel">
        <div class="modal-dialog" role="document">
        <div class="">
          <div class="">
<!--            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
          </div>
          <div class="">
            <div class="box box-widget widget-user">
                <button type="button" class="close margin-r-5" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header info-user-header bg-primary">
            <div class="col-xs-12">
                <h3 class="widget-user-username" id="clientNameInfo"></h3>
<!--
                <h4 class="widget-user-desc" id="companyIndustryInfo"></h4>
                <p>Attends Emergency: <b id="CompanyEmergenciesInfo"></b></p>
-->
            </div>
<!--
            <div class="col-xs-4">
                <input  id="vendorRate" name="input-rate" class="pull-right rating rating-loading rating-disabled" data-show-clear="false" value="" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs">
            </div>
-->
              
              
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="">
                <div class="col-sm-6">
                  <div class="">
                   <!-- <p>Contact Name: <b id="contactName"></b></p> -->
                    <p>Phone: <b id="clientContactPhoneInfo"></b></p>
                    <p>Email: <b id="clientContactEmailInfo"></b></p>
                    <p>Web: <b id="clientContactWebInfo"></b></p>
                    <p>Metropolitan Area: <b id="clientMetroAreaInfo"></b></p>
                    @if(env('APP_DEBUG')==true)
                        <p style="color:red;"><b> Client User Email (For Testing purpose):</b> <b id="clientUserEmailInfo"></b></p>
                    @endif
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6 border-left">
                  <div class="">
                    <p>Address: <b id="clientAddressInfo"></b></p>
                    <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                    <p>Zip: <b id="clientZipInfo"></b></p>
                    <p>City: <b id="clientCityInfo"></b></p>
                    <p>State: <b id="clientStateInfo"></b></p>
                    <p>Country: <b id="clientCountryInfo"></b></p>                   
                   
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="">
                <div class="col-sm-12">
                  <address>
<!--
                    <strong><li class="fa fa-plus-square"></li> Services</strong>
                    <p id="servicesListInfo"></p>
                    <strong><li class="fa fa-plus-square"></li> Atributes</strong>
                    <p id="attributesListInfo"></p>
-->
                    <strong><li class="fa fa-map-marker"></li> Client location</strong><br>
                    <div id="examples">
                        <a href="#"></a>
                    </div>
                    <br>
                    <input type="hidden" id="clientInfoGeocomplete">
                    <div id="geocomplete"  class="map_canvas_clientlist"></div>
<!--                    <img src="{{ URL::asset('assets/img/error-map.png') }}" alt="" class="img-responsive img-map-error">-->
                    <div id="errorClientInfoMap"></div>
                  </address>
                  <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Close</button>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->  
            </div>
          </div>
          </div><!-- /.end modal body -->  
          <div class="">
<!--            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
          </div>
        </div>
      </div>
    </div>

    <!-- /.End Modal more vendor info -->   