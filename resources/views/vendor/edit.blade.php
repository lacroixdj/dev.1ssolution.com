@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Vendor User Info')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Account
      <small>Company Information</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Account</li>
      <li class="active">Company</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- Box 1 -->
        <div class="box box-primary">

          <div class="box-header with-border">
            <h3 class="box-title"><li class="fa fa-building-o"></li> Company Info.</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          {!! Form::model($vendor, [
          'method' => 'POST',
          'url' => ['/account/vendor'],
          'id' => 'vendor_company_edit',
          'data-parsley-validate' => '', 
          'data-parsley-trigger' => 'focusin focusout',
          'role' => 'form'
          ]) !!}
          <div class="box-body">

            <!-- / left column -->
            <div class="col-md-6">

              <!-- company name -->
              <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Company Name</label>
                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required',  'id' => 'name', 'placeholder' => 'Company name']) !!}
                <li class="fa fa-building-o form-control-feedback"></li>
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
              </div>
              <!-- /company name -->

              <!-- industry -->
              <div class="form-group has-feedback {{ $errors->has('industry_id') ? 'has-error' : ''}}">
                <label for="">Industry:</label>
                {{ Form::select('industry_id', Array(''=>'Select your industry')+ $industries, $vendor->industry_id,
                [ 'id' => 'industry_id',
                'class' => 'form-control',
                'required' => 'required',
                'readonly' => 'readonly',
                'disabled' => 'disabled',
                ]) 
              }}
               <li class="fa fa-industry form-control-feedback"></li>
              {!! $errors->first('industry_id', '<p class="help-block">:message</p>') !!}
            </div>
            <!-- /industry -->
            
            <!-- services -->  
            <div class="form-group has-feedback {{ $errors->has('services') ? 'has-error' : ''}}">
              <label for="">Services:</label>
              {{ Form::select('services[]', $services, null,
              ['id' => 'services',
              'class' => 'form-control selectpicker',
              'multiple' => 'multiple',
              'data-actions-box' => 'true',
              'data-size' => '5',
              'data-none-selected-text' => 'Select your services',
              'data-live-search' => 'true',
              'required' => 'required'

              ]) 
            }}  
            <li class="fa fa-plus-square form-control-feedback"></li>
            {!! $errors->first('services', '<p class="help-block">:message</p>') !!}

          </div>
          <!-- /services -->

          <!-- attributes -->                 
          <div class="form-group has-feedback {{ $errors->has('attributess') ? 'has-error' : ''}}">
            <label for="">Attributes:</label>
            {{ Form::select('attributess[]', $attributes, null,
            [ 'id' => 'attributes',
            'class' => 'form-control selectpicker',
            'multiple' => 'multiple',
            'data-actions-box' => 'true',
            'data-size' => '5',
            'data-none-selected-text' => 'Select your attributes',
            'data-live-search' => 'true'

            ]) 
          }} 
          <li class="fa fa-plus-square form-control-feedback"></li>
          {!! $errors->first('attributess', '<p class="help-block">:message</p>') !!} 
        </div>
        <!-- /attributes -->


        <!-- metroarea -->
        <div class="form-group has-feedback {{ $errors->has('metroarea_id') ? 'has-error' : ''}}">
          <label for="">Metropolitan area:</label>
          {{ Form::select('metroarea_id', Array(''=>'Select your metropolitan area') + $metroareas, $vendor->metroarea_id,
          [ 'id' => 'metroarea_id',
          'class' => 'form-control',
          'required' => 'required'
          ]) 
        }}
        <li class="fa fa-map-marker form-control-feedback"></li>
        {!! $errors->first('metroarea_id', '<p class="help-block">:message</p>') !!}
      </div>
      <!-- /metroarea -->

    </div>
    <!-- / left column -->  

    <!-- right column -->  
    <div class="col-md-6">

      <!-- phone -->
      <div class="form-group has-feedback {{ $errors->has('phone') ? 'has-error' : ''}}">
        <label for="phone">Company phone:</label>                      
        {!! Form::text('phone', null, 
        [ 'class' => 'form-control', 
        'required' => 'required', 
        'id' => 'phone', 
        'placeholder' => '(000) 000-0000', 
        'data-mask' => '(000)000-0000', 
        'data-parsley-minlength' => '14' 
        ]) 
        !!}
        <li class="fa fa-phone form-control-feedback"></li>
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
      </div>
      <!-- /phone -->

      <!-- email -->
      <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : ''}}">
        <label for="email">Company email:</label>                      
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'email', 'type'=>'email', 'placeholder' => 'Company email']) !!}
        <li class="fa fa-envelope form-control-feedback"></li>
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
      </div>
      <!-- /email -->

      <!-- url -->
      <div class="form-group has-feedback {{ $errors->has('url') ? 'has-error' : ''}}">
        <label for="email">Web address:</label>                      
        {!! Form::text('url', null, ['class' => 'form-control', 'id' => 'url', 'type'=>'url', 'placeholder' => 'Company website']) !!}
        <li class="fa fa-globe form-control-feedback"></li>
        {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
      </div>
      <!-- /url -->


      <div class="form-group has-feedback {{ $errors->has('emergency') ? 'has-error' : ''}}">
        <label for=""><li class="fa  fa-exclamation-triangle"></li> This company serves emergencies?</label>
        <div class="checkbox icheck">

          <label>{!! Form::checkbox('emergency', '1') !!} Yes</label>

        </div>
        {!! $errors->first('emergency', '<p class="help-block">:message</p>') !!}
      </div>

      <!-- form buttons -->  
      <div class="form-group has-feedback pull-right">                        
        <button type="button" href="#" onClick="window.location.reload()" class="btn btn-primary btn-flat">Cancel</button>
        {!! Form::submit('Update Company', ['class' => 'btn btn-success btn-flat']) !!}
      </div>
      <!-- /form buttons -->

    </div>
    <!-- /right column -->


  </div>    
  <!-- /.box-body -->

  <div class="box-footer">                   
  </div>

  {!! Form::close() !!}

</div>
<!-- /.box -->
</div>     
</div>
<!-- /.row vendor -->

<!-- row lacation -->
<div class="row">
  <div class="col-md-12">
    <!-- Box 1 -->
    <div class="box box-primary">

      <div class="box-header with-border">
        <h3 class="box-title"><li class="fa fa-map-marker"></li> Company Location.</h3>
      </div>
      <!-- /.box-header -->

      <!-- form start -->
      {!! Form::model($vendor, [
        'method' => 'POST',
        'url' => ['/vendor/location'],  
        'id' => 'vendor_company_location_edit',              
        'data-parsley-validate' => '', 
        'data-parsley-trigger' => 'focusin focusout',
        'role' => 'form'
      ]) !!}
      <div class="box-body">

        <!-- / left column -->
        <div class="col-md-6">

          <!-- address -->
          <div class="form-group has-feedback {{ $errors->has('address') ? 'has-error' : ''}}">
            <label for="address">Address:</label>                      
            {!! Form::text('address', $vendor->addresses()->first()->address, 
            [ 'class' => 'form-control', 
              'required' => 'required', 
              'id' => 'address',
              'placeholder' => 'Type your address'
            ]) 
            !!}
            <li class="fa fa-map-marker form-control-feedback"></li>
            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
          </div>
          <!-- /address -->

          <!-- address2 -->
          <div class="form-group has-feedback {{ $errors->has('address2') ? 'has-error' : ''}}">
            <label for="address2">Address 2:</label>                      
            {!! Form::text('address2', $vendor->addresses()->first()->address2, 
              [ 
                'class' => 'form-control', 
                'id' => 'address2',
                'placeholder' => 'Type your additional address'

              ]) 
            !!}
            <li class="fa fa-map-marker form-control-feedback"></li>
            {!! $errors->first('address2', '<p class="help-block">:message</p>') !!}
          </div>
          <!-- /address2 -->


          <!-- zip code -->
          <div class="form-group has-feedback {{ $errors->has('zip') ? 'has-error' : ''}}">
            <label for="zip">Zip / Postal code:</label>                      
            {!! Form::text('zip', $vendor->addresses()->first()->zip, 
              [ 
                'class' => 'form-control', 
                'required' => 'required', 
                'id' => 'zip',
                'placeholder' => '00000-000', 
                'data-mask' => '00000-000', 
              ]) 
            !!}
            <li class="fa fa-map-marker form-control-feedback"></li>
            {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
          </div>
          <!-- /zip -->
        </div>
        <!-- / left column -->  

        <!-- right column -->  
        <div class="col-md-6">

          <!-- country -->
          <div class="form-group has-feedback {{ $errors->has('country') ? 'has-error' : ''}}">
            <label for="name">Country:</label>
            {{ Form::select(
            'country', 
            Array(''=>'Select your country') + $countries, '231',
            [
            'id' => 'country',
            'class' => 'form-control',
            'required' => 'required',
            'readonly' => 'readonly',
            'disabled' => 'disabled',

            ]
            ) 
          }}
          <li class="fa fa-map-marker form-control-feedback"></li>
          {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
        </div>
        <!-- /country  -->

        <!-- state -->
        <div class="form-group has-feedback {{ $errors->has('state') ? 'has-error' : ''}}">
          <label for="">State:</label>
          {{ Form::select(
          'state', 
          Array(''=>'Select your state') + $states, $vendor->addresses()->first()->city->state->id,
          [
          'id' => 'state',
          'class' => 'form-control',
          'required' => 'required'
          ]
          ) 
        }} 
        <li class="fa fa-map-marker form-control-feedback"></li>
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
      </div>
      <!-- /state -->

      <!-- city -->  
      <div class="form-group has-feedback {{ $errors->has('city') ? 'has-error' : ''}}">
        <label for="">City:</label>
        {{ Form::select(
        'city', 
        Array(''=>'Select your city') + $cities, $vendor->addresses()->first()->city->id,
        [ 
        'id' => 'city',
        'class' => 'form-control',
        'required' => 'required'
        ]
        ) 
      }}   
      <li class="fa fa-map-marker form-control-feedback"></li>
      {!! $errors->first('city', '<p class="help-block">:message</p>') !!}                     
    </div>
    <!-- /city -->

    <!-- form buttons -->
    <div class="form-group has-feedback pull-right">  
      <button type="button" href="#" onClick="window.location.reload()" class="btn btn-primary btn-flat">Cancel</button>
      {!! Form::submit('Update Location', ['class' => 'btn btn-success btn-flat']) !!}
    </div>
    <!-- /form buttons -->

  </div>
  <!-- /right column -->

</div>    
<!-- /.box-body -->

<div class="box-footer">                   
</div>

{!! Form::close() !!}

</div>
<!-- /.box -->
</div>     
</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<script>
  $(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
  });
  $(document).ready(function() { 

    XHRFormListener('#vendor_company_edit');
   

    XHRFormListener('#vendor_company_location_edit');


    //Seting the services values on multiselect combo
    var vendor_services = [{{ implode(",", $vendor->services->lists('id')->toArray()) }} ];        
    $('#services').val(vendor_services);
    $('#services').selectpicker('render');        

    //Seting the attributes values on multiselect combo
    var vendor_attributes = [{{ implode(",", $vendor->attributes->lists('id')->toArray()) }} ];        
    $('#attributes').val(vendor_attributes);
    $('#attributes').selectpicker('render'); 


    //OnChange industry combo must reload children services and attributes combos
    $('#industry_id').change(function() {

        //Getting elements
        var industry_id = $( this ).val();
        var attributes_combo = $('#attributes');
        var services_combo = $('#services');

        //Cleaning children combos
        attributes_combo.find('option').remove();     
        services_combo.find('option').remove();

        //Refresh the selectpicker combo plugins
        attributes_combo.selectpicker('refresh');
        services_combo.selectpicker('refresh');

            //Disabling children if no industry selected
          if (industry_id == 0){
            attributes_combo.attr( "disabled", "disabled" );
            services_combo.attr( "disabled", "disabled" );
            attributes_combo.selectpicker('refresh');
            services_combo.selectpicker('refresh');
            return;

          } 

          //Building backend urls
          var backend_url = '{{ url('/json/industries') }}';
          var attributes_url = backend_url  + '/' + industry_id + '/attributes';  
          var services_url = backend_url  + '/' + industry_id + '/services';  

            //The Ajax request
            $.ajax({
              method: "GET",
              url: attributes_url,
              dataType: 'json'
            }).done(function(data) {
                //Cleaning combo 
                attributes_combo.removeAttr('disabled');        
                //Populating up combo
                $.each( data, function( key, value ) {                    
                  attributes_combo.append($("<option />").val(key).text(value));
                });
                //Refreshing element
                attributes_combo.selectpicker('refresh');  
              });


          //The Ajax request
          $.ajax({
            method: "GET",
            url: services_url,
            dataType: 'json'
          }).done(function(data) {
              //Cleaning combo 
              services_combo.removeAttr('disabled');
              //Populating up combo
              $.each( data, function( key, value ) {
                services_combo.append($("<option />").val(key).text(value));
              });
              //Refreshing element  
              services_combo.selectpicker('refresh');               
            });

        });
          //End Industries, services and attributes combo refreshing.

          
        //Aplying validation mask to phone and zip code
        $('#phone').mask('(000) 000-0000');
        $('#zip').mask('00000-000');
        $('#vendor_company_edit').parsley();

          



         //Country, states and city chained comboboxes refresing logic
        $('#country').change(function(){

          //Getting elements
          var country_id = $(this).val();
          var states_combo = $('#state');
          var cities_combo = $('#city');

          //Clearing combos   
          states_combo.find("option:gt(0)").remove();    
          cities_combo.find("option:gt(0)").remove();

          //If no country selected then disable childrens
          if (country_id == 0){
            states_combo.attr( "disabled", "disabled" );
            cities_combo.attr( "disabled", "disabled" );
            return;
          } 

          //Compose backend url to retrieve data from models
          var countries_url = '{{ url('/json/countries') }}';
          var states_url = countries_url  + '/' + country_id + '/states';  
          
          //The ajax request
          $.ajax({
            method: "GET",
            url: states_url,
            dataType: 'json'
          }).done(function(data) {
              //Enabling child combo                 
              states_combo.removeAttr('disabled');        
              
              //Populating  state combo
              $.each( data, function( key, value ) {                    
                states_combo.append($("<option />").val(key).text(value));
              });
            });
        });

        //States and city chained comboboxes refresing logic
        $('#state').change(function(){

          //Getting elements
          var state_id = $(this).val();
          var cities_combo = $('#city');

          //Clearing combos
          cities_combo.find("option:gt(0)").remove();

          //If no state selected then disable child
          if (state_id == 0){
            cities_combo.attr( "disabled", "disabled" );
            return;
          } 

          //Compose backend url to retrieve data from models
          var states_url = '{{ url('/json/states') }}';
          var cities_url = states_url +'/' + state_id + '/cities'; 

          //The ajax request
          $.ajax({
            method: "GET",
            url: cities_url,
            dataType: 'json'
          }).done(function(data) {
              //Enabling child combo           
              cities_combo.removeAttr('disabled');

              //Populating  state combo
              $.each( data, function( key, value ) {
                cities_combo.append($("<option />").val(key).text(value));
              });
            });       
        });
});
</script>
@endsection
