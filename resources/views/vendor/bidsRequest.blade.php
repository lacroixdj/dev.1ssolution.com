@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Bids request')

@section('content')
 <!-- Content Wrapper. Contains page content -->
<style type="text/css">
    .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100px !important;
}  

.bootstrap-select > .dropdown-toggle{
     /* width: 60% !important;*/
      padding: 4px 10px !important;
      border-radius: 0 !important;
  }

.bootstrap-select.btn-group .dropdown-menu {
     /*min-width: 60% !important;*/
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

</style>




  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bids request
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">Request / Bids</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-list"></i> Bids request list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="VendorBidsRequest" class="table tale-striped table-bordered table-hover responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th data-priority="1">#</th>
                      <th data-priority="1">Type</th>
                      <th data-priority="1">Client</th>
                      <th data-priority="1">Description</th>
                      <th data-priority="1">Status</th>
                      <th data-priority="1">Bid amount</th>
                      <th>Service by</th>
                      <th data-priority="2">Location</th>                      
                      <th data-priority="1">Client Profile</th>
                      <th data-priority="1">Actions</th>
                    </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<script>
$(document).ready(function() { 
    
    $('#VendorBidsRequest').DataTable({        
        
        processing: true,
        
        pageLength: 5,
        
        order: [[ 0, "desc" ]],
        
        bLengthChange: true,
        
        lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
        
        dom: '<"pull-left"f><"pull-left rfp-toolbar"><"pull-right"l>tip',
        
        serverSide: true,
        
        responsive: true,  
        
        deferLoading: 1,                          
        
        ajax: {

          method: 'POST',

          url: '/json/datatable/listRfpVendors',

          data: function(d) {

              d._token = "{{ csrf_token() }}";
              
              d.status = (!empty($("#status_list").val()))? $("#status_list").val() : 0;

              d.type = (!empty($("#type_list").val()))? $("#type_list").val() : 0;

          }
        },
        
        columns:[


            {data: "id",            name: 'id',             sType: "numeric", type: "numeric"     },
            {data: "type",          name: 'rfps.type',      width:'5%'      },
            {data: "client.name",   name: 'client.name',    width:'15%'     },
            {data: "description",   name: 'description',    width:'20%'     },
            {
                data: "pivot.status",        name: 'pivot.status', width:'10%',
                
                render: function ( data, type, row ) {
                   
                    if ( type === 'display' ) {
                        
                        if(data === 'invited'){
                            var status = '<span class="label bg-blue">Invited</span>';
                        }
                        else if(data === 'bid'){
                            var status = '<span class="label bg-yellow">Bid</span>';
                        }
                        else if(data === 'closed'){
                            var status = '<span class="label bg-purple">Closed</span>';
                        }
                        else if(data === 'workorder'){
                            var status = '<span class="label bg-green">Workorder</span>';
                        }
                        return status;
                    }
                    
                    return data;
                }
            },
            {data: "pivot.bid_amount",    name: 'pivot.bid_amount',  width:'10%' },

          
            {
                data: "service_date",  name: 'service_date', width:'10%',
                
                render: function ( data, type, row ) {
                   
                    if ( type === 'display' ) {
                        
                        var dateTimeSplit = data;

                        var dateSplit = dateTimeSplit.split(' ');
                        
                        var dateSplit = dateSplit[0].split('-');
                        
                        var currentDate = dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];
                      
                        return currentDate;
                    }
                    
                    return data;
                }
            },
            
            
    
            {data: "location",      name: 'location',   width:'20%' },   
            
            { 
                data: "client_id",      name: 'client_id',    
             
                render: function ( data, type, row ) {


                    if ( type === 'display' ) {
                        
                        var profileButton = '<a onclick="loadClientInfo('+data+')"  class="btn btn-block btn-primary btn-flat modal-opener-btn vendor_info" ><i class="fa fa-list-alt fa-1x"></i></a>';
                    
                        
                     return profileButton;

                    }

                    return data;
                }
            },
            
            { 
                data: 'id', name: 'id', width:'10%',
             
                render: function ( data, type, row ) {
                    if ( type === 'display' ) { 

                        var buttom = '<a href="/rfp/vendor/requestdetail/'+data+'" class="btn btn-block btn-primary btn-flat"><i class="fa  fa-pencil fa-1x"></i></a>';
                     
                        return buttom;

                    }

                    return data;
                },
            }    
            
        ] 
        
    });
    
var statusComboHtml='&nbsp; Status: <select id="status_list" class="selectpicker"><option @if ($status=="all") selected @endif value="0">All</option>';
    
    statusComboHtml+='<option data-content="<span class=\'label bg-blue\'>Invited</span>" @if ($status=="invited") selected @endif>Invited</option>';
    statusComboHtml+='<option data-content="<span class=\'label label-warning\'>Bid</span>" @if ($status=="bid") selected @endif>Bid</option>';
    statusComboHtml+='<option data-content="<span class=\'label label-success\'>Workorder</span>" @if ($status=="workorder") selected @endif>Workorder</option>';
    statusComboHtml+='<option data-content="<span class=\'label bg-purple\'>Closed</span>" @if ($status=="closed") selected @endif>Closed</option></select>';

    var typeComboHtml='Type: <select id="type_list" class="selectpicker"><option @if ($type=="all") selected @endif value="0">All</option>'
    typeComboHtml+='<option @if ($type=="sr") selected @endif>SR</option>';
    typeComboHtml+='<option @if ($type=="rfp") selected @endif>RFP</option></select>';

       
    $("div.rfp-toolbar").append('&nbsp; '+typeComboHtml+'&nbsp; '+statusComboHtml);
    

    $('#status_list').change(function(){
        
        $('#VendorBidsRequest').DataTable().ajax.reload();      
        
    });

    $('#type_list').change(function(){
        
        $('#VendorBidsRequest').DataTable().ajax.reload();      
        
    });

    $('#VendorBidsRequest').DataTable().ajax.reload();

    
});

function loadClientInfo(clientId){
    
     findClientInfo(clientId, "{{{ csrf_token() }}}");   
}
</script>
@endsection