@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Vendor <a href="{{ url('/vendor/create') }}" class="btn btn-primary btn-xs" title="Add New Vendor"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> Industry Id </th><th> Name </th><th> Phone </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($vendor as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->industry_id }}</td><td>{{ $item->name }}</td><td>{{ $item->phone }}</td>
                    <td>
                        <a href="{{ url('/vendor/' . $item->id) }}" class="btn btn-success btn-xs" title="View Vendor"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/vendor/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Vendor"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/vendor', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Vendor" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Vendor',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $vendor->render() !!} </div>
    </div>

</div>
@endsection
