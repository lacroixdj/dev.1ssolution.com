@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Bids request')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bids request
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/rfp/vendor/bidsrequest">Request / Bids</a></li>
        <li class="active">Bids Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- /.tabs -->
        <div class="col-md-12">
          <!-- Custom Tabs  -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="pull-left header"><i class="fa fa-th"></i></li>
              <li class="active"><a href="#tab_1-1" data-toggle="tab">Bid Request Information</a></li>
              <li><a href="#tab_2-1" data-toggle="tab">More Info.</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">
                  <div class="row">
                   
                    <div class="col-md-6">
                        <!-- /.workorder Layout -->  
                        <div class="box box-widget widget-user-2" style="border: 1px solid #d2d6de;">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header @if($rfp->status == 'sent') bg-light-blue @elseif($rfp->status == 'bid')  bg-yellow @elseif($rfp->status == 'workorder') bg-green @elseif($rfp->status == 'closed') bg-purple @else bg-gray @endif">
                              <div class="widget-user-image">
                                <img class="" src="{{ URL::asset('assets/img/workorder-icon.png') }}" alt="User Avatar">
                              </div>
                              <!-- /.widget-user-image -->
                              <h3 class="widget-user-username">@if($rfp->type == 'SR') Service Request #{{$rfp->id}} @else RFP #{{$rfp->id}} @endif </h3>
                              <h5 class="widget-user-desc">Status: {{ ucfirst($rfp->status) }}</h5>  
                            </div>

                            <div class="box-footer">      
                             <div class="row">
                                <br>
                                <div class="col-sm-6">
                                  <div class="">
                                    <p>Is Emergency: <b id="">@if($rfp->emergency == 1) Yes @else No @endif</b></p>
                                    <p>Description: <b id="">{{$rfp->description}}</b></p>
                                    <p>Service location: <b id="">{{$rfp->location}}</b></p> 
                                   </div>
                                  <!-- /.description-block -->
                                  </div>
                                    <!-- /.col -->
                                    <div class="col-sm-6 border-left">
                                      <div class="">
                                        <p><b>Service by:</b> {{date('m/d/Y', strtotime($rfp->service_date))}}</p>
                                        <p><b>Response by:</b> {{date('m/d/Y', strtotime($rfp->response_date))}}</p>         
                                        <p><b>Previous visit required?:</b>@if($rfp->visit == 1) Yes @else No @endif</p> 
                                        <!-- form start -->
                                    {!! Form::open( 
                                        [
                                          'method' => 'POST',
                                          'url' => ['/vendor/bidUpdate/'],
                                          'id' => 'bidStore',
                                          'data-parsley-validate' => '', 
                                          'data-parsley-trigger' => 'focusout',
                                          'role' => 'form'
                                        ]) 
                                     !!}   
                                        <div class="form-group">

                                        <div class="input-group" @if($rfp->pivot->bid_amount == 0.00)  @else  @endif>

                                        @if($rfp->status == 'workorder' || $rfp->status == 'closed')
                                          <h4 class=""><li class="fa fa-dollar"></li>  Bid Amount: {{$rfp->pivot->bid_amount}}</h4>
                                        @else
                                            <span  class="input-group-addon {{ $errors->has('bid') ? 'has-error' : ''}}" >$</span>       
                                                <input id="bidAmt" name="bid" type="number" step="any" class="form-control" value="{{$rfp->pivot->bid_amount}}" required>
                                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                                <input name="id" type="text" hidden="hidden" value="{{{$rfp->id}}}">
                                                <span class="input-group-btn">
                                                <button id="bidAmtBtn" type="button" class="btn btn-success btn-flat">Bid!</button>
                                            </span>
                                            {!! $errors->first('bid', '<p class="help-block">:message</p>') !!}
                                         @endif
                                        </div>
                                        </div>
                                    {!! Form::close() !!}            
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              <p><li class="fa fa-plus-square"></li> Details: <br> <b id="">{{$rfp->details}}</b></p>
                              <p><li class="fa fa-plus-square"></li> Documents Needed: <br> <b id="">{{$rfp->documents}}</b></p>
                              <strong><li class="fa fa-map-marker"></li> Service location</strong><br>
                              <div id="examples">
                                <a href="#"></a>
                              </div>
                              <br>
                              <input type="hidden" id="geocompleteRequestDetail">
                              <div  class="map_canvas_workorder"></div>
                              <div id="errorRequestDetailMap"></div>
                            </div>
                        </div>  
                        <!-- /.end workorder layout -->   
                    </div>
                     <div class="col-md-6">
                        <!-- Mensaje Area -->
                          @include('conversation.open', array(
                            'rfp_id' => $rfp->id,
                            'vendor_id' => $vendor->id,
                            'client_id' => $client->id
                          ))
                         <!-- End Mensaje Area -->    
                       
                    </div>
                </div><!-- /.end row -->  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-1">
               <div class="row">
                    <div class="col-md-6">
                        <!-- /.widget vendor details -->
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-primary">
                              <h3 class="widget-user-username" id="companyName">Client Name: {{$rfp->client->name}} </h3>
                            </div>
                            <div class="widget-user-image">
                              <img class="img-circle" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="User Avatar">
                            </div>
                            <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="">
                                   <!-- <p>Contact Name: <b id="contactName"></b></p> -->
                                    <p>Phone: <b id="contactPhone">{{$rfp->client->phone}}</b></p>
                                    <p>Email: <b id="contactEmail">{{$rfp->client->email}}</b></p>
                                    <p>Web: <b id="contactWeb">{{$rfp->client->url}}</b></p>
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-left">
                                  <div class="">
                                    <p>Address: <b id="companyAddress">{{$client_address->address}}, {{$client_address->address2}}</b></p>
                                    <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                                    <p>Zip: <b id="companyZip">{{$client_address->zip}}</b></p>
                                    <p>City: <b id="companyCity">{{$client_address->city->name}}</b></p>
                                    <p>State: <b id="companyState">{{$client_address->city->state->name}}</b></p>
                                    <p>Country: <b id="companyCountry">{{$client_address->city->state->country->name}}</b></p>                

                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->
                              <div class="row">
                                <div class="col-sm-12">
                                  <address>
                                    <strong><li class="fa fa-map-marker"></li> Client location</strong><br>
                                    <div id="examples">
                                        <a href="#"></a>
                                    </div>
                                    <br>
                                    <input type="hidden" id="geocompleteVendor">
                                    <div id="geocompleteVendor"  class="map_canvas_vendorlist"></div>
                                    <div id="errorVendorMap"></div>
                                  </address>
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->  
                            </div>
                          </div>
                        <!-- /.end widget vendor details --> 
                    </div>
                    <div class="col-md-6">
                        <h3 class="page-header">History</h3>
                            <ul class="timeline">
                            <!-- TODO: Incluir la logica para mostrar las fechas-->  
                    
                            <!-- timeline time label -->
                            <li class="time-label">
                                  @if(date('jS F, Y',strtotime($rfp->created_at)) == date('jS F, Y',strtotime($rfp->pivot->created_at)))   
                                  <span class="bg-primary">
                                       {{date('jS F, Y', strtotime($rfp->created_at))}}
                                  </span>
                                  @else 
                                   
                                  @endif
                            </li>
                            <!-- /.timeline-label -->

                                <!-- timeline item -->
                                <li>
                                    <!-- timeline icon -->
                                    <i class="fa fa-dollar bg-yellow"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->created_at))}}</span>
                                        <h3 class="timeline-header"><a href="#">Last Bid send</a></h3>
                                        <div class="timeline-body">
                                            Bid amount ${{$rfp->pivot->bid_amount}}
                                        </div>
                                    </div>
                                </li>

                                <!-- /.Rfp o Sr date created -->  
                                <li class="time-label">
                                   @if(date('jS F, Y',strtotime($rfp->created_at)) == date('jS F, Y',strtotime($rfp->pivot->created_at)))   

                                   @else 
                                   <span class="bg-primary">
                                       {{date('jS F, Y', strtotime($rfp->created_at))}}
                                   </span>
                                   @endif

                                </li>
                                <li>

                                    <i class="fa fa-plus bg-blue"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->created_at))}}</span>

                                        <h3 class="timeline-header"><a href="#">Client create the {{$rfp->type}}</a></h3>
                                        <div class="timeline-body">
                                            Service date to: {{date('m/d/Y', strtotime($rfp->service_date))}} <br>
                                            Response before: {{date('m/d/Y', strtotime($rfp->response_date))}}
                                        </div>
                                    </div>
                                </li>
                                <!-- /.Rfp o Sr date created -->  

                                <!-- END timeline item -->
                                <li>
                                  <i class="fa fa-clock-o bg-gray"></i>
                                </li>
                            </ul>
                    </div>
               </div>

              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.end Tabs -->    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.Modal Confirm -->  
    <div class="modal fade" id="modalConfrimBid">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Notice</h4>
          </div>
          
          <div class="modal-body">
              <p>You are about to upgrade this service with a amount of $ <b id="bidConfirmLabel"></b></p> 
              <p>Are you sure?</p>
          </div>
          
           <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                <button type="button" id="saveBid" class="btn btn-success btn-flat">Save changes</button>
           </div>
          
          
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  <!-- /.End Modal Confirm -->  
  
@endsection

<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>
    
XHRFormListener('#bidStore');

// Limpiar string de direccion 

{{--*/ $address =  str_replace(array("\r","\n"), "", $rfp->location) /*--}}

var address = "{{ $address }}";

    address=address.toString();

var addressRfp = address;

mapMarker(address, '#geocompleteRequestDetail', '.map_canvas_workorder', '#errorRequestDetailMap');

$(document).ready(function() {   
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
         mapMarker(address, '#geocompleteVendor', '.map_canvas_vendorlist', '#errorVendorMap');
    
    });
    
});

$('#bidAmtBtn').click(function() {
     
     var bidAmt = $('#bidAmt').val();
     
     $( "#bidConfirmLabel" ).html( bidAmt );
          
     $('#bidConfirmLabel').val();
     
     $('#modalConfrimBid').modal('show');
 });
    
    
 $('#saveBid').click(function() {
     
     console.log('submit');
     
     if ($('#bidStore').parsley().validate()){
                
        $("#bidStore").submit();
            
        $('#modalConfrimBid').modal('hide');
         
        //$('#bidAmtBtn').attr('disabled', true);
            
        //$('#bidAmt').attr('readonly', true); 
     
     }else{
            
       
     } 
     
 });
    

 $( "#bidStore" ).submit(function( event ) {
     
     event.preventDefault();
     
     if ($('#bidStore').parsley().validate()){
                
        $('#modalConfrimBid').modal('show');
    
     }else{
            
     }   
            
 });
    
//Fix de validaciones que rompen input -- Par utilizarlas cuando existan campos
//con la clase input-group-addon.
    
    $('form').parsley({
      successClass: 'has-success',
      errorClass: 'parsley-error',
      errorsContainer: function(el) {
        return el.$element.closest('.form-group');
      },
      classHandler: function(el) {
        return el.$element.closest('input');
      },
      errorsWrapper: '<span class="help-block"></span>',
      errorTemplate: "<span></span>"
    }); 

</script>

@endsection