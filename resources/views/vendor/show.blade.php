@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Vendor {{ $vendor->id }}
        <a href="{{ url('vendor/' . $vendor->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Vendor"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['vendor', $vendor->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Vendor',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $vendor->id }}</td>
                </tr>
                <tr><th> Industry Id </th><td> {{ $vendor->industry_id }} </td></tr><tr><th> Name </th><td> {{ $vendor->name }} </td></tr><tr><th> Phone </th><td> {{ $vendor->phone }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
