@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Create RFP')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        RFP
        <small>Create</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="/rfp/client/bidsrequest">Rfp</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
    
   <div class="content">    
    <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
           <!-- /.wizar -->
           <div class="fuelux">
                <div class="wizard" data-initialize="wizard" id="myWizard">
                    <div class="steps-container">
                        <ul class="steps">
                            <li data-step="1" data-name="campaign" class="active">
                                <span class="badge">1</span>New Request
                                <span class="chevron"></span>
                            </li>
                            <li data-step="2">
                                <span class="badge">2</span>Select Vendor(s)
                                <span class="chevron"></span>
                            </li>
                            <li data-step="3" data-name="template">
                                <span class="badge">3</span>Review your request
                                <span class="chevron"></span>
                            </li>
                        </ul>
                    </div>
                    <div class="actions">
                        <button type="button" class="btn btn-flat btn-primary btn-prev">
                            <span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
                        <button type="button" id='wizard_next_button' class="btn btn-flat btn-primary btn-next" data-last="Complete">Next
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </button>
                    </div>
                    <div class="step-content">
                        <div class="step-pane active" data-step="1">
                            <h2><li class="fa  fa-list-alt"></li> New Request</h4>
                            <hr>
                            <!-- /.Form 1 -->
                            {!! Form::open([

                                'method' => 'POST',
                                'url' => ['/rfp/create'],
                                'id' => 'rfp_create',
                                'data-parsley-validate' => '', 
                                'data-parsley-trigger' => '',
                                'role' => 'form'
                              ]) 

                            !!}
                                  <!-- industry -->
                                  <div class="form-group has-feedback {{ $errors->has('industry_id') ? 'has-error' : ''}}">
                                    <label for="industry_id">Industry:</label>
                                    {{ Form::select('industry_id', Array(''=>'Select your industry')+ $industries, null,
                                      [ 
                                        'id' => 'industry_id',
                                        'class' => 'form-control',
                                        'required' => 'required'
                                      ]) 
                                    }}
                                    <li class="fa fa-industry form-control-feedback"></li>
                                    {!! $errors->first('industry_id', '<p class="help-block">:message</p>') !!}
                                  </div>
                                  <!-- /industry -->


                                  <!-- description -->
                                  <div class="form-group has-feedback {{ $errors->has('description') ? 'has-error' : ''}}">
                                      <label for="industry_id">Description:</label>
                                       {!! Form::text('description', null, 
                                          [
                                            'id' => 'description', 
                                            'placeholder' => 'Enter description...', 
                                            'class' => 'form-control', 
                                            'required' => 'required'
                                          ]) 
                                        !!}
                                       <li class="fa fa-pencil form-control-feedback"></li> 
                                       {!! $errors->first('description', '<p class="help-block">:message</p>') !!}                                      
                                  </div>
                                  <!-- /description -->


                                  <!-- location -->
                                  <div class="form-group has-feedback {{ $errors->has('location') ? 'has-error' : ''}}">
                                      <label for="industry_id">Location:</label>
                                       {!! Form::text('location', $default_location, 
                                          [
                                            'id' => 'location', 
                                            'placeholder' => 'Enter location...', 
                                            'class' => 'form-control', 
                                            'required' => 'required'
                                          ]) 
                                        !!}
                                       <li class="fa fa-map-marker form-control-feedback"></li> 
                                       {!! $errors->first('location', '<p class="help-block">:message</p>') !!}                                      
                                  </div>
                                  <!-- /location -->

                                  
                                <!-- response by -->
                                  <div class="form-group {{ $errors->has('response_date') ? 'has-error' : ''}}">
                                    <label>Response needed by:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('response_date', null, 
                                          [
                                            'id' => 'response_date', 
                                            'class' => 'form-control pull-right', 
                                            'required' => 'required',
                                            
                                          ]) 
                                        !!}
                                    </div>  
                                    {!! $errors->first('response_date', '<p class="help-block">:message</p>') !!}
                                  </div>
                                  <!-- /response by -->


                                  <!-- service by -->
                                  <div class="form-group">
                                    <label>Service needed by:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('service_date', null, 
                                          [
                                            'id' => 'service_date', 
                                            'class' => 'form-control pull-right', 
                                            'required' => 'required', 

                                          ]) 
                                        !!}
                                    </div>  
                                    {!! $errors->first('service_date', '<p class="help-block">:message</p>') !!}
                                  </div>
                                  <!-- /service by -->

                                  <!-- Radio buttons row-->
                                  <div class="row">
                                    
                                    <!-- Service type -->  
                                    <div class="col-md-4">                                                                                                                
                                      <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                                          <label>Service type:</label>
                                            
                                            <label class="radio">
                                              {!! Form::radio('type', 'RFP', true,
                                                [
                                                  'id' => 'radio_rfp'

                                                ]) 
                                              !!} Request for Proposal
                                            </label>

                                            <label class="radio">
                                              {!! Form::radio('type', 'SR', false,
                                                [
                                                  'id' => 'radio_sr'

                                                ]
                                              ) !!} Service Request
                                            </label>
                                          {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                                      </div>                                                                            
                                    </div>
                                    <!-- /Service type -->
                                    
                                    <!-- Emergency -->  
                                    <div class="col-md-4">                                                                                                                
                                      <div class="form-group {{ $errors->has('emergency') ? 'has-error' : ''}}">
                                          <label>Is it an emergency?</label>
                                            
                                            <label class="radio">
                                              {!! Form::radio('emergency', '0', true) !!} No
                                            </label>

                                            <label class="radio">
                                              {!! Form::radio('emergency', '1') !!} Yes
                                            </label>
                                          {!! $errors->first('emergency', '<p class="help-block">:message</p>') !!}
                                      </div>                                                                            
                                    </div>
                                    <!-- /Emergency -->
                                    
                                    <!-- Visit required -->  
                                    <div class="col-md-4">                                                                                                                
                                      <div class="form-group {{ $errors->has('visit') ? 'has-error' : ''}}">
                                          <label>Previous visit required?</label>
                                            
                                            <label class="radio">
                                              {!! Form::radio('visit', '0', true) !!} No
                                            </label>

                                            <label class="radio">
                                              {!! Form::radio('visit', '1') !!} Yes
                                            </label>
                                          {!! $errors->first('visit', '<p class="help-block">:message</p>') !!}
                                      </div>                                                                            
                                    </div>
                                    <!-- /Visit required -->
                                </div>
                                <!-- /.end Radio btn-->

                                <!-- details -->
                                <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                                  <label>Details of service needed:</label>
                                  {!! Form::textarea('details', null, 
                                    [
                                      'id' => 'details', 
                                      'class' => 'form-control', 
                                      'required' => 'required',
                                      'rows' => '3',
                                      'placeholder' => 'Enter details...'
                                    ]) 
                                  !!}
                                  {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
                                </div>
                                <!-- details -->

                                <!-- documents -->
                                <div class="form-group {{ $errors->has('documents') ? 'has-error' : ''}}">
                                  <label>Documents:</label>
                                  {!! Form::textarea('documents', null, 
                                    [
                                      'id' => 'documents', 
                                      'class' => 'form-control', 
                                      'rows' => '3',
                                      'placeholder' => 'Enter a list of documents needed...'
                                    ]) 
                                  !!}
                                  {!! $errors->first('documents', '<p class="help-block">:message</p>') !!}
                                </div>
                               

                                 {!! Form::hidden('draft', null, 
                                    [
                                      'id' => 'draft'
                                    ]) 
                                  !!}
                                <!-- documents -->
                              <button type="button" id="btnNext1" class="btn btn-primary btn-block btn-flat">Next <span class="glyphicon glyphicon-arrow-right"></span></button>
                              
                              {!! Form::close() !!}                              
                              
                             </br>                                                            
                             <a id="draft-button" href="#" class="pull-right"><li class="fa  fa-save"></li> Save and exit.</a>
                              <!-- End Form 1 -->  
                        </div>
                        <!-- End Step 1 -->  



                        <div class="step-pane" data-step="2">

                            <h2><li class="fa fa-users"></li> Select Vendor(s)</h4>
                            <hr>
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group search">
                                          <div class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" id="mySearchTextField" placeholder="Type to search...">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <!-- metroarea -->  
                                    <div class="form-group has-feedback {{ $errors->has('metroarea') ? 'has-error' : ''}}">
                                      {{ Form::select(
                                          'metroarea', 
                                          Array(''=>'Select your metropolitan area') + $metroareas, '0',
                                            [
                                              'id' => 'metroarea',
                                              'class' => 'form-control',
                                              'required' => 'required'
                                            ]
                                          ) 
                                      }}
                                    <li class="fa fa-map-marker form-control-feedback"></li>
                                    {!! $errors->first('metroarea', '<p class="help-block">:message</p>') !!}
                                  </div>
                                  <!-- /metroarea -->
                                </div>

                               
                                
                            </div>

                            <div class="row">
                                
                                 <div class="col-md-6">
                                    <!-- services -->  
                                    <div id="serviceContainer" class="form-group has-feedback {{ $errors->has('services') ? 'has-error' : ''}}">
                                      {{ Form::select('services[]', [] , null,
                                        [
                                          'id' => 'services',
                                          'class' => 'form-control selectpicker select2',
                                          'multiple' => 'multiple',
                                          'data-actions-box' => 'true',
                                          'data-size' => '5',
                                          'data-none-selected-text' => 'Select your services',
                                          'data-live-search' => 'true',
                                          'required' => 'true'                                         
                                          
                                      ]) 
                                    }}  
                                    <li class="fa fa-plus-square form-control-feedback"></li>
                                    {!! $errors->first('services', '<p class="help-block">:message</p>') !!}
                                  </div>
                                  <!-- /services -->
                                </div>


                                <div class="col-md-6">
                                  <!-- attributes -->                 
                                  <div class="form-group has-feedback {{ $errors->has('attributess') ? 'has-error' : ''}}">
                                    {{ Form::select('attributess[]', [], null,
                                    [ 
                                      'id' => 'attributes',
                                      'class' => 'form-control selectpicker select2',
                                      'multiple' => 'multiple',
                                      'data-actions-box' => 'true',
                                      'data-size' => '5',
                                      'data-none-selected-text' => 'Select your attributes',
                                      'data-live-search' => 'true'
                                    ]) 
                                  }} 
                                  <li class="fa fa-plus-square form-control-feedback"></li>
                                  {!! $errors->first('attributess', '<p class="help-block">:message</p>') !!} 
                                </div>
                                <!-- /attributes -->                                
                                </div>




                                <div class="col-md-4 hidden">
                                    <!-- state -->  
                                    <div class="form-group has-feedback {{ $errors->has('state') ? 'has-error' : ''}}">
                                      {{ Form::select(
                                          'state', 
                                          Array(''=>'Select your state') + $states, '0',
                                            [
                                              'id' => 'state',
                                              'class' => 'form-control',
                                              'required' => 'required'
                                            ]
                                          ) 
                                      }}   
                                    <li class="fa fa-map-marker form-control-feedback"></li>
                                    {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
                                  </div>
                                  <!-- /state -->
                                </div>


                                <div class="col-md-4 hidden">
                                  <!-- city -->                 
                                  <div class="form-group has-feedback {{ $errors->has('city') ? 'has-error' : ''}}">
                                    {{ Form::select(
                                        'city', 
                                        Array(''=>'Select your city'), '0',
                                          [
                                            'id' => 'city',
                                            'class' => 'form-control',
                                            'required' => 'required'
                                          ] 
                                        ) 
                                    }}
                                  <li class="fa fa-map-marker form-control-feedback"></li>
                                  {!! $errors->first('city', '<p class="help-block">:message</p>') !!} 
                                </div>
                                <!-- /city -->                                
                                </div>
                                
                            </div>

                           <div class="row">
                                <div class="col-md-4">
                                  <h3>Vendors matches:</h3>
                                </div>                             

                                <div class="col-md-12">
                                   <div class="table-responsive">
                                        <table id="vendorsTable" class="table tale-striped table-bordered table-hover responsive no-wrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                  <th>Select</th>
                                                  <th>Profile</th>
                                                  <th>Company:</th>
                                                  <th>Email:</th>
                                                  <th>Phone:</th>
                                                  <th>Emergency:</th>
                                                  <th>Address:</th>
                                                  <th>Met. Area:</th>
                                                 <!--  <th>City:</th>
                                                  <th>State:</th>
                                                  <th>Url:</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>                                    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                             <div class="row" style="border-top: 3px solid #3c8dbc;">
                             
                              <div class="col-md-12">
                                
                                <div class="col-xs-6">
                                  <h3>Selected Vendors<span id='vendors_count'> (0)</span></h3>
                                </div>
                                
                                
                                
                                <div class="col-xs-6 text-right" style="margin-top:20px; margin-bottom:10px;">                                  
                                  <button id="clearVendorsSelection" type="button" class="btn btn-flat btn-primary">Clear selected</button>
                                </div>

                              </div>
                              <hr> 
                              <div class="col-md-12">
                                <!-- /.box-header -->
                                <div class="table-responsive">
                                    <table id="selectedVendorsTable2" class="table table-hover">
                                        <thead>
                                          <tr>
                                            <!-- <th>Selected</th> -->
                                            <th>Company:</th>
                                            <th>Email:</th>
                                            <th>Phone:</th>
                                            <th>Emergency:</th>
                                            <th>Address:</th>
                                            <th>Met. Area:</th>
                                            <!-- <th>City:</th>
                                            <th>State:</th>
                                            <th>Url:</th> -->
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr><td colspan="6" style="text-align: center; color:red">No vendors selected</td></tr>
                                        </tbody>                                   
                                    </table>
                                 </div>
                              </div>
                            
                            </div>
                             <div class="form-group">
                               <div class="col-xs-6 padding-r-5">
                                   <button id="btnBack" class="btn btn-primary btn-block btn-flat"><span  class="glyphicon glyphicon-arrow-left"></span>  Back</button>
                               </div>
                               <div class="col-xs-6 padding-l-5">
                                   <button id="btnNext2" class="btn btn-primary btn-block btn-flat">Next   <span  class="glyphicon glyphicon-arrow-right"></span></button>
                               </div>               
                            </div>

                             </br>
                             </br>                              
                             <a id="draft-button2" href="#" class="pull-right"><li class="fa  fa-save"></li> Save and exit.</a>
                             
                        </div><!-- /.End Step 2 -->

                        <div class="step-pane" data-step="3">

                            <h2><li class="fa fa-check-square-o"></li> Review your request</h4>
                            <hr>
                            <h3> Services Request Information </h3>
                            <!-- text input -->
                            <br>
                            <div class="form-group col-md-6">
                                <p><b>Industry:</b> <span id="industry_label"></span></p>

                            </div>
                            <div class="form-group col-md-6">
                                <p><b>Description:</b> <span id="description_label"></span></p>
                            </div>
                            <div class="form-group  col-md-6">
                               <p><b>Response needed by:</b> <span id="response_by_label"></span></p>                 
                            </div>
                            <div class="form-group  col-md-6">
                               <p><b>Service needed by:</b> <span id="service_by_label"></span></p>      
                            </div>
                            <div class="form-group  col-md-12">
                               <p><b>Location:</b> <span id="location_label"></span></p>   
                            </div>

                            <!-- /.Radio btn-->
                            <div class="">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p><b>Service type:</b> <span id="service_type_label"></span></p> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p><b>Service emergency:</b> <span id="emergency_label"></span></p>   
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                         <p><b>Site visit required:</b> <span id="visit_label"></span></p> 
                                    </div>
                                </div>
                            </div>
                            <!-- /.end Radio btn-->
                            <div class="col-md-12">
                                <!-- textarea -->
                            
                                <div class="form-group">
                                  <label>Detail of service needed</label>
                                  <p><span id="details_label"></span></p>
                                </div>
                            
                                <div class="form-group">
                                  <label>Documents needed</label>
                                  <p><span id="documents_label"></span></p>                                  
                                </div>
                            
                            </div>
                            
                            <!-- /.Vendors selected -->

                            <h3>Selected Vendors</h3>
                            <div class="col-md-12">
                                <!-- /.box-header -->
                                <div class="table-responsive">
                                    <table id="selectedVendorsTable" class="table table-hover">
                                        <thead>
                                          <tr>
                                            <!-- <th>Selected</th> -->
                                            <th>Company:</th>
                                            <th>Email:</th>
                                            <th>Phone:</th>
                                            <th>Emergency:</th>
                                            <th>Address:</th>
                                            <th>Met. Area:</th>
                                            <!-- <th>City:</th>
                                            <th>State:</th>
                                            <th>Url:</th> -->
                                          </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <!-- /.End vendors Selected -->    
                            <div class="form-group">
                                <div class="col-xs-6 padding-r-5">
                                    <button id="btnBack2" class="btn btn-primary btn-block btn-flat"><span  class="glyphicon glyphicon-arrow-left"></span>  Back</button>
                                </div>
                                <div class="col-xs-6 padding-l-5">
                                      <button id="btnComplete" class="btn btn-primary btn-block btn-flat">Complete</button>
                                </div>       
                            </div>
                        </div><!-- /.End Step 3 -->
                    </div>
                </div>
            </div>
            <!-- /.end wizard -->    
          </div>
           
        </div>
    </div>
   </div>     
   
  </div>
  <!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>

var array_vendors_listed = new Array();

var array_vendors_selected = new Array();

$(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
});

$(document).ready(function() { 


    $('#response_date').datepicker({
      autoclose: true,
      todayHighlight: true,
      format: 'mm-dd-yyyy',
      startDate: '+0d',
      todayBtn: 'linked',
      enableOnReadOnly: true,
      forceParse: true,
      showOnFocus: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#service_date').datepicker('setStartDate', minDate);
    });


    $("#response_date").on('keydown', function () {
        return false;
    });
    
    $('#service_date').datepicker({
      autoclose: true,
      format: 'mm-dd-yyyy',
      startDate: '+0d',
      todayBtn: 'linked',
      forceParse: true,
      showOnFocus: true
    });
    
    $("#service_date").datepicker().on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#response_date').datepicker('setEndDate', minDate);
    });

    $("#service_date").on('keydown', function () {
        return false;
    });
    
    
//Fix de validaciones que rompen input -- Par utilizarlas cuando existan campos
//con la clase input-group-addon.
    
    $('form').parsley({
      successClass: 'has-success',
      errorClass: 'parsley-error',
      errorsContainer: function(el) {
        return el.$element.closest('.form-group');
      },
      classHandler: function(el) {
        return el.$element.closest('input');
      },
      errorsWrapper: '<span class="help-block"></span>',
      errorTemplate: "<span></span>"
    });    



    /*$('#rfp_create').parsley().on('form:error', function() {
      
      $('#myWizard').wizard('selectedItem', { step: 1 });

    });
*/
   
    XHRFormListener('#rfp_create');
    
    //Botones en el wizard
    $( "#btnBack" ).click(function( event ) {
        //  event.preventDefault();
        $('#myWizard').wizard('previous');
    
    });

    $( "#btnBack2" ).click(function( event ) {
          //event.preventDefault();
          
          $('#myWizard').wizard('previous');
    
    });
    
    //valida el estado 1 y pasa al siguiente estado desde los botones inferiores
    $( "#btnNext1" ).click(function( event ) {
        
        /*event.preventDefault();
        if ($('#rfp_create').parsley().validate() == true){
            $('#rfp_create').submit();
            $('#myWizard').wizard('next');
        }*/

        $('#myWizard').wizard('next');

    });


    //Valida el estado 2 y pasa al siguiente estado desde los botones inferiores
    $( "#btnNext2" ).click(function( event ) {        
        
        //event.preventDefault();
        //if(getSelectedVendors()) $('#myWizard').wizard('next');
        //if ($('#step2').parsley().validate()){
            //$('#myWizard').wizard('next');
        //}else{
        //}
        $('#myWizard').wizard('next');
    });


    $( "#btnComplete" ).click(function( event ) {        
    
        //event.preventDefault();
        //sendRequest();

        $('#myWizard').wizard('next');
        
    });



    $( "#draft-button" ).click(function( event ) {
          
          

          if ($('#rfp_create').parsley().validate() == true){

            $( "#draft" ).val('1');

            $('#rfp_create').submit();

          }
        
    });

    $( "#draft-button2" ).click(function( event ) {
          
         $( "#draft" ).val('1');         

         if(getSelectedVendors()) $( "#draft" ).val('1');

         else $( "#draft" ).val('0');            
    });

    
    var direction='next';


    $('#myWizard').on('changed.fu.wizard', function(e, data) {

        $( "#draft" ).val('0');     

        switch(data.step){

            case 1:

                //console.log('paso 2 a 1');

                direction='next';
            
            break;


            case 2:

                if(direction=='next') {
                
                    //console.log('paso 1 a 2');

                    if ($('#rfp_create').parsley().validate() == true) $('#rfp_create').submit();
                    
                    else $('#myWizard').wizard('previous');

                
                }else if(direction=='previous') {

                    //console.log('paso 3 a 2');

                }
            
            break;

            
            case 3:

                //console.log('paso 2 a 3');
                direction='previous';

                if(!getSelectedVendors()) $('#myWizard').wizard('previous');

        
            break;

        }

    });


    // evento wizard on complete valida paso 3
    $('#myWizard').on('finished.fu.wizard', function (evt, data) {     
       //console.log(data);
       sendRequest();

    });



    //Datatable vendorsSelectedTable
    $("#vendorsSelectedTable").DataTable();

    $( "#attributes" ).selectpicker('refresh');
    
    $( "#services" ).selectpicker('refresh');

    
    $('#services').parsley({
      successClass: 'has-success',
      errorClass: 'has-error',
      errorsContainer: function(el) {
        return el.$element.closest('.form-group');
      },
      classHandler: function(el) {
        return el.$element.closest('input');
      },
      errorsWrapper: '<span class="help-block"></span>',
      errorTemplate: "<span></span>"
    });


    var data_table_options = {

      responsive: true,
    
      processing: true,
    
      serverSide: true,
        
      deferLoading: 1,

      pageLength: 5,
  
      lengthMenu: [5, 10, 25, 50, 75, 100 ],


      ajax: {

        method: 'POST',

        url: '/rfp/vendors',

        dataType: 'json',

        encode: true,

        data: function(d) {

            d._token = "{{ csrf_token() }}";
            
            d.industry_id = $("#industry_id").val();

            d.services_ids = $("#services").val();

            d.attributes_ids = $("#attributes").val();
            
            d.metroarea_id = $("#metroarea").val();

          // d.state_id = $("#state").val();

          // d.city_id = $("#city").val();

            d.search_field = $("#mySearchTextField").val();

            

        }
      
      },

      columns: [
       
          { 
            data: 'id', 
            
            render: function ( data, type, row, index ) {
                

                

                if ( type === 'display' ) {
                  
                    //console.log($("input[name='type']:checked").val());
                    var indexRow = index['row'];
                    array_vendors_listed[indexRow] = row;

                    var checked="";
                    
                    try{

                      if(!empty(row.id) && ! empty(array_vendors_selected[(row.id)])){

                        if(array_vendors_selected[(row.id)].id == row.id){

                          checked=' checked="true" ';


                        }

                      }

                    } catch(e){

                      console.log(error);

                    }
                    


                    var controlType = ( $("input[name='type']:checked").val()=='SR' ) ? 'radio' : 'checkbox';

                    
                    //console.log (array_vendors_selected);

                                     
                    return '<input type="'+controlType+'" name="vendors_selected[]"  class="icheckbox_square-blue" value="'+data+'"   onclick="pickVendor(this,'+(indexRow.toString())+')" '+checked+' >';
                }
                
                return data;
            },
            
            className: "dt-body-center icheck"


          },

          { 
            data: 'id',
             
            render: function ( data, type, row ) {
                

                if ( type === 'display' ) {

                 var profileButton = '<div class="navbar-custom-menu">';
                 profileButton += '<ul class="list-inline table-actions">';
                 profileButton += '<li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">';                                          
                 profileButton += '<a class="modal-opener-btn"  onclick="loadVendorInfo('+data+')">';
                 profileButton += '<i class="fa fa-list-alt"></i><span ></span>';
                 profileButton += '</a></li></ul></div>';

                 return profileButton;

                }
                
                return data;
            },
           
            className: "dt-body-center icheck"


          },
         
          { data: 'name' },
         
          { data: 'email' },

          { data: 'phone' },
         
          { data: 'emergency',

            render: function ( data, type, row ) {
                
                var yes_no = ['No', 'Yes'];

                if ( type === 'display' ) {
                  return yes_no[data];
                }
                
                return data;
            } 





          },

          { data: 'address' },

          { data: 'metroarea' },

         /* { data: 'city' },

          { data: 'state' },

          { data: 'url' }*/
      
        ]
      
      };


  $("#vendorsTable").DataTable(data_table_options);
    
  $("#vendorsTable_filter").hide();

    $('#mySearchTextField').keyup(function(){

          var search_value = $(this).val();

          $("#vendorsTable").DataTable().search(search_value).draw() ;

    });
  
    $('#attributes').change(function(){

        //console.log('attributes');

        $("#vendorsTable").DataTable().ajax.reload();      
        
    });

    $('#services').change(function(){

        //console.log('services');

        $("#vendorsTable").DataTable().ajax.reload();      

    });

    $('#metroarea').change(function(){

        $("#vendorsTable").DataTable().ajax.reload();      

    });

    
    $('#city').change(function(){

        $("#vendorsTable").DataTable().ajax.reload();      

    });


    $('#industry_id').change(function(){
            
            var industry_id = $( this ).val();

            var attributes_combo = $('#attributes');

            var services_combo = $('#services');

            attributes_combo.find('option').remove();     
            
            services_combo.find('option').remove();

            attributes_combo.selectpicker('refresh');
            
            services_combo.selectpicker('refresh');

            array_vendors_listed = [];

            clearVendorSelection1();

            $("#vendorsTable").DataTable().ajax.reload();

            if (industry_id == 0){
                
                attributes_combo.attr( "disabled", "disabled" );
                
                services_combo.attr( "disabled", "disabled" );
                
                attributes_combo.selectpicker('refresh');
                
                services_combo.selectpicker('refresh');
                
                return;
            } 

            var backend_url = '{{ url('/json/industries') }}';
            
            var attributes_url = backend_url  + '/' + industry_id + '/attributes';  
            
            var services_url = backend_url  + '/' + industry_id + '/services';  

            $.ajax({

                method: "GET",

                url: attributes_url,

                dataType: 'json'

            }).done(function(data) {

                //console.log(data);                 
                attributes_combo.removeAttr('disabled');        

                $.each( data, function( key, value ) {                    

                    attributes_combo.append($("<option />").val(key).text(value));

                });
                
                attributes_combo.selectpicker('refresh');  

            });

            $.ajax({
                
                method: "GET",
                
                url: services_url,
                
                dataType: 'json'
            
            }).done(function(data) {
            
                //console.log(data);
            
                services_combo.removeAttr('disabled');
            
                $.each( data, function( key, value ) {
            
                    services_combo.append($("<option />").val(key).text(value));
            
                });

                services_combo.selectpicker('refresh');               

            });

        });


        $('#state').change(function(){

          //Getting elements
          var state_id = $(this).val();
          
          var cities_combo = $('#city');

          //Clearing combos
          cities_combo.find("option:gt(0)").remove();

          $("#vendorsTable").DataTable().ajax.reload();      

          //If no state selected then disable child
          if (state_id == 0){
          
            cities_combo.attr( "disabled", "disabled" );
          
            return;
          } 

          //Compose backend url to retrieve data from models
          
          var states_url = '{{ url('/json/states') }}';
          
          var cities_url = states_url +'/' + state_id + '/cities'; 

          //The ajax request
          $.ajax({
          
            method: "GET",
          
            url: cities_url,
          
            dataType: 'json'
          
          }).done(function(data) {
          
              //Enabling child combo           
              cities_combo.removeAttr('disabled');

              //Populating  state combo
              $.each( data, function( key, value ) {
          
                cities_combo.append($("<option />").val(key).text(value));
          
              });
          }); 

      });


      function getSelectedVendors(){

        var selected_vendors = new Array();
/*
        $.each($("input[name='vendors_selected[]']:checked"), function() {

          selected_vendors.push($(this).val());
      
        });*/

        try{

            if(!$('#services').parsley().isValid()){

              $('#services').parsley().validate();

              throw 'Error: You must select one Service at least';

          }


            if(empty(array_vendors_selected)) throw 'Error: You must select one Vendor at least';

            array_vendors_selected.forEach(function(element, index){     
       
              selected_vendors.push(index);        

            });

            submmitSelectedVendors(selected_vendors);

            return true;

        }catch(err){

            showErrorModal('Error', err, null);

            return false;

        }

      }  


      function submmitSelectedVendors(selected_vendors){  
          
        try {

          

          if(empty(selected_vendors)) throw 'Error: You must select one Vendor at least';

            //Mostrar mascara de loading

            $.ajax({

              method: 'POST',

              url: "{{ url('/rfp/vendors/attach') }}",

              data: {

                _token : "{{ csrf_token() }}",

                vendors_ids: selected_vendors,

                draft : $('#draft').val(),

                services_ids : $("#services").val()
                
              },

              dataType: 'json',

              encode: true,
              
              success: function(response) {

                // ocultar mascara de loading

                try{

                  
                  if(!empty(response.vendors_data)){

                     populateSelectedVendors(response.vendors_data);

                  }

                  

                  if(!empty(response.callback_func)){

                            showSuccessModal(response.title, response.message, response.text, response.callback_func);

                  } else {

                            showSuccessModal(response.title, response.message, response.text);
                        
                  }



                }catch(err){

                  //showErrorModal('Error', err, null);   
                }
                
              },
              
              error: function(response) {
                
                // ocultar mascara de loading
                
                var title = 'Error';
                
                var errors, message, errors_list = '';

                if(!empty(response.responseJSON)){

                  try{

                    if(!empty(response.responseJSON.error_message) ){

                      message = response.responseJSON.error_message;          

                    } else {

                      $.each(response.responseJSON, function(key, value) {
                        errors_list += '<li>' + key +': '+value+ '</li>'; 
                      });

                      message = 'Validation errors found';

                      errors=errors_list;
                    }

                  }catch(err){

                    message = err;

                  } 
                    
                } else {

                  errors = "";
      
                  message = response.status+' '+response.statusText;
                }

                showErrorModal(title, message, errors);
              }

            });

        }catch(err){
            
            showErrorModal('Error', err, null);
        }
      
      }    
});

function setRfpValues(){

    console.log('me estoy ejecutando setRfpValues');

    var industry_text = $("#industry_id option:selected").text();

    $('#industry_label').html(industry_text);

    $('#description_label').html(($('#description').val()));

    $('#location_label').html(($('#location').val()));
    
    $('#response_by_label').html(($('#response_date').val()));

    $('#service_by_label').html(($('#service_date').val()));

    $('#details_label').html(($('#details').val()));

    $('#documents_label').html(($('#documents').val()));

    var request_type = $("input[name='type']:checked").val();

    $('#service_type_label').html(request_type);

    var yes_no = ['No', 'Yes'];

    var emergency =  ($("input[name='emergency']:checked").val())*1;

    $('#emergency_label').html(yes_no[emergency]);

    var visit = ($("input[name='visit']:checked").val())*1;

    $('#visit_label').html(yes_no[visit]);
}

function setRfpValues2 () {

  setRfpValues();
  console.log('me estoy ejecutando setRfpValues2');
  //window.location.href = "{{ url('/home') }}";
  window.location.href = "{{ url('/rfp/client/bidsrequest') }}";

}

function redirectHome () {

  //window.location.href = "{{ url('/home') }}";
  window.location.href = "{{ url('/rfp/client/bidsrequest') }}";

}


function populateSelectedVendors(vendors_data) {

  $('#selectedVendorsTable tbody').remove();

  var htmlRows = '<tbody>';
  var yes_no = ['No', 'Yes'];


  $.each(vendors_data, function (i, item) {

    if(!empty(item)){ 
    
      htmlRows += '<tr>';
      //htmlRows += '<td><input type="checkbox" checked="checked" disabled="disabled"></td>';
      htmlRows +='<td>'+item.name+'</td>';
      htmlRows +='<td>'+item.email+'</td>';
      htmlRows +='<td>'+item.phone+'</td>';
      //htmlRows +='<td>'+item.emergency+'</td>';
      htmlRows +='<td>'+(yes_no[(item.emergency*1)])+'</td>';
      htmlRows +='<td>'+item.addresses[0].address+'</td>';
      htmlRows +='<td>'+item.metroarea.name+'</td>';
     /* htmlRows +='<td>'+item.addresses[0].city.name+'</td>';
      htmlRows +='<td>'+item.addresses[0].city.state.name+'</td>';
      htmlRows +='<td>'+item.url+'</td>';*/
      htmlRows += '</tr>';

    }  
  
  });

  htmlRows += '</tbody>';
  
  $('#selectedVendorsTable').append(htmlRows);

}

function sendRequest(selected_vendors){  
          
        try {


            //Mostrar mascara de loading

            $.ajax({

              method: 'POST',

              url: "{{ url('/rfp/send') }}",

              data: {

                _token : "{{ csrf_token() }}"

              },

              dataType: 'json',

              encode: true,
              
              success: function(response) {

                // ocultar mascara de loading

                try{


                    if(!empty(response.callback_func)){

                            showSuccessModal(response.title, response.message, response.text, response.callback_func);

                    } else {

                            showSuccessModal(response.title, response.message, response.text);
                        
                   }


                }catch(err){

                  showErrorModal('Error', err, null);   
                }
                
              },
              
              error: function(response) {
                
                // ocultar mascara de loading
                
                var title = 'Error';
                
                var errors, message, errors_list = '';

                if(!empty(response.responseJSON)){

                  try{

                    if(!empty(response.responseJSON.error_message) ){

                      message = response.responseJSON.error_message;          

                    } else {

                      $.each(response.responseJSON, function(key, value) {
                        errors_list += '<li>' + key +': '+value+ '</li>'; 
                      });

                      message = 'Validation errors found';

                      errors=errors_list;
                    }

                  }catch(err){

                    message = err;

                  } 
                    
                } else {

                  errors = "";
      
                  message = response.status+' '+response.statusText;
                }

                showErrorModal(title, message, errors);
              }

            });

        } catch(err){
            
            showErrorModal('Error', err, null);
        }
      
      }


function pickVendor(checkbox, index){

  if(empty(array_vendors_listed[index])) return;

  var vendor_id = array_vendors_listed[index].id;

  if(checkbox.type=='radio'){

        clearVendorSelection2();

  }
  

  if(checkbox.checked) {

/*    console.log('checked');
    
    console.log(vendor_id);
*/
     array_vendors_selected[vendor_id] =   array_vendors_listed[index];

  }else{

/*    console.log('unckecked');
    
    console.log(vendor_id);
*/
    
    var arr_copy = [];


    array_vendors_selected.forEach(function(element, index){
       
       
       if(index!=vendor_id) arr_copy[index] = element;


    });



    array_vendors_selected = arr_copy;


  }
  
    var count = 0;
    array_vendors_selected.forEach(function(element, index){       
       count++;
    });



  //console.log(array_vendors_selected);
  populateSelectedVendors2(array_vendors_selected, count);  


}


function populateSelectedVendors2(vendors_data, count) {

    var htmlRows = '<tbody>';
    var yes_no = ['No', 'Yes'];

    if(empty(count)) {

        count=0;
    
        htmlRows +='<tr><td colspan="6" style="text-align: center; color:red">No vendors selected</td></tr>';

    } else {

        vendors_data.forEach(function(item, index){

          if(!empty(item)){  

            htmlRows += '<tr>';
            //htmlRows += '<td><input type="checkbox" checked="checked" disabled="disabled"></td>';
            htmlRows +='<td>'+item.name+'</td>';
            htmlRows +='<td>'+item.email+'</td>';
            htmlRows +='<td>'+item.phone+'</td>';
            //htmlRows +='<td>'+item.emergency+'</td>';
            htmlRows +='<td>'+(yes_no[(item.emergency*1)])+'</td>';
            htmlRows +='<td>'+item.address+'</td>';
            htmlRows +='<td>'+item.metroarea+'</td>';
            /* htmlRows +='<td>'+item.city+'</td>';
            htmlRows +='<td>'+item.state+'</td>';
            htmlRows +='<td>'+item.url+'</td>';*/
            htmlRows += '</tr>';   

          }
  
        });
  }

  htmlRows += '</tbody>';

  $('#vendors_count').html(' ('+count+')');

  $('#selectedVendorsTable2 tbody').remove();
  
  $('#selectedVendorsTable2').append(htmlRows);

}





$('#radio_sr').on('ifChecked', function(event){
  
  $("#vendorsTable").DataTable().ajax.reload();      

  clearVendorSelection1();

});

$('#radio_rfp').on('ifChecked', function(event){

  $("#vendorsTable").DataTable().ajax.reload();      

  clearVendorSelection1();
  
});

$('#clearVendorsSelection').on('click', function(){

    var count = 0;
    array_vendors_selected = [];

    populateSelectedVendors2(array_vendors_selected, count);    

    $.each($("input[name='vendors_selected[]']:checked"), function() {

      $(this).attr('checked', false);
      
    });
  
});

function clearVendorSelection1(){

    var count = 0;
    array_vendors_selected = [];

    populateSelectedVendors2(array_vendors_selected, count);  

    $.each($("input[name='vendors_selected[]']:checked"), function() {

      $(this).attr('checked', false);
      
    });  

}

function clearVendorSelection2(){

    var count = 0;
    array_vendors_selected = [];

    populateSelectedVendors2(array_vendors_selected, count);    

}

function loadVendorInfo(vendorId){
    
     findVendorInfo(vendorId, "{{{ csrf_token() }}}"); 
    
}



</script>
@endsection
