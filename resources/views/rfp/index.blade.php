@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Rfp <a href="{{ url('/rfp/create') }}" class="btn btn-primary btn-xs" title="Add New Rfp"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> Client Id </th><th> User Id </th><th> Number </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($rfp as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->client_id }}</td><td>{{ $item->user_id }}</td><td>{{ $item->number }}</td>
                    <td>
                        <a href="{{ url('/rfp/' . $item->id) }}" class="btn btn-success btn-xs" title="View Rfp"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/rfp/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Rfp"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/rfp', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Rfp" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Rfp',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $rfp->render() !!} </div>
    </div>

</div>
@endsection
