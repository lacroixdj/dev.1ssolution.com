<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Workorder #{{$workorder->id}} | One Seamless Solution </title>
  
  @include('layouts.css')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
<!-- /.invoice content -->  
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Workorder #{{$workorder->id}}
            <small class="pull-right">Date: {{date('m/d/Y', strtotime($workorder->closed_date))}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>{{$client->name}}</strong><br>
            {{$client_address->address}}<br>
            {{$client_address->city->name}},
            {{$client_address->city->state->name}}.<br>
            {{$client_address->city->state->country->name}}.<br>

            Phone: {{$client->phone}}<br>
            Email: {{$client->email}}
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>{{$vendor->name}}</strong><br>
            {{$vendor_address->address}}<br>
            {{$vendor_address->city->name}},
            {{$vendor_address->city->state->name}}.<br>
            {{$vendor_address->city->state->country->name}}.<br>
            Phone: {{$vendor->phone}}<br>
            Email: {{$vendor->email}}
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #{{$workorder->close_id}}</b><br>
          <br>
<!--
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567
-->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Service Code</th>
              <th>Service Description</th>
              <th>Service Date</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{{$workorder->id}}</td>
              <td>{{$workorder->description}}</td>
              <td>{{date('m/d/Y', strtotime($workorder->service_date))}}</td>
              <td>${{$workorder->bid_amount}}</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</section>
<!-- /.End invoice content -->
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>