@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Workorders')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Workorders
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/workorder/client/view">Workorders</a></li>
        <li class="active"><a href="#">Workorder Detail</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- /.tabs -->
        <div class="col-md-12">
          <!-- Custom Tabs  -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="pull-left header"><i class="fa fa-th"></i></li>
              <li class="active"><a href="#tab_1-1" data-toggle="tab">Workorder Information</a></li>
              <li><a href="#tab_2-1" data-toggle="tab">More Info.</a></li>
              @if($workorder->status == 'open')
              
              @else  
                  <li><a href="#tab_3-1" data-toggle="tab">Invoice</a></li>
              @endif
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">
                  <div class="row">
                    
                    <div class="col-md-6">
                        <!-- /.workorder Layout -->  
                        <div class="box box-widget widget-user-2" style="border: 1px solid #d2d6de;">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header @if($workorder->status == 'open') bg-green @else bg-purple @endif">
                              <div class="widget-user-image">
                                <img class="" src="{{ URL::asset('assets/img/workorder-icon.png') }}" alt="User Avatar">
                              </div>
                              <!-- /.widget-user-image -->
                              <h3 class="widget-user-username">Workorder number:  {{$workorder->id}}</h3>
                              <h5 class="widget-user-desc">Status:@if($workorder->status == 'open') Open @else Closed @endif</h5>
                            </div>

                            <div class="box-footer">  
                             <div class="row">
                                <br>
                                <div class="col-sm-6">
                                  <div class="">
                                    <p>Is Emergency: <b id="">@if($workorder->emergency == 1) Yes @else No @endif</b></p>
                                    <p>Description: <b id="">{{$workorder->description}}</b></p>
                                    <p>Workorder service location: <b id="">{{$workorder->location}}</b></p>
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-left">
                                  <div class="">
                                    <p>Service Cost: <b id="">${{$workorder->bid_amount}}</b></p>
                                    <p>Service Date: <b id="">{{date('m/d/Y', strtotime($workorder->service_date))}}</b></p>
                                    <p>Visit Needed: <b id="">@if($workorder->visit == 1) Yes @else No @endif</b></p>
                                    @if($workorder->status == 'closed')  
                                    
                                    @else  
                                    
                                    <button id="closeWorkOrder" type="button" class="btn btn-block btn-success btn-flat">Close Workorder</button>
                                    
                                    @endif                   

                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              
                              <p><li class="fa fa-plus-square"></li> Details: <br> <b id="">{{$workorder->details}}</b></p>
                              <p><li class="fa fa-plus-square"></li> Documents Needed: <br> <b id="">{{$workorder->documents}}</b></p>
                              <strong><li class="fa fa-map-marker"></li> Service location</strong><br>
                              <div id="examples">
                                <a href="#"></a>
                              </div>
                              <br>
                              <input type="hidden" id="geocompleteWorkorder">
                              <div class="map_canvas_workorder"></div>
                              <div id="errorWorkorderMap"></div>
                            </div>
                        </div>  
                        <!-- /.end workorder layout -->   
                    </div>
                    <div class="col-md-6">
                        <!-- Mensaje Area -->
                         @include('conversation.open', array(
                            'rfp_id' => $rfp->id,
                            'workorder_id' => $workorder->id,
                            'vendor_id' => $vendor->id,
                            'client_id' => $client->id
                         ))  
                         <!-- End Mensaje Area -->                        
                    </div>
                </div><!-- /.end row -->  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-1">
               <div class="row">
                    <div class="col-md-6">
                        <!-- /.widget vendor details -->
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-primary">
                                <div class="col-sm-8">
                                    <h3 class="widget-user-username" id="companyName">{{$vendor->name}}</h3>
                                    <h4 class="widget-user-desc" id="companyIndustry">{{$vendor->industry->name}}</h4>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="pull-right">
                                        <input name="input-rate" class="pull-right rating rating-loading rating-disabled" data-show-clear="false" value="{{ $vendor->rate }}" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs">
                                    </div>
                                </div>
                            </div>
                            <div class="widget-user-image">
                              <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">
                            </div>
                            <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="">
                                   <!-- <p>Contact Name: <b id="contactName"></b></p> -->
                                    <p>Phone: <b id="contactPhone">{{$vendor->phone}}</b></p>
                                    <p>Email: <b id="contactEmail">{{$vendor->email}}</b></p>
                                    <p>Web: <b id="contactWeb">{{$vendor->url}}</b></p>
                                    <p>Metropolitan Area: <b id="CompanyMetroArea">{{$vendor->metroarea->name}}</b></p>
                                    <p>Attends Emergency: <b id="CompanyEmergencies">@if($vendor->emergency == 1) Yes @else No @endif</b></p>
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-left">
                                  <div class="">
                                    <p>Address: <b id="companyAddress">{{$vendor_address->address}}</b></p>
                                    <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                                    <p>Zip: <b id="companyZip">{{$vendor_address->zip}}</b></p>
                                    <p>City: <b id="companyCity">{{$vendor_address->city->name}}</b></p>
                                    <p>State: <b id="companyState">{{$vendor_address->city->state->name}}</b></p>
                                    <p>Country: <b id="companyCountry">{{$vendor_address->city->state->country->name}}</b></p>                   

                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->
                              <div class="row">
                                <div class="col-sm-12">
                                  <address>
                                    <strong><li class="fa fa-plus-square"></li> Services</strong>
                                    <p id="servicesList"><p>
                                                            <?php $sep=''; ?>
                                                               @foreach ($vendor_services as $service)
                                                                {{ $sep.$service }}
                                                                <?php $sep='- '; ?>
                                                            @endforeach</p>

                                    <strong><li class="fa fa-plus-square"></li> Atributes</strong>
                                    <p id="attributesList">
                                                    <p>
                                                            <?php $sep=''; ?>
                                                               @foreach ($vendor_attributes as $attributes)
                                                                {{ $sep.$attributes }}
                                                                <?php $sep='- '; ?>
                                                            @endforeach
                                                    </p>

                                    <strong><li class="fa fa-map-marker"></li> Vendor location</strong><br>
                                    <div id="examples">
                                        <a href="#"></a>
                                    </div>
                                    <br>
                                    <input type="hidden" id="geocompleteVendor">
                                    <div id="geocompleteVendor"  class="map_canvas_vendorlist"></div>
                                    <div id="errorVendorMap"></div>
                                  </address>
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->  
                            </div>
                          </div>
                        <!-- /.end widget vendor details --> 
                    </div>
                    <div class="col-md-6">
                        <h3 class="page-header">Workorder History</h3>
                        <ul class="timeline">
                        <!-- TODO: Incluir la logica para mostrar las fechas-->  
                        <!-- timeline time label -->
                        @if( $workorder->status == 'closed' || $workorder->status == 'open' )
                        <li class="time-label">
                            @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($rfp->created_at)))   
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($workorder->created_at))}}
                               </span>
                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($workorder->created_at))}}
                               </span>
                               @endif
                        </li>
                        @else
                        @endif
                        <!-- /.timeline-label -->

                            <!-- timeline item -->
                            
                        @if($workorder->status == 'open')
                        @else 
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-check bg-gray"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($workorder->updated_at))}}</span>
                                    <h3 class="timeline-header"><a href="#">Closed the workorder</a></h3>
                                    <div class="timeline-body">
                                        <!-- /.questions workorder closed -->
                                         <b>Did the Vendor arrive on time?</b><br>
                                         @if($workorder->question_1 == 'yes') Yes @else No @endif <br>

                                         <b>Job completed satisfactorily?</b><br>
                                         @if($workorder->question_2 == 'yes') Yes @else No @endif <br>
                                         <b>Service rate:</b> <br>
                                         {{-- $workorder->vendor_rate --}}
                                         @for ($i = 0; $i < $workorder->vendor_rate ; $i++)
                                            &#9733;&nbsp; 
                                         @endfor
                                          
                                         <br>
                                         <b>Service details: </b><br>
                                         {{$workorder->close_details}} <br>
                                    </div>
                                </div>
                            </li>
                        @endif
                        <!-- timeline time label -->
                        <li class="time-label">
                               @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($workorder->created_at)))   

                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($workorder->created_at))}}
                               </span>
                               @endif
                        </li>
                        <!-- /.timeline-label -->

                            <!-- timeline item -->
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-plus bg-green"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($workorder->created_at))}}</span>
                                    <h3 class="timeline-header"><a href="#">Place the workorder</a></h3>
                                    <div class="timeline-body">
                                        Bid amount ${{$rfp->pivot->bid_amount}},<br>
                                        To vendor: {{$vendor->name}}
                                    </div>
                                </div>
                            </li>

                        <!-- timeline time label -->
                        <li class="time-label">
                              @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($rfp->pivot->created_at)))   

                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($rfp->pivot->created_at))}}
                               </span>
                               @endif
                        </li>
                        <!-- /.timeline-label -->

                            <!-- timeline item -->
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-dollar bg-yellow"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->pivot->created_at))}}</span>
                                    <h3 class="timeline-header"><a href="#">Last bid recive</a></h3>
                                    <div class="timeline-body">
                                        Bid amount ${{$rfp->pivot->bid_amount}}
                                    </div>
                                </div>
                            </li>

                            <!-- /.Rfp o Sr date created -->  
                            <li class="time-label">
                               @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($rfp->created_at)))   

                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($rfp->created_at))}}
                               </span>
                               @endif

                            </li>
                            <li>

                                <i class="fa fa-plus bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->created_at))}}</span>

                                    <h3 class="timeline-header"><a href="#">Create the {{$rfp->type}}</a></h3>
                                    <div class="timeline-body">
                                        Service date to: {{date('m/d/Y', strtotime($rfp->service_date))}} <br>
                                        Response before: {{date('m/d/Y', strtotime($rfp->response_date))}}
                                    </div>
                                </div>
                            </li>
                            <!-- /.Rfp o Sr date created -->  

                            <!-- END timeline item -->
                            <li>
                              <i class="fa fa-clock-o bg-gray"></i>
                            </li>
                        </ul>
                    </div>
               </div>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3-1">
                <!-- /.invoice content -->  
                <section class="invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12">
                          <h2 class="page-header">
                            <i class="fa fa-globe"></i> Workorder #{{$workorder->id}}
                            <small class="pull-right">Date: {{date('m/d/Y', strtotime($workorder->closed_date))}}</small>
                          </h2>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          From
                          <address>
                            <strong>{{$client->name}}</strong><br>
                            {{$client_address->address}}<br>
                            {{$client_address->city->name}},
                            {{$client_address->city->state->name}}.<br>
                            {{$client_address->city->state->country->name}}.<br>

                            Phone: {{$client->phone}}<br>
                            Email: {{$client->email}}
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address>
                            <strong>{{$vendor->name}}</strong><br>
                            {{$vendor_address->address}}<br>
                            {{$vendor_address->city->name}},
                            {{$vendor_address->city->state->name}}.<br>
                            {{$vendor_address->city->state->country->name}}.<br>
                            Phone: {{$vendor->phone}}<br>
                            Email: {{$vendor->email}}
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Invoice #{{$workorder->close_id}}</b><br>
                          <br>
    <!--
                          <b>Order ID:</b> 4F3S8J<br>
                          <b>Payment Due:</b> 2/22/2014<br>
                          <b>Account:</b> 968-34567
    -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table-responsive">
                          <table class="table table-striped">
                            <thead>
                            <tr>
                              <th>Service Code</th>
                              <th>Service Description</th>
                              <th>Service Date</th>
                              <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                              <td>{{$workorder->id}}</td>
                              <td>{{$workorder->description}}</td>
                              <td>{{date('m/d/Y', strtotime($workorder->service_date))}}</td>
                              <td>${{$workorder->bid_amount}}</td>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
<!--
                        <div class="col-xs-6">
                          <p class="lead">Payment Methods:</p>
                          <img src="{{ URL::asset('assets/img/credit/visa.png') }}" alt="Visa">
                          <img src="{{ URL::asset('assets/img/credit/mastercard.png') }}" alt="Mastercard">
                          <img src="{{ URL::asset('assets/img/credit/american-express.png') }}" alt="American Express">
                          <img src="{{ URL::asset('assets/img/credit/paypal2.png') }}" alt="Paypal">

                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            Here the warning policy commission by different operators.
                          </p>
                        </div>
-->
                        <!-- /.col -->
                        <div class="col-xs-6 pull-right">
                          <p class="lead">Amount Due {{date('m/d/Y', strtotime($workorder->closed_date))}}</p>

                          <div class="table-responsive">
                            <table class="table">
                              <tbody><tr>
                                <th style="width:50%">Subtotal:</th>
                                <td>${{$workorder->bid_amount}}</td>
                              </tr>
                              <tr>
                                <th>Tax ({{$workorder->tax_amount}}%)</th>
                                <td>${{round($workorder->bid_amount * ($workorder->tax_amount/100), 2)}}</td>
                              </tr>
                              <tr>
                                <th>Total:</th>
                                
                                <td>${{round($workorder->bid_amount + ($workorder->bid_amount* ($workorder->tax_amount/100)), 2)}}</td>
                              </tr>
                            </tbody></table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                         
                          <button id="emailInvoice" type="button" class="btn btn-primary pull-right"><i class="fa fa-envelope"></i> Send invoice
                          </button>
                          
                          <a href="/workorder/invoice/{{$workorder->id}}"  style="margin-right: 5px;" target="_blank" class="btn  btn-primary pull-right"><i class="fa fa-print"></i> Print</a>
                          <br>

<!--
                          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                            <i class="fa fa-envelope"></i> Send email
                          </button>
-->
    
                        </div>
                      </div>
                </section>
                <!-- /.End invoice content -->  
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.end Tabs -->    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    
  </div>
  <!-- /.content-wrapper -->
  
    <!-- /.Modal Close Workorder -->  
    <div class="modal fade" id="modalCloseWorkorder">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Action to close workorder</h4>
          </div>
          
          <div class="modal-body">
              <p>To close the work order the seller must have made the service.<b id="bidConfirmLabel"></b></p> 
              <p>Are you sure? Once you updated so you can not modify.&hellip;</p>
              <!-- /.Workorder closed form -->  
               <!-- form start -->
                 {!! Form::open( 
                    [
                      'method' => 'POST',
                      'url' => ['/workorder/update'],
                      'id' => 'workorder_update',
                      'data-parsley-validate' => '', 
                      'data-parsley-trigger' => 'focusout',
                      'role' => 'form'
                    ]) 
                  !!}
                  <div class="box-body"> 
                        <div class="form-group {{ $errors->has('workorderId') ? 'has-error' : ''}}">
                          <label for="workorderId"><i class="fa fa-list-alt"></i> Workorder number</label>
                          <input type="text" name="workorderId" class="form-control" value="{{$workorder->id}}" readonly>
                           {!! $errors->first('workorderId', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('workorderResponse1') ? 'has-error' : ''}}">
                            <label for="question1"><i class="fa fa-plus-square"></i> Did the Vendor arrive on time?</label>
                            <select class="form-control" name="workorderResponse1" id="workorderResponses1" required>
                                <option value="">Select</option>
                            </select>
                            {!! $errors->first('workorderResponse1', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <div class="form-group {{ $errors->has('workorderResponse2') ? 'has-error' : ''}}">
                            <label for="question2"><i class="fa fa-plus-square"></i> Job completed satisfactorily?</label>
                            <select class="form-control" name="workorderResponse2" id="workorderResponses2" required>
                                <option value="">Select</option>
                            </select>
                            {!! $errors->first('workorderResponse2', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <div class="form-group {{ $errors->has('workorderResponse3') ? 'has-error' : ''}}">
                            <label for="question3"><i class="fa fa-plus-square"></i> Please rate this service </label>
                            
                            <input id="workorderResponses3" 
                            name="workorderResponse3" 
                            type="number" 
                            class="rating" 
                            class="form-control" 
                            required="" 
                            data-parsley-required
                            data-parsley-errors-container="#rateError">
                            
                            <!--
                            <select class="form-control" name="workorderResponse3" id="workorderResponses3" required>
                                <option value="">Select</option>
                            </select>
-->
                            <div id="rateError"></div>
                            {!! $errors->first('workorderResponse3', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('workorderDetail') ? 'has-error' : ''}}">
                            <label><i class="fa fa-plus-square"></i> Please share with us the reason for your rating and other feedback that will help us serve you better</label>
                            <textarea class="form-control" name="workorderDetail" rows="3" placeholder="Details here..." required></textarea>
                            {!! $errors->first('workorderDetail', '<p class="help-block">:message</p>') !!}
                        </div>
                  </div>
              <!-- /.End Workorder closed form -->  
          </div>
          
           <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                <button type="button" id="btnUpdate" class="btn btn-primary btn-flat">Close Workorder</button>
           </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.End Modal Close Workorder  -->  
    
    
    <!-- /.Modal Send Invoice -->  
    <div class="modal fade" id="modalEmailInvoice">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Send invoice by email </h4>
          </div>
          
          <div class="modal-body">
              <p>Want to make a notification of the bill to the email address of the vendor user.<b id="bidConfirmLabel"></b></p> 
              
              {!! Form::open( 
                    [
                      'method' => 'POST',
                      'url' => ['/workorder/update'],
                      'id' => 'sendInvoiceWorkorder',
                      'data-parsley-validate' => '', 
                      'data-parsley-trigger' => 'focusout',
                      'role' => 'form'
                    ]) 
              !!}
              
                  <!-- /.Input whith te vendor email  -->
                  <div class="form-group has-feedback"> 
                      <div class="input-group"> 
                           <span class="input-group-addon"><i class="fa fa-envelope"></i></span>       
                           <input type="email" class="form-control" value="{{ $vendor_user->email }}" required>
                       </div>
                  </div>
                  <!-- /input-group -->
              
              
              <br />
              
              <p>Or just send it to another email address.</p>
              <!-- /.input free to type the email address -->
              
<!--
                  <div class="input-group">
                        
                        <span class="input-group-addon">
                          <input type="radio" name="emailCheck">
                        </span>
                        
                        <input type="email" class="form-control">
                  
                  </div>
-->
                  <!-- /input-group -->
          
          </div>
          
           <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                <button type="button" id="btnUpdate" class="btn btn-primary btn-flat">Send!</button>
           </div>
        </div>
        {!! Form::close() !!}
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    
    <!-- /.End Modal Send Invoice -->  
  

@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>
    
{{--*/ $addressVendor =  str_replace(array("\r","\n"), "", $vendor_address->address) /*--}}

var address = "{{ $addressVendor }}";

{{--*/ $addressWorkorder =  str_replace(array("\r","\n"), "", $workorder->location) /*--}}

var addressWorkorder = "{{ $addressWorkorder }}";

//localizacion de Geocomplete fix para mapa que carga dentro del tab de more info.
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
    mapMarker(address, '#geocompleteVendor', '.map_canvas_vendorlist', '#errorVendorMap');
    
});
    

    
$('#closeWorkOrder').click(function() {
     
    $('#modalCloseWorkorder').modal('show');
});
    

//Send email invoice

$('#emailInvoice').click(function() {
     
    console.log('Open Email modal');
    
    $('#modalEmailInvoice').modal('show');
});

$('#sendInvoiceWorkorder').parsley({
      successClass: 'has-success',
      errorClass: 'parsley-error',
      errorsContainer: function(el) {
        return el.$element.closest('.form-group');
      },
      classHandler: function(el) {
        return el.$element.closest('input');
      },
      errorsWrapper: '<span class="help-block"></span>',
      errorTemplate: "<span></span>"
});    

//End send invoice email


$(document).ready(function() {   
        
    XHRFormListener('#workorder_update');
    
    mapMarker(addressWorkorder, '#geocompleteWorkorder', '.map_canvas_workorder', '#errorWorkorderMap');
    
    //Get the data for workorder close
    var category_url = '{{ url('json/categories/workorder_questions_responses') }}';
            $.ajax({
                method: "GET",
                url: category_url,
                dataType: 'json'
            }).done(function(data) {
                //Populating  Category combo
                $.each( data, function( key, value ) {                    
                    $('#workorderResponses1').append($("<option />").val(key).text(value));
                    $('#workorderResponses2').append($("<option />").val(key).text(value));
                    
                });
            });
    
    var category_url = '{{ url('json/categories/workorder_questions_rate') }}';
            $.ajax({
                method: "GET",
                url: category_url,
                dataType: 'json'
            }).done(function(data) {
                //Populating  Category combo
                $.each( data, function( key, value ) {                    
                    $('#workorderResponses3').append($("<option />").val(key).text(value));                    
                });
            });
    
    //Actions for submit workorder update
    
    $("#btnUpdate").on("click", function (e) {
            
            if($( "#workorderResponses3" ).val() === '0'){
                
                $('#workorderResponses3').parsley().removeError('required', {message: 'This rate is required', updateClass: true});

                $('#workorderResponses3').parsley().addError('required', {message: 'This rate is required', updateClass: true});  
            }

            
            if ($('#workorder_update').parsley().validate()){
                
                if($( "#workorderResponses3" ).val() === '0'){
                
                    $('#workorderResponses3').parsley().removeError('required', {message: 'This rate is required', updateClass: true});

                    $('#workorderResponses3').parsley().addError('required', {message: 'This rate is required', updateClass: true});  
                
                }else{
                    
                    $("#workorder_update").submit();
            
                    $('#modalCloseWorkorder').modal('hide');
                
                    $('#closeWorkOrder').attr('disabled', true); 
                    
                }
    
            }else{
            
            } 
            
    });
    
    $('#modalCloseWorkorder').on('shown.bs.modal', function() {

            $('#workorder_update').parsley();

            $(':input','#workorder_update')
                 .not(':button, :submit, :reset, :hidden, [readonly]')
                 .val('')
                 .removeAttr('checked')
                 .removeAttr('selected');

        });
    
    var status = '{{$workorder->status}}';
    
    if( status === 'closed'){
     
         $('#closeWorkOrder').attr('disabled', true); 
        
     }

        
});

//Bootstrap Star-Rate - with plugin options

$("#workorderResponses3").rating(
    {
        min:0, 
        max:5, 
        step:1, 
        size:'xs',
        step: 1,
        //starCaptions: {0: 'No rated', 1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
        starCaptions: {0: 'No rated', 1: 'Deficient', 2: 'Normal', 3: 'Good', 4: 'Very Good', 5: 'Excellent'},
        starCaptionClasses: {0: 'text', 1: 'label label-danger', 2: 'label label-warning', 3: 'label label-info', 4: 'label bg-blue', 5: 'label label-success'}
    });

</script>

@endsection