@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Workorders')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Workorders
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/workorder/vendor/view">Workorders</a></li>
        <li class="active"><a href="#">Workorder Detail</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- /.tabs -->
        <div class="col-md-12">
          <!-- Custom Tabs  -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="pull-left header"><i class="fa fa-th"></i></li>
              <li class="active"><a href="#tab_1-1" data-toggle="tab">Workorder Information</a></li>
              <li><a href="#tab_2-1" data-toggle="tab">More Info.</a></li>
              <li><a href="#tab_3-1" data-toggle="tab">Invoice</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">
                  <div class="row">
                   
                    <div class="col-md-6">
                        <!-- /.workorder Layout -->  
                        <div class="box box-widget widget-user-2" style="border: 1px solid #d2d6de;">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header @if($workorder->status == 'open') bg-green @else bg-purple @endif">
                              <div class="widget-user-image">
                                <img class="" src="{{ URL::asset('assets/img/workorder-icon.png') }}" alt="User Avatar">
                              </div>
                              <!-- /.widget-user-image -->
                              <h3 class="widget-user-username">Workorder number:  {{$workorder->id}}</h3>
                              <h5 class="widget-user-desc">Status:@if($workorder->status == 'open') Open @else Closed @endif</h5>
                            </div>

                            <div class="box-footer">  
                             <div class="row">
                                <br>
                                <div class="col-sm-6">
                                  <div class="">
                                    <p>Is Emergency: <b id="">@if($workorder->emergency == 1) Yes @else No @endif</b></p>
                                    <p>Description: <b id="">{{$workorder->description}}</b></p>
                                    <p>Workorder service location: <b id="">{{$workorder->location}}</b></p>
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-left">
                                  <div class="">
                                    <p>Service Cost: <b id="">${{$workorder->bid_amount}}</b></p>
                                    <p>Service Date: <b id="">{{date('m/d/Y', strtotime($workorder->service_date))}}</b></p>
                                    <p>Visit Needed: <b id="">@if($workorder->visit == 1) Yes @else No @endif</b></p>               
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              
                              <p><li class="fa fa-plus-square"></li> Details: <br> <b id="">{{$workorder->details}}</b></p>
                              <p><li class="fa fa-plus-square"></li> Documents Needed: <br> <b id="">{{$workorder->documents}}</b></p>
                              <strong><li class="fa fa-map-marker"></li> Service location</strong><br>
                              <div id="examples">
                                <a href="#"></a>
                              </div>
                              <br>
                              <input type="hidden" id="geocompleteWorkorder">
                              <div id="geocompleteWorkorder"  class="map_canvas_workorder"></div>
                              <div id="errorWorkorderMap"></div>
                            </div>
                        </div>  
                        <!-- /.end workorder layout -->   
                    </div>
                     <div class="col-md-6">
                        <!-- Mensaje Area -->
                         @include('conversation.open', array(
                            'rfp_id' => $rfp->id,
                            'workorder_id' => $workorder->id,
                            'vendor_id' => $vendor->id,
                            'client_id' => $client->id
                         ))  
                         <!-- End Mensaje Area -->   
                       
                    </div>
                </div><!-- /.end row -->  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-1">
               <div class="row">
                    <div class="col-md-6">
                        <!-- /.widget vendor details -->
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-primary">
                              <h3 class="widget-user-username" id="companyName">Client Name: {{$client->name}}</h3>
                            </div>
                            <div class="widget-user-image">
                              <img class="img-circle" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="User Avatar">
                            </div>
                            <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="">
                                   <!-- <p>Contact Name: <b id="contactName"></b></p> -->
                                    <p>Phone: <b id="contactPhone">{{$client->phone}}</b></p>
                                    <p>Email: <b id="contactEmail">{{$client->email}}</b></p>
                                    <p>Web: <b id="contactWeb">{{$client->url}}</b></p>
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-left">
                                  <div class="">
                                    <p>Address: <b id="companyAddress">{{$client_address->address}}</b></p>
                                    <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                                    <p>Zip: <b id="companyZip">{{$client_address->zip}}</b></p>
                                    <p>City: <b id="companyCity">{{$client_address->city->name}}</b></p>
                                    <p>State: <b id="companyState">{{$client_address->city->state->name}}</b></p>
                                    <p>Country: <b id="companyCountry">{{$client_address->city->state->country->name}}</b></p>                   

                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->
                              <div class="row">
                                <div class="col-sm-12">
                                  <address>
                                    <strong><li class="fa fa-map-marker"></li> Client location</strong><br>
                                    <div id="examples">
                                        <a href="#"></a>
                                    </div>
                                    <br>
                                    <input type="hidden" id="geocompleteVendor">
                                    <div id="geocompleteVendor"  class="map_canvas_vendorlist"></div>
                                    <div id="errorVendorMap"></div>
                                  </address>
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->  
                            </div>
                          </div>
                        <!-- /.end widget vendor details --> 
                    </div>
                    <div class="col-md-6">
                        <h3 class="page-header">Workorder History</h3>
                        <ul class="timeline">
                        <!-- timeline time label -->
                        @if( $workorder->status == 'closed' || $workorder->status == 'open' )
                        <li class="time-label">
                            @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($rfp->created_at)))   
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($workorder->created_at))}}
                               </span>
                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($workorder->created_at))}}
                               </span>
                               @endif
                        </li>
                        @else
                        @endif
                        <!-- /.timeline-label -->

                            <!-- timeline item -->
                            
                        @if($workorder->status == 'open')
                        @else 
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-check bg-gray"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($workorder->updated_at))}}</span>
                                    <h3 class="timeline-header"><a href="#">Client closed the workorder</a></h3>
                                    <div class="timeline-body">
                                        <!-- /.questions workorder closed -->
                                         <b>Did the Vendor arrive on time?</b><br>
                                         @if($workorder->question_1 == 'yes') Yes @else No @endif <br>

                                         <b>Job completed satisfactorily?</b><br>
                                         @if($workorder->question_2 == 'yes') Yes @else No @endif <br>
                                         <b>Request rate:</b> <br>
                                         {{$workorder->vendor_rate}} <br>
                                         <b>Service details: </b><br>
                                         {{$workorder->close_details}} <br>
                                    </div>
                                </div>
                            </li>
                        @endif
                        <!-- timeline time label -->
                        <li class="time-label">
                               @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($workorder->created_at)))   

                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($workorder->created_at))}}
                               </span>
                               @endif
                        </li>
                        <!-- /.timeline-label -->

                            <!-- timeline item -->
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-plus bg-green"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($workorder->created_at))}}</span>
                                    <h3 class="timeline-header"><a href="#">Client creates the workorder</a></h3>
                                    <div class="timeline-body">
                                        Bid amount ${{$rfp->pivot->bid_amount}},<br>
                                        To vendor: {{$vendor->name}}
                                    </div>
                                </div>
                            </li>

                        <!-- timeline time label -->
                        <li class="time-label">
                              @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($rfp->pivot->created_at)))   

                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($rfp->pivot->created_at))}}
                               </span>
                               @endif
                        </li>
                        <!-- /.timeline-label -->

                            <!-- timeline item -->
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-dollar bg-yellow"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->pivot->created_at))}}</span>
                                    <h3 class="timeline-header"><a href="#">Last bid sent</a></h3>
                                    <div class="timeline-body">
                                        Bid amount ${{$rfp->pivot->bid_amount}}
                                    </div>
                                </div>
                            </li>

                            <!-- /.Rfp o Sr date created -->  
                            <li class="time-label">
                               @if(date('jS F, Y',strtotime($workorder->created_at)) == date('jS F, Y',strtotime($rfp->created_at)))   

                               @else 
                               <span class="bg-primary">
                                   {{date('jS F, Y', strtotime($rfp->created_at))}}
                               </span>
                               @endif

                            </li>
                            <li>

                                <i class="fa fa-plus bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->created_at))}}</span>

                                    <h3 class="timeline-header"><a href="#">Client creates the {{$rfp->type}}</a></h3>
                                    <div class="timeline-body">
                                        Service date to: {{date('m/d/Y', strtotime($rfp->service_date))}} <br>
                                        Response before: {{date('m/d/Y', strtotime($rfp->response_date))}}
                                    </div>
                                </div>
                            </li>
                            <!-- /.Rfp o Sr date created -->  

                            <!-- END timeline item -->
                            <li>
                              <i class="fa fa-clock-o bg-gray"></i>
                            </li>
                        </ul>
                    </div>
               </div>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3-1">
                <!-- /.invoice content -->  
                <section class="invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12">
                          <h2 class="page-header">
                            <i class="fa fa-globe"></i> Workorder #{{$workorder->id}}
                            <small class="pull-right">Date: {{date('m/d/Y', strtotime($workorder->closed_date))}}</small>
                          </h2>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          From
                          <address>
                            <strong>{{$client->name}}</strong><br>
                            {{$client_address->address}}<br>
                            {{$client_address->city->name}},
                            {{$client_address->city->state->name}}.<br>
                            {{$client_address->city->state->country->name}}.<br>

                            Phone: {{$client->phone}}<br>
                            Email: {{$client->email}}
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address>
                            <strong>{{$vendor->name}}</strong><br>
                            {{$vendor_address->address}}<br>
                            {{$vendor_address->city->name}},
                            {{$vendor_address->city->state->name}}.<br>
                            {{$vendor_address->city->state->country->name}}.<br>
                            Phone: {{$vendor->phone}}<br>
                            Email: {{$vendor->email}}
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Invoice #{{$workorder->close_id}}</b><br>
                          <br>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table-responsive">
                          <table class="table table-striped">
                            <thead>
                            <tr>
                              <th>Service Code</th>
                              <th>Service Description</th>
                              <th>Service Date</th>
                              <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                              <td>{{$workorder->id}}</td>
                              <td>{{$workorder->description}}</td>
                              <td>{{date('m/d/Y', strtotime($workorder->service_date))}}</td>
                              <td>${{$workorder->bid_amount}}</td>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
<!--
                        <div class="col-xs-6">
                          <p class="lead">Payment Methods:</p>
                          <img src="{{ URL::asset('assets/img/credit/visa.png') }}" alt="Visa">
                          <img src="{{ URL::asset('assets/img/credit/mastercard.png') }}" alt="Mastercard">
                          <img src="{{ URL::asset('assets/img/credit/american-express.png') }}" alt="American Express">
                          <img src="{{ URL::asset('assets/img/credit/paypal2.png') }}" alt="Paypal">

                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            Here the warning policy commission by different operators.
                          </p>
                        </div>
-->
                        <!-- /.col -->
                        <div class="col-xs-6 pull-right">
                          <p class="lead">Amount Due {{date('m/d/Y', strtotime($workorder->closed_date))}}</p>

                          <div class="table-responsive">
                            <table class="table">
                              <tbody><tr>
                                <th style="width:50%">Subtotal:</th>
                                <td>${{$workorder->bid_amount}}</td>
                              </tr>
                              <tr>
                                <th>Tax ({{$workorder->tax_amount}}%)</th>
                                <td>${{round($workorder->bid_amount * ($workorder->tax_amount/100), 2)}}</td>
                              </tr>
                              <tr>
                                <th>Total:</th>
                                
                                <td>${{round($workorder->bid_amount + ($workorder->bid_amount* ($workorder->tax_amount/100)), 2)}}</td>
                              </tr>
                            </tbody></table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                     
                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
<!--
                          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                          </button>
-->
                          <a href="/workorder/vendor/invoice/{{$workorder->id}}"  style="margin-right: 5px;" target="_blank" class="btn  btn-primary pull-right"><i class="fa fa-print"></i> Print</a>
                          <br>
    <!--
                          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                            <i class="fa fa-download"></i> Generate PDF
                          </button>
    -->
                        </div>
                      </div>
                </section>
                <!-- /.End invoice content -->  
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.end Tabs -->    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    
  </div>
  <!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>
    

// Limpiar string de direccion 

//FIXME: Cambiar el seeder que tome el workorde

{{--*/ $address =  str_replace(array("\r","\n"), "", $workorder->location) /*--}}

var address = "{{ $address }}";

    address=address.toString();

var addressWorkorder = address;
    
console.log("Direccion: "+address);
    
$(document).ready(function() {   
            
    //localizacion de Geocomplete fix para mapa que carga dentro del tab de more info.
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
        mapMarker(address, '#geocompleteVendor', '.map_canvas_vendorlist', '#errorVendorMap');
    
    });
    
    mapMarker(addressWorkorder, '#geocompleteWorkorder', '.map_canvas_workorder', '#errorWorkorderMap');

    
    //Get the data for workorder close
    var category_url = '{{ url('json/categories/workorder_questions_responses') }}';
            $.ajax({
                method: "GET",
                url: category_url,
                dataType: 'json'
            }).done(function(data) {
                //Populating  Category combo
                $.each( data, function( key, value ) {                    
                    $('#workorderResponses1').append($("<option />").val(key).text(value));
                    $('#workorderResponses2').append($("<option />").val(key).text(value));
                    
                });
            });
    
    var category_url = '{{ url('json/categories/workorder_questions_rate') }}';
            $.ajax({
                method: "GET",
                url: category_url,
                dataType: 'json'
            }).done(function(data) {
                //Populating  Category combo
                $.each( data, function( key, value ) {                    
                    $('#workorderResponses3').append($("<option />").val(key).text(value));                    
                });
            });

        
});

</script>

@endsection