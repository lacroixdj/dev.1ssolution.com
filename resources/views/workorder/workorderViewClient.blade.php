@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Workorders')

@section('content')

<style type="text/css">
    .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100px !important;
}  

.bootstrap-select > .dropdown-toggle{
     /* width: 60% !important;*/
      padding: 4px 10px !important;
      border-radius: 0 !important;
  }

.bootstrap-select.btn-group .dropdown-menu {
     /*min-width: 60% !important;*/
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Workorders
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">Workorders</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header" id="rfpArea">
              <h3 class="box-title"><i class="fa fa-list"></i> Workorder list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- TODO: Colocar el campo Actions Fixed -->
                <table id="workorder" class="table tale-striped table-bordered table-hover responsive no-wrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th data-priority="1">#</th>
                          <th data-priority="1">Description</th>
                          <th>Service Date</th>
                          <th>Service price</th>
                          <th>Workorder Status</th>
                          <th data-priority="1">Actions</th>
                        </tr>
                    </thead>
                </table>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    
  </div>
  <!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>
$(document).ready(function() { 

     workorderTable =  $('#workorder').DataTable( {
        processing: true,
        pageLength: 5,
        order: [[ 0, "desc" ]],
        bLengthChange: true,
        lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
        dom: '<"pull-left"f><"pull-left rfp-toolbar"><"pull-right"l>tip',
        serverSide: true,
        responsive: true,  
        deferLoading: 1,
        //ajax: "/json/datatable/listWorkorders",
        ajax: {

          method: 'POST',

          url: '/json/datatable/listWorkorders',

          data: function(d) {

              d._token = "{{ csrf_token() }}";
              
              d.status = (!empty($("#status_list").val()))? $("#status_list").val() : 0;

              //d.type = (!empty($("#type_list").val()))? $("#type_list").val() : 0;

          }
        },
         
        "columns":[
            {data: "id",            name: 'id',},
            {data: "description",   name: 'description',},
            {
                data: "service_date",  name: 'service_date',

                render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        var dateTimeSplit = data;

                        var dateSplit = dateTimeSplit.split('-');

                        var currentDate = dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];

                        return currentDate;
                    }

                    return data;
                }
            },
            {
                data: "bid_amount", name: 'bid_amount',

                render: function ( data, type, row ) {

                    if ( type === 'display' ) {
                        
                        var amount = '$ '+data;
                        
                        return amount;
                    }

                    return data;
                }
            },
            {
                            data: "status",        name: 'status',

                            render: function ( data, type, row ) {

                                if ( type === 'display' ) {
                                    
                                    var status = data;
                                    
                                    if(data === 'open'){
                                         status = '<span class="label bg-green">Open</span>';
                                    }
                                    else if(data === 'closed'){
                                         status = '<span class="label bg-purple">Closed</span>';
                                    }

                                    return status;
                                }

                                return data;
                            }
                        },
            {
                data: "id",  name: 'id', 

                 render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        var id = data;
                        
                            id = '<a href="/workorder/client/detail/'+data+'" class="btn btn-block btn-primary btn-flat"><i class="fa fa-eye fa-1x"></i></a>';
                    
                        return id;
                    }

                    return data;
                }                        
            },            
        ],
        "columnDefs": [
            { "width": "5%", "targets": [0,3,] },
            { "width": "40%", "targets": [1] },
            { "width": "10%", "targets": [4,2,5] },
            {"className": "text-center",  "searchable": true,  "orderable": true, "targets": [ 4 ]},
        ]     
    });

         
    var statusComboHtml='&nbsp; Status: <select id="status_list" class="selectpicker"><option @if ($status=="all") selected @endif value="0">All</option>';
    statusComboHtml+='<option data-content="<span class=\'label label-success\'>Open</span>" @if ($status=="open") selected @endif>Open</option>';

    statusComboHtml+='<option data-content="<span class=\'label bg-purple\'>Closed</span>" @if ($status=="closed") selected @endif>Closed</option></select>';

    /*var typeComboHtml='Type: <select id="type_list" class="selectpicker"><option @if ($type=="all") selected @endif value="0">All</option>'
    
    typeComboHtml+='<option @if ($type=="sr") selected @endif>SR</option>';
    
    typeComboHtml+='<option @if ($type=="rfp") selected @endif>RFP</option></select>';
    */
//    $("div.rfp-toolbar").append('&nbsp; '+typeComboHtml+'&nbsp; '+statusComboHtml);

    $("div.rfp-toolbar").append('&nbsp; '+statusComboHtml);

    $('#status_list').change(function(){
        
        workorderTable.ajax.reload();      
        
    });

    /*$('#type_list').change(function(){
        
        workorderTable.ajax.reload();      
        
    });*/

    

    workorderTable.ajax.reload();

    
});

</script>

@endsection