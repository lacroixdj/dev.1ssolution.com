@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Bids request')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bids request
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/rfp/client/bidsrequest">Request / Bids</a></li>
        <li class="active">Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">        
               <div class="col-xs-12">
                    <h2>
                        <i class="fa fa-globe"></i>@if($rfp->type == 'SR') Service Request #{{$rfp->id}} @else RFP #{{$rfp->id}} @endif
                    </h2>
                    <!-- TODO: Formato de fechas -->  
                    <div class="col-sm-3 col-xs-6">
                        <p><b>Emergency:</b>@if($rfp->emergency == 1) Yes @else No @endif</p>
                        <p><b>Visit:</b>@if($rfp->visit == 1) Yes @else No @endif</p>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <p><b>Service by:</b> {{date('m/d/Y', strtotime($rfp->service_date))}}</p>
                        <p><b>Response by:</b> {{date('m/d/Y', strtotime($rfp->response_date))}}</p>           
                    </div>
                    <div class="col-sm-5 col-xs-12">
                       
                    </div>
                </div>             
            </div>
            <!-- /.box-header -->
            <div class="box-body">                  
                <div class="col-md-6">
                    <h3 class="box-title"><i class="fa fa-list-alt"></i> {{$rfp->type}}
                    </h3>
                </div> 
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    
                </div> 
                <div class="clearfix"></div>
                
                <div class="invoice-info">
                    <!-- info col -->
                    <div class="col-sm-4 invoice-col">
                      From
                      <address>
                        <strong>{{$rfp->client->name}}</strong><br>
                        {{$rfp->location}}<br>
                        Phone: {{$rfp->client->phone}}<br>
                        Email: {{$rfp->client->email}}
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                     
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                       
                    </div>
                    <!-- /.col -->
                </div>
                <div class="invoice-info">
                    <!-- info col -->
                    <div class="col-sm-12 invoice-col">
                      <address>
                        <strong>Description:</strong><br>
                          <p>{{$rfp->description}}</p>
                      </address>
                    </div>
                    <!-- /.col --> 
                    <!-- Docs col -->
                    <div class="col-sm-12 invoice-col">
                      <address>
                        <strong>Documents:</strong><br>
                          <p>{{$rfp->documents}}</p>
                      </address>
                    </div>
                    <!-- /.col -->
                       <div class="col-md-12">
                                <!-- /.box-header -->
                            <div class="table-responsive"> 
                                <table id="selectedVendorsTable2" class="table table-hover">
                                    <thead>
                                      <tr>
                                        <!-- <th>Selected</th> -->
                                        <th>Company:</th>
                                        <th>Email:</th>
                                        <th>Phone:</th>
                                        <th>Emergency:</th>
                                        <th>Address:</th>
                                        <th>Met. Area:</th>
                                        <!-- <th>City:</th>
                                        <th>State:</th>
                                        <th>Url:</th> -->
                                      </tr>
                                    </thead>

                                    <tbody>
                                    
                                    @foreach ($vendors_data as $vendor)
                                      <tr>
                                        <td>{{ $vendor->name }}</td>
                                        <td>{{ $vendor->email }}</td>
                                        <td>{{ $vendor->phone }}</td>
                                        <td>{{ $yes_no[($vendor->emergency)] }}</td>
                                        <td>{{ $vendor->addresses()->first()->address }}</td>
                                        <td>{{ $vendor->metroarea->name }}</td>
                                      </tr>    
                                    @endforeach


                                    </tbody>                                   
                                </table>
                           </div>
                        </div>
                    <div class="col-sm-12 invoice-col">
                      <address>
                        <strong><li class="fa fa-map-marker"></li> Map location:</strong>
                        <br><br>
                        <input type="hidden" id="geocomplete">
                        <div id="geocomplete" class="map_canvas" ></div>
                        <div id="errorRfpInfoMap"></div>
                      </address>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.End invoice-info -->  
            </div>
            <!-- /.box -->   
        </div> 
        <!--/.col  -->
       </div>
       <!-- /.row -->  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
@endsection

<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<!-- /.google api -->  
<script>

    //chequear el string de la direccion 

    {{--*/ $addressRfp =  str_replace(array("\r","\n"), "", $rfp->location) /*--}}

    var address = "{{$addressRfp}}";
    address=address.toString();

    mapMarker("{{$addressRfp}}", '#geocomplete', '.map_canvas', '#errorRfpInfoMap');

</script>



@endsection