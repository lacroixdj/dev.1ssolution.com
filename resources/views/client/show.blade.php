@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Vendor {{ $client->id }}
        <a href="{{ url('client/' . $client->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Vendor"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['client', $client->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Client',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $client->id }}</td>
                </tr>
                <tr><th> Industry Id </th><td> {{ $client->industry_id }} </td></tr><tr><th> Name </th><td> {{ $client->name }} </td></tr><tr><th> Phone </th><td> {{ $client->phone }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
