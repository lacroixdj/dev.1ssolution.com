@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Client New RFP')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Request / Bids
        <small>Create Wizard</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Request / Bids</li>
      </ol>
    </section>
    
   <div class="content">    
    <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
           <!-- /.wizar -->
           <div class="fuelux">
                <div class="wizard" data-initialize="wizard" id="myWizard">
                    <div class="steps-container">
                        <ul class="steps">
                            <li data-step="1" data-name="campaign" class="active">
                                <span class="badge">1</span>New Request
                                <span class="chevron"></span>
                            </li>
                            <li data-step="2">
                                <span class="badge">2</span>Select Vendor(s)
                                <span class="chevron"></span>
                            </li>
                            <li data-step="3" data-name="template">
                                <span class="badge">3</span>Review your request
                                <span class="chevron"></span>
                            </li>
                        </ul>
                    </div>
                    <div class="actions">
                        <button type="button" class="btn btn-flat btn-primary btn-prev">
                            <span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
                        <button type="button" class="btn btn-flat btn-primary btn-next" data-last="Complete">Next
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </button>
                    </div>
                    <div class="step-content">
                        <div class="step-pane active" data-step="1">
                            <h2><li class="fa  fa-list-alt"></li> New Request</h4>
                            <hr>
                            <!-- /.Form 1 -->
                            <form role="form" data-parsley-validate data-parsley-trigger="focusout">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Industry</label>
                                   <select class="form-control select2" style="width: 100%;">
                                      <option selected="selected">Select...</option>
                                      <option>Option 1</option>
                                      <option>Option 2</option>
                                      <option>Option 3</option>
                                      <option>Option 4</option>
                                      <option>Option 5</option>
                                      <option>Option 6</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                  <label>Description</label>
                                  <input type="text" class="form-control" placeholder="Enter ..." required>
                                </div>

                                <div class="form-group">
                                  <label>Response by</label>
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" id="datepicker" required>
                                  </div>               
                                </div>

                                <div class="form-group">
                                  <label>Service by</label>
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" id="datepicker-service" required>
                                  </div>  
                                </div>

                                
                                <div class="form-group">
                                  <label>Location</label>
                                  <input type="text" class="form-control" placeholder="Enter ...">
                                </div>
                                <!-- /.Radio btn-->
                                <div class="row">
                                    <div class="col-md-4">
                                         <div class="form-group">
                                          <label>Service emergency?</label>
                                          <label class="radio">
                                            <input type="radio" name="optradio1">Yes
                                          </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="optradio1">No
                                          </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                          <label>Service type</label>
                                          <label class="radio">
                                            <input type="radio" name="optradio2">RFP
                                          </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="optradio2">Service Request
                                          </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                          <label>Site visit required?</label>
                                          <label class="radio">
                                            <input type="radio" name="optradio3">Yes
                                          </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="optradio3">No
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.end Radio btn-->

                                <!-- textarea -->
                                <div class="form-group">
                                  <label>Detail of service needed</label>
                                  <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                  <label>Documents needed</label>
                                  <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                
                                <button id="btnNext1" class="btn btn-primary btn-block btn-flat">Next   <span class="glyphicon glyphicon-arrow-right"></span></button>
                              </form>
                              <br>
                              <a href="#" class="pull-right"><li class="fa  fa-save"></li> Save draft.</a>
                            <!-- /.End Form 1 -->  
                        </div><!-- /.End Step 1 -->  

                        <div class="step-pane" data-step="2">

                            <h2><li class="fa fa-users"></li> Select Vendor(s)</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group search">
                                          <div class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                          </div>
                                          <input type="text" class="form-control pull-right" id="mySearchTextField" placeholder="Type to search...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Services...</option>
                                          <option>Option 1</option>
                                          <option>Option 2</option>
                                          <option>Option 3</option>
                                          <option>Option 4</option>
                                          <option>Option 5</option>
                                          <option>Option 6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Atributes...</option>
                                          <option>Option 1</option>
                                          <option>Option 2</option>
                                          <option>Option 3</option>
                                          <option>Option 4</option>
                                          <option>Option 5</option>
                                          <option>Option 6</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="vendorsTable" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                      <th>Select</th>
                                      <th>Company Name</th>
                                      <th>Industry</th>
                                      <th>Contact Name</th>
                                      <th>Emergencies</th>
                                      <th>Mtro. Area</th>
                                      <th>Address</th>
                                      <th>Address 2</th>
                                      <th>zip</th>
                                      <th>Country</th>
                                      <th>State</th>
                                      <th>City</th>
                                      <th>Phone</th>
                                      <th>email</th>
                                      <th>Web</th>
                                      <th>Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                      <td><input type="checkbox"></td>
                                      <td>Larson-Armstrong Group</td>
                                      <td>Electrical</td>
                                      <td>Angel Dominguez</td>
                                      <td>Yes</td>
                                      <td>Austin</td>
                                      <td>37743 Chelsie Village Apt. 677 South Gennaroton, DE 12024-4541</td>
                                      <td>Apt. 353</td>
                                      <td>29835</td>
                                      <td>Country</td>
                                      <td>State</td>
                                      <td>City</td>
                                      <td>(471) 486-8438</td>
                                      <td>roob.shany@graham.info</td>
                                      <td>www.ortiz.com</td>
                                      <td >
                                          <div class="navbar-custom-menu">
                                           <ul class="list-inline table-actions">
                                                <li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">
                                                    <a href="#" class="modal-opener-btn" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-list-alt"></i>
                                                      <span ></span>
                                                    </a>
                                                </li>
                                            </ul>       
                                          </div>
                                      </td>
                                    <tr>
                                      <td><input type="checkbox"></td>
                                      <td>Okuneva, Carroll and Rath Group</td>
                                      <td>Roofing</td>
                                      <td>Amalia Thompson</td>
                                      <td>No</td>
                                      <td>Austin</td>
                                      <td>53263 Shany Junction Marquardtport, FL 19649-3103</td>
                                      <td>Apt. 008</td>
                                      <td>17571-8189</td>
                                      <td>Country</td>
                                      <td>State</td>
                                      <td>City</td>
                                      <td>(695) 744-4798</td>
                                      <td>weissnat.lilla@lesch.com</td>
                                      <td>www.lindgren.org</td>
                                      <td >
                                          <div class="navbar-custom-menu">
                                           <ul class="list-inline table-actions">
                                                <li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-list-alt"></i>
                                                      <span ></span>
                                                    </a>
                                                </li>
                                            </ul>       
                                          </div>
                                      </td>
                                    <tr>
                                      <td><input type="checkbox"></td>
                                      <td>Ryan-Funk LLC</td>
                                      <td>Supplies</td>
                                      <td>Rose Hirthe</td>
                                      <td>Yes</td>
                                      <td>Austin</td>
                                      <td>2075 Providenci Ramp Susannamouth, WI 60570</td>
                                      <td>Suite 744</td>
                                      <td>37516-4836</td>
                                      <td>Country</td>
                                      <td>State</td>
                                      <td>City</td>
                                      <td>(209) 448-4107</td>
                                      <td>mmoore@greenfelder.com</td>
                                      <td>www.reilly.com</td>
                                      <td >
                                          <div class="navbar-custom-menu">
                                           <ul class="list-inline table-actions">
                                                <li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-list-alt"></i>
                                                      <span ></span>
                                                    </a>
                                                </li>
                                            </ul>       
                                          </div>
                                      </td>
                                    <tr>
                                      <td><input type="checkbox"></td>
                                      <td>McGlynn-Beier Group</td>
                                      <td>Vending</td>
                                      <td>Abner Gerlach</td>
                                      <td>No</td>
                                      <td>Austin</td>
                                      <td>52046 Hartmann Glen Apt. 087 West Schuylerchester, UT 91226-2917</td>
                                      <td>Apt. 969</td>
                                      <td>79226-9715</td>
                                      <td>Country</td>
                                      <td>State</td>
                                      <td>City</td>
                                      <td>(781) 498-1011</td>
                                      <td>kjoany23@wunsch.com</td>
                                      <td>www.haag.com</td>
                                      <td >
                                          <div class="navbar-custom-menu">
                                           <ul class="list-inline table-actions">
                                                <li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-list-alt"></i>
                                                      <span ></span>
                                                    </a>
                                                </li>
                                            </ul>       
                                          </div>
                                      </td>
                                    <tr>
                                      <td><input type="checkbox"></td>
                                      <td>Ortiz, Adams and Beer Inc</td>
                                      <td>Signs </td>
                                      <td>Josue	Wiza</td>
                                      <td>Yes</td>
                                      <td>Austin</td>
                                      <td>9962 Nicolas Dale Letaland, RI 95960</td>
                                      <td>Apt. 209</td>
                                      <td>zip</td>
                                      <td>Country</td>
                                      <td>State</td>
                                      <td>City</td>
                                      <td>(597) 342-4087</td>
                                      <td>kemmer.reymundo@barrows.com</td>
                                      <td>www.hodkiewicz.biz</td>
                                      <td >
                                          <div class="navbar-custom-menu">
                                           <ul class="list-inline table-actions">
                                                <li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-list-alt"></i>
                                                      <span ></span>
                                                    </a>
                                                </li>
                                            </ul>       
                                          </div>
                                      </td>
                                    <tr>
                                      <td><input type="checkbox"></td>
                                      <td>Ernser Group PLC</td>
                                      <td>Storage</td>
                                      <td>Aracely Christiansen</td>
                                      <td>Yes</td>
                                      <td>Austin</td>
                                      <td>6885 Brendon Mill Suite 158 Donnafurt, UT 01900</td>
                                      <td>Suite 833</td>
                                      <td>90016</td>
                                      <td>Country</td>
                                      <td>State</td>
                                      <td>City</td>
                                      <td>(701) 759-8986</td>
                                      <td>kemmer.reymundo@barrows.com</td>
                                      <td>www.huels.info</td>
                                      <td >
                                          <div class="navbar-custom-menu">
                                           <ul class="list-inline table-actions">
                                                <li class="group-table-item" data-toggle="tooltip" title="" data-original-title="More info...">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-list-alt"></i>
                                                      <span ></span>
                                                    </a>
                                                </li>
                                            </ul>       
                                          </div>
                                      </td>
                                    </tr>
                                    </tbody>
                                    
                                  </table>
                                </div>
                            </div>
                             <div class="form-group">
                               <div class="col-xs-6 padding-r-5">
                                   <button id="btnBack" class="btn btn-primary btn-block btn-flat"><span  class="glyphicon glyphicon-arrow-left"></span>  Back</button>
                               </div>
                               <div class="col-xs-6 padding-l-5">
                                   <button id="btnNext2" class="btn btn-primary btn-block btn-flat">Next   <span  class="glyphicon glyphicon-arrow-right"></span></button>
                               </div>               
                            </div>
                             
                        </div><!-- /.End Step 2 -->

                        <div class="step-pane" data-step="3">

                            <h2><li class="fa fa-check-square-o"></li> Review your request</h4>
                            <hr>
                            <h3> Services Request Information </h3>
                            <!-- text input -->
                            <div class="form-group col-md-6">
                                <p><b>Industry:</b> Industry Selected</p>

                            </div>
                            <div class="form-group col-md-6">
                                <p><b>Description:</b> Description text</p>
                            </div>
                            <div class="form-group  col-md-6">
                               <p><b>Response by:</b> mm/dd/yyyy</p>                 
                            </div>
                            <div class="form-group  col-md-6">
                               <p><b>Service by:</b> mm/dd/yyyy</p>      
                            </div>
                            <div class="form-group  col-md-12">
                               <p><b>Location:</b> Location info...</p>   
                            </div>

                            <!-- /.Radio btn-->
                            <div class="">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p><b>Service type:</b> Yes or not...</p> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p><b>Service emergency:</b> RFP or Request...</p>   
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                         <p><b>Site visit required:</b> Yes or not...</p> 
                                    </div>
                                </div>
                            </div>
                            <!-- /.end Radio btn-->
                            <div class="col-md-12">
                                <!-- textarea -->
                                <div class="form-group">
                                  <label>Detail of service needed</label>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos ipsam obcaecati modi sint eum molestias, minus nemo, laboriosam iste numquam aliquid eaque consequatur, at eligendi assumenda veniam. Itaque necessitatibus, nostrum.</p>
                                </div>
                                <div class="form-group">
                                  <label>Documents needed</label>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, dolores quod totam quia? Aspernatur deleniti perferendis ratione repudiandae rerum voluptatum quisquam officiis quia qui molestias! Amet adipisci est culpa autem?</p>
                                </div>
                            </div>
                            <!-- /.Vendors selected -->
                            <h3> Vendors Selected</h3>
                            <div class="col-md-12">
                                <!-- /.box-header -->
                                <table class="table table-hover">
                                    <tr>
                                        <th>Select</th>
                                        <th>Company Name</th>
                                        <th>Contact Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" checked="checked"></td>
                                        <td>Akona</td>
                                        <td>Angel Dominguez</td>
                                        <td>Montalban 1. 3ra Av. Qta Gladis.</td>
                                        <td>(0424)139.90.78</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" checked="checked"></td>
                                        <td>Akona</td>
                                        <td>Angel Dominguez</td>
                                        <td>Montalban 1. 3ra Av. Qta Gladis.</td>
                                        <td>(0424)139.90.78</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" checked="checked"></td>
                                        <td>Akona</td>
                                        <td>Angel Dominguez</td>
                                        <td>Montalban 1. 3ra Av. Qta Gladis.</td>
                                        <td>(0424)139.90.78</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" checked="checked"></td>
                                        <td>Akona</td>
                                        <td>Angel Dominguez</td>
                                        <td>Montalban 1. 3ra Av. Qta Gladis.</td>
                                        <td>(0424)139.90.78</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" checked="checked"></td>
                                        <td>Akona</td>
                                        <td>Angel Dominguez</td>
                                        <td>Montalban 1. 3ra Av. Qta Gladis.</td>
                                        <td>(0424)139.90.78</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" checked="checked"></td>
                                        <td>Akona</td>
                                        <td>Angel Dominguez</td>
                                        <td>Montalban 1. 3ra Av. Qta Gladis.</td>
                                        <td>(0424)139.90.78</td>
                                    </tr>
                                </table>

                            </div>
                            <!-- /.End vendors Selected -->    
                            <div class="form-group">
                                <div class="col-xs-6 padding-r-5">
                                    <button id="btnBack2" class="btn btn-primary btn-block btn-flat"><span  class="glyphicon glyphicon-arrow-left"></span>  Back</button>
                                </div>
                                <div class="col-xs-6 padding-l-5">
                                      <button type="submit" class="btn btn-primary btn-block btn-flat">Complete</button>
                                </div>       
                            </div>
                        </div><!-- /.End Step 3 -->
                    </div>
                </div>
            </div>
            <!-- /.end wizard -->    
          </div>
           
        </div>
    </div>
   </div>  
   
    <!-- /.Modal more vendor info -->  
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username" id="companyName"></h3>
              <h4 class="widget-user-desc" id="companyIndustry"></h4>
              <p>Attends Emergency: <b id="CompanyEmergencies"></b></p>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="{{ URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-6">
                  <div class="">
                    <p>Contact Name: <b id="contactName"></b></p>
                    <p>Phone: <b id="contactPhone"></b></p>
                    <p>Email: <b id="contactEmail"></b></p>
                    <p>Web: <b id="contactWeb"></b></p>
                    <p>Metropolitan Area: <b id="CompanyMetroArea"></b></p>
                    
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6 border-left">
                  <div class="">
                    <p>Address: <b id="companyAddress"></b></p>
                    <p>Address 2: <b id="companyAddress2"></b></p>
                    <p>Zip: <b id="companyZip"></b></p>
                    <p>Country: <b id="companyCountry"></b></p>
                    <p>State: <b id="companyState"></b></p>
                    <p>City: <b id="companyCity"></b></p>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-sm-12">
                  <address>
                    <strong><li class="fa fa-plus-square"></li> Services</strong>
                    <p>Service List</p>
                    <strong><li class="fa fa-plus-square"></li> Atributes</strong>
                    <p>Attributes List</p>
                    <strong><li class="fa fa-map-marker"></li> Map location</strong><br>
                    <div id="examples">
                        <a href="#"></a>
                    </div>
                    <br>
                    <input type="hidden" id="geocomplete">
                    <div id="geocomplete"  class="map_canvas_vendorlist"></div>
                  </address>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->  
            </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <!-- /.End Modal more vendor info -->    
     
  </div>
  <!-- /.content-wrapper -->
  
  
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIGiifLZnwuks-f8E9otZOjZR2RtaZxxo&libraries=places"></script>


<script>

//Goolge map autocomplete
// Docs
//http://ubilabs.github.io/geocomplete/
    
function mapMarker(address){
    
    
    $('#myModal').on('show.bs.modal', function (e) {

        setTimeout(function(){
             var options = {
                    map: ".map_canvas_vendorlist"
             };

             $("#geocomplete").geocomplete(options).bind("geocode:result", function(event, result){
                    console.log("Result: " + result.formatted_address);
             })
            .bind("geocode:error", function(event, status){
                    console.log("ERROR: " + status);
            })
            .bind("geocode:multiple", function(event, results){
                    console.log("Multiple: " + results.length + " results found");
            });

            $("#geocomplete").val(address).trigger("geocode");  

        }, 200);

    })
}    
    

$(document).ready(function() { 
    
    
    $('#datepicker').datepicker({
      autoclose: true,
      todayHighlight: true,
      startDate: '+0d',
      todayBtn: 'linked',
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#datepicker-service').datepicker('setStartDate', minDate);
    });
    
    $('#datepicker-service').datepicker({
      autoclose: true,
      startDate: '+0d',
      todayBtn: 'linked',
    });
    
    $("#datepicker-service").datepicker().on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#datepicker').datepicker('setEndDate', minDate);
    });
    
    
//Fix de validaciones que rompen input -- Par utilizarlas cuando existan campos
//con la clase input-group-addon.
    
    $('form').parsley({
      successClass: 'has-success',
      errorClass: 'parsley-error',
      errorsContainer: function(el) {
        return el.$element.closest('.form-group');
      },
      classHandler: function(el) {
        return el.$element.closest('input');
      },
      errorsWrapper: '<span class="help-block"></span>',
      errorTemplate: "<span></span>"
    });   
    
//End
    
    
    
    //Botones en el wizard
    $( "#btnBack" ).click(function( event ) {
        event.preventDefault();
        $('#myWizard').wizard('previous');
    });
    $( "#btnBack2" ).click(function( event ) {
        event.preventDefault();
        $('#myWizard').wizard('previous');
    }); 
    //valida el estado 1 y pasa al siguiente estado desde los botones inferiores
    $( "#btnNext1" ).click(function( event ) {
      event.preventDefault();
        //if ($('#step1').parsley().validate()){
            $('#myWizard').wizard('next');
        //}else{
        //}
    });
    //Valida el estado 2 y pasa al siguiente estado desde los botones inferiores
    $( "#btnNext2" ).click(function( event ) {
      event.preventDefault();
        //if ($('#step2').parsley().validate()){
            $('#myWizard').wizard('next');
        //}else{
        //}
    });
    
    
//DataTable vendorsTable
    oTable = $('#vendorsTable').DataTable({
            //responsive: true,
            scrollY:        "300px",
            scrollX:        true,
            scrollCollapse: true,
            //paging:         false,
            fixedColumns:   {
                leftColumns: 1,
                rightColumns: 1
            },
    });
 
    $('#vendorsTable').on( 'click', 'tr', function () {
       var data = oTable.row(this).data();
            //1.- pasar la direccion al map-canvas
            //Funcion que inicializa el mapa con la direccion del Vendor
            mapMarker(data[4]);

            //2.- Pasar mostrar los valores en los Id Correspondientes 
            //de la ficha del vendedor
            $( "#companyName" ).text(data[1]);
            $( "#companyIndustry" ).text(data[2]);
            $( "#contactName" ).text(data[3]);

            $( "#CompanyEmergencies" ).text(data[4]);
            $( "#CompanyMetroArea" ).text(data[5]);
            $( "#companyAddress" ).text(data[6]);
            $( "#companyAddress2" ).text(data[7]);
            $( "#companyZip" ).text(data[8]);

            $( "#companyCountry" ).text(data[9]);
            $( "#companyState" ).text(data[10]);
            $( "#companyCity" ).text(data[11]);

            $( "#contactPhone" ).text(data[12]);
            $( "#contactEmail" ).text(data[13]);
            $( "#contactWeb" ).text(data[14]);       
       //fin
    });    
        
//End Data in Modal
    
    
    $('#mySearchTextField').keyup(function(){
          oTable.search($(this).val()).draw();
    });

    //$("#vendorsTable").DataTable();
    $("#vendorsTable_filter").hide();
    
    //Datatable vendorsSelectedTable
    $("#vendorsSelectedTable").DataTable();
    
    
    
}); // End Document Ready
</script>
@endsection
