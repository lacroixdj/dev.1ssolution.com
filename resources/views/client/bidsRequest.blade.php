@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Bids request')

@section('content')
 <!-- Content Wrapper. Contains page content -->
<style type="text/css">
    .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100px !important;
}  

.bootstrap-select > .dropdown-toggle{
     /* width: 60% !important;*/
      padding: 4px 10px !important;
      border-radius: 0 !important;
  }

.bootstrap-select.btn-group .dropdown-menu {
     /*min-width: 60% !important;*/
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

</style>


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bids request
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">Requests / Bids</a></li>
      </ol>
      </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header" id="rfpArea">
              <h3 class="box-title"><i class="fa fa-list"></i>&nbsp; Requests list</h3>
            </div>
            <div class="pull-right margin-r-10">
                  <button type="button" id="reloadRfpTable" class="btn btn-primary btn-sm btn-flat"><li class="fa fa-refresh"></li>  Clear filter</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              {!! Form::hidden('h_rfp_id', null, ['id' => 'h_rfp_id']) !!}
              <!-- TODO: Poner responsive esta tabla -->
              <p>Click button to load associated vendors</p>  
                <table id="rfp" class="table tale-striped table-bordered table-hover responsive no-wrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th data-priority="1">#</th>
                          <th data-priority="1">Type</th>
                          <th data-priority="1">Description</th>
                          <th data-priority="1">Status</th>
                          <th>Industry</th>
                          <th>Serv. date</th>
                          <th>Resp. date</th>                          
                          <th data-priority="1">Vendors</th>
                          <th data-priority="1">Actions</th>
                        </tr>
                    </thead>
                </table>
              
              <hr />
              
              <div class="box-header" id="vendorsArea">
                  <h3 class="box-title"><i class="fa fa-users"></i> Associated Vendors</h3>
              </div>          
                  <table id="VendorBidsRequest" class="table tale-striped table-bordered table-hover responsive no-wrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th data-priority="1">#</th>
                          <th data-priority="1">Vendor Company</th>
                          <th data-priority="3">Bid Amount.</th>
                          <th>Emergency</th>
                          <th>Address</th>
                          <th>Metro. Area</th>
                          <th data-priority="2">Bid Status</th>
                          <th data-priority="1">Vendor Profile</th>
                          <th>Url</th> 
                          <th>Zip</th> 
                          <th>City</th> 
                          <th>State</th> 
                          <th>Atributes</th>
                          <th>Services</th>
                          <th>Industry</th>
                          <th data-priority="1">Action's</th>
                          <th>id User</th>
                        </tr>
                    </thead>
                  </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content --> 
    
  </div>
  <!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>


var vendorsTable, rfpTable;
    
$(document).ready(function() { 

 rfpTable = $('#rfp').DataTable({
                    processing: true,
                    pageLength: 5,
                    order: [[ 0, "desc" ]],
                    bLengthChange: true,
                    lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
                    dom: '<"pull-left"f><"pull-left rfp-toolbar"><"pull-right"l>tip',
                    serverSide: true,
                    responsive: true,  
                    deferLoading: 1,                  
                    ajax: {

                      method: 'POST',

                      url: '/json/datatable/listRfpClient',

                      data: function(d) {

                          d._token = "{{ csrf_token() }}";
                          
                          d.status = (!empty($("#status_list").val()))? $("#status_list").val() : 0;

                          d.type = (!empty($("#type_list").val()))? $("#type_list").val() : 0;

                          d.rfp_id = (!empty($("#h_rfp_id").val()))? $("#h_rfp_id").val() : 0;

                      }
                    },

                    columns:[
                        
                        {data: "id",  name: 'id', sType: "numeric", type: "numeric", width: "5%", searchable:true },
                        
                        {data: "type", name: 'type',  width: "5%", searchable:true },
                        
                        {data: "description",   name: 'description' , width: "25%", searchable:true,

                          render: function ( data, type, row ) {

                                if ( type === 'display' ) {

                                     str = smartTruncate(data, 35);

                                    return str;
                                }

                                return data;
                            }
                        },

                        { data: "status", name: 'status', width: "10%", searchable:true,

                            render: function ( data, type, row ) {

                                if ( type === 'display' ) {
                                    
                                    var status = data;
                                    
                                    if(data === 'sent'){
                                         status = '<span class="label bg-blue">Sent</span>';
                                    }
                                    else if(data === 'bid'){
                                         status = '<span class="label bg-yellow">Bid</span>';
                                    }
                                    else if(data === 'open'){
                                         status = '<span class="label bg-aqua">Open</span>';
                                    }
                                    else if(data === 'workorder'){
                                         status = '<span class="label bg-green">Workorder</span>';
                                    }
                                    else if(data === 'closed'){
                                         status = '<span class="label bg-purple">Closed</span>';
                                    }

                                    return status;
                                }

                                return data;
                            }
                        },
                        
                        {data: "industry.name",  name: 'industry.name', width: "15%" , searchable:true },
                        
                        {data: "service_date",  name: 'service_date', width: "10%", type: 'date', 

                            render: function ( data, type, row ) {

                                if ( type === 'display' ) {

                                     var dateTimeSplit = data;

                                     var dateSplit = dateTimeSplit.split(' ');

                                     var dateSplit = dateSplit[0].split('-');

                                     var currentDate = dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];

                                    return currentDate;
                                }

                                return data;
                            }
                        },
                        
                        {data: "response_date",  name: 'response_date', width: "10%", 

                            render: function ( data, type, row ) {

                                if ( type === 'display' ) {

                                    var dateTimeSplit = data;

                                    var dateSplit = dateTimeSplit.split(' ');

                                    var dateSplit = dateSplit[0].split('-');

                                    var currentDate = dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];

                                    return currentDate;
                                }

                                return data;
                            }
                        },
                        
                        
                                                
                        { data: 'id', name: 'rfps.id', width: "10%", 
                            render: function ( data, type, row ) {
                                if ( type === 'display' ) { 
                                    
                                 var acctionsButtons = '<a id="a" href="#vendorsArea" class="btn btn-block btn-primary btn-flat" onclick="loadVendors('+data+')"><i class="fa fa-users"></i>&nbsp; <span class="badge">'+row['vendors_responses_count']+'/'+row['vendors_count']+'</span></a>';    
                                    
                                 return acctionsButtons;

                                }

                                return data;
                            },
                        
                        },
                            
                        { data: 'id', name: 'rfps.id', width: "10%",

                             render: function ( data, type, row ) {

                                if ( type === 'display' ) {

                                    var id = data;
                                    
                                        id = '<a href="/rfp/client/detail/'+data+'" class="btn btn-block btn-primary btn-flat"><i class="fa fa-eye fa-1x"></i></a>';
                                        
                                        if(row['status']=='open'){

                                            //console.log(row['total_vendor']);
                                            if(row['vendors_count']=='0'){

                                              id = '<a class="btn btn-block btn-primary btn-flat" role="button" data-toggle="popover" data-title="No vendors assigned." data-html="true" data-content="Do you wish to assign vendor(s) now? <br><br> <p class=\'text-right\' > <a href=\'/rfp/client/edit/'+data+'/2\'>Yes, assign vendors</a> | <a href=\'/rfp/client/edit/'+data+'/1\'>No, edit fields</a> </p>" data-placement="left" tabindex="'+data+'" ><i class="fa fa-pencil fa-1x"></i></a>';

                                            }else{

                                              id = '<a href="/rfp/client/edit/'+data+'/1" class="btn btn-block btn-primary btn-flat"><i class="fa fa-pencil fa-1x"></i></a>';
                                            }




                                        }

                                    return id;
                                }

                                return data;
                            }                        
                        },                          

            
                    ],
                    
                });
  
  $('#rfp').on( 'draw.dt', function () {

    $('[data-toggle="popover"]').popover({placement:'left'});   
    //console.log( 'Redraw occurred at: '+new Date().getTime() );
  
  } );


 
rfpTable.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
    //console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );

      $('[data-toggle="popover"]').popover({placement:'top'});   
} );

    var statusComboHtml='&nbsp; Status: <select id="status_list" class="selectpicker"><option @if ($status=="all") selected @endif value="0">All</option>';
    statusComboHtml+='<option data-content="<span class=\'label bg-aqua\'>Open</span>" @if ($status=="open") selected @endif>Open</option>';
    statusComboHtml+='<option data-content="<span class=\'label bg-blue\'>Sent</span>" @if ($status=="sent") selected @endif>Sent</option>';
    statusComboHtml+='<option data-content="<span class=\'label label-warning\'>Bid</span>" @if ($status=="bid") selected @endif>Bid</option>';
    statusComboHtml+='<option data-content="<span class=\'label label-success\'>Workorder</span>" @if ($status=="workorder") selected @endif>Workorder</option>';
    statusComboHtml+='<option data-content="<span class=\'label bg-purple\'>Closed</span>" @if ($status=="closed") selected @endif>Closed</option></select>';

    var typeComboHtml='Type: <select id="type_list" class="selectpicker"><option @if ($type=="all") selected @endif value="0">All</option>'
    typeComboHtml+='<option @if ($type=="sr") selected @endif>SR</option>';
    typeComboHtml+='<option @if ($type=="rfp") selected @endif>RFP</option></select>';

       
    $("div.rfp-toolbar").append('&nbsp; '+typeComboHtml+'&nbsp; '+statusComboHtml);
    

 vendorsTable =  $('#VendorBidsRequest').DataTable( {
        "processing": true,
        "serverSide": true,
        "dom": '<"pull-left"f><"pull-right"l>tip',
        "bLengthChange": false,
        "deferLoading" : 1,
        "ajax": "/json/datatable/vendorslist/null",
        rowReorder: {
                        selector: 'td:nth-child(2)'
                    },
        responsive: true,
        "columns":[
            
            {data: "pivot.rfp_id",         name: 'pivot.rfp_id',},
            
            {data: "name",                 name: 'name',},
            
            {
                data: "pivot.bid_amount", name: 'pivot.bid_amount',

                render: function ( data, type, row ) {

                    if ( type === 'display' ) {
                        
                        var amount = '$ '+data;
                        
                        return amount;
                    }

                    return data;
                }
            },
            {
                data: "emergency", name: 'emergency',

                render: function ( data, type, row ) {

                    var isemergency = 'No';
                    
                    if ( type === 'display' ) {

                         if(data === '1'){
                            
                            isemergency = 'Yes';

                         } else {
                           
                            isemergency = 'No' 
                         }
                         
                         return isemergency;
                    }

                    return data;
                }
            },
            
            {data: "addresses[0].address",   name: 'addresses[0].address',},
            
            {data: "metroarea.name",      name: 'metroarea.name',},
            
            {
                            data: "pivot.status",        name: 'pivot.status',

                            render: function ( data, type, row ) {

                                if ( type === 'display' ) {
                                    
                                    var status;
                                    
                                    if(data === 'sent'){
                                        status = '<span class="label bg-blue">Sent</span>';
                                    }
                                    else if(data === 'bid'){
                                        status = '<span class="label label-warning">Bid</span>';
                                    }
                                    else if(data === 'open'){
                                        status = '<span class="label label-gray">Open</span>';
                                    }
                                    
                                    else if(data === 'workorder'){
                                        status = '<span class="label bg-green">Workorder</span>';
                                    }
                                    else if(data === 'closed'){
                                         status = '<span class="label bg-purple">Closed</span>';
                                    }
                                    else if(data === 'invited'){
                                         status = '<span class="label bg-blue">Invited</span>';
                                    }
                                    return status;
                                }

                                return data;
                            }
            },
            { 
                data: "pivot.vendor_id",      name: 'pivot.vendor_id',    
             
                render: function ( data, type, row ) {


                    if ( type === 'display' ) {
                        
                        var profileButton = '<a onclick="loadVendorInfo('+data+')"  class="btn btn-block btn-primary btn-flat modal-opener-btn vendor_info" ><i class="fa fa-list-alt fa-1x"></i></a>';
                    
                        
                     return profileButton;

                    }

                    return data;
                }
            },        
            {data: "url",                           name: 'url',},
            
            {data: "addresses[0].zip",              name: 'addresses[0].zip',},
            
            {data: "addresses[0].city.name",        name: 'addresses[0].city.name',},
            
            {data: "addresses[0].city.state.name",  name: 'addresses[0].city.state.name',},
            
            {data: "attributes[].name",             name: 'attributes[].name',},
            
            {data: "services[].name",               name: 'services[].name',},
            
            {data: "industry.name",                 name: 'industry.name',},
            
            {
                data: "pivot.vendor_id",      name: 'pivot.vendor_id',  

                 render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        var id = data;

                            id = '<a href="/rfp/client/requestdetail/'+row['pivot']['rfp_id']+'/'+data+'"  class="btn btn-block btn-primary btn-flat"><i class="fa fa-pencil fa-1x"></i></a>';

                        return id;
                    }

                    return data;
                }                        
            },
            
            {data: "user_id",                 name: 'user_id',},
            
        ],
        "columnDefs": [
            {
                "targets": [ 3,4,5,8,9,10,11,12,13,14,16 ],
                "visible": false,
                "searchable": false,
            },
            { "width": "10%", "targets": [0,2,6] },
            { "width": "6%", "targets": [7,15] },
            { "width": "35%", "targets": [1] }
        ]
        
    });

      

    $('#status_list').change(function(){
        
        rfpTable.ajax.reload();      
        
    });

    $('#type_list').change(function(){
        
        rfpTable.ajax.reload();      
        
    });

    

    rfpTable.ajax.reload();


    $('#reloadRfpTable').on('click', function(){
    
        $("#VendorBidsRequest").find("tr:not(:first)").remove();
    
          
          $('#h_rfp_id').val('');
          rfpTable.search('');
          vendorsTable.search('');
          $('.dataTables_filter input').val('');

          $('#status_list').prop('selectedIndex',0);
          $('#status_list').val(0);
          $('#status_list').selectpicker('render');
          
          $('#type_list').prop('selectedIndex',0);
          $('#type_list').val(0);
          $('#type_list').selectpicker('render');

          rfpTable.ajax.reload();      

    }); 
           
});
    
function loadVendors(id){
        
    $('#h_rfp_id').val(id);

    rfpTable.ajax.reload();

    vendorsTable.ajax.url( '/json/datatable/vendorslist/'+id).load();

    $('.content').scrollspy({target: "#vendorsArea"});
     
}

function loadVendorInfo(vendorId){
    
     findVendorInfo(vendorId, "{{{ csrf_token() }}}"); 
    
}
 
 

</script>

@endsection