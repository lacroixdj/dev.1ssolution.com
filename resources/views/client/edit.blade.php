@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Client User Info')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Account
      <small>Company Information</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Account</li>
      <li class="active">Company</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- Box 1 -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><li class="fa fa fa-building-o"></li> Parent Corporation Info.</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- /.No Form -->  
          <div class="box-body">
            <!-- / left column -->
            <div class="col-md-6">
              <!--  Corporation name -->
              <div class="form-group has-feedback">
                <label for="corporationName">Corporation Name:</label>
                {!! Form::text('corporation', $corporation->name,
                [ 'class' => 'form-control', 
                  'id' => 'corporationName',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-building-o form-control-feedback"></li>
              </div>
              <!-- / Corporation name -->
              <!-- Corporation phone -->
              <div class="form-group has-feedback">
                <label for="corporationPhone">Corporation Phone:</label>
                {!! Form::text('corporation', $corporation->phone,
                [ 'class' => 'form-control', 
                  'id' => 'corporationPhone',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-phone form-control-feedback"></li>
              </div>
              <!-- /Corporation phone -->
            </div>
            <!-- / left column -->  

            <!-- right column -->  
            <div class="col-md-6">
              <!-- Corporation email -->
              <div class="form-group has-feedback">
                <label for="corporarionEmail">Corporation Email:</label>
                {!! Form::text('corporation', $corporation->email,
                [ 'class' => 'form-control', 
                  'id' => 'corporationEmail',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-envelope form-control-feedback"></li>
              </div>
              <!-- /Corporation email -->
              <!-- Corporation web Address -->
              <div class="form-group has-feedback">
                <label for="corporationUrl">Corporation Web Address:</label>
                {!! Form::text('corporation', $corporation->url,
                [ 'class' => 'form-control', 
                  'id' => 'corporationUrl',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-globe form-control-feedback"></li>
              </div>
              <!-- /Corporation Web Address -->
        </div>
        <!-- /right column -->


  </div>    
  <!-- /.box-body -->

  <div class="box-footer">                   
  </div>
</div>
<!-- /.box -->
</div>     
</div>
<!-- /.row -->


<!--Client main info-->

<div class="row">
    <div class="col-md-12">
        <!-- Box 1 -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><li class="fa fa fa-building-o"></li> Client Info.</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <!-- /.No Form -->  
          <div class="box-body">
            <!-- / left column -->
            <div class="col-md-6">
              <!--  Corporation name -->
              <div class="form-group has-feedback">
                <label for="corporationName">Name:</label>
                {!! Form::text('client', $client->name,
                [ 'class' => 'form-control', 
                  'id' => 'clientName',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-building-o form-control-feedback"></li>
              </div>
              <!-- / Corporation name -->
              <!-- Corporation phone -->
              <div class="form-group has-feedback">
                <label for="corporationPhone">Phone:</label>
                {!! Form::text('client', $client->phone,
                [ 'class' => 'form-control', 
                  'id' => 'clientPhone',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-phone form-control-feedback"></li>
              </div>
              <!-- /Corporation phone -->
            </div>
            <!-- / left column -->  

            <!-- right column -->  
            <div class="col-md-6">
              <!-- Corporation email -->
              <div class="form-group has-feedback">
                <label for="corporarionEmail">Email:</label>
                {!! Form::text('client', $client->email,
                [ 'class' => 'form-control', 
                  'id' => 'clientEmail',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-envelope form-control-feedback"></li>
              </div>
              <!-- /Corporation email -->
              <!-- Corporation web Address -->
              <div class="form-group has-feedback">
                <label for="corporationUrl">Web Address:</label>
                {!! Form::text('client', $client->url,
                [ 'class' => 'form-control', 
                  'id' => 'clientUrl',
                  'readonly' => 'readonly',
                  'placeholder' => ''
                ]) 
                !!}
                <li class="fa fa-globe form-control-feedback"></li>
              </div>
              <!-- /Corporation Web Address -->
        </div>
        <!-- /right column -->
      </div>    
      <!-- /.box-body -->
      <div class="box-footer">                   
      </div>
    </div>
    <!-- /.box -->
    </div>     
</div>
<!-- /.row -->

<!--End Client main info-->


<!-- row Facility Location Info -->
<div class="row">
  <div class="col-md-12">
    <!-- Box 1 -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><li class="fa fa-map-marker"></li> Client Facility Location.</h3>
      </div>
      <!-- /.box-header -->

      <!-- form start -->
      {!! Form::model($client, [
        'method' => 'POST',
        'url' => ['/client/location'],  
        'id' => 'facility_location_edit',              
        'data-parsley-validate' => '', 
        'data-parsley-trigger' => 'focusin focusout',
        'role' => 'form'
      ]) !!}
    <div class="box-body">
        <!-- / left column -->
        <div class="col-md-6">

          <!-- address -->
          <div class="form-group has-feedback {{ $errors->has('address') ? 'has-error' : ''}}">
            <label for="address">Address:</label>                      
                {!! Form::text('address', $client->addresses()->first()->address, 
                [ 'class' => 'form-control', 
                  'required' => 'required', 
                  'id' => 'address',
                  'placeholder' => 'Type your address'
                ]) 
                !!}
                <li class="fa fa-map-marker form-control-feedback"></li>
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
          </div>
          <!-- /address -->

          <!-- address2 -->
          <div class="form-group has-feedback {{ $errors->has('address2') ? 'has-error' : ''}}">
            <label for="address2">Address 2:</label>                      
                {!! Form::text('address2', $client->addresses()->first()->address2, 
                  [ 
                    'class' => 'form-control', 
                    'id' => 'address2',
                    'placeholder' => 'Type your additional address'
                  ]) 
                !!}
                <li class="fa fa-map-marker form-control-feedback"></li>
                {!! $errors->first('address2', '<p class="help-block">:message</p>') !!}
          </div>
          <!-- /address2 -->
            
          <!-- phone -->
          <div class="form-group has-feedback {{ $errors->has('phone') ? 'has-error' : ''}}">
            <label for="phone">Phone:</label>                      
                {!! Form::text('phone', $client->addresses()->first()->phone, 
                  [ 
                    'class' => 'form-control', 
                    'id' => 'phone',
                    'placeholder' => '(000)000-0000',
                    'required' => '',
                    'data-parsley-minlength' => '14'
                  ]) 
                !!}
                <li class="fa fa-phone form-control-feedback"></li>    
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
          </div>
          <!-- /phone -->
          <!-- zip code -->
          <div class="form-group has-feedback {{ $errors->has('zip') ? 'has-error' : ''}}">
            <label for="zip">Zip / Postal code:</label>                      
                {!! Form::text('zip', $client->addresses()->first()->zip, 
                  [ 
                    'class' => 'form-control', 
                    'required' => 'required', 
                    'id' => 'zip',
                    'placeholder' => '00000-000', 
                    'data-mask' => '00000-000', 
                  ]) 
                !!}
                <li class="fa fa-location-arrow form-control-feedback"></li>   
                {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
           </div>
           <!-- /zip -->
        </div>
        <!-- / left column -->  
        <!-- right column -->  
        <div class="col-md-6">

            <!-- country -->
            <div class="form-group has-feedback {{ $errors->has('country') ? 'has-error' : ''}}">
                <label for="name">Country:</label>
                {{ Form::select(
                'country', 
                Array(''=>'Select your country') + $countries, '231',
                [
                'id' => 'country',
                'class' => 'form-control',
                'required' => 'required',
                'readonly' => 'readonly',
                'disabled' => 'disabled',
                ]) 
              }}
              <li class="fa fa-map-marker form-control-feedback"></li>
              {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
            </div>
            <!-- /country  -->

            <!-- state -->
            <div class="form-group has-feedback {{ $errors->has('state') ? 'has-error' : ''}}">
                  <label for="">State:</label>
                  {{ Form::select(
                  'state', 
                  Array(''=>'Select your state') + $states, $client->addresses()->first()->city->state->id,
                  [
                  'id' => 'state',
                  'class' => 'form-control',
                  'required' => 'required'
                  ]
                  ) 
                }} 
                <li class="fa fa-map-marker form-control-feedback"></li>
                {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
            </div>
            <!-- /state -->

            <!-- city -->  
            <div class="form-group has-feedback {{ $errors->has('city') ? 'has-error' : ''}}">
                <label for="">City:</label>
                {{ Form::select(
                'city', 
                Array(''=>'Select your city') + $cities, $client->addresses()->first()->city->id,
                [ 
                'id' => 'city',
                'class' => 'form-control',
                'required' => 'required'
                ]
                ) 
              }}   
              <li class="fa fa-map-marker form-control-feedback"></li>
              {!! $errors->first('city', '<p class="help-block">:message</p>') !!}                     
            </div>
            <!-- /city -->
            
            <!-- metroarea -->
            <div class="form-group has-feedback {{ $errors->has('metroarea_id') ? 'has-error' : ''}}">
              <label for="">Metropolitan area:</label>
                {{ Form::select('metroarea_id', Array(''=>'Select your metropolitan area') + $metroareas, $client->metroarea_id,
                      [ 'id' => 'metroarea_id',
                      'class' => 'form-control',
                      'required' => 'required'
                      ])
                }}
                <li class="fa fa-map-marker form-control-feedback"></li>
                {!! $errors->first('metroarea_id', '<p class="help-block">:message</p>') !!}
            </div>
            <!-- /metroarea -->
            
            <!-- form buttons -->
            <div class="form-group has-feedback pull-right">  
                  <button type="button" href="#" onClick="window.location.reload()" class="btn btn-primary btn-flat">Cancel</button>
                  {!! Form::submit('Update Location', ['class' => 'btn btn-success btn-flat']) !!}
            </div>
            <!-- /form buttons -->

      </div>
        <!-- /right column -->
    </div>    
    <!-- /.box-body -->
    <div class="box-footer"></div>

{!! Form::close() !!}

      </div>
<!-- /.box -->
    </div>     
</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<script>
  $(document).ready(function() { 

    XHRFormListener('#facility_location_edit');
          
        //Aplying validation mask to phone and zip code
        $('#phone').mask('(000) 000-0000');
        $('#zip').mask('00000-000');

         //Country, states and city chained comboboxes refresing logic
        $('#country').change(function(){

          //Getting elements
          var country_id = $(this).val();
          var states_combo = $('#state');
          var cities_combo = $('#city');

          //Clearing combos   
          states_combo.find("option:gt(0)").remove();    
          cities_combo.find("option:gt(0)").remove();

          //If no country selected then disable childrens
          if (country_id == 0){
            states_combo.attr( "disabled", "disabled" );
            cities_combo.attr( "disabled", "disabled" );
            return;
          } 

          //Compose backend url to retrieve data from models
          var countries_url = '{{ url('/json/countries') }}';
          var states_url = countries_url  + '/' + country_id + '/states';  
          
          //The ajax request
          $.ajax({
            method: "GET",
            url: states_url,
            dataType: 'json'
          }).done(function(data) {
              //Enabling child combo                 
              states_combo.removeAttr('disabled');        
              
              //Populating  state combo
              $.each( data, function( key, value ) {                    
                states_combo.append($("<option />").val(key).text(value));
              });
            });
        });

        //States and city chained comboboxes refresing logic
        $('#state').change(function(){

          //Getting elements
          var state_id = $(this).val();
          var cities_combo = $('#city');

          //Clearing combos
          cities_combo.find("option:gt(0)").remove();

          //If no state selected then disable child
          if (state_id == 0){
            cities_combo.attr( "disabled", "disabled" );
            return;
          } 

          //Compose backend url to retrieve data from models
          var states_url = '{{ url('/json/states') }}';
          var cities_url = states_url +'/' + state_id + '/cities'; 

          //The ajax request
          $.ajax({
            method: "GET",
            url: cities_url,
            dataType: 'json'
          }).done(function(data) {
              //Enabling child combo           
              cities_combo.removeAttr('disabled');

              //Populating  state combo
              $.each( data, function( key, value ) {
                cities_combo.append($("<option />").val(key).text(value));
              });
            });       
        });
});
</script>
@endsection
