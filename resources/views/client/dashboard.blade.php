@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Client Dashboard')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard      
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ol>
  </section>
  <section class="content">
     <!-- /.indicators panel  -->  
     
     <!-- Workorders  -->  
     <div class="row">
        <div class="col-md-12 col-sm-12">
        <div class="box box-primary">
            
            <div class="box-header with-border">
              <h3 class="box-title box-title2"><li class="fa fa-list"></li> Workorders</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">           

                <!-- end row -->
                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-green" id="workorder-open">
                      <span class="info-box-icon icon"><i class="fa fa-check-square-o"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Open</span>
                          <span class="info-box-number">{{ $wko_data['open'] }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $wko_data['open_p'] }}%"></div>
                          </div>
                          <span class="progress-description">{{ $wko_data['open_p'] }}%</span>
                        </div>
                    </div>
                    </div>

                  
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-purple" id="workorder-closed">
                      <span class="info-box-icon"><i class="fa fa-lock"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Closed</span>
                          <span class="info-box-number">{{ $wko_data['closed'] }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $wko_data['closed_p'] }}%"></div>
                          </div>
                          <span class="progress-description">{{ $wko_data['closed_p'] }}%</span>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-maroon" id="workorder-all">
                      <span class="info-box-icon icon"><i class="fa fa-plus"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Total</span>
                          <span class="info-box-number">{{ $wko_data['total'] }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                          </div>
                          <span class="progress-description">100%</span>
                        </div>
                    </div>
                    </div>


                </div>
                <!-- end row --> 
                
            </div>
            <!-- ./box-body -->
          </div>
          <!-- /.box -->
        </div>
        </div>


     <!-- /end Workorders  -->  




     <div class="row">
        <!-- /.RFP-->  
        <div class="col-md-12 col-sm-12">

          <div class="box box-primary collapsed-box">
            
            <div class="box-header with-border">
              <h3 class="box-title box-title2"><li class="fa fa-users"></li> Requests for Proposals</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">           

                <!-- end row -->
                <div class="row">
                  
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-aqua" id="rfp-open">
                      <span class="info-box-icon"><i class="fa fa-pencil"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Open</span>
                          <span class="info-box-number">{{ $rfp_data['open']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $rfp_data['open_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $rfp_data['open_p']  }}%</span>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-light-blue" id="rfp-sent">
                      <span class="info-box-icon icon"><i class="fa fa-send"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Sent</span>
                          <span class="info-box-number">{{ $rfp_data['sent']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $rfp_data['sent_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $rfp_data['sent_p']  }}%</span>
                        </div>
                    </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-yellow" id="rfp-bid">
                      <span class="info-box-icon"><i class="fa fa-inbox"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Bid</span>
                          <span class="info-box-number">{{ $rfp_data['bid']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $rfp_data['bid_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $rfp_data['bid_p']  }}%</span>
                        </div>
                    </div>
                    </div>
                  


                  
                </div>
                <!-- end row -->

                <!-- row -->
                <div class="row">
                  
                  
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-green" id="rfp-workorder">
                      <span class="info-box-icon icon"><i class="fa fa-check-square-o"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Workorder</span>
                          <span class="info-box-number">{{ $rfp_data['workorder']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $rfp_data['workorder_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $rfp_data['workorder_p']  }}%</span>
                        </div>
                    </div>
                    </div>


                 
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-purple" id="rfp-closed">
                      <span class="info-box-icon"><i class="fa fa-lock"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Closed</span>
                          <span class="info-box-number">{{ $rfp_data['closed']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $rfp_data['closed_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $rfp_data['closed_p']  }}%</span>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-maroon" id="rfp-all">
                      <span class="info-box-icon icon"><i class="fa fa-plus"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Total</span>
                          <span class="info-box-number">{{ $rfp_data['total']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                          </div>
                          <span class="progress-description">100%</span>
                        </div>
                    </div>
                    </div>


                </div>
                <!-- end row --> 
                
            </div>
            <!-- ./box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- /.End RFP -->
        
        <!-- /.Services Request-->  
        <div class="col-md-12 col-sm-12">
          <div class="box box-primary collapsed-box" >
            <div class="box-header with-border">
              <h3 class="box-title box-title2"><li class="fa fa-user"></li> Service Requests</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <!-- end row -->
                <div class="row">
                  
                 
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-aqua" id="sr-open">
                      <span class="info-box-icon"><i class="fa fa-pencil"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Open</span>
                          <span class="info-box-number">{{ $sr_data['open']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $sr_data['open_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $sr_data['open_p']  }}%</span>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-light-blue" id="sr-sent">
                      <span class="info-box-icon icon"><i class="fa fa-send"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Sent</span>
                          <span class="info-box-number">{{ $sr_data['sent']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $sr_data['sent_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $sr_data['sent_p']  }}%</span>
                        </div>
                    </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-yellow" id="sr-bid">
                      <span class="info-box-icon"><i class="fa fa-inbox"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Bid</span>
                          <span class="info-box-number">{{ $sr_data['bid']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $sr_data['bid_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $sr_data['bid_p']  }}%</span>
                        </div>
                    </div>
                    </div>
                  


                  
                </div>
                <!-- end row -->

                <!-- row -->
                <div class="row">
                  
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-green" id="sr-workorder">
                      <span class="info-box-icon icon"><i class="fa fa-check-square-o"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Workorder</span>
                          <span class="info-box-number">{{ $sr_data['workorder']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $sr_data['workorder_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $sr_data['workorder_p']  }}%</span>
                        </div>
                    </div>
                    </div>


                

                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-purple" id="sr-closed">
                      <span class="info-box-icon"><i class="fa fa-lock"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Closed</span>
                          <span class="info-box-number">{{ $sr_data['closed']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: {{ $sr_data['closed_p']  }}%"></div>
                          </div>
                          <span class="progress-description">{{ $sr_data['closed_p']  }}%</span>
                        </div>
                    </div>
                    </div>
                    

                    <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box bg-maroon" id="sr-all">
                      <span class="info-box-icon icon"><i class="fa fa-plus"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Total</span>
                          <span class="info-box-number">{{ $sr_data['total']  }}</span>
                          <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                          </div>
                          <span class="progress-description">100%</span>
                        </div>
                    </div>
                    </div>


                </div>
                <!-- end row --> 
                  



            </div>
            <!-- ./box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- /.End Services Request Indicators -->
        
      </div><!-- /.row -->

      


      <div class="row">
        <!-- /.Pending client responses -->
        <div class="col-md-8 col-xs-12">
          <div class="box box-primary">
            
            <div class="box-header with-border" id="inboxBox">
              <h3 class="box-title box-title2"><li class="fa fa-envelope"></li>&nbsp; Inbox</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Refresh" id="refreshInbox"><i class="fa fa-refresh"></i>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="clientInbox" class="table tale-striped table-bordered table-hover responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>Type</th>
                  <th>Vendor</th>
                  <th>Message</th>
                  <th>Date</th>
                  <th data-priority="1">View</th>
                  <th data-priority="1">Detail</th>
                  <th data-priority="1">Profile</th>                  
                </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
            
            <div  id="inboxLoading" class="overlay hidden" >
              <i class="fa fa-refresh fa-spin chat-spinner"></i>
            </div>  
          </div>
          <!-- /.box -->
        </div>
        <!-- /.End pending client responses --> 
        
        <!-- /.Last Conversartions -->
        <div class="col-md-4 col-xs-12">
          

                         <!-- Mensaje Area -->
                          <!-- /.Mensaje Area -->
  
    <!-- /.End Mensaje Area -->  

<div class="box box-primary direct-chat direct-chat-primary"  id="chatBox">
    <div class="box-header with-border" >
        <h3 class="box-title box-title2"><li class="fa fa-comments"></li>&nbsp;Messages</h3>
            <div class="box-tools pull-right">
                <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
                <button type="button" id="chatCollapse" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    <!-- Conversations are loaded here -->
        <div id="direct-chat-messages-body" class="direct-chat-messages direct-chat-messages-dashboard" >
        <!-- Message. Default to the left -->
      
    </div>
        <!--/.direct-chat-messages-->
    </div>
    <!-- /.box-body -->    
    <div class="box-footer">
        <form action="#" method="post">
            <div class="input-group">
                <input id="input-message" name="message" placeholder="Type Message ..." class="form-control" type="text">
                <span class="input-group-btn">
                    <button type="button" id="send-message" class="btn btn-primary btn-flat">Send</button>
                </span>
            </div>
        </form>
    </div>
    <!-- /.box-footer-->
    <div  id="chatEmpty" class="overlay" >
      
      <div class="chat-empty text-center">
        <i class="fa fa-comments chat-empty-fa"></i>
        <h5>Click on inbox messages</h4>
      </div>        
    </div>
    <div  id="chatLoading" class="overlay hidden" >
      <i class="fa fa-refresh fa-spin chat-spinner"></i>
    </div>
</div>

        </div>
        <!-- /. End Last Conversations -->    
      </div><!-- /.row -->    
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script id="chat_message_template" type="text/template">
    <div class="direct-chat-msg">
        <!-- message-info -->
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-left"></span>
            <span class="direct-chat-timestamp pull-right"></span>
        </div>
        <!-- end message-info -->

        <!-- message img -->
        <img class="direct-chat-img" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="Message User Image">
        <!-- end message img -->

        <!-- message text -->
        <div class="direct-chat-text">
        </div>
        <!-- end message text -->
    </div>
</script>

<script id="chat_message_template_right" type="text/template">
    <div class="direct-chat-msg right">
        
        <!-- message-info -->
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-right"></span>
            <span class="direct-chat-timestamp pull-left"></span>
        </div>
        <!-- end message-info -->
        
        <!-- message img -->
        <img class="direct-chat-img" src="{{ URL::asset('assets/img/user-a white-bg.png') }}" alt="Message User Image">
        <!-- end message img -->

        <!-- message text -->
        <div class="direct-chat-text">
        </div>
        <!-- end message text -->
    </div>
</script>


<!-- Pusher lib (It loads from the web, because it doesn't make sense to use a local version of the lib --pusher won't work without internet connection ) -->
<script src="//js.pusher.com/3.0/pusher.min.js"></script>

<script>

    
    // Send on enter/return key
    function checkSend(e) {
    
        if (e.keyCode === 13) {
    
            return sendMessage();
    
        }
    }

    
    // Handle the send button being clicked
    function sendMessage() {
    
        var messageText = $('#input-message').val();
    
        if(messageText.length < 2) {
    
            return false;
        }

        // Build POST data and make AJAX request
        var data = {
        
            text: messageText,

            socketId: pusher.connection.socket_id,

            //chatchannel:

        };
        
        $.post('/conversation/message', data).success(sendMessageSuccess);

        // Ensure the normal browser event doesn't take place
        return false;
    }

    
    // Handle the success callback
    function sendMessageSuccess() {
    
        $('#input-message').val('')
    
        console.log('message sent successfully');
    }

    
    // Build the UI for a new message and add to the DOM
    function addMessage(data) {
    
        console.log(data);
        // Create element from template and set values    
        var el = createMessageEl(data.user_id);
    
        el.find('.direct-chat-text').html(data.text);

        el.find('.direct-chat-name').text(data.username);

        //el.find('.direct-chat-img').attr('src', data.avatar)
        
        // Utility to build nicely formatted time
        //el.find('.direct-chat-timestamp').text(strftime('%H:%M:%S %P', new Date(data.timestamp)));
        el.find('.direct-chat-timestamp').text(data.timestamp);
        
        var messages = $('#direct-chat-messages-body');
        
        messages.append(el)
        

        // Make sure the incoming message is shown
        messages.scrollTop(messages[0].scrollHeight);
    }

    // Creates an activity element from the template
    function createMessageEl(user_id) {
        var text = false;

        if(user_id=={{ auth()->user()->id }}) text = $('#chat_message_template_right').text();
        else text = $('#chat_message_template').text();

        var el = $(text);
        return el;
    }

    
    var pusher = new Pusher('{{env("PUSHER_KEY")}}', {
        
        authEndpoint: '/conversation/auth',
        
        auth: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
        }
    });

    // Added Pusher logging
    Pusher.log = function(msg) {
    
        console.log(msg);
    
    };
    
    var channel = false;    

    var subscribed_channels = new Array();


    function unsuscribe_channels(){

        console.log('inside unsuscribe_channels');
        if(empty(subscribed_channels.length)) return;
        console.log('suscribed_channels', subscribed_channels);

        for(i=0;i<subscribed_channels.length;i++){

            if(!empty(subscribed_channels[i])){

              try{
                  console.log('before unsubscribe channel', subscribed_channels[i]);
                  pusher.unsubscribe(subscribed_channels[i]);
                  console.log('after unsubscribe');

              }catch(e){
                  console.log('error unsubscribing', e);
              }


            }          
        }

        subscribed_channels = new Array();
    }


    function clear_messages_box () {

      $('#direct-chat-messages-body').empty();
      $("#chatLoading").removeClass('hidden');
      $("#chatEmpty").hide();
      //$.AdminLTE.boxWidget.collapse($('#chatCollapse'));        
      
    }

    function restore_messages_box () {

      
      $("#chatLoading").addClass('hidden');
      //$.AdminLTE.boxWidget.collapse($('#chatCollapse'));        
      
    }

    
    function open_conversation (rfp_id, vendor_id, workorder_id){

       /* console.log(arguments);
        console.log('inside open conversation');
        return;
*/
        if(empty(rfp_id) || empty(vendor_id)) return false; 

        //
        unsuscribe_channels();
        clear_messages_box();

        // Ensure CSRF token is sent with AJAX requests
        $.ajaxSetup({

            headers: {
        
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        
        });

             
        var backend_url = "{{ url('/conversation/open') }} ";
        
        $.ajax({

            method: "POST",

            url: backend_url,

            dataType: 'json',

            data: {

                _token : "{{ csrf_token() }}",

                rfpId: rfp_id,

                workorderId: workorder_id,

                vendorId: vendor_id,
            }

        }).done(function(data) {

            console.log(data.chatChannel);  

             


            $.ajaxSetup({

                data: {
        
                    channel: data.chatChannel
                }
        
            });
            

            channel = pusher.subscribe(data.chatChannel);
            
            subscribed_channels.push(data.chatChannel);

            channel.bind('new-message', addMessage);   
           
            $("#chatLoading").addClass('hidden');
            
            loadMessages(data.messages);           
            
        });          

    }

    
    function loadMessages(messages){

        if(!empty(messages)) {

            messages.forEach(function(item, index){

                data = {

                    user_id: item.user_id,

                    text: item.text,

                    timestamp: item.created_at,

                    username: item.user.name+' '+item.user.lastname

                }    
                
                addMessage(data) ;

            });

        }

        restore_messages_box();
    }


    function loadVendorInfo(vendorId){
    
        findVendorInfo(vendorId, "{{{ csrf_token() }}}"); 
    
    }

$(document).ready(function() { 

    // send button click handling
    $('#send-message').click(sendMessage);
    
    // input message on enter key press handling
    $('#input-message').keypress(checkSend);
    
    // initialize channel subscription and auth methods against the backend
    //open_conversation();


    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });

    $('.content').scrollspy({target: "#chatBox"});

    $('.content').scrollspy({target: "#inboxBox"});

    $('#clientInbox').DataTable({
      
      paging: true,
      
      processing: true,
      
      pageLength: 5,
      
      lengthChange: false,
      
      responsive: true,
      
      dom: '<"pull-left"f>tip',
      
      ordering: false,
      
      sorting: false,
      
      aaSorting: [],
      
      info: true,

      serverSide: true,

      deferLoading: 1,

      language: {
        
        emptyTable: "You have not received any message yet"
      },
      
      ajax: {

        method: 'POST',

        url: '/json/client/inbox',

        data: function(d) {

          d._token = "{{ csrf_token() }}";
          
        }
      },
      


      columns:[

        {data: "type",  name: "type", width: "5%", searchable:true,

            render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        if(data=='SR' || data=='RFP' ){
                          
                            link = '<a href="/rfp/client/requestdetail/'+row['rfp_id']+'/'+row['vendor_id']+'">'+data+'</a>&nbsp';
                        
                        } else if (data== 'WKO') {

                            link = '<a href="/workorder/client/detail/'+row['workorder_id']+'">'+data+'</a>&nbsp';

                        }
                        
                        return link;
                    }

                    return data;
                }


        },

        //width: "30%",

        {data: "name",  name: "name",  searchable:true,

            render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        str = smartTruncate(data, 25);

                        link = '<a href="#inboxBox" onclick="loadVendorInfo('+row['vendor_id']+')">'+str+'</a>';
                        
                        return link;
                    }

                    return data;
                }

        },
      
        //width: "35%",
        {data: "text",   name: 'text' ,  searchable:true,

            render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        str = smartTruncate(data, 25);

                        //button = '<a href="#chatBox" class="btn btn-primary btn-flat" role="button" data-toggle="tooltip" title="View on chat" onclick="open_conversation('+row['rfp_id']+','+row['vendor_id']+','+row['workorder_id']+')"><i class="fa fa-comments fa-1x"></i></a>&nbsp';

                        //return  button+str+' ('+row['messages_count']+')';

                        badge = '<span data-toggle="tooltip" title="" class="badge bg-primary" data-original-title="Messages">'+row['messages_count']+'</span>&nbsp;&nbsp;';
                        
                        link = '<a href="#chatBox" onclick="open_conversation('+row['rfp_id']+','+row['vendor_id']+','+row['workorder_id']+')">'+str+'</a>';

                        return  badge+link;
                    }

                    return data;
                }
        },

        
        { data: "created_at",  name: "created_at", width: "15%" },

        {data: "id",   name: 'id' , width: "5%", searchable:false,

            render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                      button = '<a href="#chatBox" class="btn btn-primary btn-flat" role="button" data-toggle="tooltip" title="View on chat" onclick="open_conversation('+row['rfp_id']+','+row['vendor_id']+','+row['workorder_id']+')"><i class="fa fa-comments fa-1x"></i></a>&nbsp';

                      return  button;
                    }

                    return data;
                }
        },


        {data: "type",  name: "type", width: "5%", searchable:false,

            render: function ( data, type, row ) {

                    if ( type === 'display' ) {
                        
                        if(data=='SR' || data=='RFP' ){
                          
                          button = '<a href="/rfp/client/requestdetail/'+row['rfp_id']+'/'+row['vendor_id']+'" class="btn btn-primary btn-flat" role="button" data-toggle="tooltip" title="View details"><i class="fa fa-list-alt fa-1x"></i></a>&nbsp';
                        
                        } else if (data== 'WKO') {

                          button = '<a href="/workorder/client/detail/'+row['workorder_id']+'" class="btn btn-primary btn-flat" role="button" data-toggle="tooltip" title="View details"><i class="fa fa-list-alt fa-1x"></i></a>&nbsp';

                        }

                        return  button;
                    }

                    return data;
                } 
        },

        {data: "vendor_id",  name: "vendor_id", width: "5%", searchable:false,

            render: function ( data, type, row ) {

                    if ( type === 'display' ) {

                        button = '<a class="btn btn-primary btn-flat" role="button" data-toggle="tooltip" title="Profile" onclick="loadVendorInfo('+data+')"><i class="fa fa-user fa-1x"></i></a>&nbsp';
                        
                        return  button;
                    }

                    return data;
                }

        }      



      ]


    });




    //RFP LINK FILTERS
    $('#refreshInbox').on('click',function(){
        $("#inboxLoading").removeClass('hidden');
        $('#clientInbox').DataTable().ajax.reload();
           setTimeout(function(){ 
              $("#inboxLoading").addClass('hidden');

           }, 3000)
    });


    window.setInterval(function(){
        $("#inboxLoading").removeClass('hidden');
        $('#clientInbox').DataTable().ajax.reload();
        $("#inboxLoading").addClass('hidden');
    }, 10000);
    

    $('#rfp-open').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/rfp/open') }}";
    });



    $('#rfp-sent').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/rfp/sent') }}";
    });

    $('#rfp-bid').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/rfp/bid') }}";
    });

    $('#rfp-workorder').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/rfp/workorder') }}";
    }); 

    $('#rfp-closed').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/rfp/closed') }}";
    });  

    //ALL RFP
    $('#rfp-all').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/rfp/all') }}";
    });
    

    //SR LINK FILTERS
    $('#sr-open').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/sr/open') }}";
    });

    $('#sr-sent').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/sr/sent') }}";
    });

    $('#sr-bid').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/sr/bid') }}";
    });
    
    $('#sr-workorder').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/sr/workorder') }}";
    }); 

    $('#sr-closed').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/sr/closed') }}";
    }); 

    //ALL SR
    $('#sr-all').on('click',function(){
      window.location.href = "{{ url('/rfp/client/bidsrequest/sr/all') }}";
    });
    



    //WORKORDER LINK FILTERS
    $('#workorder-open').on('click',function(){
      window.location.href = "{{ url('/workorder/client/view/workorder/open') }}";
    }); 

    $('#workorder-closed').on('click',function(){
      window.location.href = "{{ url('/workorder/client/view/workorder/closed') }}";
    });  
    
    $('#workorder-all').on('click',function(){
      window.location.href = "{{ url('/workorder/client/view/workorder/all') }}";
    });  


    $('#clientInbox').DataTable().ajax.reload();


    
});
</script>

@endsection
