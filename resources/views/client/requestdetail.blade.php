@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Bids request')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bids request
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/rfp/client/bidsrequest">Request / Bids</a></li>
        <li class="active">Bids Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- /.tabs -->
        <div class="col-md-12">
          <!-- Custom Tabs  -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="pull-left header"><i class="fa fa-th"></i></li>
              <li class="active"><a href="#tab_1-1" data-toggle="tab">Bid Request Information</a></li>
              <li><a href="#tab_2-1" data-toggle="tab">More Info.</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">
                  <div class="row">
                   
                    <div class="col-md-6">
                        <!-- /.workorder Layout -->  
                        <div class="box box-widget widget-user-2" style="border: 1px solid #d2d6de;">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header @if($rfp->status == 'sent') bg-light-blue @elseif($rfp->status == 'bid')  bg-yellow @elseif($rfp->status == 'workorder') bg-green @elseif($rfp->status == 'closed') bg-purple @else bg-gray @endif">
                              <div class="widget-user-image">
                                <img class="" src="{{ URL::asset('assets/img/workorder-icon.png') }}" alt="User Avatar">
                              </div>
                              <!-- /.widget-user-image -->
                              <h3 class="widget-user-username">@if($rfp->type == 'SR') Service Request #{{$rfp->id}} @else RFP #{{$rfp->id}} @endif </h3>
                              <h5 class="widget-user-desc">Status: {{ ucfirst($rfp->status) }}</h5>  
                            </div>

                            <div class="box-footer">      
                             <div class="row">
                                <br>
                                <div class="col-sm-6">
                                  <div class="">
                                    <p>Is Emergency: <b id="">@if($rfp->emergency == 1) Yes @else No @endif</b></p>
                                    <p>Description: <b id="">{{$rfp->description}}</b></p>
                                    <p>Service location: <b id="">{{$rfp->location}}</b></p> 
                                   </div>
                                  <!-- /.description-block -->
                                  </div>
                                    <!-- /.col -->
                                    <div class="col-sm-6 border-left">
                                      <div class="">
                                        <p><b>Service by:</b> {{date('m/d/Y', strtotime($rfp->service_date))}}</p>
                                        <p><b>Response by:</b> {{date('m/d/Y', strtotime($rfp->response_date))}}</p>         
                                        <p><b>Previous visit required?:</b>@if($rfp->visit == 1) Yes @else No @endif</p> 
                                        <h4 class=""><li class="fa fa-dollar"></li>  Bid Amount: {{$rfp->pivot->bid_amount}}</h4>
                            
                                       <div class="">
                                           @if($rfp->pivot->bid_amount== '0.00')  
                                           @else 
                                           <!-- /.form que hace el place del workorder -->  
                                           {!! Form::open([

                                                'method' => 'POST',
                                                'url' => ['/workorder/create'],
                                                'id' => 'workorder_create',
                                                'data-parsley-validate' => '', 
                                                'data-parsley-trigger' => '',
                                                'role' => 'form'
                                              ]) 

                                            !!}

                                            {!! Form::hidden('rfpId', $rfp->id,
                                                    [
                                                      'id' => 'rfpId',
                                                    ]) 
                                            !!}

                                            {!! Form::hidden('vendorId', $vendor->id,
                                                    [
                                                      'id' => 'vendorId',
                                                    ]) 
                                            !!}
                                            
                                             <button id="convertWorkOrder" type="button" class="btn btn-block btn-success btn-flat"> Convert to Workorder</button>

                                            {!! Form::close() !!}  
                                            <!-- /.end form -->  
                                            <!-- /.end if -->  
                                            @endif
                                          </div>     
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              <p><li class="fa fa-plus-square"></li> Details: <br> <b id="">{{$rfp->details}}</b></p>
                              <p><li class="fa fa-plus-square"></li> Documents Needed: <br> <b id="">{{$rfp->documents}}</b></p>
                              <strong><li class="fa fa-map-marker"></li> Service location</strong><br>
                              <div id="examples">
                                <a href="#"></a>
                              </div>
                              <br>
                              <input type="hidden" id="geocompleteWorkorder">
                              <div id=""  class="map_canvas_workorder"></div>
                              <div id="errorWorkorder"></div>
                            </div>
                        </div>  
                        <!-- /.end workorder layout -->   
                    </div>
                     <div class="col-md-6">
                         <!-- Mensaje Area -->
                          @include('conversation.open', array(
                            'rfp_id' => $rfp->id,
                            'vendor_id' => $vendor->id,
                            'client_id' => $client->id
                          ))
                         <!-- End Mensaje Area -->   
                       
                    </div>
                </div><!-- /.end row -->  
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-1">
               <div class="row">
                    <div class="col-md-6">
                        <!-- /.widget vendor details -->
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-primary">
                               <div class="col-sm-7">
                                  <h3 class="widget-user-username" id="companyName">{{$vendor->name}}</h3>
                                  <h4 class="widget-user-desc" id="companyIndustry">{{$vendor->industry->name}}</h4>
                                  </p>
                                </div>
                                <div class="col-sm-4">
                                    <input name="input-rate" class="pull-right rating rating-loading rating-disabled" data-show-clear="false" value="{{ $vendor->rate }}" data-min="0" data-max="5" data-step="1" data-readonly="true" data-size="xs">
                                </div>
                            </div>
                            <div class="widget-user-image">
                              <img class="img-circle" src="{{URL::asset('assets/img/vendor-white-bg.png') }}" alt="User Avatar">
                            </div>
                            <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="">
                                    <p>Phone: <b id="contactPhone">{{$vendor->phone}}</b></p>
                                    <p>Email: <b id="contactEmail">{{$vendor->email}}</b></p>
                                    <p>Web: <b id="contactWeb">{{$vendor->url}}</b></p>
                                    <p>Metropolitan Area: <b id="CompanyMetroArea">{{$vendor->metroarea->name}}</b></p>
                                    <p>Attends Emergency: <b id="CompanyEmergencies">@if($vendor->emergency == 1) Yes @else No @endif</b></p>
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6 border-left">
                                  <div class="">
                                    <p>Address: <b id="companyAddress">{{$vendorAddresses->address}}, {{$vendorAddresses->address2}}</b></p>
                                    <!-- <p>Address 2: <b id="companyAddress2"></b></p> -->
                                    <p>Zip: <b id="companyZip">{{$vendorAddresses->zip}}</b></p>
                                    <p>City: <b id="companyCity">{{$vendorAddresses->city->name}}</b></p>
                                    <p>State: <b id="companyState">{{$vendorAddresses->city->state->name}}</b></p>
                                    <p>Country: <b id="companyCountry">{{$vendorAddresses->city->state->country->name}}</b></p>      
                                  </div>
                                  <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->
                              <div class="row">
                                <div class="col-sm-12">
                                  <address>
                                    <strong><li class="fa fa-map-marker"></li> Vendor location</strong><br>
                                    <div id="examples">
                                        <a href="#"></a>
                                    </div>
                                    <br>
                                    <input type="hidden" id="geocompleteVendor">
                                    <div id="geocompleteVendor"  class="map_canvas_vendorlist"></div>
                                    <div id="errorVendorMap"></div>
                                  </address>
                                </div>
                                <!-- /.col -->
                              </div>
                              <!-- /.row -->  
                            </div>
                          </div>
                        <!-- /.end widget vendor details --> 
                    </div>
                    <div class="col-md-6">
                        <h3 class="page-header">History</h3>
                            <ul class="timeline">
                            <!-- TODO: Incluir la logica para mostrar las fechas-->  
                    
                            <!-- timeline time label -->
                            <li class="time-label">
                                  @if(date('jS F, Y',strtotime($rfp->created_at)) == date('jS F, Y',strtotime($rfp->pivot->created_at)))   
                                  <span class="bg-primary">
                                       {{date('jS F, Y', strtotime($rfp->created_at))}}
                                  </span>
                                  @else 
                                   
                                  @endif
                            </li>
                            <!-- /.timeline-label -->

                                <!-- timeline item -->
                                <li>
                                    <!-- timeline icon -->
                                    <i class="fa fa-dollar bg-yellow"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->created_at))}}</span>
                                        <h3 class="timeline-header"><a href="#">Last Bid send</a></h3>
                                        <div class="timeline-body">
                                            Bid amount ${{$rfp->pivot->bid_amount}}
                                        </div>
                                    </div>
                                </li>

                                <!-- /.Rfp o Sr date created -->  
                                <li class="time-label">
                                   @if(date('jS F, Y',strtotime($rfp->created_at)) == date('jS F, Y',strtotime($rfp->pivot->created_at)))   

                                   @else 
                                   <span class="bg-primary">
                                       {{date('jS F, Y', strtotime($rfp->created_at))}}
                                   </span>
                                   @endif

                                </li>
                                <li>

                                    <i class="fa fa-plus bg-blue"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{date('h:i:s A', strtotime($rfp->created_at))}}</span>

                                        <h3 class="timeline-header"><a href="#">Client create the {{$rfp->type}}</a></h3>
                                        <div class="timeline-body">
                                            Service date to: {{date('m/d/Y', strtotime($rfp->service_date))}} <br>
                                            Response before: {{date('m/d/Y', strtotime($rfp->response_date))}}
                                        </div>
                                    </div>
                                </li>
                                <!-- /.Rfp o Sr date created -->  

                                <!-- END timeline item -->
                                <li>
                                  <i class="fa fa-clock-o bg-gray"></i>
                                </li>
                            </ul>
                    </div>
               </div>

              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.end Tabs -->    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.Modal Confirm -->  
    <div class="modal fade" id="modalConfrimRfp">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Notice</h4>
          </div>
          
          <div class="modal-body">
              <p>You are about to accept this service for a amount of $ {{$rfp->pivot->bid_amount}} <b id="bidConfirmLabel"></b></p> 
              <p>Are you sure? Once you updated so you can not modify.&hellip;</p>
          </div>
          
           <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                <button type="button" id="saveWorkorder" class="btn btn-success btn-flat">Accept</button>
           </div>
           
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  <!-- /.End Modal Confirm -->  
  
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

<script>
    
XHRFormListener('#bidStore');


{{--*/ $addressClient =  str_replace(array("\r","\n"), "", $vendorAddresses->address) /*--}}

var address = "{{$vendorAddresses->city->state->country->name}}, {{$vendorAddresses->city->state->name}}, {{$vendorAddresses->city->name}}, {{$addressClient}}, {{$vendorAddresses->address2}}";
    address=address.toString();

{{--*/ $addressRfp =  str_replace(array("\r","\n"), "", $rfp->location) /*--}}
    
var addressRfp = "{{ $addressRfp }}";  
    addressRfp=addressRfp.toString();



    mapMarker(addressRfp, '#geocompleteWorkorder', '.map_canvas_workorder', '#errorWorkorder');
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
         mapMarker(address, '#geocompleteVendor', '.map_canvas_vendorlist', '#errorVendorMap');
        
    });
        

XHRFormListener('#workorder_create');
        
//SLIMSCROLL FOR CHAT WIDGET
$('#chat-box').slimScroll({
    height: '250px'
});
    
    
$('#convertWorkOrder').click(function() {
     
    $('#modalConfrimRfp').modal('show');
});
    
    
$('#saveWorkorder').click(function() {
     
    console.log('submit');
                
    $("#workorder_create").submit();
            
    $('#modalConfrimRfp').modal('hide');
         
    $('#convertWorkOrder').attr('disabled', true); 
     
 });

$(document).ready(function() {
    //localizacion de Geocomplete
    //$("#geocompleteWorkorder").val(addressRfp).trigger("geocode");
    
    //$("#geocompleteVendor").val(address).trigger("geocode");
    
    
    var status = '{{$rfp->status}}';
    
    //TODO: Validar del lado del servidor esto
    if( status === 'workorder' || status === 'closed'){
        $('#convertWorkOrder').attr('disabled', true); 
    }
});


</script>

@endsection