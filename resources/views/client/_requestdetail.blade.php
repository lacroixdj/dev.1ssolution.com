@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Bids request')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bids request
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/rfp/client/bidsrequest">Request / Bids</a></li>
        <li class="active">Bids Request</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">        
               <div class="col-xs-12">
                    <h2>
                        <i class="fa fa-globe"></i>@if($rfp->type == 'SR') Service Request #{{$rfp->id}} @else RFP #{{$rfp->id}} @endif
                    </h2>
                    <!-- TODO: Formato de fechas -->  
                    <div class="col-sm-3 col-xs-6">
                        <p><b>Emergency:</b>@if($rfp->emergency == 1) Yes @else No @endif</p>
                        <p><b>Visit:</b>@if($rfp->visit == 1) Yes @else No @endif</p>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <p><b>Service by:</b> {{date('m/d/Y', strtotime($rfp->service_date))}}</p>
                        <p><b>Response by:</b> {{date('m/d/Y', strtotime($rfp->response_date))}}</p>           
                    </div>
                    <div class="col-sm-5 col-xs-12">
                        <div class="form-group">
                            <div class="input-group pull-right">
                                <h3 class="margin-t-0">Bid amount $ <b>{{$rfp->pivot->bid_amount}}</b></h3>
                            </div>
                        </div>                        
                    </div>
                  
                </div>             
            </div>
            <!-- /.box-header -->
            <div class="box-body">                  
                <div class="col-md-6">
                    <h3 class="box-title"><i class="fa fa-list-alt"></i> {{$rfp->type}}
                    </h3>
                </div> 
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    
                </div> 
                <div class="clearfix"></div>
                
                <div class="invoice-info">
                    <!-- info col -->
                    <div class="col-sm-4 invoice-col">
                      From
                      <address>
                        <strong>{{$rfp->client->name}}</strong><br>
                        {{$rfp->location}}<br>
                        Phone: {{$rfp->client->phone}}<br>
                        Email: {{$rfp->client->email}}
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      To
                      <address>
                        <strong>{{$vendor->name}}</strong><br>
                        {{$vendorAddresses->address}}<br>
                        {{$vendorAddresses->address2}}<br>
                        Phone: {{$vendor->phone}}<br>
                        Email: {{$vendor->email}}
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                       <div class="pull-right">
                           @if($rfp->pivot->bid_amount== '0.00')  
                           @else 
                           <!-- /.form que hace el place del workorder -->  
                           {!! Form::open([

                                'method' => 'POST',
                                'url' => ['/workorder/create'],
                                'id' => 'workorder_create',
                                'data-parsley-validate' => '', 
                                'data-parsley-trigger' => '',
                                'role' => 'form'
                              ]) 

                            !!}
                            
                            {!! Form::hidden('rfpId', $rfp->id,
                                    [
                                      'id' => 'rfpId',
                                    ]) 
                            !!}
                            
                            {!! Form::hidden('vendorId', $vendor->id,
                                    [
                                      'id' => 'vendorId',
                                    ]) 
                            !!}

                            {!! Form::close() !!}  
                            <!-- /.end form -->  
                            <!-- /.end if -->  
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button id="convertWorkOrder" type="button" class="btn btn-block btn-success btn-flat"> Convert to Workorder</button>
                        <br>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="invoice-info">
                    <!-- info col -->
                    <div class="col-sm-12 invoice-col">
                      <address>
                        <strong>Description:</strong><br>
                          <p>{{$rfp->description}}</p>
                      </address>
                    </div>
                    <!-- /.col --> 
                    <!-- Docs col -->
                    <div class="col-sm-12 invoice-col">
                      <address>
                        <strong>Documents:</strong><br>
                          <p>{{$rfp->documents}}</p>
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 invoice-col">
                      <address>
                        <strong><li class="fa fa-map-marker"></li> Service location:</strong>
                        <br><br>
                        <input type="hidden" id="geocomplete">
                        <div id="geocomplete" class="map_canvas" ></div>
                      </address>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.End invoice-info -->  
             
            </div>
            <!-- /.box -->   
        </div> 
        <!-- /.Message area -->
        <!-- Chat box -->
        <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-comments-o"></i>
              <h3 class="box-title">Request Messages</h3>
            </div>
            <div class="box-body chat" id="chat-box">
              <!-- chat item -->
              <div class="item">
                <img src="{{ URL::asset('assets/img/user-a.png') }}" alt="user image" class="online">

                <p class="message">
                  <a href="#" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                   Vendor User
                  </a>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus aliquam, veniam fugiat! Architecto sequi sapiente, placeat tempora mollitia molestiae quam facere, deleniti dicta quas nobis assumenda! Ad iure quo unde.
                </p>
              </div>
              <!-- /.item -->
              <!-- chat item -->
              <div class="item">
                <img src="{{ URL::asset('assets/img/user-b.png') }}" alt="user image" class="offline">

                <p class="message">
                  <a href="#" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                    Client User
                  </a>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus ea, aliquid inventore temporibus eum soluta at a perferendis, fugiat nemo aut sapiente, necessitatibus, molestias. Accusantium porro, asperiores debitis aperiam sunt.
                </p>
              </div>
              <!-- /.item -->
              <!-- chat item -->
              <div class="item">
                <img src="{{ URL::asset('assets/img/user-b.png') }}" alt="user image" class="offline">

                <p class="message">
                  <a href="#" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                    Client User
                  </a>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus odit minus qui laboriosam, nemo totam dignissimos nulla atque eius voluptates error, a cumque quos optio eveniet, dolores consectetur molestiae! Esse!
                </p>
              </div>
              <!-- /.item -->
              <!-- chat item -->
              <div class="item">
                <img src="{{ URL::asset('assets/img/user-a.png') }}" alt="user image" class="online">

                <p class="message">
                  <a href="#" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                    Vendor User
                  </a>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, nobis nesciunt! Autem, ratione distinctio, cumque eaque dolore sit nulla, fugit esse blanditiis officiis maxime corrupti nisi cupiditate dolorem tempore, ut!
                </p>
              </div>
              <!-- /.item -->
            </div>
            <!-- /.chat -->
            <div class="box-footer">
              <div class="input-group">
                <input class="form-control" placeholder="Type message...">

                <div class="input-group-btn">
                  <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Send</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box (chat box) --> 
         <!-- /.End Message area -->  
        <!--/.col  -->
       </div>
       <!-- /.row -->  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.Modal Confirm -->  
    <div class="modal fade" id="modalConfrimRfp">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Notice</h4>
          </div>
          
          <div class="modal-body">
              <p>You are about to accept this service for a amount of $ {{$rfp->pivot->bid_amount}} <b id="bidConfirmLabel"></b></p> 
              <p>Are you sure? Once you updated so you can not modify.&hellip;</p>
          </div>
          
           <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat margin-r-5" data-dismiss="modal">Cancel</button>
                <button type="button" id="saveWorkorder" class="btn btn-success btn-flat">Accept</button>
           </div>
           
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  <!-- /.End Modal Confirm -->  
  
@endsection

<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<!-- /.google api -->  
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIGiifLZnwuks-f8E9otZOjZR2RtaZxxo&libraries=places"></script>

<script>

//Goolge map autocomplete
// Docs
//http://ubilabs.github.io/geocomplete/
$(function(){
        
    var options = {
      map: ".map_canvas"
    };

    $("#geocomplete").geocomplete(options)
      .bind("geocode:result", function(event, result){
        console.log("Result: " + result.formatted_address);
      })
      .bind("geocode:error", function(event, status){
        console.log("ERROR: " + status);
      })
      .bind("geocode:multiple", function(event, results){
        console.log("Multiple: " + results.length + " results found");
      });

});

    
XHRFormListener('#workorder_create');
        
//SLIMSCROLL FOR CHAT WIDGET
$('#chat-box').slimScroll({
    height: '250px'
});
    
    
$('#convertWorkOrder').click(function() {
     
    $('#modalConfrimRfp').modal('show');
});
    
    
$('#saveWorkorder').click(function() {
     
    console.log('submit');
                
    $("#workorder_create").submit();
            
    $('#modalConfrimRfp').modal('hide');
         
    $('#convertWorkOrder').attr('disabled', true); 
     
 });

$(document).ready(function() {
    //localizacion de Geocomplete
    $("#geocomplete").val("{{$rfp->location}}").trigger("geocode");
    var status = '{{$rfp->status}}';
    
    //TODO: Validar del lado del servidor esto
    if( status === 'workorder' || status === 'closed'){
        $('#convertWorkOrder').attr('disabled', true); 
    }
});

</script>



@endsection