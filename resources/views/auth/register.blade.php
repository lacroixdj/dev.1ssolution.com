@extends('layouts.app')

    @section('content')
        <!-- /.login-box -->  
<!--            <div class="login-box wow fadeInUp" data-wow-duration="1s" style="animation-delay: 200ms;">-->
            <div class="login-box vertical-center">
                <div class="box-square">
                    <!-- .login-logo -->  
                    <div class="login-logo">
                       <!-- /.if custom login -->
                         @if (session('logo')!='')
                             <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/custom-logos/'.session('logo').'.png') }}" class="img-responsive img-center"/>
                            </a>
                         @else
                            <!-- /.else -->    
                            <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center"/>
                                <b>One </b>Seamless Solution
                            </a>
                         @endif
                    </div>
                    <!-- /.login-logo -->
                
                    <!-- .login-box-body -->            
                    <div class="login-box-body">        
                        <p class="login-box-msg">Register new account</p>
                        
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs pull-right">
                                <li id="tab1" class="active"><a href="#tab_1" data-toggle="tab">Vendor Account</a></li>
                                <li id="tab2"><a href="#tab_2" data-toggle="tab">Client Account</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <!-- /.login form -->
                                        <form role="form" method="POST" action="{{ url('/register') }}" data-parsley-validate data-parsley-trigger="focusin focusout">    
                                            {{ csrf_field() }}
<!--                                            <p class="login-box-msg"><b>Vendor account</b></p>-->

                                            <!-- full name -->
                                                <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <input id="name" type="text" name="name" required="required" value="{{ old('name') }}" class="form-control" placeholder="Your first name">
                                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            <!-- /fullname -->


                                            <!-- email -->
                                                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <input id="email" type="email" name="email"  required="required" value="{{ old('email') }}" class="form-control" placeholder="Email">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            <!-- /email -->

                                            <!-- row -->
                                                <div class="row">
                                                    <!-- col -->
                                                        <div class="col-xs-8">
                                                            <!-- remember me -->
                                                                <div class="checkbox icheck">
                                                                    <label>
                                                                        <input type="checkbox" name="terms" required="required"> I agree to the <a href="/terms" target="_blank">terms</a>
                                                                    </label>
                                                                </div>
                                                            <!-- /.remember me -->
                                                        </div>
                                                    <!-- /col -->
                                                    <!-- /col -->
                                                        <!-- submit -->
                                                            <div class="col-xs-4">
                                                                <button type="submit" class="btn btn-primary btn-block btn-flat btn-custom">Register</button>
                                                            </div>
                                                        <!-- /submit -->
                                                    <!-- /.col -->
                                                </div>
                                            <!-- row -->
                                        </form>                
                                    <!--login form -->
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <form id="client" role="form" method="POST" action="{{ url('/register/registerClient') }}   " data-parsley-validate data-parsley-trigger="focusin focusout">    
                                            {{ csrf_field() }}
                                            <!-- <p class="login-box-msg"><b>Client account</b></p>-->

                                            <!-- full name -->
                                                <div class="form-group has-feedback {{ $errors->has('client-name') ? ' has-error' : '' }}">
                                                    <input id="client-name" type="text" name="client-name" value="{{ old('client-name') }}" class="form-control" placeholder="Your first name" required>
                                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                    @if ($errors->has('client-name'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('client-name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            <!-- /fullname -->


                                            <!-- email -->
                                                <div class="form-group has-feedback {{ $errors->has('client-email') ? ' has-error' : '' }}">
                                                    <input id="client-email" type="email" name="client-email" value="{{ old('client-email') }}" class="form-control" placeholder="Email" required>
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                    @if ($errors->has('client-email'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('client-email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            <!-- /email -->
                                            
                                            <!-- passcode -->
                                                <div class="form-group has-feedback {{ $errors->has('passcode') ? ' has-error' : '' }}">
                                                    <input id="passcode" type="password"  name="passcode" value="{{ old('passcode') }}" class="form-control" placeholder="Corporation passcode" required>
                                                    <span class="glyphicon glyphicon-lock  form-control-feedback"></span>
                                                    @if ($errors->has('passcode'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('passcode') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            <!-- /pascode -->


                                            <!-- row -->
                                                <div class="row">
                                                    <!-- col -->
                                                        <div class="col-xs-8">
                                                            <!-- remember me -->
                                                                <div class="checkbox icheck">
                                                                    <label>
                                                                        <input type="checkbox" name="terms" required> I agree to the <a href="/terms" target="_blank">terms</a>
                                                                    </label>
                                                                </div>
                                                            <!-- /.remember me -->
                                                        </div>
                                                    <!-- /col -->
                                                    <!-- /col -->
                                                        <!-- submit -->
                                                            <div class="col-xs-4">
                                                                <button type="submit" class="btn btn-primary btn-block btn-flat btn-custom">Register</button>
                                                            </div>
                                                        <!-- /submit -->
                                                    <!-- /.col -->
                                                </div>
                                            <!-- row -->
                                        </form>                
                                    <!--login form -->
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom --> 

                        <!-- social-auth-links 
                            <div class="social-auth-links text-center">
                                <p>- OR -</p>
                                <a href="#" class="btn btn-block btn-social btn-linkedin btn-flat"><i class="fa fa-linkedin"></i> Sign in using Linkedin</a>
                            </div>
                        <-- /.social-auth-links -->

                        <!-- action links -->    
                            <a href="{{ url('/login') }}">I already have a membership</a><br>
                          <!--   <a href="{{ url('/register') }}" class="text-center">Register a new membership</a> -->
                         <!-- /action links -->    
                    </div>
                    <!-- /.login-box-body -->
                </div><!-- /.box square -->   
            </div>
            <!-- /.login-box -->
        
        <!-- /.Custom Scripts  -->
        <script>
            $("#client").ready(function(){
                if ($("#client").children('.form-group').hasClass('has-error')){
                    if($("#tab_1").hasClass('active')){
                        $("#tab_1").removeClass('active');
                        $("#tab1").removeClass('active');
                        
                        $("#tab2").addClass('active');
                        $("#tab_2").addClass('active');
                    }
                 
                }else{
                }
            });
        </script>
        <!-- /.End Custom Scripts  -->  
    @endsection
