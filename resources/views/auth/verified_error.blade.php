@extends('layouts.app')

@section('title')
Activation Error
@endsection

@section('content')
<div class='row'>
<div class='col-lg-4'>
</div>

<div class='col-lg-4'>
    
<div class="x_panel" style="margin-top: 10%">
        
    <div class="x_title">
    <h2>Account Activation error!</h2>
           
    <div class="clearfix"></div>
    </div>   
    
        @if (count($errors) > 0)
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>    
        @endif
    
    
    
        <div class="x_content ">
        
        <div class=" alert  alert-error">    
        <i class="fa fa-times"> </i>  There was an error during  verification process, please try again or contact support team.
        </div> 
        
        </div>  <!--  content-->
        </div>  <!--  panel-->
    
   </div>
   <div class='col-lg-4'>
   </div>
</div>  <!-- row -->  
@endsection 