@extends('layouts.app')

@section('title')
User registration
@endsection

@section('content')

<!-- /. Modal Welcome message -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color:#222222">Account Registration</h4>
      </div>
      <div class="modal-body">
<!-- 	    <img src="img/1ssolution-logo.png" class="img-responsive img-center"/> -->
          <h3 class="text-left" style="color:#222222">
              <i class="fa fa-check" style="color:green"> </i>&nbsp;Your account has been successfully registered!
	    </h3>
          <br>
        <h4 class="text-justify" style="color:#222222">
            We have sent you an email with the activation instructions, please proceed to activate your account.      
        </h4>
          
          
      </div>
      <div class="modal-footer text-right">
          <button type="button" class="btn btn-flat btn-primary btn-lg" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
<!-- /.Modal welcome -->


<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#myModal2').modal('show');
        $('#myModal2').on('hidden.bs.modal',function(){
            window.location.href =  "  {{ url('/login') }}";
        });
    });
        
</script>
@endsection 