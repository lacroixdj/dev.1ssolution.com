@extends('layouts.app')

@section('title')
Account Activation
@endsection
@section('content')

<style type="text/css">
  	body {
            background-image: none !important;
            background-color: #367fa9 !important;
        }
  	
</style>

<div class="container hold-transition wizard-page fuelux">
    <div class="row">   
        <div class="col-sm-10 col-sm-offset-1">
             @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif   
            <div class="landing-header-wizard wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                <!-- /.login-logo -->
                <div class="logo-landing">
                   <div class="row">
                       <div class="col-sm-6 col-md-6 ">
                            <img src="{{ URL::asset('assets/img/1ssolution-logo-type.png') }}" class=" pull-left logo-letras"/>
                        </div>
                    </div>
                </div>
                <hr class="margin-b-0">

                <div class="login-box-body wizard-box">
                    <!-- /.Vendor wizard -->       
                    <div class="wizard wizard-vendor" data-initialize="wizard" id="myWizard">
                        <div class="steps-container">
                            <ul class="steps">
                                <li data-step="1" data-name="campaign" class="active">
                                    <span class="badge">1</span>User account
                                    <span class="chevron"></span>
                                </li>
                                <li data-step="2">
                                    <span class="badge">2</span>Parent corporation
                                    <span class="chevron"></span>
                                </li>
                                <li data-step="3" data-name="template">
                                    <span class="badge">3</span>Facility location
                                    <span class="chevron"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="actions">
                            <button type="button" class="btn btn-flat btn-primary btn-prev">
                                <span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
                            <button type="button" class="btn btn-primary btn-flat btn-next" data-last="Complete">Next
                                <span class="glyphicon glyphicon-arrow-right"></span>
                            </button>
                        </div>

                         <form method="POST" action="{{ url('/setCredentialsClient') }}" id="client-account" data-parsley-validate data-parsley-trigger="focusin focusout">
                            {!! csrf_field() !!}      
                            <div class="step-content">   
                                <div class="step-pane active sample-pane" data-step="1">
                                    <!-- /.User Acount -->            
                                    <div class="form-wizard" id="step1" data-parsley-validate data-parsley-trigger="focusin focusout">
                                            <h3 class=""><li class="fa fa-user"></li> User account information</h3>
                                            <div class="form-group has-feedback">
                                              <label for="">Email:</label>
                                              <input type="text" class="form-control" id="email" name="email"  value="{{ $user->email }}" readonly>
                                              <li class="fa fa-envelope form-control-feedback"></li>
                                            </div>
                                            <div class="form-group has-feedback">
                                              <label for="">First Name:</label>
                                              <input type="text" class="form-control" id="fName" name="name" value="{{ $user->name }}" required>
                                              <li class="fa fa-user form-control-feedback"></li>
                                            </div>
                                            <div class="form-group has-feedback">
                                              <label for="">Last Name:</label>
                                              <input type="text" class="form-control" id="lName" name="lastname" placeholder="" value="" required>
                                              <li class="fa fa-user form-control-feedback"></li>
                                            </div>
                                            <div class="form-group has-feedback">
                                              <label for="">Contact Phone:</label>
                                              <input type="text" class="form-control" id="userPhone" name="phone" placeholder="(000) 000-0000" data-mask="(000)000-0000" placeholder="" data-parsley-minlength="14" value="" required>
                                              <li class="fa fa-phone-square form-control-feedback"></li>
                                            </div>
                                            <div id="pwd-container">
                                                <div class="form-group has-feedback">
                                                  <label for="">Password:</label>
                                                  <input type="password" class="form-control" id="password" name="password" placeholder=" " data-parsley-minlength="6" value="" required>
                                                  <li class="fa fa-lock form-control-feedback"></li>
                                                </div>
                                                <div class="form-group has-feedback" style="">
                                                    <div class="pwstrength_viewport_progress"></div>
                                                </div>
                                            </div>
                                            <div class="form-group has-feedback">
                                              <label for="">Retype Password:</label>
                                              <input type="password" class="form-control" id="passConfirm" placeholder="" name="password_confirmation" data-parsley-equalto="#password" value="" required>
                                              <li id="pwmatch"  class="fa fa-remove form-control-feedback" style="color:#dd4b39;"></li>
                                            </div>
                                            <button id="btnNext1" class="btn btn-primary btn-block btn-flat">Next   <span class="glyphicon glyphicon-arrow-right"></span></button>
                                            <!-- /.End User Acount -->   
                                    </div>  
                                    <!-- /.End User Acount -->
                                </div> 
                                      
                                <div class="step-pane sample-pane"  data-step="2">
                                   <!-- /.Vendor Corporation Info -->
                                   <div class="form-wizard" id="step2" data-parsley-validate data-parsley-trigger="focusin focusout">
                                        <h3 class=""><li class="fa fa-building-o"></li> Parent Corporation Info.</h3>
                                        <!-- <form role="form" method="POST" action="{{ url('/setCredentials') }}" id="vendor-corporation">-->

                                            <div class="form-group">
                                              <label for="">Corporation name:</label>
                                              <input type="text" class="form-control" id="company" placeholder="" name="vendorname" value="{{ $corporation->name }}" required readonly>
                                            </div>
                                            <div class="form-group">
                                              <label for="">Corporation phone:</label>
                                              <input type="text" class="form-control" id="phone" name="vendorphone" placeholder="(000) 000-0000" data-parsley-minlength="14" value="{{ $corporation->phone }}" required readonly>
                                              <li class="fa fa-phone-square form-control-feedback"></li>
                                            </div>
                                            <div class="form-group">
                                              <label for="">Corporation email:</label>
                                              <input type="text" class="form-control" id="corporationEmail" name="corporationEmail" data-parsley-type="email" value="{{ $corporation->email }}" required readonly>
                                              <li class="fa fa-envelope form-control-feedback"></li>
                                            </div>
                                            <div class="form-group">
                                              <label for="">Corporation Web address:</label>
                                              <input type="text" class="form-control" id="webAddress" name="url" placeholder="" value="{{ $corporation->url }}" readonly>
                                              <li class="fa fa-globe form-control-feedback"></li>
                                            </div>   
                                            <div class="form-group">
                                               <div class="col-xs-6 padding-r-5">
                                                   <button id="btnBack" class="btn btn-primary btn-block btn-flat"><span  class="glyphicon glyphicon-arrow-left"></span>  Back</button>
                                               </div>
                                               <div class="col-xs-6 padding-l-5">
                                                   <button id="btnNext2" class="btn btn-primary btn-block btn-flat">Next   <span  class="glyphicon glyphicon-arrow-right"></span></button>
                                               </div>             
                                            </div>         
                                   </div>
                                   <!-- /.End Vendor Corporation Info -->    
                                </div>
                                <div class="step-pane sample-pane" data-step="3">
                                    <!-- /.Vendor Location Info -->
                                    <div class="form-wizard" id="step3" data-parsley-validate data-parsley-trigger="focusin focusout">
                                    <h3 class=""><li class="fa fa-map-marker"></li> Client Facility Location Information.</h3>
                                          <div class="form-group">
                                              <label for="">Address:</label>
                                               <input name="address" id="address" class="form-control" type="text" placeholder="Type your address" value=""  required/>
                                            </div>
                                            
                                            <div class="form-group">
                                              <label for="">Address 2:</label>
                                              <input type="text" class="form-control" id="address2" name="address2" placeholder="Type additional" value="" address>
                                            </div>
                                                                                
                                            <div class="form-group has-feedback">
                                              <label for="">Phone:</label>
                                              <input type="text" class="form-control" id="facilityPhone" name="facilityphone" placeholder="(000) 000-0000" data-mask="(000)000-0000" placeholder="" data-parsley-minlength="13" value="" required>
                                              <li class="fa fa-phone-square form-control-feedback"></li>
                                            </div>
                                          
                                              <div class="form-group">
                                              <label for="">Country:</label>
                                              {{ Form::select(
                                                  'country', 
                                                   Array(''=>'Select your country') + $countries, '231',
                                                        [
                                                            'id' => 'country',
                                                            'class' => 'form-control',
                                                            'required' => 'required',
                                                            'readonly' => 'readonly',
                                                            'disabled' => 'disabled',
                                                        ]
                                                  ) 
                                              }}
                                            </div>  

                                            <div class="form-group">
                                              <label for="">State:</label>
                                              {{ Form::select(
                                                  'state', 
                                                  Array(''=>'Select your state') + $states, '0',
                                                        [
                                                            'id' => 'state',
                                                            'class' => 'form-control',
                                                            'required' => 'required'
                                                        ]
                                                  ) 
                                                }} 
                                            </div>

                                            <div class="form-group">
                                              <label for="">City:</label>
                                              {{ Form::select(
                                                  'city', 
                                                  Array(''=>'Select your city'), '0',
                                                    [
                                                      'id' => 'city',
                                                      'class' => 'form-control',
                                                      'required' => 'required'
                                                    ] 
                                                  ) 
                                                }}   
                                            </div>

                                            <div class="form-group">
                                              <label for="">Metropolitan area:</label>
                                                {{ Form::select(
                                                        'metroarea', 
                                                        Array(''=>'Select your metropolitan area') + $metroareas, '0',
                                                        [
                                                            'id' => 'metroarea',
                                                            'class' => 'form-control',
                                                            'required' => 'required'
                                                        ]
                                                    ) 
                                                }}
                                            </div>

                                            <div class="form-group">
                                              <label for="">Zip / Postal code: </label>
                                              <input type="text"  class="form-control" id="zipCode" name="zip" placeholder="" value="" required>
                                            </div>

                                            <div class="form-group">
                                              <div class="col-xs-6 padding-r-5">
                                                   <button id="btnBack2" class="btn btn-primary btn-block btn-flat"><span  class="glyphicon glyphicon-arrow-left"></span>  Back</button>
                                              </div>
                                              <div class="col-xs-6 padding-l-5">
                                                  <button type="submit" class="btn btn-primary btn-block btn-flat">Complete</button>
                                              </div>       
                                            </div>
                                        </div>
                                        <!-- /.Vendor Location Info -->  
                                </div>
                            </div>
                        </form>    
                    </div>
                    <!-- /.End Vendor Wizard -->   
                    <hr/>
                    <!--<a href="" class="text-center padding-l-r-30 padding-t-b-30"><li class="fa fa-arrow-left"></li> Back to sign up</a>     -->
                </div>
                <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->

            <!-- /. Modal Welcome message -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" style="color:#222222">Account activation</h4>
                  </div>
                  <div class="modal-body">
            <!-- 	    <img src="img/1ssolution-logo.png" class="img-responsive img-center"/> -->
                      <h3 class="text-left" style="color:#222222">
                          <i class="fa fa-check" style="color:green"> </i>&nbsp; Congratulations your account has been activated!
                    </h3>
                      <br>
                    <h4 class="text-justify" style="color:#222222">You are one step closer to get into <b>One Seamless Solution</b> marketplace. Please, fill up your client profile completely.</h4>
                  </div>
                  <div class="modal-footer text-right">
                    <button type="button" class="btn btn-flat btn-primary btn-lg" data-dismiss="modal">Ok</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.Modal welcome -->

            <!-- /. Modal success -->
            <div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"  style="color:#222222">Information Done</h4>
                  </div>
                  <div class="modal-body">
            <!-- 	    <img src="img/1ssolution-logo.png" class="img-responsive img-center"/> -->
                    <h3 class="text-center"  style="color:#222222">
                       <i class="fa fa-check" style="color:green"> </i>&nbsp;  You have completed your client profile successfully!
                    </h3>
                    <h4 class="text-center"  style="color:#222222">
                         Please sign in...
                    </h4>
                  </div>
                  <div class="modal-footer text-center">
                    <button type="button" class="btn btn-flat btn-primary btn-lg" data-dismiss="modal">Ok </button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.Modal success -->

            <!-- /. Modal Error -->
            <div class="modal modal-danger fade" id="myModalFailure" tabindex="-1" role="dialog" aria-labelledby="myModalFailureLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalFailureLabel">Error</h4>
                  </div>
                  <div class="modal-body">
            <!--        <img src="img/1ssolution-logo.png" class="img-responsive img-center"/> -->
                        <h3 class="text-center" >
                           <i class="fa fa-exclamation-triangle"> </i>&nbsp;  There are some errors while procesing the request,  please check form values and try again.
                        </h3>
                        <ul id="errors_lists">
                    </div>
                  </div>
                  <div class="modal-footer text-center">
                    <button type="button" class="btn btn-flat btn-lg btn-outline btn-danger" data-dismiss="modal">Ok </button>
                  </div>
                </div>
            </div>
            <!-- /.Modal success -->

            <!-- /.Loader Spinner-->  
            <div class="loading hidden_elem"></div>
        </div>
    </div>    
</div>

<!-- /.google api -->  
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs9NXUhnOBIA5oxPA7biSjeoBxE9Q7csI&libraries=places"></script>    
                     -->
<!-- Page Scripts -->
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

<script type="text/javascript">       
jQuery(document).ready(function () {
    
    "use strict";
    // Inicializa las validaciones 
    $('#step1').parsley();
    $('#step2').parsley();  
    $('#step3').parsley();
    
        
    //valida el estado 1 y pasa al siguiente estado desde los botones inferiores
    $( "#btnNext1" ).click(function( event ) {
      event.preventDefault();
        if ($('#step1').parsley().validate()){
            $('#myWizard').wizard('next');
        }else{
        }
    });
    //Valida el estado 2 y pasa al siguiente estado desde los botones inferiores
    $( "#btnNext2" ).click(function( event ) {
      event.preventDefault();
        if ($('#step2').parsley().validate()){
            $('#myWizard').wizard('next');
        }else{
        }
    });
         
         //Valida los 2 primeros pasos desde los botones del Wizard
         $('#myWizard').on('changed.fu.wizard', function(e, data) {
                e.preventDefault();

                // Valida Paso 1
                if(data.step == '2' && $('#step1').parsley().validate() == true) {
                }
                else if(data.step == '2' && $('#step1').parsley().validate() == false) {
                     $('#myWizard').wizard('previous');
                }
                
                // Valida Pado 2
                else if(data.step == '3' && $('#step2').parsley().validate() == true) {
        
                }
                else if(data.step == '3' && $('#step2').parsley().validate() == false) {
                     $('#myWizard').wizard('previous');
                }else{
                    
                }
         });
    
        // evento wizard on complete valida paso 3
        $('#myWizard').on('finished.fu.wizard', function (evt, data) {     
            if($('#step3').parsley().validate() == true) {
                $( "#client-account" ).submit();
            }
        });
    
        // Botones de back en el formulario
        $( "#btnBack" ).click(function( event ) {
          event.preventDefault();
          $('#myWizard').wizard('previous');
        });
        $( "#btnBack2" ).click(function( event ) {
              event.preventDefault();
              $('#myWizard').wizard('previous');
        });
            
    
        $( "#client-account" ).on( "submit", function( event ) {
                event.preventDefault();
            
            if($('#step3').parsley().validate() == true) {
                   //$('#services_values').val($('#services').val());
                    //$('#attributes_values').val($('#attributes').val());
                    var form_data = $(this).serialize();
                    $.ajax({
                        method: "POST",
                        url: this.action,
                        data: form_data,
                        dataType: 'json',
                        encode: true,
                        success: function(data) {
                            $('#myModalSuccess').modal('show');
                        },
                        error: function(data) {
                            var errors = data.responseJSON; //this will get the errors response data.
                            var errorsHtml = '';
                            $.each(errors , function( key, value ) {
                                //console.log(key, value)
                                errorsHtml += '<li>' + key +': '+value+ '</li>'; //showing only the first error.
                            });
                            //errorsHtml += '</ul>';
                            $( '#errors_lists' ).html( errorsHtml ); 
                            $('#myModalFailure').modal('show');

                        }
                    })
                }else{
                    
                }
        });  
        

        $("body").removeClass("login-page");
        $("body").addClass("bg-blue");
        // descomentar para activet el modal
        $('#myModal').modal('show');       

        $('#myModalSuccess').on('hidden.bs.modal',function(){
            window.location.href =  "  {{ url('/login') }}";
        });

        // Password Enforcement
        var options = {};
        options.ui = {
            container: "#pwd-container",
            progressBarEmptyPercentage: 10,
            progressBarMinPercentage: 10,
            showVerdictsInsideProgressBar: true,
            colorClasses: ["danger", "danger", "warning", "warning", "success", "success"],
            viewports: {
                progress: ".pwstrength_viewport_progress",

            }
        };
        options.common = {
            //debug: true,
            onLoad: function () {
                $('#messages').text('');
            }
        };
        options.rules = {
            activated: {
                wordTwoCharacterClasses: true,
                wordRepetitions: true,

            }
        };
        $('#password').pwstrength(options);


        //Password match
        $("input[type=password]").keyup(function(){
            //alert('aqui');
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");

            if($("#password").val() == $("#passConfirm").val()){
                $("#pwmatch").removeClass("fa-remove");
                $("#pwmatch").addClass("fa-check");
                $("#pwmatch").css("color","#00A41E");
            }else{
                $("#pwmatch").removeClass("fa-check");
                $("#pwmatch").addClass("fa-remove");
                $("#pwmatch").css("color","#FF0004");
            }
        });
        //End Password Match

        // hide / show loader 
        jQuery('#changevisibility').click(function() {
            if(jQuery('.loading').hasClass('hidden_elem')) {
                jQuery('.loading').removeClass('hidden_elem');
            } else {
                jQuery('.loading').addClass('hidden_elem'); 
            }
        });


        // jquery Mask
        $('#userPhone').mask('(000) 000-0000');
        $('#phone').mask('(000) 000-0000');
        $('#zipCode').mask('00000-000');

        //$( "#attributes" ).selectpicker('refresh');
        //$( "#services" ).selectpicker('refresh');

       


        $('#country').change(function(){


            var country_id = $(this).val();
            var states_combo = $('#state');
            var cities_combo = $('#city');

            //states_combo.find('option').remove();     
            states_combo.find("option:gt(0)").remove();    
            //cities_combo.find('option').remove();
            cities_combo.find("option:gt(0)").remove();

            if (country_id == 0){
                states_combo.attr( "disabled", "disabled" );
                cities_combo.attr( "disabled", "disabled" );
                return;
            } 

            var countries_url = '{{ url('/json/countries') }}';
            var states_url = countries_url  + '/' + country_id + '/states';  
            
            $.ajax({
                method: "GET",
                url: states_url,
                dataType: 'json'
            }).done(function(data) {
                 //console.log(data);                 
                states_combo.removeAttr('disabled');        
                $.each( data, function( key, value ) {                    
                    states_combo.append($("<option />").val(key).text(value));
                });
            });

        });


        $('#state').change(function(){

            var state_id = $(this).val();
            var cities_combo = $('#city');
            cities_combo.find("option:gt(0)").remove();

            if (state_id == 0){
              cities_combo.attr( "disabled", "disabled" );
              return;
            } 

            var states_url = '{{ url('/json/states') }}';
            var cities_url = states_url +'/' + state_id + '/cities'; 

            $.ajax({
                method: "GET",
                url: cities_url,
                dataType: 'json'
            }).done(function(data) {
                //console.log(data);
                cities_combo.removeAttr('disabled');
                $.each( data, function( key, value ) {
                    cities_combo.append($("<option />").val(key).text(value));
                });
            });
        
        });


});
    
</script>

@endsection 
