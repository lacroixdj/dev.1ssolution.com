@extends('layouts.app')

    @section('content')
        <!-- /.login-box -->  
<!--            <div class="login-box wow fadeInUp" data-wow-duration="1s" style="animation-delay: 200ms;">-->
            <div class="login-box vertical-center">
                <!-- .login-logo -->
                <div class="box-square">
                    <div class="login-logo">
                        <!-- /.if custom login -->
                         @if (session('logo')!='')
                             <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/custom-logos/'.session('logo').'.png') }}" class="img-responsive img-center"/>
                            </a>
                         @else
                            <!-- /.else -->    
                            <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center"/>
                                <b>One </b>Seamless Solution
                            </a>
                         @endif
                        
                    </div>
                <!-- /.login-logo -->
                <!-- .login-box-body -->            
                    <div class="login-box-body">        
                        <p class="login-box-msg">Sign in to start your session</p>
                        <!-- /.login form -->
                            @if (session('reset-status'))
                            <div class="alert alert-success">
                                {{ session('reset-status') }}
                            </div>
                            @endif
                            <form id="login-form" role="form" method="POST" action="{{ url('/login') }}" data-parsley-validate>
                                {{ csrf_field() }}
                                <!-- email -->
                                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required>
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                <!-- /email -->
                                <!-- pass -->
                                    <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                <!-- ./pass -->
                                <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                            <div class="col-xs-8">
                                                <!-- remember me -->
                                                    <div class="checkbox icheck">
                                                        <label>
                                                            <input type="checkbox" name="remember">    Remember Me
                                                        </label>
                                                    </div>
                                                <!-- /.remember me -->
                                            </div>
                                        <!-- /col -->
                                        <!-- /col -->
                                            <!-- submit -->
                                                <div class="col-xs-4">
                                                    <button id="login-form-submit" type="submit" class="btn btn-primary btn-block btn-flat btn-custom">Sign In</button>
                                                </div>
                                            <!-- /submit -->
                                        <!-- /.col -->
                                    </div>
                                <!-- row -->
                            </form>                
                        <!--login form -->

                        <!-- social-auth-links 
                            <div class="social-auth-links text-center">
                                <p>- OR -</p>
                                <a href="#" class="btn btn-block btn-social btn-linkedin btn-flat"><i class="fa fa-linkedin"></i> Sign in using Linkedin</a>
                            </div>
                        <-- /.social-auth-links -->
                        
                        <!-- register-link -->
                            <div class="social-auth-links text-center">
                                <p>- OR -</p>
                                <a href="{{ url('/register') }}" class="btn btn-primary btn-block btn-flat btn-custom"> Register New Account</a>
                            </div>
                        <!-- /.register-link -->

                        <!-- action links -->    
                            <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
                          <!--   <a href="{{ url('/register') }}" class="text-center">Register a new membership</a> -->
                         <!-- /action links -->    
                    </div>
                <!-- /.login-box-body --> 
                </div> <!-- /.end box -->  
            </div>
        <!-- /.login-box -->
    @endsection
