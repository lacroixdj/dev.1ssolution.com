@extends('layouts.app')

    @section('content')
        <!-- /.login-box -->  
<!--            <div class="login-box wow fadeInUp" data-wow-duration="1s" style="animation-delay: 200ms;">-->
            <div class="login-box vertical-center">
                <div class="box-square">
                    <!-- .login-logo -->
                    <div class="login-logo">
                       <!-- /.if custom login -->
                         @if (session('logo')!='')
                             <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/custom-logos/'.session('logo').'.png') }}" class="img-responsive img-center"/>
                            </a>
                         @else
                            <!-- /.else -->    
                            <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center"/>
                                <b>One </b>Seamless Solution
                            </a>
                         @endif
                    </div>
                    <!-- /.login-logo -->
                    <!-- .login-box-body -->            
                    <div class="login-box-body">        
                        <p class="login-box-msg">Complete the form below to change the password</p>
                        <!-- /.password change form -->
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                            <form role="form" method="POST" action="{{ url('/password/reset') }}" data-parsley-validate data-parsley-trigger="focusin focusout">
                                {{ csrf_field() }}
                                <!-- email -->
                                   <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" name="email"  value="{{ $email or old('email') }}" class="form-control" placeholder="Email"  required readonly>
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                <!-- /email -->
                                <!-- pass -->
                                <div id="pwd-container">
                                        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" data-parsley-minlength="6" required>
                                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    <!-- /.passmeter -->
                                    <div class="form-group has-feedback" style="">
                                        <div class="pwstrength_viewport_progress"></div>
                                    </div>
                                    <!-- /.passmeter -->        
                                <!-- ./pass -->
                                </div>
                                 <!-- pass confirm -->
                                    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" data-parsley-equalto="#password" required>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                <!-- ./pass confirm -->
                                <!-- row -->
                                    <div class="row">
                                        <!-- submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-block btn-flat btn-custom"> Reset Password</button>
                                        </div>
                                        <!-- /submit -->
                                    </div>
                                <!-- row -->
                            </form>     
                            <br>           
                            <!--password change form -->
                            <a href="{{ url('/login') }}"><i class="fa fa-arrow-left"></i> Back to Login</a><br>
                    </div>
                    <!-- /.login-box-body -->
                </div> 
            </div>
        <!-- /.login-box -->
        
        <script type="text/javascript">
        jQuery(document).ready(function () {
            // Password Enforcement
            var options = {};
            options.ui = {
                container: "#pwd-container",
                progressBarEmptyPercentage: 30,
                progressBarMinPercentage: 30,
                showVerdictsInsideProgressBar: true,
                colorClasses: ["danger", "danger", "warning", "warning", "success", "success"],
                viewports: {
                    progress: ".pwstrength_viewport_progress",

                }
            };
            options.common = {
                //debug: true,
                onLoad: function () {
                    $('#messages').text('');
                }
            };
            options.rules = {
                activated: {
                    wordTwoCharacterClasses: true,
                    wordRepetitions: true,

                }
            };
            $('#password').pwstrength(options);
        });
        </script>
    @endsection
