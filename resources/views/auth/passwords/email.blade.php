@extends('layouts.app')

    @section('content')
        <!-- /.login-box -->  
<!--            <div class="login-box wow fadeInUp" data-wow-duration="1s" style="animation-delay: 200ms;">-->
            <div class="login-box vertical-center">
                <div class="box-square">
                    <!-- .login-logo -->
                    <div class="login-logo">
                        <!-- /.if custom login -->
                         @if (session('logo')!='')
                             <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/custom-logos/'.session('logo').'.png') }}" class="img-responsive img-center"/>
                            </a>
                         @else
                            <!-- /.else -->    
                            <a href="{{ url('/login') }}">
                                <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center"/>
                                <b>One </b>Seamless Solution
                            </a>
                         @endif
                    </div>
                    <!-- /.login-logo -->
                    <!-- .login-box-body -->            
                    <div class="login-box-body">        
                        <p class="login-box-msg">Enter your email or username below and we will send you a link to reset your password</p>
                        <!-- /.Pass reset form -->
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @else
                            <form role="form" method="POST" action="{{ url('/password/email') }}"
                               data-parsley-validate data-parsley-trigger="focusin focusout">
                                {{ csrf_field() }}
                                <!-- email -->
                                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required>
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                <!-- /email -->
                                <!-- /.captcha -->
                                <div class="form-group has-feedback {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                    <div id="captcha">
                                        {!! app('captcha')->display(); !!}
                                    </div>
                                    
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <!-- /.end captcha -->  
                                <!-- row -->
                                    <div class="row">
                                        <!-- submit -->
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-block btn-flat btn-custom">Send Password Reset Link</button>
                                        </div>
                                        <!-- /submit -->
                                    </div>
                                <!-- row -->
                            </form>
                            @endif        
                            <br>        
                            <!--End Pass reset form -->   
                            <a href="{{ url('/login') }}"><i class="fa fa-arrow-left"></i> Back to Login</a><br>
                    </div>
                    <!-- /.login-box-body -->
                </div> 
            </div>
        <!-- /.login-box -->
    @endsection
