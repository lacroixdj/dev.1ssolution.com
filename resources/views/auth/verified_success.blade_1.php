@extends('layouts.app')

@section('title')
Account Activation
@endsection
@section('content')
<div class="container">
    <div class='row'>
        <div class='col-lg-4'>
        </div>
        <div class='col-lg-4'>
            <div class="x_panel" style="margin-top: 5%">        
                <div class="x_title">
                    <h2> Account Activation</h2>         
                    <div class="clearfix"></div>
                    </div>    
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                </div>    
                <div class="x_content ">
                    <div class=" alert  alert-success">    
                        <i class="fa fa-check"> </i>  Account was activated successfully, now you must setup your passwords.    
                    </div> 
                </div>  <!--  content-->
            </div>  <!--  panel-->   
        </div>
        <div class='col-lg-4'>
        </div>
    </div> <!-- row -->  

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading panelcustom">Setup your passwords</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/setCredentials') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




</div> <!-- container-->  
@endsection 