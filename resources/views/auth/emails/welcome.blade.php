<h2>Hi {{$name}}, Welcome to Crossover News!</h2></br>

<h3>We are very glad for you membership in our community.</h3></br>

<p>Before you able to publish news articles, you must activate your account and set your password following this link:  <a href="{!! url('/verify', ['code'=>$verification_code]) !!}"> Activate Account</a>.<p>

If the link above is broken you can copy and paste this url on your browser: {!! url('/verify', ['code'=>$verification_code]) !!}

</br><b>Thanks for your time.</b>


