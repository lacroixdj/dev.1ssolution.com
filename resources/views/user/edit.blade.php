@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Vendor User Info')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">

    <h1>
      User Account
      <small>User Information</small>
    </h1>
    
    <ol class="breadcrumb">
      <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Account</li>
      <li class="active">User</li>
    </ol>
  
  </section>
  
  <section class="content">
  
    <div class="row">
      <div class="col-md-12">
        <!-- Box 1 -->
        <div class="box box-primary">

          <div class="box-header with-border">
            <h3 class="box-title"><li class="fa fa-user"></li> User</h3>
          </div>
          <!-- /.box-header -->
          
          <!-- form start -->
          {!! Form::model($user, 
            [
              'method' => 'POST',
              'url' => ['/account/user'],
              'id' => 'user_edit',
              'data-parsley-validate' => '', 
              'data-parsley-trigger' => 'focusin focusout',
              'role' => 'form'
            ]) 
          !!}
          <div class="box-body">

            <!-- / left column -->
            <div class="col-md-6">

              <!-- email -->
              <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : ''}}">
                <label for="email">Email:</label>                      
                {!! Form::email('email', null, 
                  [
                    'class' => 'form-control', 
                    'required' => 'required', 
                    'id' => 'email', 
                    'type'=>'email', 
                    'placeholder' => 'Email',
                    'readonly' => 'readonly',
                    'disabled' => 'readonly',
                  ]) 
                !!}
                <li class="fa fa-envelope form-control-feedback"></li>
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
              </div>
              <!-- /email -->

               <!-- phone -->
              <div class="form-group has-feedback {{ $errors->has('phone') ? 'has-error' : ''}}">
                <label for="phone">Phone:</label>                      
                {!! Form::text('phone', null, 
                  [ 
                    'class' => 'form-control', 
                    'required' => 'required', 
                    'id' => 'phone', 
                    'placeholder' => '(000) 000-0000', 
                    'data-mask' => '(000)000-0000', 
                    'data-parsley-minlength' => '14' 
                  ]) 
                !!}
                <li class="fa fa-phone form-control-feedback"></li>
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
              </div>
              <!-- /phone -->
            </div>
            <!-- / left column -->  

            <!-- right column -->  
            <div class="col-md-6">

                <!-- user name -->
              <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name:</label>
                {!! Form::text('name', null, 
                  [
                    'class' => 'form-control', 
                    'required' => 'required',  
                    'id' => 'name', 
                    'placeholder' => 'Name'
                  ]) 
                !!}
                <li class="fa fa-user form-control-feedback"></li>
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
              </div>
              <!-- /username -->

              <!-- user lastname -->
              <div class="form-group has-feedback {{ $errors->has('lastname') ? 'has-error' : ''}}">
                <label for="lastname">Last name:</label>
                {!! Form::text('lastname', null, 
                  [
                    'class' => 'form-control', 
                    'required' => 'required',  
                    'id' => 'name', 
                    'placeholder' => 'Last name'
                  ]) 
                !!}
                <li class="fa fa-user form-control-feedback"></li>
                {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
              </div>
              <!-- /lastname -->

              <!-- form buttons -->  
              <div class="form-group pull-right">                        
                <button type="button" href="#" onClick="window.location.reload()" class="btn btn-primary btn-flat">Cancel</button>
                {!! Form::submit('Update User', ['class' => 'btn btn-success btn-flat']) !!}
              </div>
              <!-- /form buttons -->

            </div>
            <!-- /right column -->


          </div>    
          <!-- /.box-body -->

          <div class="box-footer">                   
          </div>

          {!! Form::close() !!}

        </div>
        <!-- /.box -->
      </div>     
    </div>
    <!-- /.row user -->

    <!-- row password -->
    <div class="row">
      <div class="col-md-12">
        
        <!-- Box 1 -->
        <div class="box box-primary">

          <div class="box-header with-border">
            <h3 class="box-title"><li class="fa fa-lock"></li> Change password</h3>
          </div>
          <!-- /.box-header -->

          <!-- form start -->
          {!! Form::model($user, 
            [
              'method' => 'POST',
              'url' => ['/user/passwd'],  
              'id' => 'user_passwd_edit',              
              'data-parsley-validate' => '', 
              'data-parsley-trigger' => 'focusin focusout',
              'role' => 'form'
            ]) 
          !!}
          <div class="box-body">

            <div id="pwd-container">  
            <!-- / left column -->
            <div class="col-md-6">
              {!! Form::hidden('token', null) !!}
                
                <!-- pass -->
                 
                                         

              <!-- ./pass -->
                 <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">            
                  <label for="name">New password:</label>
                  <input id="password" type="password" class="form-control" name="password" placeholder="New password" data-parsley-minlength="6" required>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>                            

                   <!-- pass confirm -->
              <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="name">Confirm password:</label>
                <input id="password" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" data-parsley-equalto="#password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password_confirmation'))
                <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
              </div>
              <!-- ./pass confirm -->
            

            </div>
            <!-- / left column -->  

            <!-- right column -->  
            <div class="col-md-6">

              <!-- pass -->
                  

               
              <!-- ./pass -->

                <!-- /.passmeter -->
                <div class="form-group has-feedback" style="">
                <label for="name">Strength:</label> 
                  <div class="pwstrength_viewport_progress"></div>
                </div>
                <!-- /.passmeter --> 

                <div class="form-group has-feedback {{ $errors->has('current_password') ? ' has-error' : '' }}">            
                   <label for="name">Current password:</label> 
                  <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Password" data-parsley-minlength="6" required>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  @if ($errors->has('current_password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('current_password') }}</strong>
                    </span>
                  @endif
                </div>  

           

           

              <!-- form buttons -->
              <div class="form-group pull-right">  
                <button type="button" href="#" onClick="window.location.reload()" class="btn btn-primary btn-flat">Cancel</button>
                {!! Form::submit('Update Password', ['class' => 'btn btn-success btn-flat', 'id' => 'update_change_password']) !!}
              </div>
              <!-- /form buttons -->
            </div>
            <!-- /right column -->
              </div>

          </div>    
          <!-- /.box-body -->

          <div class="box-footer">                   
          </div>

          {!! Form::close() !!}

        </div>
        <!-- /.box -->
      </div>     
    </div>
    <!-- /.row -->


  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<script>
  $(document).ready(function() { 

    $('#user_edit').parsley();
    $('#user_passwd_edit').parsley();

    //Aplying validation mask to phone and zip code
    $('#phone').mask('(000) 000-0000');

    XHRFormListener('#user_edit');
    XHRFormListener('#user_passwd_edit');   


    var options = {};
    
    options.ui = {
      container: "#pwd-container",
      progressBarEmptyPercentage: 30,
      progressBarMinPercentage: 30,
      showVerdictsInsideProgressBar: true,
      colorClasses: ["danger", "danger", "warning", "warning", "success", "success"],
      viewports: {
        progress: ".pwstrength_viewport_progress",
      }
    };
    
    options.common = {
      //debug: true,
      onLoad: function () {
        $('#messages').text('');
      }
    };

    options.rules = {
      activated: {
        wordTwoCharacterClasses: true,
        wordRepetitions: true,

      }
    };
    
    $('#password').pwstrength(options);
  });
</script>
@endsection
