@extends('layouts.master')
   
<!-- /.Page Title -->           
@section('title', 'Vendor User Info')
    
@section('content')
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Account
        <small>User Information</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Account</li>
        <li class="active">User Info.</li>
      </ol>
    </section>
	<section class="content">
      <div class="row">
        <div class="col-md-12">
            <!-- Box 1 -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><li class="fa fa-user"></li> User Info.</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                <div class="box-body">
                      <!-- /.Columna izquierda -->  
                       <div class="col-md-6">
                           <div class="form-group has-feedback">
                              <label for="">Vendor No.</label>
                              <input type="text" class="form-control" id="vendorId" value="123456" required disabled>
                              <li class="fa fa-black-tie form-control-feedback"></li>
                            </div>
                           <div class="form-group has-feedback">
                              <label for="">User type</label>
                              <input type="text" class="form-control" id="vendorId" value="Admin" required disabled>
                              <li class="fa fa-sitemap form-control-feedback"></li>
                            </div>
                           <div class="form-group has-feedback">
                              <label for="">Email</label>
                              <input type="email" class="form-control" id="email" value="VendorEmail@mail.com" required disabled>
                              <li class="fa fa-envelope form-control-feedback"></li>
                            </div>
                           <div class="form-group has-feedback">
                              <label for="">First Name</label>
                              <input type="text" class="form-control" id="fName" placeholder="" required>
                              <li class="fa fa-user form-control-feedback"></li>
                            </div>
                           <div class="form-group has-feedback">
                              <label for="">Last Name</label>
                              <input type="text" class="form-control" id="lName" placeholder="" required>
                              <li class="fa fa-user form-control-feedback"></li>
                            </div>                    
                       </div>
                       <!-- /.Columna derecha -->  
                       <div class="col-md-6">
                            <div class="form-group has-feedback">
                              <label for="">Contact Phone</label>
                              <input type="text" class="form-control" id="userPhone" placeholder="" required>
                              <li class="fa fa-phone-square form-control-feedback"></li>
                            </div>
                            <div id="pwd-container">
                                <div class="form-group has-feedback">
                                  <label for="">Password</label>
                                  <input type="password" class="form-control" id="password" placeholder=" " required>
                                  <li class="fa fa-lock form-control-feedback"></li>
                                </div>

                                <div class="form-group has-feedback" style="">
                                    <div class="pwstrength_viewport_progress"></div>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                              <label for="">Retype Password</label>
                              <input type="password" class="form-control" id="passConfirm" placeholder="" required>
                              <li id="pwmatch"  class="fa fa-remove form-control-feedback" style="color:#dd4b39;"></li>
                            </div>
                            <div class="form-group pull-right">  
                                <button type="submit" class="btn btn-success btn-flat ">Update</button>
                                <button type="submit" class="btn btn-danger btn-flat">Delete</button>
                                <button type="submit" class="btn btn-primary btn-flat">Cancel</button>
                            </div>
                        </div>    
                </div>
                <!-- /.box-body -->
                    <div class="box-footer">

                    </div>
                </form>
              </div>
              <!-- /.box -->
        </div>     
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
            <!-- Box 2 -->
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><li class="fa fa-map-marker"></li> User Location.</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                      <!-- /.Columna izquierda -->  
                       <div class="col-md-6">
                            <!-- /.inicio de location -->
                            <div class="form-group">
                              <label for="">Type Adress</label>
                               <input id="geocomplete" class="form-control" type="text" placeholder="Type in an address" value="Texas" />
                            </div>

                            <div class="form-group">
                              <label for="">Street Addres</label>
                              <input name="street_address" type="text" class="form-control" id="addres1" placeholder="" required>
                            </div>
                            <div class="form-group">
                              <label for="">Address 2</label>
                              <input name="formatted_address" type="text" class="form-control" id="addres2" placeholder=" " required>
                            </div>

                            <div class="form-group">
                              <label for="">City</label>
                              <input name="locality" type="text" class="form-control" id="city" placeholder="" required>
                           </div> 
                       </div>
                       <!-- /.Columna derecha -->  
                       <div class="col-md-6">
                            <div class="form-group">
                                  <label for="">State</label>
                                  <select class="form-control" required>
                                        <option value="" selected disabled>Select your state</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">Dist of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                  </select>
                            </div>

                            <div class="form-group">
                              <label for="">Zip / Postal Code </label>
                              <input type="text"  name="postal_code" class="form-control" id="zipCode" placeholder="" required>
                            </div>

                            <div class="form-group">
                                  <label for="">Country</label>
                                  <select class="form-control" required disabled>
                                        </option><option value='Afghanistan' >Afghanistan</option><option value='Albania' >Albania</option><option value='Algeria' >Algeria</option><option value='American Samoa' >American Samoa</option><option value='Andorra' >Andorra</option><option value='Angola' >Angola</option><option value='Antigua and Barbuda' >Antigua and Barbuda</option><option value='Argentina' >Argentina</option><option value='Armenia' >Armenia</option><option value='Australia' >Australia</option><option value='Austria' >Austria</option><option value='Azerbaijan' >Azerbaijan</option><option value='Bahamas' >Bahamas</option><option value='Bahrain' >Bahrain</option><option value='Bangladesh' >Bangladesh</option><option value='Barbados' >Barbados</option><option value='Belarus' >Belarus</option><option value='Belgium' >Belgium</option><option value='Belize' >Belize</option><option value='Benin' >Benin</option><option value='Bermuda' >Bermuda</option><option value='Bhutan' >Bhutan</option><option value='Bolivia' >Bolivia</option><option value='Bosnia and Herzegovina' >Bosnia and Herzegovina</option><option value='Botswana' >Botswana</option><option value='Brazil' >Brazil</option><option value='Brunei' >Brunei</option><option value='Bulgaria' >Bulgaria</option><option value='Burkina Faso' >Burkina Faso</option><option value='Burundi' >Burundi</option><option value='Cambodia' >Cambodia</option><option value='Cameroon' >Cameroon</option><option value='Canada' >Canada</option><option value='Cape Verde' >Cape Verde</option><option value='Cayman Islands' >Cayman Islands</option><option value='Central African Republic' >Central African Republic</option><option value='Chad' >Chad</option><option value='Chile' >Chile</option><option value='China' >China</option><option value='Colombia' >Colombia</option><option value='Comoros' >Comoros</option><option value='Congo, Democratic Republic of the' >Congo, Democratic Republic of the</option><option value='Congo, Republic of the' >Congo, Republic of the</option><option value='Costa Rica' >Costa Rica</option><option value='Côte d&#039;Ivoire' >Côte d&#039;Ivoire</option><option value='Croatia' >Croatia</option><option value='Cuba' >Cuba</option><option value='Cyprus' >Cyprus</option><option value='Czech Republic' >Czech Republic</option><option value='Denmark' >Denmark</option><option value='Djibouti' >Djibouti</option><option value='Dominica' >Dominica</option><option value='Dominican Republic' >Dominican Republic</option><option value='East Timor' >East Timor</option><option value='Ecuador' >Ecuador</option><option value='Egypt' >Egypt</option><option value='El Salvador' >El Salvador</option><option value='Equatorial Guinea' >Equatorial Guinea</option><option value='Eritrea' >Eritrea</option><option value='Estonia' >Estonia</option><option value='Ethiopia' >Ethiopia</option><option value='Faroe Islands' >Faroe Islands</option><option value='Fiji' >Fiji</option><option value='Finland' >Finland</option><option value='France' >France</option><option value='French Polynesia' >French Polynesia</option><option value='Gabon' >Gabon</option><option value='Gambia' >Gambia</option><option value='Georgia' >Georgia</option><option value='Germany' >Germany</option><option value='Ghana' >Ghana</option><option value='Greece' >Greece</option><option value='Greenland' >Greenland</option><option value='Grenada' >Grenada</option><option value='Guam' >Guam</option><option value='Guatemala' >Guatemala</option><option value='Guinea' >Guinea</option><option value='Guinea-Bissau' >Guinea-Bissau</option><option value='Guyana' >Guyana</option><option value='Haiti' >Haiti</option><option value='Honduras' >Honduras</option><option value='Hong Kong' >Hong Kong</option><option value='Hungary' >Hungary</option><option value='Iceland' >Iceland</option><option value='India' >India</option><option value='Indonesia' >Indonesia</option><option value='Iran' >Iran</option><option value='Iraq' >Iraq</option><option value='Ireland' >Ireland</option><option value='Israel' >Israel</option><option value='Italy' >Italy</option><option value='Jamaica' >Jamaica</option><option value='Japan' >Japan</option><option value='Jordan' >Jordan</option><option value='Kazakhstan' >Kazakhstan</option><option value='Kenya' >Kenya</option><option value='Kiribati' >Kiribati</option><option value='North Korea' >North Korea</option><option value='South Korea' >South Korea</option><option value='Kosovo' >Kosovo</option><option value='Kuwait' >Kuwait</option><option value='Kyrgyzstan' >Kyrgyzstan</option><option value='Laos' >Laos</option><option value='Latvia' >Latvia</option><option value='Lebanon' >Lebanon</option><option value='Lesotho' >Lesotho</option><option value='Liberia' >Liberia</option><option value='Libya' >Libya</option><option value='Liechtenstein' >Liechtenstein</option><option value='Lithuania' >Lithuania</option><option value='Luxembourg' >Luxembourg</option><option value='Macedonia' >Macedonia</option><option value='Madagascar' >Madagascar</option><option value='Malawi' >Malawi</option><option value='Malaysia' >Malaysia</option><option value='Maldives' >Maldives</option><option value='Mali' >Mali</option><option value='Malta' >Malta</option><option value='Marshall Islands' >Marshall Islands</option><option value='Mauritania' >Mauritania</option><option value='Mauritius' >Mauritius</option><option value='Mexico' >Mexico</option><option value='Micronesia' >Micronesia</option><option value='Moldova' >Moldova</option><option value='Monaco' >Monaco</option><option value='Mongolia' >Mongolia</option><option value='Montenegro' >Montenegro</option><option value='Morocco' >Morocco</option><option value='Mozambique' >Mozambique</option><option value='Myanmar' >Myanmar</option><option value='Namibia' >Namibia</option><option value='Nauru' >Nauru</option><option value='Nepal' >Nepal</option><option value='Netherlands' >Netherlands</option><option value='New Zealand' >New Zealand</option><option value='Nicaragua' >Nicaragua</option><option value='Niger' >Niger</option><option value='Nigeria' >Nigeria</option><option value='Northern Mariana Islands' >Northern Mariana Islands</option><option value='Norway' >Norway</option><option value='Oman' >Oman</option><option value='Pakistan' >Pakistan</option><option value='Palau' >Palau</option><option value='Palestine, State of' >Palestine, State of</option><option value='Panama' >Panama</option><option value='Papua New Guinea' >Papua New Guinea</option><option value='Paraguay' >Paraguay</option><option value='Peru' >Peru</option><option value='Philippines' >Philippines</option><option value='Poland' >Poland</option><option value='Portugal' >Portugal</option><option value='Puerto Rico' >Puerto Rico</option><option value='Qatar' >Qatar</option><option value='Romania' >Romania</option><option value='Russia' >Russia</option><option value='Rwanda' >Rwanda</option><option value='Saint Kitts and Nevis' >Saint Kitts and Nevis</option><option value='Saint Lucia' >Saint Lucia</option><option value='Saint Vincent and the Grenadines' >Saint Vincent and the Grenadines</option><option value='Samoa' >Samoa</option><option value='San Marino' >San Marino</option><option value='Sao Tome and Principe' >Sao Tome and Principe</option><option value='Saudi Arabia' >Saudi Arabia</option><option value='Senegal' >Senegal</option><option value='Serbia' >Serbia</option><option value='Seychelles' >Seychelles</option><option value='Sierra Leone' >Sierra Leone</option><option value='Singapore' >Singapore</option><option value='Sint Maarten' >Sint Maarten</option><option value='Slovakia' >Slovakia</option><option value='Slovenia' >Slovenia</option><option value='Solomon Islands' >Solomon Islands</option><option value='Somalia' >Somalia</option><option value='South Africa' >South Africa</option><option value='Spain' >Spain</option><option value='Sri Lanka' >Sri Lanka</option><option value='Sudan' >Sudan</option><option value='Sudan, South' >Sudan, South</option><option value='Suriname' >Suriname</option><option value='Swaziland' >Swaziland</option><option value='Sweden' >Sweden</option><option value='Switzerland' >Switzerland</option><option value='Syria' >Syria</option><option value='Taiwan' >Taiwan</option><option value='Tajikistan' >Tajikistan</option><option value='Tanzania' >Tanzania</option><option value='Thailand' >Thailand</option><option value='Togo' >Togo</option><option value='Tonga' >Tonga</option><option value='Trinidad and Tobago' >Trinidad and Tobago</option><option value='Tunisia' >Tunisia</option><option value='Turkey' >Turkey</option><option value='Turkmenistan' >Turkmenistan</option><option value='Tuvalu' >Tuvalu</option><option value='Uganda' >Uganda</option><option value='Ukraine' >Ukraine</option><option value='United Arab Emirates' >United Arab Emirates</option><option value='United Kingdom' >United Kingdom</option><option value='United States' selected>United States</option><option value='Uruguay' >Uruguay</option><option value='Uzbekistan' >Uzbekistan</option><option value='Vanuatu' >Vanuatu</option><option value='Vatican City' >Vatican City</option><option value='Venezuela' >Venezuela</option><option value='Vietnam' >Vietnam</option><option value='Virgin Islands, British' >Virgin Islands, British</option><option value='Virgin Islands, U.S.' >Virgin Islands, U.S.</option><option value='Yemen' >Yemen</option><option value='Zambia' >Zambia</option><option value='Zimbabwe' >Zimbabwe</option>
                                  </select>
                            </div>
                            <br>
                            <div class="form-group pull-right">  
                                <button type="submit" class="btn btn-success btn-flat ">Update</button>
                                <button type="submit" class="btn btn-danger btn-flat">Delete</button>
                                <button type="submit" class="btn btn-primary btn-flat">Cancel</button>
                            </div>
                            <br>
                            <br>
                            
                        </div>
                        
<!--
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Please drag the marker to your position.</label>
                                <div class="map_canvas" id="map_canvas"></div>
                            </div>
                        </div>
-->
                        <!-- /.fin de location -->   
                        
                       </div>    
                  </div>
                  <!-- /.box-body -->
                </form>
              </div>
              <!-- /.box 2-->     
      </div><!-- /.end row 2 -->  
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
  
  
<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
    <script>
      $(function () {
        
            // Password Enforcement
            var options = {};
            options.ui = {
                container: "#pwd-container",
                progressBarEmptyPercentage: 10,
                progressBarMinPercentage: 10,
                showVerdictsInsideProgressBar: true,
                colorClasses: ["danger", "danger", "warning", "warning", "success", "success"],
                viewports: {
                    progress: ".pwstrength_viewport_progress",

                }
            };
            options.common = {
                //debug: true,
                onLoad: function () {
                    $('#messages').text('');
                }
            };
            options.rules = {
                activated: {
                    wordTwoCharacterClasses: true,
                    wordRepetitions: true,

                }
            };
            $('#password').pwstrength(options);


            //Password match
            $("input[type=password]").keyup(function(){
                //alert('aqui');
                var ucase = new RegExp("[A-Z]+");
                var lcase = new RegExp("[a-z]+");
                var num = new RegExp("[0-9]+");

                if($("#password").val() == $("#passConfirm").val()){
                    $("#pwmatch").removeClass("fa-remove");
                    $("#pwmatch").addClass("fa-check");
                    $("#pwmatch").css("color","#00A41E");
                }else{
                    $("#pwmatch").removeClass("fa-check");
                    $("#pwmatch").addClass("fa-remove");
                    $("#pwmatch").css("color","#FF0004");
                }
            });
            //End Password Match

          
      });
    </script>
@endsection
    