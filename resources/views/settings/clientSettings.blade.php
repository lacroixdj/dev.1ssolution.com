@extends('layouts.master')

<!-- /.Page Title -->           
@section('title', 'Client User Settings')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">

    <h1>
      Client User Settings
      <small></small>
    </h1>
    
    <ol class="breadcrumb">
      <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Settings</li>
    </ol>
  
  </section>
  
  <section class="content">
  
    <div class="row">
      <div class="col-md-12">
        <!-- Box 1 -->
        <!-- /.tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="pull-left header"><i class="fa fa-cog"></i></li>
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Messages</a></li>
                <li><a href="#tab_2-1" data-toggle="tab" aria-expanded="true">Appearance</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="page-header">Notification Control</h3>
                             <!-- form start -->
                              {!! Form::model($settings, [
                                'method' => 'POST',
                                'url' => ['/client/settings'],  
                                'id' => 'settings_upd',              
                                'data-parsley-validate' => '', 
                                'data-parsley-trigger' => 'focusout',
                                'role' => 'form'
                              ]) !!}
                                <!-- text input -->

                                <h1></h1>

                                <div class="col-md-4">
                                    <div class="form-group has-feedback {{ $errors->has('emailMessages') ? 'has-error' : ''}}">
                                      <label>{{$settings[0]->name}}</label><br>
                                      <input type="checkbox" name="emailMessages" @if($settings[0]->pivot->status == 1) checked @else @endif>
                                      {!! $errors->first('emailMessages', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback {{ $errors->has('smsMessages') ? 'has-error' : ''}}">
                                      <label>{{$settings[1]->name}}</label><br>
                                      <input type="checkbox" name="smsMessages" @if($settings[1]->pivot->status == 1) checked @else @endif>
                                      {!! $errors->first('smsMessages', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-4">

                                </div>   
                              {!! Form::close() !!}
                              <!-- /.end form --> 
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2-1">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="page-header">Custom Appearance </h3>
                            <h4 class="page-header">Blue </h4>
                            
                               
                               <!-- form start -->
                              {!! Form::model($settings, [
                                'method' => 'POST',
                                'url' => ['/client/skinSettings'],  
                                'id' => 'theme_upd',              
                                'data-parsley-validate' => '', 
                                'data-parsley-trigger' => 'focusout',
                                'role' => 'form'
                              ]) !!}
                              
                              <input type="text" id="skinSelect" name="skinSelect" value="" hidden="true">
    
                            
                                    
                             <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_default.png') }}" alt="">
                                            <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-ssolution">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-ssolution">Select</button>
                                            </div>
                                        </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_blue_light.png') }}" alt="">
                                             <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-blue-light">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-blue-light">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                         <div class="margin-b-20">
                                             <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_blue.png') }}" alt="">
                                              <div class="margin-t-10 padding-l-r-1-5-em">  
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-blue">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-blue">Select</button>
                                            </div>
                                         </div>
                                     </div>
                             
                                    
                                </div>
                            </div> 
                            
                            <h4 class="page-header">Green </h4>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                         <div class="margin-b-20">
                                             <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_green_sidebar.png') }}" alt="">
                                              <div class="margin-t-10 padding-l-r-1-5-em">  
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-green-sidebar">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-green-sidebar">Select</button>
                                            </div>
                                         </div>
                                    </div>  
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                         <div class="margin-b-20">
                                             <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_green_light.png') }}" alt="">
                                              <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-green-light">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-green-light">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_green.png') }}" alt="">
                                            <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-green">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-green">Select</button>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                             <h4 class="page-header">Purple </h4>
                             <div class="row">
                                <div class="col-xs-12">
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_purple_sidebar.png') }}" alt="">
                                             <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-purple-sidebar">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-purple-sidebar">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_purple_light.png') }}" alt="">
                                             <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-purple-light">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-purple-light">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                         <div class="margin-b-20">
                                             <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_purple.png') }}" alt="">
                                              <div class="margin-t-10 padding-l-r-1-5-em">  
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-purple">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-purple">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="row">
                                <div class="col-xs-12">
                                     <h4 class="page-header">Red </h4>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_red_sidebar.png') }}" alt="">
                                             <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-red-sidebar">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-red-sidebar">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                         <div class="margin-b-20">
                                             <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_red_light.png') }}" alt="">
                                              <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-red-light">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-red-light">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_red.png') }}" alt="">
                                            <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-red">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-red">Select</button>
                                            </div>
                                        </div>
                                     </div>
                                 </div>
                             </div>
                             
                             <h4 class="page-header">Yellow </h4>
                             <div class="row">
                                <div class="col-xs-12">
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_yellow_sidebar.png') }}" alt="">
                                             <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-yellow-sidebar">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-yellow-sidebar">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                        <div class="margin-b-20">
                                            <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_yellow_light.png') }}" alt="">
                                             <div class="margin-t-10 padding-l-r-1-5-em">
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-yellow-light">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-yellow-light">Select</button>
                                            </div>
                                         </div>
                                     </div>
                                     <div class="col-xs-12 col-sm-4 col-md-4">
                                         <div class="margin-b-20">
                                             <img class="img-responsive img-skin-thumb center-block" src="{{ URL::asset('assets/img/thumb_yellow.png') }}" alt="">
                                              <div class="margin-t-10 padding-l-r-1-5-em">  
                                                <button type="button" class="previewSkin btn btn-block btn-primary btn-flat" value="skin-yellow">Preview</button>
                                                <button type="button" class="selectSkin btn btn-block btn-success btn-flat" value="skin-yellow">Select</button>
                                            </div>
                                         </div>
                                    </div>
                                 </div>
                             </div>
                        {!! Form::close() !!}         
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <!-- /.End tabs -->  
      </div>     

    </div>
    <!-- /.row messages -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')
<script>
  
    $("[name='emailMessages']").bootstrapSwitch();
    
    $("[name='smsMessages']").bootstrapSwitch();

    XHRFormListener('#settings_upd');

    
    $("[type='checkbox']").on('switchChange.bootstrapSwitch', function () {
        
        //submit del form
        if ($('#settings_upd').parsley().validate()){
                
                $("#settings_upd").submit();
            
    
            }else{
            
            } 
        
    });

 /**
 * Loads a CSS file from the supplied URL
 * @param {String} url    The URL of the CSS file, if its relative
 *                        it will be to the current page's url
 *        {classToAdd}    The class to asign in body tag
 *
 * @return {HTMLElement}  The <link> which was appended to the <head>
 */
 function loadcss(url, classToAdd) {
   
     var head = document.getElementsByTagName('head')[0],
   
         link = document.createElement('link');
   
     link.type = 'text/css';
   
     link.rel = 'stylesheet';
   
     link.href = url;
   
     head.appendChild(link);
     
   //Remove de la clase skin del body y agregar la clase ssolution de prueba
   //
    var classList = $('body')[0].className.split(' '); 
    
    var classToRemove = classList[classList.length - 1];
     
     $( "body" ).fadeOut( 500, function() {
        
         document.body.className = document.body.className.replace(classToRemove, classToAdd);
     
     });
         
     $("body").fadeIn(500);
     
   return link;
 }
 
 //Event click for preview theme
 $( ".previewSkin" ).click(function() {
    
    var skin = $(this).attr("value");
    
    var url = '{{URL::asset('assets/css/skins/')}}';
     
    loadcss(url+'/'+skin+'.css', skin);
});


 XHRFormListener('#theme_upd');
 
 //Event click for select theme
 
 $( ".selectSkin" ).click(function() {
    
    console.log($(this).attr("value")+' selectcted');
     
    //Llamar por ajax la funcion que actualice el skin en el usuario 
     
    var newValue = $(this).attr("value");
    
    $('#skinSelect').val(newValue);
         
    $("#theme_upd").submit();
     
    var skin = $(this).attr("value");
    
    var url = '{{URL::asset('assets/css/skins/')}}';
     
    loadcss(url+'/'+skin+'.css', skin); 
     
});

</script>
@endsection
