@extends('layouts.error')

<!-- /.Page Title -->           
@section('title', 'Be right back.')

@section('content')

<div class="container">
   <div class="row">
       <div class="col-md-12 margin-t-40 margin-b-40 login-logo">
          <a href="{{ url('/') }}">
              <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center img-error-template"/>
              <b>One </b>Seamless Solution
          </a>
          <hr>
       </div>
   </div>
   <div class="row">
       <div class="col-md-6">
           <img src="{{ URL::asset('assets/img/503.png') }}" class="img-responsive img-center error-img"/>
       </div>
       <div class="col-md-6 error-message">
            <h1 class="error-text text-center">Be right back.</h1>
       </div>
   </div> 
   <hr>   
</div>
   
 


@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

@endsection
