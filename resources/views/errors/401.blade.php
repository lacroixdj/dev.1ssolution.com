@extends('layouts.error')

<!-- /.Page Title -->           
@section('title', '401 Unauthorized.')

@section('content')

<div class="container">
   <div class="row">
       <div class="col-md-12 margin-t-40 margin-b-40 login-logo">
          <a href="{{ url('/') }}">
              <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center img-error-template"/>
              <b>One </b>Seamless Solution
          </a>
          <hr>
       </div>
   </div>
   <div class="row">
       <div class="col-md-6">
           <img src="{{ URL::asset('assets/img/401.png') }}" class="img-responsive img-center error-img"/>
       </div>
       <div class="col-md-6 error-message">
            <h1 class="error-text text-center">Unauthorized</h1>
            <h1 class="error-text text-center"> {{ $HTTP_ErrorMessage or 'Sorry, looks like something went wrong.'}}</h1>
            <a href="{{ url('/') }}" class="btn btn-success btn-block btn-flat">Go to login page</a>
       </div>
   </div> 
   <hr>   
</div>
   
 


@endsection


<!-- /.Page Custom JavaScript -->     
@section('pageCustomJavaScript')

@endsection
