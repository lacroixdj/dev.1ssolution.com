@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Metroarea {{ $metroarea->id }}
        <a href="{{ url('metroarea/' . $metroarea->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Metroarea"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['metroarea', $metroarea->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Metroarea',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $metroarea->id }}</td>
                </tr>
                <tr><th> Name </th><td> {{ $metroarea->name }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
