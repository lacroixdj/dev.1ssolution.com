@extends('layouts.app')
@section('content')
    <!-- .login-box -->  
        <div class="login-box wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
            <!-- .login-logo -->
                <div class="login-logo">
                    <a href="{{ url('/home') }}">
                        <img src="{{ URL::asset('assets/img/1ssolution-logo.png') }}" class="img-responsive img-center"/>
                        <b>One </b>Seamless Solution
                    </a>
                </div>
            <!-- /.login-logo -->
            <!-- .login-box-body -->            
                <div class="login-box-body">
                    <hr />
                    <h1 class="text-center blue-onessolutio">Coming soon...</h1>
                    <h4 class="text-center blue-onessolutio">We are working on an amazing App!</h3>
                    <hr />
                    <!-- button type="submit" class="btn btn-large btn-primary btn-block btn-flat">SIGN IN DEMO</button -->
                    <a class="btn btn-large btn-primary btn-block btn-flat" href="{{ url('/login') }}">SIGN IN DEMO</a>
                </div>
            <!-- /.login-box-body -->
        </div>
    <!-- /.login-box -->  
<script src="//js.pusher.com/3.0/pusher.min.js"></script>
<script>

Pusher.log = function(msg) {
  console.log(msg);
};


var pusher = new Pusher("{{env("PUSHER_KEY")}}")
var channel = pusher.subscribe('test-channel');
channel.bind('test-event', function(data) {
  alert(data.text);
});
</script>
@endsection
