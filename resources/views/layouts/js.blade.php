       
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script> $.widget.bridge('uibutton', $.ui.button); </script>
            <!-- Bootstrap 3.3.6 -->
             <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
            <!-- iCheck -->
            <script src="{{ URL::asset('assets/plugins/bower_components/iCheck/icheck.min.js') }}"></script>
            <!-- Wow animation --> 
            <script src="{{ URL::asset('assets/plugins/bower_components/wow/dist/wow.min.js') }}"></script>
            <!-- dataTables -->
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/media/js/dataTables.bootstrap.min.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/AutoFill/js/dataTables.autoFill.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/AutoFill/js/autoFill.bootstrap.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/FixedColumns/js/dataTables.fixedColumns.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/RowReorder/js/dataTables.rowReorder.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/Responsive/js/dataTables.responsive.js') }}"></script>
            
            <script src="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/Responsive/js/responsive.bootstrap.min.js') }}"></script>
            
            <!-- SlimScroll -->
            <script src="{{ URL::asset('assets/plugins/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
            <!-- FastClick -->
            <script src="{{ URL::asset('assets/plugins/bower_components/fastclick/lib/fastclick.js') }}"></script>
            <!-- FuelUx -->
            <script src="{{ URL::asset('assets/plugins/bower_components/fuelux/dist/js/fuelux.js') }}"></script>
            <!-- Passmeter -->
            <script src="{{ URL::asset('assets/plugins/bower_components/pwstrength-bootstrap/dist/pwstrength-bootstrap.min.js') }}"></script>
            <!-- BootstrapMultiselect -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
            <!-- Geocomplete -->
            <script src="{{ URL::asset('assets/plugins/bower_components/geocomplete/jquery.geocomplete.js') }}"></script>
            <!-- jquery Mask -->
            <script src="{{ URL::asset('assets/plugins/bower_components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
            <!-- Bootstrap Switch -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
            <!-- validator -->
            <script src="{{ URL::asset('assets/plugins/bower_components/parsleyjs/dist/parsley.js') }}"></script>
            <!-- Bootstrap Star-rate -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap-star-rating/js/star-rating.js') }}"></script>
            <!-- Noty V2 -->
            <script src="{{ URL::asset('assets/plugins/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
            <!-- Noty V2 - Theme for ssolution -->
            <script src="{{ URL::asset('assets/plugins/bower_components/noty/js/noty/themes/ssolution.js') }}"></script>
            <!-- Google Maps Api -->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIGiifLZnwuks-f8E9otZOjZR2RtaZxxo&libraries=places"></script>  
            <!-- App -->
            <script src="{{ URL::asset('assets/js/app.js') }}"></script>
            <!-- Custom Js -->
            <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
            <!--  demo purposes -->
            <script src="{{ URL::asset('assets/js/demo.js') }}"></script>
            <!-- Laravel Elixir -->
            {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
           
    

