<!-- Left side column. Menu sidebar -->
    <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                  <!-- sidebar menu: : style can be found in sidebar.less -->
                  <ul class="sidebar-menu">
                    <li class="{{ Request::segment(2) === 'dashboard' ? 'active' : null }}">   
                        @if (session('user_type')=='vendor')                      
                            <a href="{{ url('vendor/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                        @elseif (session('user_type')=='client')
                            <a href="{{ url('client/dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a> 
                        @endif
                    </li>
                    
                     
                     <li class="{{ Request::segment(1) === 'account' ? 'active' : null }} treeview">
                      
                      <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Account</span>
                        
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                      </a>

                      <ul class="treeview-menu">
                            <li  class="treeview {{ Request::segment(2) === 'user' ? 'active' : null }}">
                                <a href="{{ url('account/user') }}"><i class="fa fa-circle-o"></i> User account</a>
                            </li>  
                        @if (session('user_type')=='vendor')                      
                            <li class="treeview {{ Request::segment(2) === 'vendor' ? 'active' : null }}">
                                <a href="{{ url('account/vendor') }}"><i class="fa fa-circle-o"></i> Vendor account</a>
                            </li>
                        @elseif (session('user_type')=='client')
                            <li class="treeview {{ Request::segment(2) === 'client' ? 'active' : null }}">
                                <a href="{{ url('account/client') }}"><i class="fa fa-circle-o"></i> Client account</a>
                            </li> 
                        @endif
                         
                      </ul>
                    </li>
                  
                    
                  
                     <li class="{{ Request::segment(1) === 'rfp' ? 'active' : null }} treeview">
                      
                       @if (session('user_type')=='vendor')                      
                        
                            <a href="{{ url('/rfp/vendor/bidsrequest') }}"><i class="fa fa-inbox"></i> <span>Requests / Bids</span></a>
                    
                       @else
                        
                            <a href="#">
                                <i class="fa fa-inbox"></i>
                                <span>Requests / Bids</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-right pull-right"></i>
                                </span>
                            </a>

                            <ul class="treeview-menu">
                            
                            @if (session('user_type')=='client')
                                <li class="treeview {{ Request::segment(2) === 'create' ? 'active' : null }}">
                                    <a href="{{ url('rfp/create') }}"><i class="fa fa-circle-o"></i> New request</a>
                                </li> 
                            @endif

                            @if (session('user_type')=='client')
                                <li class="treeview {{ Request::segment(3) === 'bidsrequest' ? 'active' : null }}">
                                    <a href="{{ url('/rfp/client/bidsrequest') }}"><i class="fa fa-circle-o"></i> View all</a>
                                </li>
                            @endif
                        
                            </ul>
                        
                        @endif
                        
                     
                      
                    </li>
                    
                     <li  class="{{ Request::segment(1) === 'workorder' ? 'active' : null }} treeview"> 
                       @if (session('user_type')=='vendor')                      
                            <a href="{{ url('/workorder/vendor/view') }}"><i class="fa fa-list"></i> <span>Workorders</span></a>
                       @elseif (session('user_type')=='client')
                            <a href="{{ url('/workorder/client/view') }}"><i class="fa fa-list"></i><span>Workorders</span></a> 
                       @endif
                      
                    </li>

                    @if (session('user_type')=='client')
                    <li class="{{ Request::segment(1) === 'vendors' ? 'active' : null }} treeview">
                      <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Vendors</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                      </a>
                      
                      <ul class="treeview-menu">
                       
                        
<!--                        <li><a href="#"><i class="fa fa-circle-o"></i> Invite</a></li>-->
                        
                        <li class="treeview {{ Request::segment(2) === 'viewall' ? 'active' : null }}">
                        
                            <a href="{{ url('/vendors/viewall') }}"><i class="fa fa-circle-o"></i> View all</a>
                        
                        </li>
                      
                        <li class="treeview {{ Request::segment(2) === 'favorites' ? 'active' : null }}">
                            
                            <a href="{{ url('/vendors/favorites') }}"><i class="fa fa-circle-o"></i> Favorites</a>
                            
                        </li>
                        
                         <li class="treeview {{ Request::segment(2) === 'invitations' ? 'active' : null }}">
                        
                            <a href="{{ url('/vendors/invitations') }}"><i class="fa fa-circle-o"></i> Invitations</a>
                        
                        </li>
                      </ul>
                    
                    </li>
                    @endif
                    
                    @if (session('user_type')=='vendor')
<!--
                    <li class="treeview">
                      <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Clients</span>
                        <i class="fa fa-angle-left pull-right"></i>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Favorites</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> View all</a></li>
                      </ul>
                    </li>
-->
                    @endif
                    

                   
                    
                    <li class="{{ Request::segment(2) === 'settings' ? 'active' : null }}">
                       @if (session('user_type')=='vendor')                      
                            <a href="{{ url('vendor/settings') }}"><i class="fa fa-cog"></i> <span>Settings</span></a>
                       @elseif (session('user_type')=='client')
                            <a href="{{ url('client/settings') }}"><i class="fa fa-cog"></i><span>Settings</span></a> 
                       @endif
                    </li>
                     
                    <li>
                      <a href="/logout">
                        <i class="fa fa-sign-out"></i> <span>Logout</span>
                      </a>
                    </li>

                  </ul>
                </section>
                <!-- /.sidebar -->
              </aside>  
    <!-- /. End Sidebar, menu principal app-->
