<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--laravel-default meta name="viewport" content="width=device-width, initial-scale=1"-->
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >-->
        <meta content="initial-scale=1" name="viewport">
        <meta name="_token" content="{!! csrf_token() !!}"/>

       
        <title>One Seamless Solution</title>
        <!-- Assets -->
            <!-- Css Styles -->
                <!-- Bootstrap 3.3.6 -->
                <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">  
                <!-- Font Awesome -->
                <link href="{{ URL::asset('assets/css/font-awesome.css') }}" rel="stylesheet">
                <!-- Ionicons -->
                <link href="{{ URL::asset('assets/css/ionicons.css') }}" rel="stylesheet">
                <!-- Theme style -->
                <link href="{{ URL::asset('assets/css/AdminLTE.css') }}" rel="stylesheet">           
                <!-- iCheck -->
                <link href="{{ URL::asset('assets/plugins/bower_components/iCheck/skins/square/blue.css') }}" rel="stylesheet">
                <!-- Animate Css -->
                <!--[if gte IE 9]><!-->
                <link href="{{ URL::asset('assets/plugins/bower_components/wow/css/libs/animate.css') }}" rel="stylesheet">   
                <!--<![endif]-->
                <!-- FuelUx -->
                <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/fuelux/dist/css/fuelux.css') }}">
                <!-- BootstrapMultiselect -->
                <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/bootstrap-select/dist/css/bootstrap-select.css') }}">
                <!-- BootstrapValidator -->
                <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/parsleyjs/src/parsley.css') }}">
                <!-- styleCustom -->
                <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">           
                <!-- Responsive -->
                <link href="{{ URL::asset('assets/css/responsive.css') }}" rel="stylesheet">
                 <!-- Skins  -->
<!--                <link href="{{ URL::asset('assets/css/skins/skin-blue.css') }}" rel="stylesheet">           -->
                @if (session('skin')!='')
                    <link href="{{ URL::asset('assets/css/skins/'.session('skin').'.css') }}" rel="stylesheet">           
                @endif           
                <!-- Laravel Elixir -->
                {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
            <!-- /Css Styles -->
            
            <!-- Favicons -->
                <link href="{{ URL::asset('assets/img/icon_32x32.png') }}" rel="shortcut icon" type="image/x-icon">
                <link href="{{ URL::asset('assets/img/icon_32x32.png') }}" rel="apple-touch-icon">
                <link href="{{ URL::asset('assets/img/icon_32x32@2x.png') }}" rel="apple-touch-icon" sizes="72x72">            
                <link href="{{ URL::asset('assets/img/icon_128x128@2x.png') }}" rel="apple-touch-icon" sizes="114x114" >
            <!-- /Favicons -->
            
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <!-- modernizr - custom -->
            <script src="{{ URL::asset('assets/js/modernizr-custom.js') }}"></script>
            <!-- jQuery 2.2.0 -->
            <script src="{{ URL::asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
            <!-- jQuery UI 1.11.4 -->
            <script src="{{ URL::asset('assets/plugins/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- /Assets -->
    </head>
    
    <body id="app-layout" class="hold-transition login-page">
        <!-- Body Content -->
            @yield('content')
        <!-- /Body Content -->
        
        <!-- JavaScripts -->
            
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script> $.widget.bridge('uibutton', $.ui.button); </script>
            <!-- Bootstrap 3.3.6 -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
            <!-- iCheck -->
            <script src="{{ URL::asset('assets/plugins/bower_components/iCheck/icheck.min.js') }}"></script>
            <!-- Wow animation --> 
            <script src="{{ URL::asset('assets/plugins/bower_components/wow/dist/wow.min.js') }}"></script>
            <!-- SlimScroll -->
            <script src="{{ URL::asset('assets/plugins/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
            <!-- FastClick -->
            <script src="{{ URL::asset('assets/plugins/bower_components/fastclick/lib/fastclick.js') }}"></script>       
            <!-- FuelUx -->
            <script src="{{ URL::asset('assets/plugins/bower_components/fuelux/dist/js/fuelux.js') }}"></script>
            <!-- Passmeter -->
            <script src="{{ URL::asset('assets/plugins/bower_components/pwstrength-bootstrap/dist/pwstrength-bootstrap.min.js') }}"></script>
            <!-- BootstrapSelect -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
            <!-- Geocomplete -->
            <script src="{{ URL::asset('assets/plugins/bower_components/geocomplete/jquery.geocomplete.js') }}"></script>
            <!-- jquery Mask -->
            <script src="{{ URL::asset('assets/plugins/bower_components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
            <!-- validator -->
            <script src="{{ URL::asset('assets/plugins/bower_components/parsleyjs/dist/parsley.js') }}"></script>
            <!-- Bootstrap Star-rate -->
            <script src="{{ URL::asset('assets/plugins/bower_components/bootstrap-star-rating/js/star-rating.js') }}"></script>
            <!-- App -->
            <script src="{{ URL::asset('assets/js/app.js') }}"></script>
            <!-- Custom Js -->
            <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
            <!--  demo purposes -->
            <script src="{{ URL::asset('assets/js/demo.js') }}"></script>
            <!-- Laravel Elixir -->
            {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
            <script>
                $(function () {
                  $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                  });
                });
            </script>
            <script type="text/javascript">
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
            </script>
        <!-- JavaScripts -->
    </body>
</html>
