<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--laravel-default meta name="viewport" content="width=device-width, initial-scale=1"-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
        <title>One Seamless Solution</title>
        <!-- Assets -->
            <!-- Css Styles -->
                <!-- Bootstrap 3.3.6 -->
                <link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet">  
                <!-- Font Awesome -->
                <link href="{{ URL::asset('assets/css/font-awesome.css') }}" rel="stylesheet">
                <!-- Ionicons -->
                <link href="{{ URL::asset('assets/css/ionicons.css') }}" rel="stylesheet">
                <!-- Theme style -->
                <link href="{{ URL::asset('assets/css/AdminLTE.css') }}" rel="stylesheet">           
                <!-- Skins  -->
                <link href="{{ URL::asset('assets/css/AdminLTE.css') }}" rel="stylesheet">           
                <!-- iCheck -->
                <link href="{{ URL::asset('assets/plugins/iCheck/square/blue.css') }}" rel="stylesheet">           
                <!-- Morris chart -->
                <link href="{{ URL::asset('assets/plugins/morris/morris.css') }}" rel="stylesheet">           
                <!-- jvectormap -->
                <link href="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">           
                <!-- Date Picker -->
                <link href="{{ URL::asset('assets/plugins/datepicker/datepicker3.css') }}" rel="stylesheet">           
                <!-- Daterange picker -->
                <link href="{{ URL::asset('assets/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">           
                <!-- bootstrap wysihtml5 - text editor -->
                <link href="{{ URL::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">           
                <!-- DataTables -->
                <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">           
                <!-- Animate Css -->
                <link href="{{ URL::asset('assets/css/animate.css') }}" rel="stylesheet">           
                <!-- styleCustom -->
                <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">           
                <!-- Responsive -->
                <link href="{{ URL::asset('assets/css/responsive.css') }}" rel="stylesheet">           
                <!-- Laravel Elixir -->
                {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
            <!-- /Css Styles -->
            
            <!-- Favicons -->
                <link href="{{ URL::asset('assets/img/icon_32x32.png') }}" rel="shortcut icon" type="image/x-icon">
                <link href="{{ URL::asset('assets/img/icon_32x32.png') }}" rel="apple-touch-icon">
                <link href="{{ URL::asset('assets/img/icon_32x32@2x.png') }}" rel="apple-touch-icon" sizes="72x72">            
                <link href="{{ URL::asset('assets/img/icon_128x128@2x.png') }}" rel="apple-touch-icon" sizes="114x114" >
            <!-- /Favicons -->
            
            <!-- Fonts -->            
            <!-- /Fonts -->
            
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            
        <!-- /Assets -->
    </head>
    
    <body id="app-layout">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Laravel
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

        <!-- JavaScripts -->
            <!-- jQuery 2.2.0 -->
            <script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
            <!-- jQuery UI 1.11.4 -->
            <script src="{{ URL::asset('assets/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script> $.widget.bridge('uibutton', $.ui.button); </script>
            <!-- Bootstrap 3.3.6 -->
            <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
            <!-- iCheck -->
            <script src="{{ URL::asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
            <!-- Wow animation --> 
            <script src="{{ URL::asset('assets/js/wow.min.js') }}"></script>
            <!-- dataTables -->
            <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
            <!-- SlimScroll -->
            <script src="{{ URL::asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
            <!-- ChartJS 1.0.1 -->
            <script src="{{ URL::asset('assets/plugins/chartjs/Chart.min.js') }}"></script>
            <!-- FastClick -->
            <script src="{{ URL::asset('assets/plugins/fastclick/fastclick.js') }}"></script>
            <!-- App -->
            <script src="{{ URL::asset('assets/js/app.min.js') }}"></script>
            <!-- Custom Js -->
            <script src="{{ URL::asset('assets/js/custom.js') }}"></script>
            <!--  demo purposes -->
            <script src="{{ URL::asset('assets/js/demo.js') }}"></script>
            <!-- Laravel Elixir -->
            {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
            <script>
                $(function () {
                  $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                  });
                });
            </script>
        <!-- JavaScripts -->
    </body>
</html>
