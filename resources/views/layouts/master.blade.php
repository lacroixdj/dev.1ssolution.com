<!-- /.system theme -->  
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title') | One Seamless Solution </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="initial-scale=1" name="viewport">
<!--  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">-->
  <!-- CSS
    ================================================== -->
    @include('layouts.css')
        <!-- Favicons -->
            <link href="{{ URL::asset('assets/img/icon_32x32.png') }}" rel="shortcut icon" type="image/x-icon">
            <link href="{{ URL::asset('assets/img/icon_32x32.png') }}" rel="apple-touch-icon">
            <link href="{{ URL::asset('assets/img/icon_32x32@2x.png') }}" rel="apple-touch-icon" sizes="72x72">            
            <link href="{{ URL::asset('assets/img/icon_128x128@2x.png') }}" rel="apple-touch-icon" sizes="114x114" >
        <!-- /Favicons -->
        
        <!-- modernizr - custom -->
        <script src="{{ URL::asset('assets/js/modernizr-custom.js') }}"></script>
        
        <!-- jQuery 2.2.0 -->
        <script src="{{ URL::asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
        
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL::asset('assets/plugins/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
        
        <!-- /Assets -->
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body class="hold-transition sidebar-mini {{ session('skin')}} ">
       
<div class="wrapper">
    
    <!-- /.Header: logo and user profile, shortcuts  -->  
    @include('layouts.header') 
    
    <!-- /. Sidebar: App navigation menu-->  
    @include('layouts.sidebar')
    
    
    <!-- /. Page Content -->  
    @yield('content')
    
    <!-- /.Footer Content --> 
    @include('layouts.footer') 
    
    
</div> <!-- /.End wrapper-->  

    <!-- /Body Content -->
    

    <!-- JavaScripts -->
    @include('layouts.js') 
    <!-- JavaScripts -->
        
    @yield('pageCustomJavaScript')
        
    </body>
</html>
<!-- /.End system theme -->   