        <!-- Bootstrap 3.3.6 -->
        <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet"> 
        <!-- Font Awesome -->
        <link href="{{ URL::asset('assets/css/font-awesome.css') }}" rel="stylesheet">
        <!-- Ionicons -->
        <link href="{{ URL::asset('assets/css/ionicons.css') }}" rel="stylesheet">
        <!-- Theme style -->
        <link href="{{ URL::asset('assets/css/AdminLTE.css') }}" rel="stylesheet">           
        <!-- Skins  -->
        <link href="{{ URL::asset(('assets/css/skins/'.session('skin').'.css')) }}" rel="stylesheet">           
        <!-- iCheck -->
        <link href="{{ URL::asset('assets/plugins/bower_components/iCheck/skins/square/blue.css') }}" rel="stylesheet">           
        <!-- DataTables -->
        <link href="{{ URL::asset('assets/plugins/bower_components/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
        
        <link href="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/Responsive/css/responsive.bootstrap.css') }}" rel="stylesheet">                  
        
        <link href="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/AutoFill/css/autoFill.bootstrap.css') }}" rel="stylesheet">
        
        <link href="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/FixedHeader/css/fixedHeader.bootstrap.css') }}" rel="stylesheet">    <link href="{{ URL::asset('assets/plugins/bower_components/datatables/extensions/FixedColumns/css/fixedColumns.bootstrap.css') }}" rel="stylesheet">                  
        
        <!-- Bootstrap Switch -->
        <link href="{{ URL::asset('assets/plugins/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet">      
        <!-- Animate Css -->
        <!--[if gte IE 9]><!-->
        <link href="{{ URL::asset('assets/plugins/bower_components/wow/css/libs/animate.css') }}" rel="stylesheet">   
        <!--<![endif]-->
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
        <!-- FuelUx -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/fuelux/dist/css/fuelux.css') }}">
        <!-- BootstrapMultiselect -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/bootstrap-select/dist/css/bootstrap-select.css') }}">
        <!-- Parsley Validator -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/parsleyjs/src/parsley.css') }}">
        <!-- Bootstrap Star-rate -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bower_components/bootstrap-star-rating/css/star-rating.min.css') }}">
        <!-- styleCustom -->
        <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">           
        <!-- Responsive -->
        <link href="{{ URL::asset('assets/css/responsive.css') }}" rel="stylesheet">           
        <!-- Laravel Elixir -->
        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
        <!-- /Css Styles -->
