       <!-- /.Footer -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2016.</strong> All rights reserved.
            <a href="#" data-toggle="modal" data-target="#privacyModal">Privacy Policy</a>    |     <a href="#" data-toggle="modal" data-target="#termsModal">Terms of Use</a>    |     <a href="#"  data-toggle="modal" data-target="#supportModal">Support</a>  
    </footer>  
    <!-- /.End Footer --> 
    <!-- General Modals -->
@include('modals/success')
@include('modals/error')
@include('modals/warning')
@include('modals/message')
@include('modals/privacy')
@include('modals/terms')
@include('modals/support')
@include('modals/vendorProfileInfo')
@include('modals/clientProfileInfo')

<!-- /General Modals -->

<!-- Custom Modals -->
@include('modals/vendor_profile_check')