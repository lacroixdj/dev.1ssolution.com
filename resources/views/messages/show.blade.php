@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Message {{ $message->id }}
        <a href="{{ url('messages/' . $message->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Message"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['messages', $message->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Message',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $message->id }}</td>
                </tr>
                <tr><th> Conversation Id </th><td> {{ $message->conversation_id }} </td></tr><tr><th> User Id </th><td> {{ $message->user_id }} </td></tr><tr><th> Socket Id </th><td> {{ $message->socket_id }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
