<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'phone', 
        'email',
        'type',
        'status',
        'verification_code',
    ];
    
    
    public function user(){
        return $this->belongsTo(User::class);
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
//    protected $hidden = [
//        'password', 'remember_token',
//    ];
    
    
}
