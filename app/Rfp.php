<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rfp extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rfps';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [

        'client_id',  'user_id', 'industry_id',  'number', 

        'status', 'emergency', 'description', 

        'location', 'service_date', 'response_date', 

        'type', 'visit', 'details', 'documents'
    ];


    protected $dates = [

        'created_at', 'updated_at', 'deleted_at',
        'service_date', 'response_date'

    ];

    //belongs to user 
    public function user(){
        return $this->belongsTo(User::class);
    }


    //belongs to client 
    public function client(){
        return $this->belongsTo(Client::class);
    }

    //belongs to industry 
    public function industry(){
        return $this->belongsTo(Industry::class);
    }

    //Many to Many relationship with rfp_vendors
    public function vendors(){

        return $this->belongsToMany(Vendor::class)
        
            ->withPivot('bid_amount', 'status')
        
            ->withTimestamps();
    }

    //Many to Many relationship with rfp_service
    public function services(){

        return $this->belongsToMany(Service::class)

            ->withTimestamps();
    } 


    //Many to Many relationship with rfp_vendors
    public function vendors_responses(){

        return $this->belongsToMany(Vendor::class)
        
            ->wherePivotIn('status', ['bid','workorder','closed']);
    }


    //Number correlative mutator
    public function setNumberAttribute($value){


        $result = Rfp::where('client_id', '=', $value) 
            
            ->orderBy('id', 'desc')
            
            ->orderBy('number', 'desc')

            ->first();

        if($result) $number = ($result->number+1);
        else $number = 1;

        $this->attributes['number'] = $number;

    }  
    
}
