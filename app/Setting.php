<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [

         'name', 'group', 'status'
    ];
    
//     public function users(){
//    	return $this->belongsToMany('\App\User','setting_user')->withPivot('setting_id','status'); 
//	} 
    
    //Many to Many relationship with setting_users
    public function users(){

        return $this->belongsToMany(User::class)
        
            ->withPivot('setting_id', 'user_id')
        
            ->withTimestamps();
    }  
}
