<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vendors';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name','industry_id', 'metroarea_id', 
        'phone', 'sms_address', 'mms_address','url', 'email','emergency','rate'
    ];
    
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function industry(){
        return $this->belongsTo(Industry::class);
    }

    public function services(){
        return $this->belongsToMany('App\Service');
    }
    
    public function attributes(){
        return $this->belongsToMany('App\Attribute');
    }

    public function addresses(){
        return $this->hasMany(VendorAddresses::class);
    }
    
    public function metroarea(){
        return $this->belongsTo(Metroarea::class);
    }
    

    //Many to Many relationship with rfp_vendors
    public function rfps(){

        return $this->belongsToMany(Rfp::class) 
            
            ->withPivot('bid_amount', 'status')
            
            ->withTimestamps();
    }
    
    public function workorders(){
        return $this->hasMany(Workorder::class);
    }

    public function conversations(){
        return $this->hasMany(Conversation::class);
    }
    
    //has many conversation in reverse order
    public function last_conversations(){
    
        return $this->hasMany(Conversation::class)->latest();
    
    }


}
