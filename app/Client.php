<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = ['name', 'corporation_id', 'metroarea_id', 'phone', 'sms_address', 'mms_address', 'url', 'email'];
    
    
    public function user(){
        return $this->belongsTo(User::class);
    }

    //belongsto corporation 
    public function coporation(){
        return $this->belongsTo(Coporation::class);
    }
    
    //has many Adresses
    public function addresses(){
        return $this->hasMany(ClientAddress::class);
    }

    //has many rfps
    public function rfps(){
        return $this->hasMany(Rfp::class);
    }
    
    //has many workorders
    public function workorders(){
        return $this->hasMany(Workorder::class);
    }

    //has many conversations
    public function conversations(){
    
        return $this->hasMany(Conversation::class);
    
    }

    //has many conversation in reverse order
    public function last_conversations(){
    
        return $this->hasMany(Conversation::class)->latest();
    
    }

   
    public function metroarea(){
        return $this->belongsTo(Metroarea::class);
    }
    
    //Many to Many relationship with vendors
    public function vendors_favorites(){
        
        return $this->belongsToMany(Vendor::class)
            
            ->withPivot('client_id', 'vendor_id')
            
            ->withTimestamps();
    }
    

}
