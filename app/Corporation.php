<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'corporations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'passcode', 'phone', 'email', 'url','corporation_type'];
}
