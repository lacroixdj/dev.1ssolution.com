<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname','phone', 
        'email','sms_address', 'mms_address', 'password','verification_code',
        'verified', 'haslogged'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function vendors(){
        return $this->hasMany(Vendor::class);
    }
    
    public function clients(){
        return $this->hasMany(Client::class);
    }
    
    public function rfps(){
        return $this->hasMany(Rfp::class);
    }
    
    public function supportTickets(){
        return $this->hasMany(SupportTickets::class);
    }
    
    //Many to Many relationship with setting_users
    public function settings(){
        
        return $this->belongsToMany(Setting::class) 
            
            ->withPivot('setting_id', 'status', 'setting_value')
            
            ->withTimestamps();
    }
    
    public function invites(){
        
        return $this->hasMany(Invite::class);
    
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function last_messages(){
    
        return $this->hasMany(Message::class)->latest();
    
    }


    
}
