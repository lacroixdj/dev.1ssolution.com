<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model {
   
    protected $table = 'industries';

    public function vendors(){
        return $this->hasMany(Vendor::class);
    }

    public function attributes(){
        return $this->hasMany(Attribute::class);
    }

    public function services(){
        return $this->hasMany(Service::class);
    }
}
