<?php

namespace App\Jobs;


use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use Log;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class Postman extends Job implements ShouldQueue {
    use InteractsWithQueue, SerializesModels;

    public $data = array();
        
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data) {

        $this->data = $data;
        
        // PREPARING DATA:
        if(!empty($data['mode']) && $data['mode']=='text') {

            $this->data['view'] = ((!empty($data['view'])) ? $data['view'] :  'emails.sms');

            $this->data['container'] = array('text' => $data['view']);

        }else{

            $this->data['container'] = ((!empty($data['view'])) ? $data['view'] :  'emails.plain');
        }

                    
        $this->data ['email_from'] =  ((!empty($data['email_from'])) ? $data['email_from'] :  'info@1ssolution.com');

        $this->data ['sender_name'] = ((!empty($data['sender_name'])) ? $data['sender_name'] :  'One Seamless Solution');      

        $this->data ['email_to'] = $data['email_to'];

        $this->data ['receiver_name'] =$data['receiver_name'];
        
        $this->data ['subject'] = ((!empty($data['subject'])) ? $data['subject'] :  'Notification - One Seamless Solution.');        
      
        //Log::info($this->data);
    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle () {
       
        //Using queues is better
        $data = $this->data;

        Mail::send($data['container'] , $data, function($message) use ($data)  {
                
                $message->from($data['email_from'], $data['sender_name']);
                
                $message->subject($data['subject']);
                
                $message->to($data['email_to']);
        
        });  

        Log::info('Mail sent successfully');
        
    }

}


