<?php

namespace App;

use \Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    
        'conversation_id', 
    
        'user_id',

        'channel', 
    
        'socket_id', 
    
        'status',

        'text',

    ];

    protected $dates = [

        'created_at', 'updated_at'

    ];

    public function conversation(){
    
        return $this->belongsTo(Conversation::class);
        
    }

    public function user(){
        
        return $this->belongsTo(User::class);
    
    }


    public function getCreatedAtAttribute($value){

        return Carbon::parse($value)->diffForHumans();
    
    }

    public function getUpdatedAtAttribute($value){

        return Carbon::parse($value)->diffForHumans();
    
    }

}
