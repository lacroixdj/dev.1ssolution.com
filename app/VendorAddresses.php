<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorAddresses extends Model {
    protected $table = 'vendor_addresses';
    protected $fillable = [
    	'vendor_id', 'city_id','address','address2',
    	'zip','phone', 'main_address'
    ];

    public function vendor(){
        return $this->belongsTo(Vendor::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

}
