<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event){
       
        //Checking the user type
        $user_type = "";
        if(!empty(  $event->user->vendors()->first()->id)){
            $user_type='vendor';
        } else if (!empty(  $event->user->clients()->first()->id)){
            $user_type='client';
        }
        
        session(['user_type' => $user_type, 'haslogged' => $event->user->haslogged  ]);
        //dd(session()->all());
        
         //Checking if the user has logged for first time or not 
        if($event->user->haslogged!=1){
            $event->user->haslogged=1;
            $event->user->save();
        }

        //Set the skin settings in session
        
        //getSettingValue esta en helpers.php
        
        session()->put('skin', getSettingValue(3, $event->user->id)[0]);   
        
        session()->put('logo', getSettingValue(3, $event->user->id)[1]);
        
        session()->put('corporationName', getSettingValue(3, $event->user->id)[2]);   
        
        

    }
}

