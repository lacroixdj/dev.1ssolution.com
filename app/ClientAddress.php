<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientAddress extends Model
{
    protected $table = 'client_addresses';
    protected $fillable = [
    	'client_id', 'city_id','address','address2',
    	'zip','phone', 'main_address',
    ];
    
     public function client(){
        return $this->belongsTo(Client::class);
    }
    
     public function city(){
        return $this->belongsTo(City::class);
    }
}