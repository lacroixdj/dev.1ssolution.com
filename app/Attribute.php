<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model {

    protected $table = 'attributes';    
    
    public function industry(){
        return $this->belongsTo(Industry::class);
    }
        
    public function vendors(){
        return $this->belongsToMany(Vendor::class);
    }   
}
