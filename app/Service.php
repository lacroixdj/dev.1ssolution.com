<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {
    
    protected $table = 'services';

    public function industry(){
        return $this->belongsTo(Industry::class);
    }
    
    public function vendors(){
        return $this->belongsToMany(Vendor::class);
    }


    public function rfps(){

        return $this->belongsToMany(Rfp::class)

            ->withTimestamps();
    }
}
