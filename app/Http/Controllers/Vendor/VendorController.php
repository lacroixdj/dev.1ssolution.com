<?php

namespace App\Http\Controllers\Vendor;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Vendor;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

//custom
use App\Industry;
use App\Attribute;
use App\Service;
use App\Country;
use App\State;
use App\City;
use App\Metroarea;


class VendorController extends Controller {

    /**
    * Class constructor.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function __construct(){
         
        $this->middleware('auth');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit(){

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        }

       if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No vendor account found");
            
            abort(403, "No vendor account found");   
        }

        

        $industries = Industry::all()->sortBy('name')->lists('name','id')->toArray();
        
        $attributes = Attribute::where('industry_id', $vendor->industry_id)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $services = Service::where('industry_id', $vendor->industry_id)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $countries =  Country::all()->sortBy('name')->lists('name','id')->toArray();
        
        $states =     State::where('country_id', 231)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $cities =     City::where('state_id', $vendor->addresses()->first()->city->state->id)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $metroareas = Metroarea::all()->sortBy('name')->lists('name','id')->toArray();

        return view('vendor.edit', compact('vendor','industries','attributes', 'services','countries','states','cities','metroareas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update(Request $request){
        //Validating
        
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
            
        }


        $this->validate($request, [ 
        
            'name' => 'required', 
            'phone' => 'required', 
            'email' => 'required', 
            'services' => 'required' 
        
        ]);

        $data = [ 

            'name' => $request->name, 
            'phone' => $request->phone, 
            'metroarea_id' => $request->metroarea_id, 
            'email' => $request->email,
            'url' => $request->url,
            'emergency' => $request->emergency,
        
        ];

        //Selecting
        //$vendor = Vendor::findOrFail($id);
       
        //Updating
        $result = $vendor->update($data);

        if($result){

            //Validating services
            if (!empty($request->services)) $services = $request->services;
            else $services = array();

            //Validating attributes
            if (!empty($request->attributess)) $attributes = $request->attributess;
            else $attributes = array();


            $vendor->services()->sync($services);
            $vendor->attributes()->sync($attributes);

            Session::flash('flash_message', 'Vendor updated!');


            $response = [
            
                'title' => 'Company information',
                'message' => 'Success! You have updated your company information',
                'text' => 'Background verification process would be applied',
            
            ];   
        
            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error updating the vendor'], 422);
        }        
        
    }


    /**
     *
     * @param  int  $id
     *
     * @return void
     */
    public function updateAddress(Request $request){
       
       if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 404);
            
        }
       


       //Validating
       $this->validate($request, [

            'city' => 'required', 
            'address' => 'required', 
            'zip' => 'required' 

        ]);

       //Selecting
       //$vendor = Vendor::findOrFail($id);
       
       //dd($request->all());
       $data= [

           'city_id' => $request->city, 
           'address' => $request->address, 
           'address2' => $request->address2,
           'zip' => $request->zip
       ];

        $result = $vendor->addresses()->first()->update($data);

        if($result) {

            Session::flash('flash_message', 'Vendor updated!');
            
            $response = [

                'title' => 'Company Location',
                'message' => 'Success! You have updated your company location',
                'text' => '',

            ];   
        
            return response()->json($response, 200);
            //return redirect('vendor');
        } else {

            return response()->json(['error_message' => 'There was an error updating the vendor address'], 422);
        }        
    }
  
    

    public function vendorProfile(Request $request)
    {
        
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client account found"], 403);
            
        }
        //Query.
       
        $vendor = Vendor::where('id', '=', $request->vendorId)
                    
                    ->with('industry','services','attributes')
                    
                    ->with('metroarea')
                    
                    ->with('user')
                    
                    ->firstOrFail();
        
        //Main Address.
        
        $vendor_address = $vendor->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
        
        //Check the vendor is favorited for this client.
        
        $vendor_favorited = $client->vendors_favorites()->where('vendor_id', '=', $request->vendorId)->get();

        if (!$vendor_favorited->isEmpty()){
            
            $vendor_favorited = '1';
        
        }else{
            
            $vendor_favorited = '0';
        
        }

        //foreach de services.
        
        $service_all = null;
        
        $i = 0;
        
        $len = count($vendor->services);
        
        foreach ($vendor->services as $services){ 
            
            if ($i < $len - 1) {
                
                $service_all.= $services->name.' - ';
                
            } else if ($i == $len - 1) {
                
                $service_all.= $services->name;
           
            }
            
            $i++;
        }
        
        //foreach de attributes.
        
        $attributes_all = null;
        
        $i = 0;
        
        $len = count($vendor->attributes);
        
        foreach ($vendor->attributes as $attributes){ 
            
            if ($i < $len - 1) {
                
                $attributes_all.= $attributes->name.' - ';
                
            } else if ($i == $len - 1) {
                
                $attributes_all.= $attributes->name;
           
            }
            
            $i++;
        }
        
        //
        
         if ($request->isMethod('post')){    
            
             $response = array(
                
                 'vendorName' => $vendor->name,
                
                 'industry' => $vendor->industry->name,
                
                 'emergency' => $vendor->emergency,
                 
                 'rate' => $vendor->rate,
                 
                 'phone' => $vendor->phone,
                 
                 'email' => $vendor->email,
                 
                 'web' => $vendor->url,
                 
                 'metroArea' => $vendor->metroarea->name,
                 
                 //'address' => $vendor_address->address,
                 
                 'address' => $vendor_address->address,
                 
                 'city' => $vendor_address->city->name,
                 
                 'state' => $vendor_address->city->state->name,
                 
                 'country' => $vendor_address->city->state->country->name,
                 
                 'zip' => $vendor_address->zip,
                 
                 'services' => $service_all,
                 
                 'attributes' => $attributes_all,
                 
                 'vendorUserEmail' => $vendor->user->email,
                 
                 'vendorFavorited' => $vendor_favorited,
                 
                
            );
         
         }
        
        return response()->json($response);
    }

    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return void
    //  */
    // public function index()
    // {
    //     $vendor = Vendor::paginate(15);

    //     return view('vendor.index', compact('vendor'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    // public function create()
    // {
    //     return view('vendor.create');
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @return void
    //  */
    // public function store(Request $request)
    // {
    //     $this->validate($request, ['industry_id' => 'required', 'name' => 'required', 'phone' => 'required', 'email' => 'required', ]);

    //     Vendor::create($request->all());

    //     Session::flash('flash_message', 'Vendor added!');

    //     return redirect('vendor');
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  *
    //  * @return void
    //  */
    // public function show($id)
    // {
    //     $vendor = Vendor::findOrFail($id);

    //     return view('vendor.show', compact('vendor'));
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  *
    //  * @return void
    //  */
    // public function destroy($id){

    //     Vendor::destroy($id);

    //     Session::flash('flash_message', 'Vendor deleted!');

    //     return redirect('vendor');
    // }
}
