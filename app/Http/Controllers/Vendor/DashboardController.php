<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Vendor;
use Carbon\Carbon;
use Session;


class DashboardController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function dashboard(){

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMekssage', "No vendor account found");

            abort(403, "No vendor account found");   
        
        }


        //$data = $vendor->rfps()->select('rfp_vendor.status', DB::raw('count(*) as total'))->groupBy('rfp_vendor.status')->get();

        //$rfp_data ['open'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'open', 'type'=>'RFP'])->count();

        $rfp_data ['invited'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'invited', 'type'=>'RFP'])->count();
        
        $rfp_data ['bid'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'bid', 'type'=>'RFP'])->count();
        
        $rfp_data ['workorder'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'workorder', 'type'=>'RFP'])->count();
        
        $rfp_data ['closed'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'closed', 'type'=>'RFP'])->count();
        
        //$rfp_data ['total'] =  $vendor->rfps()->where(['type'=>'RFP'])->count();

        $rfp_data ['total'] =  $vendor->rfps()->where([['type','=', 'RFP'],['rfp_vendor.status','!=','open']])->count();


        
        if ( $rfp_data ['total'] > 0 ){

            //$rfp_data ['open_p'] = round((($rfp_data ['open'] / $rfp_data ['total']) * 100) , 1);

            $rfp_data ['invited_p'] = round((($rfp_data ['invited'] / $rfp_data ['total']) * 100), 1);

            $rfp_data ['bid_p'] = round((($rfp_data ['bid'] / $rfp_data ['total']) * 100), 1);

            $rfp_data ['workorder_p'] = round((($rfp_data ['workorder'] / $rfp_data ['total']) * 100), 1);

            $rfp_data ['closed_p'] = round((($rfp_data ['closed'] / $rfp_data ['total']) * 100), 1);

        } else {

            //$rfp_data ['open_p'] = 

            $rfp_data ['invited_p'] =  $rfp_data ['bid_p'] = $rfp_data ['workorder_p'] = $rfp_data ['closed_p'] =  $rfp_data ['total'] = 0;

        }
        


        //$sr_data ['open'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'open', 'type'=>'SR'])->count();

        $sr_data ['invited'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'invited', 'type'=>'SR'])->count();

        $sr_data ['bid'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'bid', 'type'=>'SR'])->count();

        $sr_data ['workorder'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'workorder', 'type'=>'SR'])->count();

        $sr_data ['closed'] =  $vendor->rfps()->where(['rfp_vendor.status'=>'closed', 'type'=>'SR'])->count();

        //$sr_data ['total'] =  $vendor->rfps()->where(['type'=>'SR'])->count();

        $sr_data ['total'] =  $vendor->rfps()->where([['type','=', 'SR'],['rfp_vendor.status','!=','open']])->count();

        
        if ( $sr_data ['total'] > 0 ){

            //$sr_data ['open_p'] = round((($sr_data ['open'] / $sr_data ['total']) * 100) , 1);

            $sr_data ['invited_p'] = round((($sr_data ['invited'] / $sr_data ['total']) * 100), 1);

            $sr_data ['bid_p'] = round((($sr_data ['bid'] / $sr_data ['total']) * 100), 1);

            $sr_data ['workorder_p'] = round((($sr_data ['workorder'] / $sr_data ['total']) * 100), 1);

            $sr_data ['closed_p'] = round((($sr_data ['closed'] / $sr_data ['total']) * 100), 1);

        } else {

        	//$sr_data ['open_p'] =

            $sr_data ['invited_p'] =  $sr_data ['bid_p'] = $sr_data ['workorder_p'] = $sr_data ['closed_p'] =  $sr_data ['total'] = 0;

        }
        
        //WORKORDER DATA
        $wko_data ['open'] = $rfp_data ['workorder'] + $sr_data ['workorder'];

        $wko_data ['closed'] = $rfp_data ['closed'] + $sr_data ['closed'];
        
        $wko_data ['total'] = $wko_data ['open'] + $wko_data ['closed'];

        if($wko_data ['total'] > 0) {

            $wko_data ['open_p'] = round((($wko_data ['open'] / $wko_data ['total']) * 100) , 1);

            $wko_data ['closed_p'] = round((($wko_data ['closed'] / $wko_data ['total']) * 100), 1);

        } else {

            $wko_data ['total'] = $wko_data ['open_p'] = $wko_data ['closed_p'] = 0;
        }

        return view('vendor/dashboard', compact('rfp_data','sr_data','wko_data'));
              
    }
}
