<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

//custom


class UserController extends Controller {

    
    /**
    * Class constructor.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function __construct(){
         
        $this->middleware('auth');
    }


    /**
    * Display the specified resource.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function show() {

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        }

        $id = Auth::User()->id;

        $user = User::findOrFail($id);

        return view('user.show', compact('user'));
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function edit(){      


        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        }

        $id = Auth::User()->id;

        $user =  User::findOrFail($id);

        return view('user.edit', compact('user'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function update(Request $request){
        

        if(!(Auth::check())) {

            return response()->json(['error_message' => "Forbidden, you don't have access"], 403);
            
        }

        $id = Auth::User()->id;

        //Validating     

        $this->validate($request, [        
            'name' => 'required', 
            'lastname' => 'required', 
            'phone' => 'required'           
        ]);
        
        //Api resp de 24-7 ->app/http/helper.php    
        $sms_mms = getTextAddress($request['phone']);
        
        $data = [ 
            'name' => $request->name, 
            'lastname' => $request->lastname, 
            'phone' => $request->phone,
            'sms_address' => $sms_mms['sms_address'],
            'mms_address' => $sms_mms['mms_address'],
        ];

        //Selecting
        $user = User::findOrFail($id);
       
        //Updating
        $result = $user->update($data);

        if($result){

            Session::flash('flash_message', 'User updated!');

            $response = [
        
                'title' => 'User information',
                'message' => 'Success! You have updated your user information',
                'text' => '',
        
            ];

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error updating the user'], 422);

        }
           
        
        
        
    }


    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function passwd(Request $request){
        

        if(!(Auth::check())) {

            return response()->json(['error_message' => "Forbidden, you don't have access"], 403);
            
        }

        $id = Auth::User()->id;

        //Validating     

        $this->validate($request, [        
            
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6',          
        ]);

        if ( ! (Hash::check( $request->current_password, ( Auth::User()->password ) ) ) ) {
            
           return response()->json(['error_message' => 'The current password does not match'], 403);
           
        }


        $data = [ 
            'password' => bcrypt($request->password)            
        ];

        //Selecting
        $user = User::findOrFail($id);
       
        //Updating
        $result = $user->update($data);

        if($result){

            Session::flash('flash_message', 'Password updated!');

            $response = [
        
                'title' => 'User password',
                'message' => 'Success! You have updated your password',
                'text' => '',
        
            ];

            return response()->json($response, 200);
        } else {

            return response()->json(['error_message' => 'There was an error updating the user'], 422);

        }
        
}



    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    // public function index()  {

    //     $user = User::paginate(15);

    //     return view('user.index', compact('user'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    // public function create(){
    //     return view('user.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    // public function store(Request $request) {
        
    //     $this->validate($request, 
    //         [
    //             'name' => 'required', 
    //             'phone' => 'required', 
    //             'email' => 'required', 
    //         ]);

    //     user::create($request->all());

    //     Session::flash('flash_message', 'User added!');

    //     return redirect('user');
    // }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    // public function destroy($id){

    //     //User::destroy($id);

    //     //Session::flash('flash_message', 'User deleted!');

    //     //return redirect('user');
    // }
}
