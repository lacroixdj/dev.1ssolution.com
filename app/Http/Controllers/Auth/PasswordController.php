<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use DB;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;


    protected $redirectTo = '/login';


    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    /*@override changing email subject to custom text
     * 
      @return string */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Recover account access';
    }
    
    /*@override changin email information adding username
     * 
     */
    public function postEmail(Request $request)
    {
       $this->validate($request, ['email' => 'required']);
           
       $user = DB::table('users')
                ->where('email','like',$request->email)
                ->orwhere('name','like',$request->email)->first();
        
       
       if(!empty($user)){
           
        $email=array("email"=>$user->email);   
          
        $response = Password::sendResetLink($email, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });     
                           
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('status', trans($response));

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
        
        }else {           
                return redirect()->back()->withErrors(['invalidcredential' => "Wrong User or email"]);
              }
        
           
    }
    
        
    
       public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect('/reset_success')->with('status', trans($response));

            default:
                return redirect()->back()
                            ->withInput($request->only('email'))
                            ->withErrors(['email' => trans($response)]);
        }
    }
    
    
    /**
    * Send a reset link to the given user.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function sendResetLinkEmail(Request $request)
   {
       //dd('Angel amigo '.$request);
       //$this->validateSendResetLinkEmail($request);
       $this->validate($request, ['email' => 'required|email', 'g-recaptcha-response' => 'required|captcha']);

       $broker = $this->getBroker();

       $response = Password::broker($broker)->sendResetLink(
           $this->getSendResetLinkEmailCredentials($request),
           $this->resetEmailBuilder()
       );

       switch ($response) {
           case Password::RESET_LINK_SENT:
               return $this->getSendResetLinkEmailSuccessResponse($response);
           case Password::INVALID_USER:
           default:
               return $this->getSendResetLinkEmailFailureResponse($response);
       }
   }
    
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        //dd('Aqui');
        $this->validate(
            $request,
            $this->getResetValidationRules(),
            $this->getResetValidationMessages(),
            $this->getResetValidationCustomAttributes()
        );

        $credentials = $this->getResetCredentials($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);
            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }
    
     /**
     * Get the response for after a successful password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        //dd($response);
        //return redirect()->back()->with('status', 'Your password has been reset successfully!');
        return redirect('/login')->with('reset-status', 'Your password has been reset successfully!');    
        //return redirect('/password/reset/')->with('status', 'Your password has been reset successfully!');    
    }
    
    
}
