<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

//Custom
use Mail;
use App\Industry;
use App\Country;
use App\State;
use App\Metroarea;
use App\Corporation;
use App\Client; 
use App\Setting;
use App\Jobs\Postman;
use App\Invite;
use App\Categories;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        if(isset($data['passcode'])){
            $data['email'] = $data['client-email'];
            //Client Validation Register Form
             return Validator::make($data, [
                'client-name' => 'required|max:255',
                'client-email' => 'required|email|max:255|unique:users,email',                 
                'passcode' => 'required|min:6|exists:corporations,passcode'
            ]);   
        }else{
            //Vendor validation Register Form 
            return Validator::make($data, [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                //'password' => 'required|min:6|confirmed',
            ]);   
        }
            
    }
    
    public function getRegister(){        
        return view('auth.register');
    }
    
    
    // @override in this method i am able to resgister without autologin
    public function postRegister(Request $request){
        $validator = $this->validator($request->all());
        
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
 
        $this->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => str_random(30),
            'verified'=> false
        ]);      

        return view('auth.register_s');
    }
            
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data){

        $data['verification_code'] = str_random(30);

        $user= User::create([
        
            'name' => $data['name'],
        
            'email' => $data['email'],
        
            'password' => bcrypt($data['password']),
        
            'verification_code' => $data['verification_code'],
        
            'verified'=> false
            
        ]);
        
        /*
            Mail::send('emails.welcome', $data, function($message) use ($data) {
                    $message->from('info@1ssolution.com', "One Seamless Solution");
                    $message->subject("Welcome to One Seamless Solution");
                    $message->to($data['email']);
            });
        */

        $data2 = [
            
            'mode' => 'html',

            'view' => 'emails.welcome',
            
            'email_to' => $data['email'],

            'subject' => 'Welcome to One Seamless Solution',

            'receiver_name' => $data['name'],

            'name' => $data['name'],

            'verification_code' => $data['verification_code'],

            'img_url' => url(('/'))                                   
        ];

        $job = (new Postman($data2))->onQueue('emails')->delay(3);           

        $this->dispatch($job);
        
    }
    
    public function verify($token){

        try {   
            
            if(empty(User::where('verification_code', $token)->first()) && empty(Invite::where('verification_code', $token)->first())){
                 view()->share('HTTP_ErrorMessage', 'Sorry, Verification Token is invalid or has expired');
                 abort(403, 'Sorry, Verification Token is invalid or has expired.');
            }
            
            //$user = User::where('verification_code', $token)->first();
            
            //if(empty($user)) {
            //    view()->share('HTTP_ErrorMessage', 'Sorry, Verification Token is invalid or has expired');
            //    abort(403, 'Sorry, Verification Token is invalid or has expired.');
            //}
        
        } catch(NotFoundHttpException $e){
            view()->share('HTTP_ErrorMessage', 'Sorry, Verification Token is invalid or has expired');
            abort(403, 'Sorry, Verification Token is invalid or has expired.');
        }
                        
        //Si el usuario viene del registro normal
        if(!empty(User::where('verification_code', $token)->first())){
            
            $user = User::where('verification_code', $token)->first();
            
            session(['user_id' => $user->id]);
            
            $result = $user->update(['verified'=>'1']);
        
        }
        
        //si el usuario viene por invitacion 
        if(!empty(Invite::where('verification_code', $token)->first())){
            
            if(empty(User::where('verification_code', $token)->first())){
            
                $user = Invite::where('verification_code', $token)->first();

                $user = User::create([

                    'name' => $user->name,

                    'email' => $user->email,

                    'verification_code' => $user->verification_code,

                    'verified'=> false

                ]);

                session(['user_id' => $user->id]);
            }
            
            $result = $user->update(['verified'=>'1']);
        
        }
        
        
        
        $industries = Industry::all()->sortBy('name')->lists('name','id')->toArray();
        $countries =  Country::all()->sortBy('name')->lists('name','id')->toArray();
        $states =     State::where('country_id', 231)->get()->sortBy('name')->lists('name','id')->toArray();
        $metroareas = Metroarea::all()->sortBy('name')->lists('name','id')->toArray();


//        session(['user_id' => $user->id]);
//        $result = $user->update(['verified'=>'1']);

        if($result){           
            return view('auth.verified_success', [
                    'user' => $user, 
                    'industries' => $industries,
                    'countries' => $countries,
                    'states' => $states,
                    'metroareas' => $metroareas,
                ]
            );
            
        } else {
            return view('auth.verified_error');
        }
    
    
    }
    
    //@override to check if the user was verified then it can log in
    protected function getCredentials(Request $request) {
        return array_merge($request->only($this->loginUsername(), 'password'),array('verified'=>1));
    }
    
    // to check if the user was verified and email is correct then he can log in
    protected function getCredentialsEmail(Request $request)  {
        return array_merge(array('email'=>$request->input('name'), 'password'=> $request->input('password') ,  'verified'=>1));
    }

    public function setCredentials(Request $request){
        
        //dd($request);
        $this->validate($request, [
            'password' => 'required|min:6|confirmed', 
            'name' => 'required|max:255', 
            'lastname' => 'required|max:255',
            'phone' => 'required|max:255',
            'vendorname' => 'required|max:255',
            'industry' => 'required|max:255',
            'services_values' => 'required|max:255',
            //'country' => 'required|max:255',
            'state' => 'required|max:255',      
            'city' => 'required|max:255',
            'address' => 'required|max:255',      
            'zip' => 'required|max:255',
            'vendorphone' => 'required|max:255',
            'vendoremail' => 'required|max:255',
            'metroarea' => 'required|max:255',
        ] );
        
        //Api resp de 24-7 ->app/http/helper.php    
        $sms_mms = getTextAddress($request['phone']);
        
        $user_id = session('user_id'); 
        $user = User::where('id', $user_id)->first();
        $password = bcrypt($request->password);
        $data= [
            'password' => $password, 
            'name' => $request->name, 
            'lastname' => $request->lastname,
            'phone' => $request->phone,
            'sms_address' => $sms_mms['sms_address'],
            'mms_address' => $sms_mms['mms_address'],
        ];
        
        $result = $user->update($data);
        
       
        
        if($result){ 
            
                // Set Settings 
                
                //------------------------------------------
            
                //Messages settings
            
                $settings = Setting::where('status',1)->get(); 
                
                $user->settings()->sync($settings);
                
                //Skin Settings
                
                $setting_skin=  $user->settings()->where('setting_id', 3)->get();
                                  
                $setting_skin[0]->pivot->setting_value =  'skin-ssolution';
    
                $result =  $setting_skin[0]->pivot->save();
            
                //set default tax setting
                
                $category_tax_default = Categories::where('table','=','tax_default_value')->first();
            
                $category_tax_default = $category_tax_default->value;
            
                $setting_tax=  $user->settings()->where('setting_id', 4)->get();
                                                      
                $setting_tax[0]->pivot->setting_value = $category_tax_default;
    
                $result =  $setting_tax[0]->pivot->save();
                
                //End Set Settings 
                
                //------------------------------------------
                
                
                $user->vendors()->create([
                    'name' => $request->vendorname,
                    'industry_id' => $request->industry,
                    'url' => $request->url,
                    'email' => $request->vendoremail,
                    'emergency' => $request->emergency,
                    'phone' => $request->phone,
                    'sms_address' => $sms_mms['sms_address'],
                    'mms_address' => $sms_mms['mms_address'],
                    'metroarea_id' => $request->metroarea,
                ]);
                
                $user->vendors()->first()->addresses()->create([
                    'city_id' => $request->city,
                    'address' => $request->address,      
                    'address2' => $request->address2,      
                    'zip' => $request->zip,
                    'phone' => $request->vendorphone,  
                    'main_address' => true
                ]);

                if(!empty($request->services_values))                    
                    $user->vendors()->first()->services()->attach(explode(",", $request->services_values));

                if(!empty($request->attributes_values))
                    $user->vendors()->first()->attributes()->attach(explode(",", $request->attributes_values));
            
                //verificar si el vendedor viene por invitacion y en caso de true cambiar el estatus y avisar a los 
                // usuarios que lo invitaron que ya esta disponible en el marketplace
                if(!empty(Invite::where('email', '=', $user->email )->where('status', '=', 'sent')->where('type', '=', 'client-to-vendor')->first())){                    
                    
                    $users = Invite::where('email', '=', $user->email)->with('user')->where('type', '=', 'client-to-vendor')->get();
                    
                    //Si Hay invitaciones en el modelo invite, se manda una notificacion a cada
                    //usuario que realizo la invitacion avisando que el vendedor se registro.
                    if (!$users->isEmpty()) { 
                        
                        foreach ($users as $user_invite) {
                                                    
                            //Check the setting for recive emails
                            $checkEmail = checkSetting(1, $user_invite->user->id);
            
                            if($checkEmail == 1){ 
                                
                                
                                     $dataEmail = [
            
                                        'mode' => 'html',

                                        'view' => 'emails.invite_accepted',

                                        'email_to' =>  $user_invite->user->email,

                                        'receiver_name' => $user_invite->user->name.' '. $user_invite->user->lastname,

                                        'subject' => $user->name.' '.$user->lastname.', accepted your invitation',    

                                        'userInvited' => $user->name.' '.$user->lastname,

                                        'invite_url' => url(('/vendors/favorites/'.$user->id)),

                                        'img_url' => url(('/')),


                                    ];

                                    $job = ( new Postman($dataEmail))

                                        ->onQueue('emails')

                                        ->delay(3);    

                                    $this->dispatch($job);

                            }
                        
                        }                    
                    }
                    
                    //Por ultimo marco la invitacion como Joined
                    $result = Invite::where('email', '=', $user->email)->update(['status'=>'joined']);
                     
                }

            return response()->json(['request_status' => 'success'], 200);
            
        }else{
            
            return response()->json(['request_status' => 'failure'], 422);
        }
    }
        
    public function postRegisterClient(Request $request){
            //Validate entry 
            
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
                $this->throwValidationException(
                    $request, $validator
                );
        }
            
        $this->createClientEmail([
                'name' => $request['client-name'],
                'email' => $request['client-email'],
                'password' => str_random(30),
                'verified'=> false,
                'passcode' => $request['passcode'],
        ]);      
            
        return view('auth.register_s');
        
    }
    
    protected function createClientEmail(array $data){
            
            $data['verification_code'] = str_random(30);
                            
            $user= User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'verification_code' => $data['verification_code'],
                'verified'=> false, 
            ]);
            

            /*
                Mail::send('emails.welcomeClient', $data, function($message) use ($data){
                    $message->from('info@1ssolution.com', "One Seamless Solution");
                    $message->subject("Welcome to One Seamless Solution");
                    $message->to($data['email']);
                });   
            */

            $data2 = [
            
                    'mode' => 'html',

                    'view' => 'emails.welcomeClient',
                    
                    'email_to' => $data['email'],

                    'subject' => 'Welcome to One Seamless Solution',

                    'receiver_name' => $data['name'],

                    'name' => $data['name'],

                    'passcode' => $data['passcode'],

                    'verification_code' => $data['verification_code'],

                    'img_url' => url(('/'))                                   
            ];

                $job = (new Postman($data2))->onQueue('emails')->delay(3);           

                $this->dispatch($job);
        }

    public function verifyClient($token,$passcode) {


            //Verificacion de Token;
        
            try {   
                
                $user = User::where('verification_code', $token)->first();
                
                if(empty($user)) {

                    view()->share('HTTP_ErrorMessage', 'Sorry, Verification Token is invalid or has expired');
                    
                    abort(403, 'Sorry, Verification Token is invalid or has expired.');
                }

            } catch(NotFoundHttpException $e) {
                
                view()->share('HTTP_ErrorMessage', 'Sorry, Verification Token is invalid or has expired');
                
                abort(403, 'Sorry, Verification Token is invalid or has expired.');
            }
        

            $corporation = Corporation::where('passcode', $passcode)->first();
            
            $countries =  Country::all()->sortBy('name')->lists('name','id')->toArray();
            
            $states =     State::where('country_id', 231)->get()->sortBy('name')->lists('name','id')->toArray();
            
            $metroareas = Metroarea::all()->sortBy('name')->lists('name','id')->toArray();
            
            session(['user_id' => $user->id, 'corporation_id' => $corporation->id]);
            
            //check user verify
            $result = $user->update(['verified'=>'1']);

            if($result){           
                return view('auth.verified_success_client', [
                        'user'          => $user, 
                        'countries'     => $countries,
                        'states'        => $states,
                        'corporation'   => $corporation,
                        'metroareas'   => $metroareas,
                    ]
                );

            }else{
                return view('auth.verified_error');
            }
        }
        
    public function setCredentialsClient(Request $request){
    
            $this->validate($request, [
                'password' => 'required|min:6|confirmed', 
                'name' => 'required|max:255', 
                'lastname' => 'required|max:255',
                'phone' => 'required|max:255',
                'state' => 'required|max:255',      
                'city' => 'required|max:255',
                'address' => 'required|max:255',      
                'metroarea' => 'required|max:255',      
                'zip' => 'required|max:255',
            ] );

            //Api resp de 24-7 ->app/http/helper.php    
            $sms_mms = getTextAddress($request['phone']);

            $corporation_id = session('corporation_id');
            
            $user_id = session('user_id');
            
            $user = User::where('id', $user_id)->first();
            
            $password = bcrypt($request->password);
            
            $data= [
                'password' => $password, 
                'name' => $request->name, 
                'lastname' => $request->lastname,
                'phone' => $request->phone,
                'sms_address' => $sms_mms['sms_address'],
                'mms_address' => $sms_mms['mms_address'],
            ];

            $result = $user->update($data);
            
            if($result){ 
                    
                    $settings = Setting::where('status',1)->get(); 
                    
                    $user->settings()->sync($settings);
                
                    //El id setting 3 correspondiente de los custom Sking hay que hacer el update y colocarle el calor de la columna setting_value == 'skin_ssolution'
                
                    $setting_skin=  $user->settings()->where('setting_id', 3)->get();
                                  
                    $setting_skin[0]->pivot->setting_value =  'skin-ssolution';

                    $result =  $setting_skin[0]->pivot->save();
                
                    //fin
                    
                    $user->clients()->create([
                        'corporation_id' => $corporation_id,
                        'name' => $request->name,
                        'phone' => $request->phone,
                        'email' => $request->email,
                        'url' => $request->url,
                        'sms_address' => $sms_mms['sms_address'],
                        'mms_address' => $sms_mms['mms_address'],
                        'metroarea_id' => $request->metroarea
                    ]);

                    $user->clients()->first()->addresses()->create([
                        'city_id' => $request->city,
                        'address' => $request->address,      
                        'address2' => $request->address2,      
                        'zip' => $request->zip,
                        'phone' => $request->facilityphone,      
                        'main_address' => true
                    ]);

                //return redirect('/login');
                return response()->json(['request_status' => 'success'], 200);

            }else{
                //return redirect()->back()->withErrors(['failure' => "There was a failure updating credentials"]);
                return response()->json(['request_status' => 'failure'], 422);
            }
        }
    
 
}
