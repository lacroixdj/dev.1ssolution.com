<?php

namespace App\Http\Controllers\Workorder;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Rfp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;

//custom
use Log;
use App\Client;
use App\Vendor;
use App\Industry;
use App\Attribute;
use App\Service;
use App\State;
use App\Metroarea;
use App\Jobs\Postman;
use Datatables;
use Mail;

class WorkorderController extends Controller
{
 
    public function __construct(){
         
        $this->middleware('auth');
    }



    /**
     * Create Workorder from /rfp/client/requestdetail
     *
     * @param  post request, 
     * @return \Illuminate\Http\Response
     */
    public function createWorkorder(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
        
        //Validate post param
        $this->validate($request, [      

            'rfpId' => 'required', 

            'vendorId' => 'required' 

        ]);
        
        //Validate this workorder its belong to this client
        
        //Obtener parametros de vendor, rfp, vendor user
        $rfp = Rfp::findOrFail($request->rfpId);
        
        $vendor = Vendor::findOrFail($request->vendorId);
        
        $rfp_vendor = $vendor->rfps()->findOrFail($request->rfpId); 
        
        $vendorUser = Auth::User()->findOrFail($vendor->user_id);
        
        //Obtener el porcentaje del tax
        //El setting_id -> 4 corresponde al tax_amount parametro de user setting
        $settings_tax =  $vendorUser->settings()->where('setting_id', 4)->get();
        
        //dd(floatval($settings_tax[0]->pivot->setting_value));
      
        //save the workorder
        $data = [
             'user_id' => Auth::User()->id, 

             'client_id' => $client->id, 
             
             'vendor_id' => $vendor->id,
            
             'industry_id' => $vendor->industry_id,
            
             'rfp_id' => $rfp->id,
            
             'status' => 'open', //TODO: Agregar los estatus de workorder y RFP en la tabla atributes 
            
             'emergency' => $rfp->emergency,
            
             'description' => $rfp->description,
            
             'location' => $rfp->location,
            
             'bid_amount' => $rfp_vendor->pivot->bid_amount,
            
             'tax_amount' => floatval($settings_tax[0]->pivot->setting_value),
            
             'service_date' => $rfp->service_date,
            
             'type' => $rfp->type,
            
             'visit' => $rfp->visit,
            
             'details' => $rfp->details, 
            
             'documents' => $rfp->documents,
        ];
        
        $result = false;   
        
        $result = $client->workorders()->create($data);   
        
        if($result) {
            
            //update the rfp
            
            $rfp->status = 'workorder';
            
            $rfp_vendor->pivot->status = 'workorder';
                        
            $result_rfp_status_pivot = $rfp_vendor->pivot->save();
            
            $result_rfp_status = $rfp->save();

            //end update
            
            //Send the emails
            
            //Vendor email
            
            $data = [
            
                    'view' => 'emails.workorderVendor',
                    
                    'email_to' => $vendorUser->email,

                    'receiver_name' => $vendorUser->name,
                    
                    'subject' => 'You have been awarded with a Workorder #'.$result->id,    
                    
                    'id' => $result->id,
                
                    'workorderDescription' => $result->description,    
                
                    'clientPhone' => Auth::User()->phone,
                
                    'clientName' => Auth::User()->name,

                    'clientEmail' => Auth::User()->email,
                    
                    'serviceDate' => $result->service_date,
                    
                    'location' => $result->location,
                    
                    'details' => $result->details,
                    
                    'documents' => $result->documents,

                    'vendor_password' => $vendorUser->passwordplain,
                
                    'img_url' => url(('/')),

                    'workorder_url' =>  url(('/workorder/vendor/detail/'.$result->id.'/')),

                                   
                ];
             
            $checkEmail = checkSetting(1, $vendorUser->id);
            
            if($checkEmail == 1){
            
                $job = ( new Postman($data))->onQueue('emails')->delay(1);    

                $this->dispatch($job);
            }
            
            //Client Email
            
            $dataClient = [

                    'mode' => 'html',

                    'view' => 'emails.workorderClient',
                    
                    'email_to' => Auth::User()->email,

                    'receiver_name' => Auth::User()->name,
                    
                    //'subject' => 'Notice of Workorder creation #'.$result->id,    

                    'subject' => 'This is a new workorder notice #'.$result->id,    
                    
                    'id' => $result->id,
                
                    'workorderDescription' => $result->description,    
                
                    'vendorPhone' => $vendorUser->phone,
                
                    'vendorName' => $vendorUser->name,

                    'vendorEmail' => $vendorUser->email,
                    
                    'serviceDate' => $result->service_date,
                    
                    'location' => $result->location,
                    
                    'details' => $result->details,
                    
                    'documents' => $result->documents,

                    'vendor_password' => Auth::User()->passwordplain,
                
                    'img_url' => url(('/')),

                    'workorder_url' => url(('/workorder/client/detail/'.$result->id.'/')),

                                   
                ];
            
            $checkEmail = checkSetting(1, Auth::User()->id);
            
            if($checkEmail == 1){
            
                $job = ( new Postman($dataClient))->onQueue('emails')->delay(2); 

                $this->dispatch($job);
            }
            
            //end emails
            
            //Send SMS Client
            
            $checkSms = checkSetting(2, Auth::User()->id);
            
            if($checkEmail == 1){
           
                if( Auth::User()->sms_address != '' || Auth::User()->sms_address != null ){

                    $dataSms = [

                        'mode' => 'text',

                        'view' => 'emails.sms',

                        'email_to' => Auth::User()->sms_address,

                        'receiver_name' => Auth::User()->name.' '.Auth::User()->lastname ,

                        'subject' => 'Notice of Workorder creation', 

                        'message_to' => 'Notice of Workorder creation #'.$result->id.', To Vendor: '.$vendorUser->name.' '.$vendorUser->name.'.'.', click on the following url - '.url(('/workorder/client/detail/'.$result->id.'/')).' -- The One Seamless Solution team.', 

                    ];
                    //TODO: Revisar el envio con el otro proceso sin que mande el attachment html.
                    /*Mail::send(array('text' => 'emails.sms'), $dataSms, function($message) use ($dataSms)
                    {
                        $message->from('info@1ssolution.com', "One Seamless Solution");
                        $message->subject($dataSms['subject']);
                        $message->to($dataSms['email_to']);
                    });   */

                    $job2 = ( new Postman($dataSms))

                        ->onQueue('emails')

                        ->delay(3);    

                    $this->dispatch($job2);


                 }
            
            }
            
            $checkSms = checkSetting(2, $vendorUser->id);
            
            if($checkSms == 1){
            
                if( $vendorUser->sms_address != '' || $vendorUser->sms_address != null ){

                    $dataSms2 = [

                        'mode' => 'text',

                        'view' => 'emails.sms',

                        'email_to' => $vendorUser->sms_address,

                        'receiver_name' => $vendorUser->name.' '.$vendorUser->lastname ,

                        'subject' => 'Notice of Workorder assigned', 

                        'message_to' => 'Notice of Workorder assigned #'.$result->id.', For client: '.Auth::User()->name.' '.Auth::User()->lastname.'.'.', click on the following url - '.url(('/workorder/vendor/detail/'.$result->id.'/')).' -- The One Seamless Solution team.', 

                    ];  

                    $job3 = ( new Postman($dataSms2))

                        ->onQueue('emails')

                        ->delay(3);    

                    $this->dispatch($job3);


                 }
            }
            
            //Send SMS Vendor
            
            //end email sms
            
            Session::flash('flash_message', 'Workorder Created!');
            
            $response = [

                'title' => 'Workorder',
        
                'message' => 'Success! You sent the workorder to '.$vendor->name.' ',
        
                'text' => '',

            ];           
        
            return response()->json($response, 200);
            
        } else {

            return response()->json(['error_message' => 'There was an error creating the workorder'], 422);
       
        }       
         

    }
    
     /**
     * view client workorders
     *
     * @param  none, get workorder asociated to client or vendor user
     * @return json list workorders for datatable
     */
    public function viewWorkorders(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }
        
        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
        
        $array_where = [];

        //if(!empty($request->type)) $array_where [] = ['type','=', $request->type];

        if(!empty($request->status)) $array_where [] = ['status','=', $request->status];

        $workorders = $client->workorders()->where($array_where); 
        
        return Datatables::of($workorders)->make(true);
    }
    
    /**
     * view Vendors workorders
     *
     * @param  none, get workorder asociated to client or vendor user
     * @return json list workorders for datatable
     */
    public function viewWorkordersVendor(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
        
        //$workorder = DB::table('workorders')->where('vendor_id', $vendor->id);   
        
        $array_where = [];

        //if(!empty($request->type)) $array_where [] = ['type','=', $request->type];

        if(!empty($request->status)) $array_where [] = ['status','=', $request->status];

        $workorders = $vendor->workorders()->where($array_where); 

        return Datatables::of($workorders)->make(true);
    }
    
     /**
     * view client workorder detail
     *
     * 
     * 
     */
    public function workorderDetail($id){
        
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        $workorder = $client->workorders()->findOrFail($id);
                
        $vendor = Vendor::findOrFail($workorder->vendor_id);
        
        $rfp = $vendor->rfps()->findOrFail($workorder->rfp_id);
        
        $vendor_address = $vendor->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
        
        $vendor_services = $vendor->services->lists('name');  
        
        $vendor_attributes = $vendor->attributes->lists('name');
        
        $vendor_user = \App\User::findOrFail($vendor->user_id);
        
        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
                       
        return view('workorder.workorderDetailClient', compact('workorder',
                                                               'vendor', 
                                                               'vendor_address',
                                                               'vendor_services',
                                                               'vendor_attributes',
                                                               'client',
                                                               'client_address',
                                                               'rfp',
                                                               'vendor_user'));
    }   
    
    
    /**
     * view vendor workorder detail
     *
     * 
     * 
     */
    public function workorderDetailVendor($id){
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        $workorder = $vendor->workorders()->findOrFail($id);
                        
        $client = Client::findOrFail($workorder->client_id);
                
        $rfp = $vendor->rfps()->findOrFail($workorder->rfp_id);
                
        $vendor_address = $vendor->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
        
        //$vendor_services = $vendor->services->lists('name');  
        
        //$vendor_attributes = $vendor->attributes->lists('name');  
        
        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
        
        //dd($client_address->address);
        //dd($workorder->location);
                       
        return view('workorder.workorderDetailVendor', compact('workorder',
                                                               'vendor',
                                                               'client',
                                                               'client_address',
                                                               'vendor_address',
                                                               'rfp'));
    }
    
    
    
    /**
     * view client workorder detail
     *
     * 
     * 
     */
    public function workorderInvoice($id){
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        $workorder = $client->workorders()->findOrFail($id);
                
        $vendor = Vendor::findOrFail($workorder->vendor_id);
        
        $vendor_address = $vendor->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
        
        $vendor_services = $vendor->services->lists('name');  
        
        $vendor_attributes = $vendor->attributes->lists('name');  
        
        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
               
        //dd($client_address);
        
        return view('workorder.workorderPrint', compact('workorder',
                                                               'vendor', 
                                                               'vendor_address',
                                                               'vendor_services',
                                                               'vendor_attributes',
                                                               'client',
                                                               'client_address'));
    }
    
    
    /**
     * view client workorder detail
     *
     * 
     * 
     */
    public function workorderInvoiceVendor($id){
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        $workorder = $vendor->workorders()->findOrFail($id);
        
        $client = Client::findOrFail($workorder->client_id);
                
        $vendor = Vendor::findOrFail($workorder->vendor_id);
        
        $vendor_address = $vendor->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
        
        $vendor_services = $vendor->services->lists('name');  
        
        $vendor_attributes = $vendor->attributes->lists('name');  
        
        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
               
        
        return view('workorder.workorderPrintVendor', compact('workorder',
                                                               'vendor', 
                                                               'vendor_address',
                                                               'vendor_services',
                                                               'vendor_attributes',
                                                               'client',
                                                               'client_address'));
    }
    
    
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
                
        $this->validate($request, [
            
            'workorderId' => 'required', 

            'workorderResponse1' => 'required', 

            'workorderResponse2' => 'required',
            
            'workorderResponse3' => 'required',

        ]);
        
        //Update the fields
        
        $data = [

            'question_1' => $request->workorderResponse1, 
            
            'question_2' => $request->workorderResponse2, 
            
            'vendor_rate' => $request->workorderResponse3, 

            'close_details' => $request->workorderDetail, 

            'close_id' => $request->workorderId, 
            
            'closed_date' => Carbon::now(),

            'status' => 'closed',

        ];

        $result = false;        
    
        //$rfp_id = $request->session()->get($request->workorderId');

        $workorder = $client->workorders()->findOrFail($request->workorderId);

        $result = $workorder->update($data);
        
        
                
        if($result) {
            
            //Update RFP
        
            $rfp = $client->rfps()->findOrFail($workorder->rfp_id);

            $rfp->status = 'closed';

            $rfp->save();

            //update vendor rfp

            $vendor = Vendor::findOrFail($workorder->vendor_id);
            
            $vendorUser = Auth::User()->findOrFail($vendor->user_id);

            $vendorRfp = $vendor->rfps()->findOrFail($workorder->rfp_id);

            $vendorRfp->pivot->status = 'closed';

            $vendorRfp->pivot->save(); 

            Session::flash('flash_message', 'Workorder closed');
            
             $response = [

                    'title' => 'Close Workorder',
            
                    'message' => 'Workorder update successfully',

                    'callback_func' => '',
            
                    'text' => '',
                    
                ]; 
            
            
            //Update vendor rate
            
            //1.- obtener todos los wororders del vendor 
            
                $countWorkorder = $vendor->workorders()->where('status', 'closed')->count();
            
                $sumWorkorder = $vendor->workorders()->where('status', 'closed')->sum('vendor_rate');
            
            //2.- Promedio 
            
                $rate = $sumWorkorder / $countWorkorder;
            
            //3.- updtae vendor rate
            
                $update_rate = $vendor->rate = $rate;
            
                $vendor->save();
            
            //End update vendor rate 
            
            
            // Email to the vendor notify the workorder closure
            
            $data = [
            
                    'mode' => 'html',

                    'view' => 'emails.workorderVendorCosed',
                    
                    'email_to' => $vendorUser->email,
                
                    'receiver_name' => $vendorUser->name. ' '.$vendorUser->lastname,
                    
                    'subject' => 'Notice of workorder closed #'.$workorder->id,    
                    
                    'id' => $workorder->id,
                
                    'clientName' => $client->name.' '.$client->lastname,

                    'workorderRate' => $request->workorderResponse3,

                    'workorderComments' =>  $request->workorderDetail,

                    'workorderDescription' =>  $workorder->description,
                    
                    'vendorPassword' =>  $vendorUser->passwordplain,

                    'workorder_url' => url(('/workorder/vendor/detail/'.$workorder->id)),
             
                    'img_url' => url(('/')),

                                   
                ];
            
            $checkEmail = checkSetting(1, $vendorUser->id);
            
            if($checkEmail == 1){
            
                $job = ( new Postman($data))

                ->onQueue('emails')

                ->delay(3);    

                $this->dispatch($job);
            
            }
            // End Email
            
            // SMS to Vendor 
            
            $checkSms = checkSetting(2, $vendorUser->id);
            
            if($checkSms == 1){
                
                if( $vendorUser->sms_address != '' || $vendorUser->sms_address != null && $checkSms == 1){

                    $dataSms = [

                        'mode' => 'text',

                        'view' => 'emails.sms',

                        'email_to' =>  $vendorUser->sms_address,

                        'receiver_name' =>  $vendorUser->name.' '. $vendorUser->lastname ,

                        'subject' => 'Notice of workorder closed', 

                        'message_to' => 'Notice of workorder closed #'.$workorder->id.', click on the following url - '.url('/workorder/vendor/detail/'.$workorder->id).' -- The One Seamless Solution team.', 

                    ];   

                    $job2 = ( new Postman($dataSms))

                        ->onQueue('emails')

                        ->delay(3);    

                    $this->dispatch($job2);

                }
            }
            
            // End Sms
            

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error closing the workorder'], 422);
       
        } 
     
    }
    
}
