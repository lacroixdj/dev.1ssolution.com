<?php

namespace App\Http\Controllers\Corporation;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Corporation;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CorporationController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $corporation = Corporation::paginate(15);

        return view('corporation.index', compact('corporation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('corporation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'passcode' => 'required', 'phone'=>'required', 'email'=>'required','url'=>'required', 'corporation_type' => 'required', ]);

        Corporation::create($request->all());

        Session::flash('flash_message', 'Corporation added!');

        return redirect('corporation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $corporation = Corporation::findOrFail($id);

        return view('corporation.show', compact('corporation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $corporation = Corporation::findOrFail($id);

        return view('corporation.edit', compact('corporation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'passcode' => 'required', 'phone'=>'required', 'email'=>'required','url'=>'required', 'corporation_type' => 'required',]);

        $corporation = Corporation::findOrFail($id);
        $corporation->update($request->all());

        Session::flash('flash_message', 'Corporation updated!');

        return redirect('corporation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Corporation::destroy($id);

        Session::flash('flash_message', 'Corporation deleted!');

        return redirect('corporation');
    }
}
