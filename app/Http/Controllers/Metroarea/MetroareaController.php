<?php

namespace App\Http\Controllers\Metroarea;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Metroarea;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class MetroareaController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $metroarea = Metroarea::paginate(15);

        return view('metroarea.index', compact('metroarea'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('metroarea.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        Metroarea::create($request->all());

        Session::flash('flash_message', 'Metroarea added!');

        return redirect('metroarea');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $metroarea = Metroarea::findOrFail($id);

        return view('metroarea.show', compact('metroarea'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $metroarea = Metroarea::findOrFail($id);

        return view('metroarea.edit', compact('metroarea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $metroarea = Metroarea::findOrFail($id);
        $metroarea->update($request->all());

        Session::flash('flash_message', 'Metroarea updated!');

        return redirect('metroarea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Metroarea::destroy($id);

        Session::flash('flash_message', 'Metroarea deleted!');

        return redirect('metroarea');
    }
}
