<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;

//custom
use App\Country;
use App\State;
use App\City;
use App\Corporation;
use App\Metroarea;



class ClientController extends Controller {

    
    public function __construct(){
        
        $this->middleware('auth');
    
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show()  {

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            abort(403, "No client account found");   
        
        }
        
        return view('client.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit(){


        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }

        //$client =  Client::findOrFail($id);

        $corporation = Corporation::where('id', $client->corporation_id)->first();
        
        $countries = Country::all()->sortBy('name')->lists('name','id')->toArray();
        
        $states = State::where('country_id', 231)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $cities = City::where('state_id', $client->addresses()->first()->city->state->id)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $metroareas = Metroarea::all()->sortBy('name')->lists('name','id')->toArray();
        
        return view('client.edit', compact('client','corporation','countries','states','cities', 'metroareas'));
    
    }


    /**
     *
     * @param  int  $id
     *
     * @return void
     */
    public function updateAddress(Request $request){

        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client account found"], 403);
            
        }
       
       
       $this->validate($request, [

            'city'  =>  'required', 
            
            'address' => 'required', 
            
            'zip'   =>  'required',
            
            'phone' =>  'required',
            
            'metroarea_id' =>  'required',

        ]);

       
       //$client = Client::findOrFail($id);
       
       //dd($request->metroarea_id);
        
       $data= [
            
           'city_id' => $request->city, 
           
           'address' => $request->address, 
           
           'address2' => $request->address2,
           
           'zip' => $request->zip,
           
           'phone' => $request->phone,
           
       ];

        
        $result = $client->addresses()->first()->update($data);
        
        $client->metroarea_id = $request->metroarea_id;
        
        $client->save();
        

        if($result) {

            Session::flash('flash_message', 'Client updated!');
            
            $response = [

                'title' => 'Facility Location',
        
                'message' => 'Success! You have updated your facility location',
        
                'text' => '',

            ];   
        
            return response()->json($response, 200);
            
        } else {

            return response()->json(['error_message' => 'There was an error updating the client address'], 422);
       
        }       



    }
    
    
    public function clientProfile(Request $request)
    {
        
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client account found"], 403);
            
        }
        
        //Query
       
        $client = Client::where('id', '=', $request->clientId)
                    
                    //->with('industry','services','attributes')
                    
                    ->with('metroarea')
                    
                    ->with('user')
                    
                    ->firstOrFail();
        
        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
               
        //
        
         if ($request->isMethod('post')){    
            
             $response = array(
                
                 'clientName' => $client->name,
                
                 'phone' => $client->phone,
                 
                 'email' => $client->email,
                 
                 'web' => $client->url,
                 
                 'metroArea' => $client->metroarea->name,
                 
                 'address' => $client_address->address,
                 
                 'city' => $client_address->city->name,
                 
                 'state' => $client_address->city->state->name,
                 
                 'country' => $client_address->city->state->country->name,
                 
                 'zip' => $client_address->zip,
                 
                 'clientUserEmail' => $client->user->email,
                 
                
            );
         
         }
        
        return response()->json($response);
    }
    
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return void
    //  */
    // public function index()
    // {
    //     $client = Client::paginate(15);

    //     return view('client.index', compact('client'));
    // }

    // *
    //  * Show the form for creating a new resource.
    //  *
    //  * @return void
     
    // public function create()
    // {
    //     return view('client.create');
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @return void
    //  */
    // public function store(Request $request)
    // {
    //     $this->validate($request, ['industry_id' => 'required', 'name' => 'required', 'phone' => 'required', 'email' => 'required', ]);

    //     Client::create($request->all());

    //     Session::flash('flash_message', 'client added!');

    //     return redirect('client');
    // }


    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  *
    //  * @return void
    //  */
    // public function destroy($id){

    //     Client::destroy($id);

    //     Session::flash('flash_message', 'Client deleted!');

    //     return redirect('client');
    // }    
}
