<?php


namespace App\Http\Controllers\Client;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Client;
use Carbon\Carbon;
use Session;


class DashboardController extends Controller {

	public function __construct(){

        $this->middleware('auth');
    
    }
	
    //Retorna el Dashboard
    public function dashboard(){

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMekssage', "No client account found");

            abort(403, "No client account found");   
        
        }


        //$data = $client->rfps()->select('status', DB::raw('count(*) as total'))->groupBy('status')->get();

        $rfp_data ['open'] =  $client->rfps()->where(['status'=>'open', 'type'=>'RFP'])->count();

        $rfp_data ['sent'] =  $client->rfps()->where(['status'=>'sent', 'type'=>'RFP'])->count();
        
        $rfp_data ['bid'] =  $client->rfps()->where(['status'=>'bid', 'type'=>'RFP'])->count();
        
        $rfp_data ['workorder'] =  $client->rfps()->where(['status'=>'workorder', 'type'=>'RFP'])->count();
        
        $rfp_data ['closed'] =  $client->rfps()->where(['status'=>'closed', 'type'=>'RFP'])->count();
        
        $rfp_data ['total'] =  $client->rfps()->where(['type'=>'RFP'])->count();

        
        if ( $rfp_data ['total'] > 0 ){

            $rfp_data ['open_p'] = round((($rfp_data ['open'] / $rfp_data ['total']) * 100) , 1);

            $rfp_data ['sent_p'] = round((($rfp_data ['sent'] / $rfp_data ['total']) * 100), 1);

            $rfp_data ['bid_p'] = round((($rfp_data ['bid'] / $rfp_data ['total']) * 100), 1);

            $rfp_data ['workorder_p'] = round((($rfp_data ['workorder'] / $rfp_data ['total']) * 100), 1);

            $rfp_data ['closed_p'] = round((($rfp_data ['closed'] / $rfp_data ['total']) * 100), 1);

        } else {

            $rfp_data ['open_p'] = $rfp_data ['sent_p'] =  $rfp_data ['bid_p'] = $rfp_data ['workorder_p'] = $rfp_data ['closed_p'] =  $rfp_data ['total'] = 0;

        }
        


        $sr_data ['open'] =  $client->rfps()->where(['status'=>'open', 'type'=>'SR'])->count();

        $sr_data ['sent'] =  $client->rfps()->where(['status'=>'sent', 'type'=>'SR'])->count();

        $sr_data ['bid'] =  $client->rfps()->where(['status'=>'bid', 'type'=>'SR'])->count();

        $sr_data ['workorder'] =  $client->rfps()->where(['status'=>'workorder', 'type'=>'SR'])->count();

        $sr_data ['closed'] =  $client->rfps()->where(['status'=>'closed', 'type'=>'SR'])->count();

        $sr_data ['total'] =  $client->rfps()->where(['type'=>'SR'])->count();

        
        if ( $sr_data ['total'] > 0 ){

            $sr_data ['open_p'] = round((($sr_data ['open'] / $sr_data ['total']) * 100) , 1);

            $sr_data ['sent_p'] = round((($sr_data ['sent'] / $sr_data ['total']) * 100), 1);

            $sr_data ['bid_p'] = round((($sr_data ['bid'] / $sr_data ['total']) * 100), 1);

            $sr_data ['workorder_p'] = round((($sr_data ['workorder'] / $sr_data ['total']) * 100), 1);

            $sr_data ['closed_p'] = round((($sr_data ['closed'] / $sr_data ['total']) * 100), 1);

        } else {

            $sr_data ['open_p'] = $sr_data ['sent_p'] =  $sr_data ['bid_p'] = $sr_data ['workorder_p'] = $sr_data ['closed_p'] =  $sr_data ['total'] = 0;

        }


        //WORKORDER DATA
        $wko_data ['open'] = $rfp_data ['workorder'] + $sr_data ['workorder'];

        $wko_data ['closed'] = $rfp_data ['closed'] + $sr_data ['closed'];
        
        $wko_data ['total'] = $wko_data ['open'] + $wko_data ['closed'];

        if($wko_data ['total'] > 0) {

            $wko_data ['open_p'] = round((($wko_data ['open'] / $wko_data ['total']) * 100) , 1);

            $wko_data ['closed_p'] = round((($wko_data ['closed'] / $wko_data ['total']) * 100), 1);

        } else {

            $wko_data ['total'] = $wko_data ['open_p'] = $wko_data ['closed_p'] = 0;
        }


        return view('client/dashboard', compact('rfp_data','sr_data','wko_data'));
              
    }
}
