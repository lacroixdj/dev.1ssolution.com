<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\Input;
use Response;

//custom
use Log;
use App\Client;
use App\Vendor;
use App\Industry;
use App\Attribute;
use App\Service;
use App\State;
use App\Categories;
use App\Metroarea;
use App\Jobs\Postman;
use App\Invite;
use App\User;
use Datatables;
use Mail;

class VendorFavoritesController extends Controller
{
    public function __construct(){
         
        $this->middleware('auth');
    }

    
    /**
     * Show all of the users for the application.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");    
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
             
        $industries = Industry::all()->sortBy('name')->lists('name','id')->toArray();
        
        $categories = Categories::where('table', 'vendors_short')->lists('label','value')->toArray();
                
        // FILTERS
       
        $name = Input::get('filter', null);
        
        $industry_id = Input::get('industry_id', null); 
        
        if ($industry_id == "null")
        {
             
             $industry_id = null;
             
        }
        
        $short_id = Input::get('short_id', null); 
    
        if ($short_id == "null")
        {
             
             $short_id = null;
             
             $short_id_arr = null;
             
        }
        else
        {
            
            $short_id_arr = explode("-", $short_id);
        
        }
        //Prelod the vendors favorites
        
        $vendorInFavotites = $client->vendors_favorites()->get()->toArray();
        
        //dd(array_pluck($vendorInFavotites, 'id'));
        
        $vendorInFavotites = array_pluck($vendorInFavotites, 'id');
        //dd(count($vendorInFavotites));
    
        
        // QUERY 
        $vendors = \App\Vendor::where('metroarea_id', '=', $client->metroarea_id)

        ->with('industry') 
            
        ->with('user')

        ->with('metroarea')

        ->with('addresses');
            
        // Add name filter
        $vendors = is_null($name) ? $vendors : $vendors->where('vendors.name', 'like', '%'.$name.'%');
        
        // Add industry_id filter
        $vendors = is_null($industry_id) ? $vendors : $vendors->where('industry_id', $industry_id);
        
        // Add short_id filter
        $vendors = is_null($short_id) ? $vendors : $vendors->orderBy($short_id_arr[0],$short_id_arr[1]);

        //laravel Paginate 
        $vendors = $vendors->paginate(12);
        
        
        //Vendors Invitations Query
        $vendorsIvitations = Invite::where('user_id','=', Auth::User()->id)->where('type', '=', 'client-to-vendor');
        
        //laravel Paginate 
        $vendorsIvitations = $vendorsIvitations->paginate(6);
        
        //dd($vendorsIvitation);
        
                                
        return view('vendorInvite.vendorAll', compact('vendors', 'name', 'industries', 'categories','industry_id', 'short_id','vendorInFavotites','vendorsIvitations'));
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function autocomplete()
    {
        
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");    
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        $term = Input::get('term');
        
        $results = array();
        
        $queries =  \App\Vendor::where('metroarea_id', '=', $client->metroarea_id)
            
            ->with('industry')
            
            //->with('user')
            
            //->with('metroarea')
            
            //->with('addresses')
            
            ->where('vendors.name', 'LIKE', '%'.$term.'%')
            
            //->orWhere('industry.name', 'LIKE', '%'.$term.'%')
            
            ->take(5)->get();
	
        //dd($queries);
        foreach ($queries as $query){
	    
            $results[] = [ 'id' => $query->id, 'value' => $query->name ];
            
	   }
        
        return Response::json($results);
    
    }
    
     /**
     * Attach Vendor Favorites.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function attachVendor(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            return response()->json(['error_message' => "No client account found"], 403);
        
        }
        
        if ($client->vendors_favorites->contains($request->sincValue)) {
            
            $result = 'NO-OK';
            
        } else {
            
            $result = $client->vendors_favorites()->attach($request->sincValue);
            
            $result = 'Ok';
        }
        
        if($result=='Ok') {
                                    
            Session::flash('flash_message', 'Vendor Attach');
            
             $response = [

                    'title' => 'Vendor Favorite',
            
                    'message' => 'Vendor added successfully',

                    'callback_func' => 'markFavorite',
            
                    'text' => '',
                 
                    'widget_type' => 'flashMessage',
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'The vendor is already in favorites'], 422);
       
        } 
        
    }
    
    
    /**
     * Attach Vendor Invitatio to Favorites.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function attachInvitationToFavorite(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            return response()->json(['error_message' => "No client account found"], 403);
        
        }
        
        $result = false;
        
        $vendorUser = User::where('email', '=', $request->sincValue)->firstOrFail();
        
        //verifiacar que exista un vendedor asociado
        if($vendorUser->vendors()->where('user_id', '=',$vendorUser->id)->exists()){
            
            $vendor = $vendorUser->vendors()->where('user_id', '=',$vendorUser->id)->firstOrFail();
                        
            if ($client->vendors_favorites->contains($vendor->id)) {
                        
            } else {
                
                $result = $client->vendors_favorites()->attach($vendor->id);
                
                $result_update = Invite::where('user_id', '=', Auth::User()->id)
                                        ->where('email', '=', $request->sincValue)
                                        ->update(['status' => 'favorite'] );
                
                $result = 'OK';
            }
            

        } else {

        }
        
        if($result == 'OK') {
                        
            Session::flash('flash_message', 'Vendor Attach');
            
             $response = [

                    'title' => 'Vendor Favorite',
            
                    'message' => 'Vendor added successfully',

                    'callback_func' => 'reloadPage',
            
                    'text' => '',
                 
                    'widget_type' => '',
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'The vendor is already in favorites'], 422);
       
        } 
        
    }
    
     /**
     * Detach Vendor Favorites.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detachVendor(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            return response()->json(['error_message' => "No client account found"], 403);
        
        }
                    
        $this->validate($request, [

            'sincValue' => 'required', 

        ]);
        
        //dd($request->sincValue);
        
        if ($client->vendors_favorites->contains($request->sincValue)) {
            
            
            $result = $client->vendors_favorites()->detach($request->sincValue);
            
            $result = 'Ok';
            
           
            
        } else {
            
             $result = 'NO-OK';
        }
        
        if($result=='Ok') {
                        
            Session::flash('flash_message', 'Vendor Detach');
            
             $response = [

                    'title' => 'Vendor Favorite',
            
                    'message' => 'Vendor remove successfully',

                    'callback_func' => '',
            
                    'text' => '',
                 
                    'widget_type' => 'flashMessage',
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'Error removing the vendor'], 422);
       
        } 
        
    }
    
    
     /**
     * Invite Vendor Favorites.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inviteVendor(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            return response()->json(['error_message' => "No client account found"], 403);
        
        }
                    
        $this->validate($request, [

            'name' => 'required', 

            'email' => 'required'
        ]);
        
        
        if (Invite::where('email', '=', Input::get('email'))->where('user_id', '=', Auth::User()->id)->exists()) {
            
            return response()->json(['error_message' => 'The vendor is already invited'], 422);
                    
        }else{
            
            $invite = Invite::create([
            
                'user_id' => Auth::User()->id,

                'name' => $request->name,

                'email' => $request->email,

                'phone' => $request->phone,

                'type' => 'client-to-vendor',

                'status' => 'sent',

                'verification_code' => str_random(30),

            ]);
            
        }
        
        if($invite) {
                        
            $retrev_invite = Invite::where('email', '=', $request->email)->where('user_id', '=', Auth::User()->id)->first();
                                    
            Session::flash('flash_message', 'Vendor Invite');
            
            $data = [
            
                    'mode' => 'html',

                    'view' => 'emails.vendor_invite',
                    
                    'email_to' => $request->email,

                    'receiver_name' => $request->name,
                    
                    'subject' => Auth::User()->name.' '.Auth::User()->lastname.', invites you to participate in One Seamless Solution, a service providers marketpalce',    

                    'verification_code' => $retrev_invite->verification_code,
                
                    'img_url' => url(('/')),
                ];
                        
             
            $job = ( new Postman($data))

                ->onQueue('emails')

                ->delay(3);    

            $this->dispatch($job);
            
            
            $response = [

                    'title' => 'Vendor Favorite',
            
                    'message' => 'Vendor invited successfully!',

                    'callback_func' => 'reloadPage',
            
                    'text' => '',
                 
                    'widget_type' => '',
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'The vendor is already in favorites'], 422);
       
        } 
            
    }
    
    
    
    /**
     * Load vendor favrotes view
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function favorites(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");    
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
             
        $industries = Industry::all()->sortBy('name')->lists('name','id')->toArray();
        
        $categories = Categories::where('table', 'vendors_short')->lists('label','value')->toArray();
                
        // FILTERS
       
        $name = Input::get('filter', null);
        
        $industry_id = Input::get('industry_id', null); 
        
        if ($industry_id == "null")
        {
             
             $industry_id = null;
             
        }
        
        $short_id = Input::get('short_id', null); 
    
        if ($short_id == "null")
        {
             
             $short_id = null;
             
             $short_id_arr = null;
             
        }
        else
        {
            
            $short_id_arr = explode("_", $short_id);
        
        }
        //Prelod the vendors favorites
        
        $vendorInFavotites = $client->vendors_favorites()->get()->toArray();
        
        //dd(array_pluck($vendorInFavotites, 'id'));
        
        $vendorInFavotites = array_pluck($vendorInFavotites, 'id');
        //dd($vendorInFavotites);
    
        
        // QUERY 
        $vendors = $client->vendors_favorites()

        ->with('industry') 
            
        ->with('user')

        ->with('metroarea')

        ->with('addresses');
            
        // Add name filter
        $vendors = is_null($name) ? $vendors : $vendors->where('vendors.name', 'like', '%'.$name.'%');
        
        // Add industry_id filter
        $vendors = is_null($industry_id) ? $vendors : $vendors->where('industry_id', $industry_id);
        
        // Add short_id filter
        $vendors = is_null($short_id) ? $vendors : $vendors->orderBy($short_id_arr[0],$short_id_arr[1]);

        //laravel Paginate 
        $vendors = $vendors->paginate(6);        
        
        //Vendors Invitations Query
        $vendorsIvitations = Invite::where('user_id','=', Auth::User()->id)
                            
                            ->where('type', '=', 'client-to-vendor')
                    
                            ->whereNotIn('status', ['favorite'])
                            
                            ->orderBy('status', 'asc'); 
        
        //Not in favorite
        
        //laravel Paginate 
        $vendorsIvitations = $vendorsIvitations->paginate(6);
        
        //dd($vendorsIvitation);
                                
        return view('vendorInvite.vendorFavorites', compact('vendors', 'name', 'industries', 'categories','industry_id', 'short_id','vendorInFavotites', 'vendorsIvitations'));
    }
    
    
    /**
     * Show all of the users for the application.
     *
     * @return Response
     */
    public function indexInvites(Request $request)
    {
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");    
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        
        $shorInvitesDate = Categories::where('table', 'invitations_short')->lists('label','value')->toArray();
        
        //Short by date 
        
        $short_id = Input::get('short_id', null); 
        
         if ($short_id == "null")
        {
             
             $short_id = null;
             
             $short_id_arr = null;
             
        }
        else
        {
            
            $short_id_arr = explode("-", $short_id);
        
        }
        
        //------------
        
        //Filter by status
        
        $filterInvitesStatus = Categories::where('table', 'invitations_filter')->lists('label','value')->toArray();
        
        $filter_id = Input::get('filter_id', null); 
        
        if ($filter_id == "null")
        {
             
             $filter_id = null;
             
        }
        
        //--------------
        
        //Filter by name
        
        $name = Input::get('filter', null);
        
        //The query
        
        $vendorsIvitations = Invite::where('user_id','=', Auth::User()->id)
                            
                            ->where('type', '=', 'client-to-vendor')
                    
                            ->whereNotIn('status', ['favorite', 'invitation-removed']);
                            
                            //->orderBy('status', 'asc'); 
        
        //dd(count($vendorsIvitations));
        
        $vendorsIvitations = is_null($name) ? $vendorsIvitations : $vendorsIvitations->where('invites.name', 'like', '%'.$name.'%');
        
        // Add short_id filter
        $vendorsIvitations = is_null($short_id) ? $vendorsIvitations : $vendorsIvitations->orderBy($short_id_arr[0],$short_id_arr[1]);
        
        // Add filter for Status
        $vendorsIvitations = is_null($filter_id) ? $vendorsIvitations : $vendorsIvitations->where('status', $filter_id);
        
        
        //laravel Paginate 
        $vendorsIvitations = $vendorsIvitations->paginate(12);
        
        //dd($vendorsIvitation);
                                
        return view('vendorInvite.vendorInvited', compact('name','vendorsIvitations', 'shorInvitesDate', 'short_id', 'filterInvitesStatus', 'filter_id'));
             
    }
    
    
     /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function autocompleteInvite()
    {
        
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");    
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }
        
        $term = Input::get('term');
        
        $results = array();
        
        $queries =  \App\Invite::where('user_id', '=' ,$client->user_id )->where('name', 'LIKE', '%'.$term.'%')->take(100)->get();
	
        //dd($queries);
        foreach ($queries as $query){
	    
            $results[] = [ 'id' => $query->id, 'value' => $query->name ];
            
	   }
        
        return Response::json($results);
    
    }


}


