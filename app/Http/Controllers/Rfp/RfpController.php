<?php

namespace App\Http\Controllers\Rfp;


use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Rfp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;


//custom
use Log;
use App\Client;
use App\Vendor;
use App\Industry;
use App\Attribute;
use App\Service;
use App\State;
use App\Metroarea;
use App\Jobs\Postman;
use Datatables;
use Mail;


class RfpController extends Controller  {


    /**
    * Class constructor.
    *
    * @param  int  $id
    *
    * @return void
    */
    public function __construct(){
         
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $rfp = Rfp::paginate(15);

        return view('rfp.index', compact('rfp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request) {

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }



        if ($request->session()->has('last_rfp_id')) $request->session()->forget('last_rfp_id');

        $industries = Industry::all()->sortBy('name')->lists('name','id')->toArray();

        $states =     State::where('country_id', 231)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $metroareas = Metroarea::all()->sortBy('name')->lists('name','id')->toArray();

        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();

        

        $default_location= $client_address->address.', '.$client_address->city->name.', '.$client_address->city->state->name;

        


        return view('rfp.create', compact('industries', 'states', 'metroareas', 'default_location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request) {

        
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client account found"], 403);
        
        }

        
        
        $this->validate($request, [            

            'description' => 'required', 

            'location' => 'required', 

            'emergency' => 'required',

            'service_date' => 'required', 

            'response_date' => 'required', 

            'type' => 'required', 

            'visit' => 'required', 

            'details' => 'required'

        ]);

        $service_date_arr = explode('-', $request->service_date);
        
        $service_date = Carbon::create($service_date_arr[2], $service_date_arr[0], $service_date_arr[1]);

        $response_date_arr = explode('-', $request->response_date);
        
        $respose_date = Carbon::create($response_date_arr[2], $response_date_arr[0], $response_date_arr[1]);

        
        $data = [

            'user_id' => Auth::User()->id, 

            'industry_id' => $request->industry_id, 

            'emergency' => $request->emergency, 

            'description' => $request->description, 

            'location' => $request->location, 

            'service_date' => $service_date, 

            'response_date' => $respose_date, 

            'type' => $request->type, 

            'visit' => $request->visit, 

            'details' => $request->details, 

            'documents' => $request->documents, 

        ];

        $result = false;        

        if ($request->session()->has('last_rfp_id')) {
    
            $rfp_id = $request->session()->get('last_rfp_id');

            $rfp = $client->rfps()->findOrFail($rfp_id);

            $result = $rfp->update($data);

        } else {

            $data['number'] = $client->id;

            $rfp = $result = $client->rfps()->create($data);            

        }    


        if($result) {

            Session::flash('flash_message', 'Rfp created');

       
            $request->session()->put('last_rfp_id', $rfp->id);

     
            if (!empty($request->draft)) {

                $wizzard_id = false;

                $callback_func = 'setRfpValues2';

                $message = 'Success! Request has been saved, you can edit and complete it later...';
            
            }else{
                
                //$wizzard_id = 'myWizard';

                $wizzard_id = false;


                $callback_func = 'setRfpValues';

                $message = 'Success! Request has been saved';
            }


            $response = [

                'title' => 'Rfp',
        
                'message' => $message,
        
                'text' => '',

                'object_id' => $rfp->id,

                'wizard_id' => $wizzard_id,

                'callback_func' => $callback_func,

                                

            ];   
        
            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error creating the request'], 422);
       
        }

        // Rfp::create($request->all());

        // Session::flash('flash_message', 'Rfp added!');

        // return redirect('rfp');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $rfp = Rfp::findOrFail($id);

        return view('rfp.show', compact('rfp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id, $step)    {
        

        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client account found");   
        
        }

        if(empty($step) || !is_numeric($step)) $step = 1;
        elseif ($step <1 || $step>2) $step = 1;


        //$rfp = Rfp::findOrFail($id);

        //return view('rfp.edit', compact('rfp'));

        $rfp = $client->rfps()->findOrFail($id);

        if (session()->has('last_rfp_id')) session()->forget('last_rfp_id');

        session()->put('last_rfp_id', $rfp->id);



        $industries = Industry::all()->sortBy('name')->lists('name','id')->toArray();

        $states =     State::where('country_id', 231)->get()->sortBy('name')->lists('name','id')->toArray();
        
        $metroareas = Metroarea::all()->sortBy('name')->lists('name','id')->toArray();

        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();
                

        $yes_no = array('No', 'Yes');
        
        $vendors_data = $rfp->vendors()->with('addresses.city.state')->with('metroarea')->get(); 

        $vendors_count = $vendors_data->count();

        //$vendors_data = json_encode($vendors_data);        
        
        if(!empty($vendors_data) && count($vendors_data)){

            foreach ($vendors_data as $key => $value) {
            
                $vendors_data[$key]->services = $value->services->lists('name');      

                $vendors_data[$key]->attributes = $value->attributes->lists('name');      

            }

        }   
         
        return view('rfp.edit', compact('industries', 'states', 'metroareas', 'rfp', 'vendors_data','yes_no','vendors_count','step'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['client_id' => 'required', 'user_id' => 'required', 'number' => 'required', 'status' => 'required', 'emergency' => 'required', 'description' => 'required', 'location' => 'required', 'service_date' => 'required', 'response_date' => 'required', 'type' => 'required', 'visit' => 'required', 'details' => 'required', 'documents' => 'required', ]);

        $rfp = Rfp::findOrFail($id);
        
        $rfp->update($request->all());

        Session::flash('flash_message', 'Rfp updated!');

        return redirect('rfp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Rfp::destroy($id);

        Session::flash('flash_message', 'Rfp deleted!');

        return redirect('rfp');
    }


    /**
     *
     * @param  object  $request
     *
     * @return void
     */
    public function listVendors(Request $request) {

        // Get industry_id:

        $industry_id = $request->input('industry_id');

        if(empty($industry_id)) {

            // If there is no industry_id do not return result set:

            $response['recordsTotal'] = $response['recordsFiltered'] = 0;

            $response['data'] = [];

        } else {

            // otherwise:

            // Get filters if any:

            $services_ids = $request->input('services_ids');

            $attributes_ids = $request->input('attributes_ids');
            
            $metroarea_id = $request->input('metroarea_id');
            
            $state_id = $request->input('state_id');
           
            $city_id = $request->input('city_id');

            $search_field = $request->input('search_field');

            // Get pagination limmit and offset:

            $start = (!empty($request->input('start')))? $request->input('start') : 0;

            $length = (!empty($request->input('length')))? $request->input('length') : 5;           
          
            // Get column headers:

            $columns = $request->input('columns');

            // Get order by column:

            $order = $request->input('order');

            $order_by = (!empty( $columns[($order[0]['column'])]['data'])) ? $columns[($order[0]['column'])]['data'] : 'name';

            $order_by = ($order_by == 'id') ? 'vendors.name': $order_by;  

            $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';

            // Build DB Query:

            $vendors = DB::table('vendors')

                ->select(

                    'vendors.*', 

                    'vendor_addresses.address', 

                    'vendor_addresses.zip', 

                    'metroareas.name as metroarea',

                    'cities.name as city',

                    'states.name as state'
                )

                ->join('metroareas', 'vendors.metroarea_id', '=', 'metroareas.id')

                ->join('vendor_addresses', 'vendors.id', '=', 'vendor_addresses.vendor_id')

                ->join('cities', 'vendor_addresses.city_id', '=', 'cities.id')

                ->join('states', 'cities.state_id', '=', 'states.id')                

                ->join('service_vendor', 'vendors.id', '=', 'service_vendor.vendor_id')
                
                ->leftJoin('attribute_vendor', 'vendors.id', '=', 'attribute_vendor.vendor_id')
                
                ->where('industry_id', '=', $industry_id)

                ->where('main_address', '=', 1)

                ->when($metroarea_id, function ($query) use ($metroarea_id) {
                
                        return $query->where('metroarea_id', '=', $metroarea_id);
                })

                ->when($state_id, function ($query) use ($state_id) {
                
                        return $query->where('states.id', '=', $state_id);
                })

                ->when($city_id, function ($query) use ($city_id) {
                
                        return $query->where('city.id', '=', $city_id);
                })

                ->when($services_ids, function ($query) use ($services_ids) {
                
                        return $query->whereIn('service_vendor.service_id', $services_ids);
                })
                
                ->when($attributes_ids, function ($query) use ($attributes_ids) {
                
                        return $query->whereIn('attribute_vendor.attribute_id', $attributes_ids);
                })

                ->when($search_field, function ($query) use ($search_field) {

                    return $query->where(function ($query) use ( $search_field ) {

                            $search_param [] = strtolower("%$search_field%");
                            
                            $query->whereRaw( 'LOWER( `vendors`.`name`) like ?', $search_param);

                            $query->orWhereRaw( 'LOWER( `vendors`.`phone`) like ?', $search_param);
                            
                            $query->orWhereRaw( 'LOWER( `vendors`.`email`) like ?', $search_param);
                            
                            $query->orWhereRaw( 'LOWER( `vendors`.`url`) like ?', $search_param);
                            
                            $query->orWhereRaw( 'LOWER( `address`) like ?', $search_param);
                            
                            $query->orWhereRaw( 'LOWER( `cities`.`name`) like ?', $search_param);
                            
                            $query->orWhereRaw( 'LOWER( `states`.`name` ) like ?', $search_param);

                            $query->orWhereRaw( 'LOWER( `metroareas`.`name`) like ?', $search_param);
    
                        });                   
                
                })

                ->orderBy($order_by, $order_dir)

                ->distinct();

                //dd($vendors->toSq());

            //Get rows count:               
            
            $response['recordsTotal'] = $response['recordsFiltered'] = count(($vendors->get()));

            // Get paginated data set:

            $response['data'] = $vendors->skip($start)->take($length)->get();

            foreach ($response['data'] as $key => $value) {

                $vendor = Vendor::find(($value->id));

                $response['data'][$key]->services = $vendor->services->lists('name');

                $response['data'][$key]->attributes = $vendor->attributes->lists('name');

            }

        }

        // Returning the response in Json Format:

        return json_encode($response);

    }

     /**
     *
     * @param  object  $request
     *
     * @return void
     */
    public function requestVendors(Request $request) {

        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client account found"], 403);
        
        }

        //dd($request);

        $this->validate($request, [

            'vendors_ids' => 'required', 

            'services_ids' => 'required'
        ]);

        if ($request->session()->has('last_rfp_id')) {
    
            $rfp_id = $request->session()->get('last_rfp_id');

            $rfp = $client->rfps()->findOrFail($rfp_id);

            $vendors_ids = $request->input('vendors_ids');

            $services_ids = $request->input('services_ids');

            $result = $rfp->vendors()->sync($vendors_ids);

            $result2 = $rfp->services()->sync($services_ids);

            if($result && $result2) {

                if (!empty($request->draft)) {

                    $wizzard_id = false;

                    $callback_func = 'redirectHome';

                    $message = 'Success! Selected Vendors have been saved , you can edit and complete it later...';
            
                }else{

                    //$wizzard_id = 'myWizard';

                    $wizzard_id = false;

                    $callback_func = '';

                    $message = 'Success! Selected Vendors have been saved ';
                }

                Session::flash('flash_message', 'Vendors attached to Rfp successfully');
            
                $response = [

                    'title' => 'Rfp',
            
                    'message' => $message,
            
                    'text' => '',

                    'object_id' => $rfp_id,

                    'vendors_data' => $rfp->vendors()->with('addresses.city.state')->with('metroarea')->get(),

                    'wizard_id' => $wizzard_id,

                    'callback_func' => $callback_func                          
                ];   
        
                return response()->json($response, 200);

            } else {

                return response()->json(['error_message' => 'There was an error attaching Vendors to Rfp'], 422);
       
            }

        } else {

            return response()->json(['error_message' => 'There was an error getting the request id'], 422);

        }

    }


    /**
     *
     * @param  object  $request
     *
     * @return void
     */
    public function sendRequest(Request $request) {

        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client account found"], 403);
        
        }

        //dd($request);

        if ($request->session()->has('last_rfp_id')) {
    
            $rfp_id = $request->session()->get('last_rfp_id');

            $rfp = $client->rfps()->findOrFail($rfp_id);

            $data = [

                'status' => 'sent'

            ];


            $rfp->status = 'sent';

            $result = $rfp->update();
            
            //$result = $rfp->update($data);

            Log::info("Request vendors invitation emails queue started");

            $yes_no = array('No', 'Yes');

            foreach ($rfp->vendors as $vendor) {
            
                //$vendor->pivot->status = 'sent';

                $vendor->pivot->status = 'invited';

                $result *= $vendor->pivot->save();
                
                $data = [

                    'mode' => 'html',
            
                    'view' => 'emails.request_vendor',
                    
                    'email_to' => $vendor->user->email,

                    'receiver_name' => $vendor->user->name,
                    
                    'subject' => 'You has been requested for a service',    
                    
                    'vendor_company_name' => $vendor->name,

                    'client_company_name' => $client->name,

                    'client_company_phone' => $client->phone,

                    'client_company_email' => $client->email,

                    'vendor_password' => $vendor->user->passwordplain,
                    
                    'rpf_id' => $rfp->id,

                    'bid_url' => url(('/rfp/vendor/requestdetail/'.$rfp->id)),

                    'rpf_type' => $rfp->type,

                    'rpf_description' => $rfp->description,

                    'rpf_location' => $rfp->location,

                    'rpf_response_by' => $rfp->response_date,

                    'rpf_service_by' => $rfp->service_date,

                    'rpf_details' => $rfp->details,

                    'rfp_documents' => $rfp->documents,

                    'rfp_visit' => $yes_no[($rfp->visit)], 

                    'rfp_emergency' => $yes_no[($rfp->emergency)], 
                    
                    'img_url' => url(('/')),

                                   
                ];

                
                //Vrify if vendor user accept recive emails
                $check = checkSetting(1, $vendor->user->id);

                if($check == 1){
                    
                    $job = ( new Postman($data))
                    
                            ->onQueue('emails')

                            ->delay(3);
                

                    $this->dispatch($job);
                }
                
                //Send sms to vendor user phone
                
                $checkSms = checkSetting(2, $vendor->user->id);
                
                if( $vendor->user->sms_address != '' && $checkSms == 1 ){

                    $dataSms = [

                        'mode' => 'text',

                        'view' => 'emails.sms',

                        'email_to' =>  $vendor->user->sms_address,

                        'receiver_name' =>  $vendor->user->name.' '. $vendor->user->lastname ,

                        'subject' => 'You has been requested for a service', 

                        'message_to' => 'You has been requested for a service, To view, click on the following url - '.url('/rfp/vendor/requestdetail/'.$rfp->id).' -- The One Seamless Solution team.'

                    ];
                    
                    $job2 = ( new Postman($dataSms))
                    
                            ->onQueue('emails')

                            ->delay(3);
                

                    $this->dispatch($job2);


                }
                
            
            }   

            Log::info("Request vendors invitation emails queue finished");

            if($result) {

                Session::flash('flash_message', 'Rfp sent successfully');
            
                $response = [

                    'title' => 'Rfp',
            
                    'message' => 'Rfp sent successfully',

                    'callback_func' => 'redirectHome',
            
                    'text' => '',
                    
                ];   
        
                return response()->json($response, 200);

            } else {

                return response()->json(['error_message' => 'There was an error sending the RFP'], 422);
       
            }

        } else {

            return response()->json(['error_message' => 'There was an error sending the RFP'], 422);

        }

    }
    

    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function listRfpVendors(Request $request) {


        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
        
        $array_where = [

            ['rfp_vendor.status','!=', 'open'],

        ];

        
        if(!empty($request->type)) $array_where [] = ['rfps.type','=', $request->type];

        if(!empty($request->status)) $array_where [] = ['rfp_vendor.status','=', $request->status];


        $rfps = $vendor->rfps()

                ->with('industry')

                ->with('client')
        
                ->where($array_where)

                ->get();
                
        return Datatables::of($rfps)->make(true);

        /*$first = DB::table('rfp_vendor')
            
            ->select([ 'rfps.id', 'rfps.type','rfps.description','rfps.service_date as service_date', 'rfp_vendor.bid_amount', 'rfp_vendor.status', 'rfps.location','users.name', 'users.email'])
            
            ->join('rfps', 'rfp_vendor.rfp_id', '=','rfps.id' )
            
            ->join('clients', 'rfps.client_id', '=', 'clients.id')
            
            ->join('users', 'rfps.user_id', '=', 'users.id')
            
            ->where('rfp_vendor.vendor_id', $vendor['id']) 
            
            ->where('rfp_vendor.status','!=', 'open');*/

    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function rfpVendorsDetails($id){
        
        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No vendor account found");
            
            abort(403, "No vendor account found");   
        }

        $rfp = $vendor->rfps()->find($id);

        if(empty($rfp)) {

            view()->share('HTTP_ErrorMessage', "No vendor account found");
            
            abort(403, "No vendor account found");

        }
        
        $client = Client::findOrFail($rfp->client_id);
        
        $client_address = $client->addresses()->with('city.state.country')->where('main_address','=', 1)->first();

        $vendorAddresses = $vendor->addresses()->first();

        return view('vendor.requestdetail', compact('rfp','vendor','vendorAddresses', 'client', 'client_address'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function bidUpdate(Request $request){
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->vendors->first())) {

            $vendor =  Auth::User()->vendors()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
        
        //Validating
        $this->validate($request, [
            'id' => 'required',
            'bid' => 'required', 

        ]);
        
        //Guardar el bid 
        $data= [
           'id' => $request->id,
           'bid_amount' => $request->bid,
        ];
        
        //TODO: Validar si ya el cambo bid_amount es diferente de 0.00 no poder volverlo a modificar
        
        $myRfp = $vendor->rfps()->findOrFail($request->id);
        
        $myRfp->pivot->bid_amount = $request->bid;
        
        $myRfp->pivot->status = 'bid';

        $myRfp->status = 'bid';
                        
        $result = $myRfp->pivot->save(); 

        $result2 = $myRfp->save(); 
        
        //Client user data
        
        $clientUser = Client::findOrFail($myRfp->client_id);

        $clientUser = Auth::User()->findOrFail($clientUser->user_id);
        
        $data = [
            
                    'mode' => 'html',

                    'view' => 'emails.setBidAmount',
                    
                    'email_to' => $clientUser->email,

                    'receiver_name' => $clientUser->name,
                    
                    'subject' => 'Bid amount in request #'.$myRfp->id,    
                    
                    'id' => $myRfp->id,
                
                    'clientName' => $clientUser->name,

                    'clientEmail' => $clientUser->email,

                    'client_password' => $clientUser->passwordplain,

                    'serviceDescription' =>  $myRfp['description'],

                    'serviceDetails' =>  $myRfp['details'],

                    'vendorName' =>  $vendor['name'],

                    'vendorPhone' =>  $vendor['phone'],

                    'vendorEmail' =>  $vendor['email'],

                    'vendorId' =>  $vendor['id'],

                    'bid_url' => url(('/rfp/client/requestdetail/'.$myRfp->id.'/'.$vendor->id)),
             
                    'img_url' => url(('/')),

                                   
                ];
                
        if($result && $result2) {
        //if($result) {

            Session::flash('flash_message', 'Bid send!');
            
            $response = [

                'title' => 'Bid',
        
                'message' => 'Success! You have updated the bid for this request',
        
                'text' => '',

            ];   
            
            $checkEmail = checkSetting(1, $clientUser->id);
            
            if($checkEmail == 1){
            
                $job = ( new Postman($data))

                ->onQueue('emails')

                ->delay(3);    

                $this->dispatch($job);
            
            }
            
            //Send sms to vendor user phone
            
            $checkSms = checkSetting(2, $clientUser->id);
            
            if($checkSms == 1){
                
                if( $clientUser->sms_address != '' || $clientUser->sms_address != null && $checkSms == 1){

                    $dataSms = [

                        'mode' => 'text',

                        'view' => 'emails.sms',

                        'email_to' =>  $clientUser->sms_address,

                        'receiver_name' =>  $clientUser->name.' '. $clientUser->lastname ,

                        'subject' => 'Bid amount in request', 

                        'message_to' => 'Bid amount in request #'.$myRfp->id.', click on the following url - '.url('/rfp/client/requestdetail/'.$myRfp->id.'/'.$vendor->id).' -- The One Seamless Solution team.', 

                    ];
                    //TODO: Lo envio con este metodo, por el otro crea un html adjunto, revisr esto.
                    /*Mail::send(array('text' => 'emails.sms'), $dataSms, function($message) use ($dataSms)
                    {
                        $message->from('info@1ssolution.com', "One Seamless Solution");
                        $message->subject($dataSms['subject']);
                        $message->to($dataSms['email_to']);
                    });*/   

                    $job2 = ( new Postman($dataSms))

                        ->onQueue('emails')

                        ->delay(3);    

                    $this->dispatch($job2);

                }
            }
            
        
            return response()->json($response, 200);
            
        } else {

            return response()->json(['error_message' => 'There was an error updating the bid for this request'], 422);
       
        }       
        
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function listRfpClients(Request $request) {

        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
                 
        $first = DB::table('rfps')
            
            ->select([ 'rfps.id', 'rfps.type','rfps.description', 'vendors.name', 'rfps.service_date as service_date', 'rfp_vendor.bid_amount', 'rfp_vendor.status' ,'rfp_vendor.vendor_id'])
            
            ->join('rfp_vendor', 'rfp_vendor.rfp_id', '=', 'rfps.id')
            
            ->join('vendors', 'rfp_vendor.vendor_id', '=', 'vendors.id')
            
            ->where('rfps.client_id', $client['id']);    
        
        return Datatables::of($first)->make(true);
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function rfpClientsDetails($id, $vendor_id){
        if(!(Auth::check())) {

           view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");
            
            abort(403, "No client account found");   
        
        }
                    
        $vendor = Vendor::findOrFail($vendor_id);
        
        $rfp = $vendor->rfps()->findOrFail($id);
                 
        $vendorAddresses = $vendor->addresses()->findOrFail($vendor_id);
         
        return view('client.requestdetail', compact('rfp','vendor','vendorAddresses','client'));
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    //DATATABLE GET RFPS OF CLIENT
    //TODO: Esta funcion elimina la anterior que llena la listas del cliente

     public function listRfpClient(Request $request) {
         
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }
        
        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } 
        
        else {

            return response()->json(['error_message' => "No client account found"], 403);
        
        }

        $array_where = [];

        if(!empty($request->rfp_id)) $array_where [] = ['rfps.id','=', $request->rfp_id];

        if(!empty($request->type)) $array_where [] = ['rfps.type','=', $request->type];

        if(!empty($request->status)) $array_where [] = ['rfps.status','=', $request->status];


        $rfps = $client->rfps()

                ->with('industry')

                ->withCount(['vendors', 'vendors_responses'])   

                ->where($array_where)

                ->get();
        
        return Datatables::of($rfps)->make(true);
    }


    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
     public function listRfpVendorsBid($rfp_id) {
         
       
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
        
            $vendors_data = '';    

            $rfp = $client->rfps()->find($rfp_id); 

            //$vendors_data = $rfp->vendors()->with('addresses.city.state')->with('metroarea')->with('rfps')->with('industry')->get(); 
            
            $vendors_data = $rfp->vendors()->with('user')->with('addresses.city.state')->with('metroarea')->with('industry')->get(); 


            foreach ($vendors_data as $key => $value) {

                $vendors_data[$key]->services = $value->services->lists('name');      

                $vendors_data[$key]->attributes = $value->attributes->lists('name');      

            }
            
            //dd($vendors_data);
            
            //dd('aqui');
         
            return Datatables::of($vendors_data)->make(true);
         
    }
    

    public function RfpDetail($id){
        
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first())) {

            $client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No vendor account found"], 403);
        
        }
                            
        
        $yes_no = array('No', 'Yes');

        $rfp = $client->rfps()->findOrFail($id); 

        $vendors_data = $rfp->vendors()->with('addresses.city.state')->with('metroarea')->get(); 

        
        if(!empty($vendors_data) && count($vendors_data)){

            foreach ($vendors_data as $key => $value) {
            
                $vendors_data[$key]->services = $value->services->lists('name');      

                $vendors_data[$key]->attributes = $value->attributes->lists('name');      

            }

        }         

        //dd($vendors_data);
        /*
        echo "<pre>";
        echo json_encode($vendors_data);
        echo "</pre>";
        die();*/

        return view('client.rfpdetail', compact('rfp','vendors_data','yes_no'));
    }




}
