<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


class EmailController extends Controller {   

    
    public function postman (array $data, $mode="single"){

    	// VALIDATION:

    	$this->validate( $data, ['email_to' => 'required|email|max:255']);


		// PREPARING DATA:
        
        $data ['view'] = ((!empty($data['view'])) ? $data['view'] :  'emails.plain');
            
        $data ['email_from'] =  ((!empty($data['email_from'])) ? $data['email_from'] :  'info@1ssolution.com');

	    $data ['sender_name'] = ((!empty($data['sender_name'])) ? $data['sender_name'] :  'One Seamless Solution');      

        $data ['email_to'] = $data['email_to'];

        $data ['receiver_name'] =$data['receiver_name'];
        
        $data ['subject'] = ((!empty($data['subject'])) ? $data['subject'] :  'Notification - One Seamless Solution.');
        
        $data ['message'] = $data['message'];
        
        $data ['options'] = $data['options'];            
        
        


		switch ($mode) {

			case 'single':
				
				 return $this->send($email_data);
				
			break;

			case 'queue':
				
				return $this->queue($email_data);
				
			break;

			case 'print':
				
				dd($data);
				
			break;
			
			case 'return':
			default:
				
				return $data;
				
			break;

		}

   
    }


    // SEND SINGLE EMAIL;

	protected function send (array $data){

    	Mail::send($data['view'] , $data, function($message) use ($data) {
                
                $message->from($data['email_from'], $data['sender_name']);
                
                $message->subject($data['subject']);
                
                $message->to($data['email']);
        
        });

        return response()->json(['message' => 'Message has been sent']);

    }


    // PLACE EMAIL TO QUEUE TO BE SENT;    
    
    protected function queue (array $data)    {
       
        //Using queues is better
        
        Mail::queue($data['view'] , $data, function($message) use ($data) {
                
                $message->from($data['email_from'], $data['sender_name']);
                
                $message->subject($data['subject']);
                
                $message->to($data['email']);
        
        });    

        return response()->json(['message' => 'Message has been placed in the email queue']);
    }

}
