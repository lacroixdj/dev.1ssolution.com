<?php

namespace App\Http\Controllers\Support;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Support;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;

//custom
use Mail;
use App\Categories;
use App\Jobs\Postman;


class SupportController extends Controller
{
    
    public function __construct(){
        
        $this->middleware('auth');
    
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @return void
    */
    public function store(Request $request) {
        
        if(!(Auth::check())) {

            return response()->json(['error_message' => "Forbidden, you don't have access"], 403);
        }
        
        $user = Auth::User();
        
        $this->validate($request, [
            
            'supportEmail' => 'required', 

            'supportCategory' => 'required', 

            'supportDetail' => 'required',

        ]);
        
        $data = [

            'user_id' => Auth::User()->id, 

            'status' => 'open', 

            'support_category' => $request->supportCategory, 

            'description' => $request->supportDetail, 

        ];
        
        
        $result = $user->supportTickets()->create($data);
     
        if($result) {

            Session::flash('flash_message', 'The support ticket has ben created');
            
            $response = [

                'title' => 'Support System',
        
                'message' => 'Success! The support ticket has ben created', 
                
                'text' => '',

            ];
            
            $category = Categories::where('value', $data['support_category'])->where('table', 'support_category') ->first();
            
            //Email to user account  
            $this->createSupportEmail([

                'mode' => 'html',

                'view' => 'emails.supportUser',

                'email_to' => Auth::User()->email,

                'receiver_name' => Auth::User()->name.' '.Auth::User()->lastname,

                'subject' => "Support Request #".$result['id'],    

                'id' => $result['id'],

                'category' =>  $category['label'],

                'description'=> $result['description'],

                'img_url' => url(('/')),

             ]);
            
             //Email to support account
             $this->createSupportEmail([

                'mode' => 'html',

                'view' => 'emails.supportUser',

                'email_to' => 'support@1ssolution.com',

                'receiver_name' => 'Support Team',

                'subject' => "Support Request #".$result['id'],    

                'id' => $result['id'],

                'category' =>  $category['label'],

                'description'=> $result['description'],

                'img_url' => url(('/')),

             ]);
        
            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error creating the request'], 422);
       
        }

    }
    
    protected function createSupportEmail(array $data){
        
        $job = ( new Postman($data))

                ->onQueue('emails')

                ->delay(3);

        $this->dispatch($job); 

    }
}


