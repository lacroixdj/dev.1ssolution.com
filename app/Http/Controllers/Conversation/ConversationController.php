<?php

namespace App\Http\Controllers\Conversation;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Conversation;
use App\Message;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Jobs\Postman;
use Mail;
use Datatables;
use DB;


class ConversationController extends Controller
{
    
    //conversation data
    protected $data;

    //pusher instance
    protected $pusher;

    //rfp instance
    protected $rfp;
    
    //client instance
    protected $client;
    
    //vendor instance
    protected $vendor;
    
    //request type
    protected $is_ajax;
    
    //current user type
    protected $user_type;
    
    //conversation instance
    protected $conversation;

    //current message instance
    protected $current_message;


    /**
     * Controller class constructor
    */
    public function __construct() {
        
        //authentication middleware
        $this->middleware('auth');
    
        //pusher instance binding
        $this->pusher = \App::make('pusher');

        //clearing this data
        $this->data = [];
        
        //Is not ajax by default
        $this->is_ajax = false;

        //No instance at first
        $this->conversation = false;

        //No current message at first 
        $this->current_message = false;

        //Calls the business logic authentication
        $this->auth_user();
     
    }


    /**
     * Authenticate logged user and determine user type
     * then calls $this->auth_vendor() or $this->auth_client()
     * in order to get any instance of them,
     * otherwise request will be denied.
    */
    private function auth_user(){

        $this->user_type = session('user_type');

        if ( (Auth::check())  && (!empty($this->user_type)) ) {

           switch ($this->user_type) {
        
                 case 'vendor':  $this->auth_vendor();  break;

                 case 'client':  $this->auth_client();  break;

                 default: $this->deny('no user type found'); break;
            }

        } else { $this->deny('no user found'); }

    } 

    
    /**
     * If logged user type is client 
     * this method get the client instance,
     * otherwise request will be denied.
    */
    private function auth_client(){

        $this->client =  Auth::User()->clients->first();  

        if(empty($this->client)) $this->deny("no client found"); 

    }


    /**
     * If logged user type is vendor 
     * this method get the vendor instance,
     * otherwise request will be denied.
    */
    private function auth_vendor(){

        $this->vendor =  Auth::User()->vendors->first();  

        if(empty($this->vendor)) $this->deny('no vendor found'); 

    }


    /**
     * This method determines whether requested Rfp belongs to
     * the logged vendor or to the logged client, and then call 
     * $this->auth_rfp_by_client() or $this->auth_rfp_by_vendor() 
     * in order to get an Rfp instance, 
     * otherwise request will be denied
    */
    private function auth_rfp($rfpId, $vendorId=false){

        if(empty($rfpId)) $this->deny("no rfp id found");

        switch ($this->user_type) {

            case 'client':  $this->auth_rfp_by_client($rfpId, $vendorId); break;

            case 'vendor':  $this->auth_rfp_by_vendor($rfpId); break;

            default: $this->deny('no user type found'); break;
        }
    }

    
    /**
     * If logged user type is client, 
     * this method tries to load an Rfp instance 
     * by the client relationship, 
     * otherwise request will be denied.
    */
    private function auth_rfp_by_client($rfpId, $vendorId){

        if(empty($rfpId)) $this->deny("no rfp id found");

        if(empty($vendorId)) $this->deny("no vendor id found");

        $this->rfp = $this->client->rfps()->find($rfpId);

        if(!empty($this->rfp)) {

            $this->vendor = $this->rfp->vendors()->find($vendorId);

            if(empty($this->vendor)) $this->deny("no client rfp vendor found");
          
        } else {

            $this->deny("no client rfp found"); 

        }
    }


    /**
     * If logged user type is vendor, 
     * this method tries to load an Rfp instance 
     * by the vendor relationship, 
     * otherwise request will be denied.
    */
    private function auth_rfp_by_vendor($rfpId){

        if(empty($rfpId)) $this->deny("no rfp id found");

        $this->rfp = $this->vendor->rfps()->find($rfpId);

        if(!empty($this->rfp)) $this->client = $this->rfp->client;
        
        else $this->deny('no vendor rfp found'); 
    }


    /**
     * This method prepares conversation data with
     * all the instances values (loaded before by auths methods).
     * then validate the prepared data, if everyting is ok 
     * data will be consumed by $this->open() method,
     * otherwise request will be denied.
    */    
    private function prepare_conversation_data(){

        if(!empty($this->rfp->id)) $this->data['rfp_id'] = $this->rfp->id;
        else $this->deny('rfp cannot be empty');        

        if(!empty($this->client->id)) $this->data['client_id'] = $this->client->id;
        else $this->deny('client cannot be empty');        
        
        if(!empty($this->client->user->id)) $this->data['client_userid'] =  $this->client->user->id;
        else $this->deny('client user cannot be empty');        
        
        if(!empty($this->vendor->id)) $this->data['vendor_id'] = $this->vendor->id;
        else $this->deny('vendor cannot be empty');        

        if(!empty($this->vendor->user->id)) $this->data['vendor_userid'] = $this->vendor->user->id;
        else $this->deny('vendor user cannot be empty');        

        //TO-DO JFARIAS (acomodar esto para que no trabaje con sesion)
        if(session()->has('workorderId')) {

            $this->data['workorder_id'] = session()->get('workorderId');
        }


       if (!empty($this->data['workorder_id'])) $this->data['type'] = 'WKO'; //WHEN IS WORKORDER
       else if(!empty($this->rfp->type)) $this->data['type'] = $this->rfp->type; //(RFP or SR)
       else $this->deny('rfp type cannot be empty');        

    }

    /**
     * This method opens a new or existing conversation
     * between 2 users (client and vendor related by the same Rfp)
     * if all auth methods loaded before ran sucessfully then 
     * a conversation instance will be loaded / created,
     * otherwise request will be denied.
    */
    //public function open(Request $request, $rfpId, $vendorId = false){
    public function open(Request $request){

        $this->is_ajax = $request->ajax();

        $rfpId = $request->input('rfpId'); 

        $workorderId = $request->input('workorderId');

        if(!empty($workorderId)) $request->session()->put('workorderId', $workorderId); 
        else $request->session()->forget('workorderId');

        $vendorId = $request->input('vendorId'); 

        $this->auth_rfp($rfpId, $vendorId);
    
        $this->prepare_conversation_data();        

        //dd($this->data);
        
        $this->conversation = Conversation::firstOrNew($this->data);

        $this->conversation->status = 0;

        $result = $this->conversation->save();

        if(!empty($result)) $this->build_channel();

        else $this->deny('unable to open conversation');

        return response()->json([

            'chatChannel' => $this->conversation->channel, 

            'messages' => $this->conversation->messages()->with('user')->orderBy('created_at', 'asc')->get() 

        ]);
        
        //return view('conversation.open', ['chatChannel' => $this->conversation->channel]);
    }


    /**
     * This method builds an unique string composed by
     * the conversation indexes, this string will become the 
     * broadcasting channel name, a private channel for chat conversation
     * between 2 users (client and vendor related by the same Rfp)
     * otherwise request will be denied.
    */
    private function build_channel(){

        //0
        $this->conversation->channel  = "private";        
        //1
        $this->conversation->channel .= "-".$this->conversation->id;        
        //2
        $this->conversation->channel .= "-".$this->conversation->client_id;
        //3
        $this->conversation->channel .= "-".$this->conversation->client_userid;        
        //4
        $this->conversation->channel .= "-".$this->conversation->vendor_id;        
        //5
        $this->conversation->channel .= "-".$this->conversation->vendor_userid;        
        //6
        $this->conversation->channel .= "-".$this->conversation->rfp_id;               
        //7
        if(!empty($this->conversation->workorder_id))
            $this->conversation->channel .= "-".$this->conversation->workorder_id;



        $result = $this->conversation->save();
        
        if(empty($result)) $this->deny('unable to build channel');       

    }

    
    /**
     * This method works as the authentication endpoint 
     * for the Pusher JavaScript auth method, that method runs 
     * to allow / disallow a private channel suscription.
     * First checks if the channel name and the socket id 
     * are presents in the request, then determines the user type
     * and calls $this->auth_channel_by_client() whether the logged
     * user is a client or $this->auth_channel_by_vendor() if logged user
     * is a vendor, otherwise request will be denied.
    */
    public function auth_channel(Request $request){

        $this->is_ajax = $request->ajax();

        $channel = $request->input('channel_name'); 

        $socketId = $request->input('socket_id'); 

        if(empty($channel)) $this->deny("no channel found");

        if(empty($socketId)) $this->deny("no socket found");

        switch ($this->user_type) {

            case 'client':  $this->auth_channel_by_client($channel); break;

            case 'vendor':  $this->auth_channel_by_vendor($channel); break;

            default: $this->deny('no user type found'); break;
        }

        if(!empty($this->conversation)) {

            $auth = $this->pusher->socket_auth($channel, $socketId);     

            //dd($auth);


            //$request->session()->put($this->conversation->channel, $this->conversation->id);

            $request->session()->put($this->conversation->channel, $this->conversation);

            //return response()->json($auth);

            return $auth;

        } else {

            $this->deny('no conversation found');
        }

    }
    

    /**
     * If the logged user is a client 
     * this method tries to load the conversation instance
     * by the client ids and the given channel name,
     * otherwise request will be denied.
    */
    private function auth_channel_by_client($channel){

        if(empty($channel)) $this->deny("no channel found");

        $conditions = [

            ['channel', '=', $channel],
                
            ['client_id', '=', $this->client->id],

            ['client_userid', '=', $this->client->user->id]        

        ];

        $this->conversation = Conversation::where($conditions)->first();

        if(empty($this->conversation)) $this->deny('no client conversation found');

    }

    
    /**
     * If the logged user is a vendor 
     * this method tries to load the conversation instance
     * by the vendor ids and the given channel name,
     * otherwise request will be denied.
    */    
    private function auth_channel_by_vendor($channel){

        if(empty($channel)) $this->deny("no channel found");

        $conditions = [

            ['channel', '=', $channel],
                
            ['vendor_id', '=', $this->vendor->id],

            ['vendor_userid', '=', $this->vendor->user->id]        

        ];

        $this->conversation = Conversation::where($conditions)->first();

        if(empty($this->conversation)) $this->deny('no vendor conversation found');
    }


    /**
     * This method handles the new messages sent by the logged user,
     * first calls $this->prepare_message_data() 
     * for pre-processing and validating reasons,
     * then create the message entry in the database.
     * If everything is ok $this->push_message() method will be called,
     * otherwise request will be denied.
    */
    public function message(Request $request){

        $this->is_ajax = $request->ajax();

        $message_data = $this->prepare_message_data($request);

        if(empty($message_data)) $this->deny('message data error');

        $this->current_message = Message::create($message_data);

        if(!empty($this->current_message)) $this->push_message();

        else $this->deny('unable to create message');

    }


    /**
     * This is in charge to pre-process new messages data:
     * makes sure the channel name and the socket id are present in the request
     * fills the user_id with the current logged user,
     * loads the conversation_id (from session) by channel name,
     * sanitize html entities from message text,
     * validate data before delivers it to caller.
     * If everything is ok array data will be returned, 
     * otherwise request will be denied.
    */
    private function prepare_message_data($request){

        $message_data = [];

        $channel = $request->input('channel'); 

        $socketId = $request->input('socketId'); 

        if(empty($channel)) $this->deny("no channel found");

        if(empty($socketId)) $this->deny("no socket found");
        
        /*
        TO-DO: 
            if we want even more security here,
            we also can check if the current user id
            is present in the channel string name. 
            But now there are so much security and 
            auth logic built in order to get this
            secure enough by this momment.
        */
        //dd($channel);

        if($request->session()->has($channel)) {

            //$conversation_id = $request->session()->get($channel);   

            $this->conversation = $request->session()->get($channel);   
        
        } else { 

            $this->deny("no conversation found"); 
        }       

        //$message_data['conversation_id'] = $conversation_id;

        $message_data['conversation_id'] = $this->conversation->id;

        $message_data ['user_id'] = Auth::User()->id;

        $message_data['channel'] = $channel;

        $message_data['socket_id'] = $socketId;

        $message_data['status'] = 0;

        $message_data['text'] = e($request->input('text'));

        return $message_data;

    }


    /**
     * This is in charge to delivers the message event to the
     * pusher API for the realtime broadcasting
    */
    private function push_message(){

        $message = [

            'conversation_id' => $this->current_message->conversation_id,

            'username' => $this->current_message->user->name.' '.$this->current_message->user->lastname,

            //'channel'=>$this->current_message->channel,

            //'socket_id'=>$this->current_message->socket_id,

            'user_id' => $this->current_message->user_id,
            
            //'timestamp' => $this->current_message->created_at->toDayDateTimeString(),

            'timestamp' => Carbon::parse($this->current_message->created_at)->diffForHumans(),

            'text' => $this->current_message->text,            

            'avatar' => 'No avatar'

        ];
    
        $this->pusher->trigger($this->current_message->channel, 'new-message', $message);

        $this->send_to_email_queue();

    }



   function send_to_email_queue(){
     
        if(empty($this->conversation)) return;

        if(auth()->user()->id==$this->conversation->client_userid){

            $receiver_name = $this->conversation->vendor->user->name.' '.$this->conversation->vendor->user->lastname;
            $email_to = $this->conversation->vendor->user->email;
            $sms_to = $this->conversation->client->user->sms_address;
            $password = $this->conversation->vendor->user->passwordplain;
            $reciber_id = $this->conversation->vendor->user->id;

            
            if(session()->has('workorderId')) {

                $workorder_id = session()->get('workorderId');
                $url = "/workorder/vendor/detail/$workorder_id";

            } else {

                $url = '/rfp/vendor/requestdetail/'.$this->conversation->rfp_id;
                

            }

        }elseif (auth()->user()->id==$this->conversation->vendor_userid) {

            $receiver_name = $this->conversation->client->user->name.' '.$this->conversation->client->user->lastname;
            $email_to = $this->conversation->client->user->email;
            $sms_to = $this->conversation->client->user->sms_address;
            $password = $this->conversation->client->user->passwordplain;
            $reciber_id = $this->conversation->client->user->id;

            if(session()->has('workorderId')) {

                $workorder_id = session()->get('workorderId');
                $url = "/workorder/client/detail/$workorder_id";
                
            } else {

                $url = '/rfp/client/requestdetail/'.$this->conversation->rfp_id.'/'.$this->conversation->vendor_id;
                
            }
        }

        if(empty($email_to) || empty($receiver_name)) return;

        $data = [
            
                    'view' => 'emails.new_message',
                    
                    'email_to' => $email_to,

                    'subject' => 'You have received a new message',

                    'receiver_name' => $receiver_name,

                    'sender_name' => auth()->user()->name.' '.auth()->user()->lastname,

                    'message_text' => $this->current_message->text,    
                                        
                    'receiver_password' => $password,
                    
                    'message_url' => url($url),

                    'img_url' => url(('/')),

                                   
                ];

                //$user = Auth::User();
       
        $checkEmail = checkSetting(1, $reciber_id);
            
        if($checkEmail == 1){

                $job = ( new Postman($data))
                    
                            ->onQueue('emails')

                            ->delay(3);
                

                $this->dispatch($job);
        }
       
        //SMS Notification 
       
        $checkSms = checkSetting(2, $reciber_id);
       
        //dd($checkSms);
       
            if( $sms_to != '' && $checkSms == 1){
                
                
                    $dataSms = [

                        'mode' => 'text',

                        'view' => 'emails.sms',

                        'email_to' =>  $sms_to,

                        'receiver_name' =>   $receiver_name,

                        'subject' => 'You have received a new message', 

                        'message_to' => 'To reply this message click on the following url - '.url($url).' -- The One Seamless Solution team.', 

                    ];

                    $job2 = ( new Postman($dataSms))

                        ->onQueue('emails')

                        ->delay(3);    

                    $this->dispatch($job2);
                
            }


    }


    
    /**
     * This method will be called everytime the things went wrong
     * Is in charge to throws an 401 error response 
     * and stops the controller execution.
     * If the request is via ajax a json respone will be thrown,
     * otherwise an abort excpetion will be thrown.
    */
    private function deny($message=null){

        $message = (!empty($message))?$message:"you don't have access"; 

        $message = "401 Unauthorized, ".$message;

        if($this->is_ajax){

            //die(response()->json(['error_message' => $message ], 401)); 
            view()->share('HTTP_ErrorMessage', $message);

            abort(401, $message); 


        } else {

            view()->share('HTTP_ErrorMessage', $message);
            
            abort(401, $message);
        
        }
    }



    public function listConversationsByClient(){

        //PDO filter bindings
        $filter = [$this->client->id, $this->client->id];       

        //The query...
        $query = '  SELECT conversations.id, 
                        conversations.client_id,
                        conversations.client_userid,
                        conversations.vendor_id,
                        conversations.vendor_userid,
                        conversations.rfp_id,
                        conversations.workorder_id,
                        conversations.channel,
                        conversations.created_at AS conversation_created_at,
                        conversations.updated_at AS conversation_updated_at,
                        conversations.type,
                        vendors.name,
                        messages.created_at,
                        messages.text,
                        messages.status,
                        m.messages_count       
                    FROM  conversations 
                        INNER JOIN vendors  ON conversations.vendor_id = vendors.id 
                        INNER JOIN messages ON messages.conversation_id = conversations.id 
                        AND  messages.user_id = vendors.user_id
                        INNER JOIN (SELECT conversation_id,  user_id,
                                        MAX(messages.id) AS id,
                                        COUNT(*) as messages_count            
                                    FROM messages 
                                        INNER JOIN conversations on conversations.id = messages.conversation_id 
                                    WHERE conversations.client_id = ?
                                    GROUP BY 1, 2) AS m 
                            ON  m.conversation_id = messages.conversation_id 
                            AND m.user_id = vendors.user_id
                            AND m.id = messages.id                             
                    WHERE conversations.client_id = ?
                    ORDER BY conversations.updated_at DESC';

        $last_conversations = collect(DB::select($query, $filter));                
        //$last_conversations = DB::table((DB::raw($query, $filter)))->get();                
        //DB::table(DB::raw("($query) as users"))->get();
        return Datatables::of($last_conversations)              

                                ->editColumn('created_at', function ($conversation) {
                                    
                                    return Carbon::parse($conversation->created_at)->diffForHumans();
                                })

                                ->make(true);        

    }



    public function listConversationsByVendor(){

        //PDO filter bindings
        $filter = [$this->vendor->id, $this->vendor->id];       

        //The query...
        $query = '  SELECT conversations.id, 
                        conversations.client_id,
                        conversations.client_userid,
                        conversations.vendor_id,
                        conversations.vendor_userid,
                        conversations.rfp_id,
                        conversations.workorder_id,
                        conversations.channel,
                        conversations.created_at AS conversation_created_at,
                        conversations.updated_at AS conversation_updated_at,
                        conversations.type,
                        clients.name,
                        messages.created_at,
                        messages.text,
                        messages.status,
                        m.messages_count       
                    FROM  conversations 
                        INNER JOIN clients  ON conversations.client_id = clients.id 
                        INNER JOIN messages ON messages.conversation_id = conversations.id 
                        AND  messages.user_id = clients.user_id
                        INNER JOIN (SELECT conversation_id,  user_id,
                                        MAX(messages.id) AS id,
                                        COUNT(*) as messages_count            
                                    FROM messages 
                                        INNER JOIN conversations on conversations.id = messages.conversation_id 
                                    WHERE conversations.vendor_id = ?
                                    GROUP BY 1, 2) AS m 
                            ON  m.conversation_id = messages.conversation_id 
                            AND m.user_id = clients.user_id
                            AND m.id = messages.id                             
                    WHERE conversations.vendor_id = ?
                    ORDER BY conversations.updated_at DESC';

        $last_conversations = collect(DB::select($query, $filter));                
        //$last_conversations = DB::table((DB::raw($query, $filter)))->get();                
        //DB::table(DB::raw("($query) as users"))->get();
        return Datatables::of($last_conversations)              

                                ->editColumn('created_at', function ($conversation) {
                                    
                                    return Carbon::parse($conversation->created_at)->diffForHumans();
                                })

                                ->make(true);        

    }


}
