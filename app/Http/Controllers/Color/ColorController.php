<?php

namespace App\Http\Controllers\Color;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Color;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $color = Color::paginate(15);

        return view('color.index', compact('color'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'hexcode' => 'required', ]);

        Color::create($request->all());

        Session::flash('flash_message', 'Color added!');

        return redirect('color');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $color = Color::findOrFail($id);

        return view('color.show', compact('color'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $color = Color::findOrFail($id);

        return view('color.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'hexcode' => 'required', ]);

        $color = Color::findOrFail($id);
        $color->update($request->all());

        Session::flash('flash_message', 'Color updated!');

        return redirect('color');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Color::destroy($id);

        Session::flash('flash_message', 'Color deleted!');

        return redirect('color');
    }
}
