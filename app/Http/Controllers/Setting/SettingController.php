<?php

namespace App\Http\Controllers\Setting;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;

class SettingController extends Controller
{
    public function __construct(){
         
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(){


        if(!(Auth::check())) {

            view()->share('HTTP_ErrorMessage', "Forbidden, you don't have access");
            
            abort(403, "Forbidden, you don't have access");   
        
        }

        if(!empty(Auth::User()->clients->first()) || !empty(Auth::User()->vendors->first())) {

            
        } else {

            view()->share('HTTP_ErrorMessage', "No client account found");

            abort(403, "No client or vendor account found");   
        
        }
         
        $settings =  Auth::User()->settings()->get();
                  
        $client =    Auth::User()->clients()->first();
        
        $vendor =    Auth::User()->vendors()->first();
         
         //dd($settings[3]->pivot->setting_value);
         
        
        if($client){
            
            return view('settings.clientSettings', compact('settings'));
        
        }
        if($vendor){
            
            return view('settings.vendorSettings', compact('settings'));
            
        }
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first()) || !empty(Auth::User()->vendors->first()) ) {

            //$client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client or vendor account found"], 403);
            
        }
       
        if($request->emailMessages == 'on'){
        
            $request->emailMessages = 1;
        
        }else{
            
            $request->emailMessages = 0;
        
        }

        if($request->smsMessages == 'on'){
        
            $request->smsMessages = 1;
        
        }else{
        
            $request->smsMessages = 0;
        
        }
        
        //Email
        
        $settings_email =  Auth::User()->settings()->where('setting_id', 1)->get();
              
        $settings_email[0]->pivot->status =  $request->emailMessages;
        
        $result =  $settings_email[0]->pivot->save();
        
        //Sms
        
        $settings_sms =  Auth::User()->settings()->where('setting_id', 2)->get();
              
        $settings_sms[0]->pivot->status =  $request->smsMessages;
        
        $result_2 =  $settings_sms[0]->pivot->save();
        

        
        if($result && $result_2) {
            
            Session::flash('flash_message', 'Settings Change');
            
             $response = [

                    'title' => 'Settings',
            
                    'message' => 'Updated Successfully',

                    'callback_func' => '',
            
                    'text' => '',
                 
                    'widget_type' => 'flashMessage'
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error change the settings'], 422);
       
        } 
        
    }
    
    
    /**
     * Upate the skin.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSkin(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first()) || !empty(Auth::User()->vendors->first()) ) {

            //$client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client or vendor account found"], 403);
            
        }
        
        $settings_skin =  Auth::User()->settings()->where('setting_id', 3)->get();
        
        $settings_skin[0]->pivot->setting_value =  $request->skinSelect;
        
        $result =  $settings_skin[0]->pivot->save();
        
        if($result) {
            
            session()->put('skin',  $request->skinSelect);
            
            Session::flash('flash_message', 'Skin Change');
            
             $response = [

                    'title' => 'Skin',
            
                    'message' => 'Updated Successfully',

                    'callback_func' => '',
            
                    'text' => '',
                 
                    'widget_type' => 'flashMessage',
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error change the settings'], 422);
       
        } 
    }
    
    public function updateTax(Request $request)
    {
        if(!(Auth::check())) {

           return response()->json(['error_message' => "Forbidden, you don't have access"], 403);  
        }

        if(!empty(Auth::User()->clients->first()) || !empty(Auth::User()->vendors->first()) ) {

            //$client =  Auth::User()->clients()->firstOrFail();
            
        } else {

            return response()->json(['error_message' => "No client or vendor account found"], 403);
            
        }
        
        $this->validate($request, [            

            'servicesTax' => 'required', 

        ]);

        
        $settings_tax =  Auth::User()->settings()->where('setting_id', 4)->get();
        
        $settings_tax[0]->pivot->setting_value =  $request->servicesTax;
        
        $result =  $settings_tax[0]->pivot->save();
        
        if($result) {
                        
            Session::flash('flash_message', 'Tax Change');
            
             $response = [

                    'title' => 'Update Tax',
            
                    'message' => 'Updated Successfully',

                    'callback_func' => '',
            
                    'text' => '',
                 
                    'widget_type' => 'flashMessage',
                    
                ]; 

            return response()->json($response, 200);

        } else {

            return response()->json(['error_message' => 'There was an error change the tax'], 422);
       
        } 
        
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
