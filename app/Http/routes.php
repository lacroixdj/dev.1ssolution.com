<?php

use Illuminate\Support\Facades\App;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// ROOT:
	
//	Route::get('/', function () { 
//
//		session()->put('skin', "skin-ssolution");
//
//		return redirect('/login'); 
//	
//	});

// END


// AUTH:

	Route::auth();
	
	Route::get('/register', 'Auth\AuthController@getRegister');
	
	Route::post('/register', 'Auth\AuthController@postRegister');
	
	Route::get('/logout', function(){
		
		Auth::logout();
		
		Session::flush();
		
		return Redirect::to('/login');
	
	});

	Route::get('/flush', function(){
		
		SSH::run(
			[
    			'cd /home/ssolution/public_html/qa.demo',
    			'sh flush-laravel-full.sh',
    			
			],

			function($line) {
    			echo $line."</br>";
			}
		);	
	});

// END


// USER:  

	Route::get('account/user', 'Auth\UserController@edit');

	Route::post('account/user', 'Auth\UserController@update');

	Route::post('user/passwd', 'Auth\UserController@passwd');

// END


// HOME:
	
	Route::get('/home', function () {

		if(!empty(session('user_type')) && session('user_type')=='vendor'){
			
			return redirect('/vendor/dashboard');
		
		} elseif (!empty(session('user_type')) && session('user_type')=='client') {
	
			//return redirect('account/client');

			return redirect('/client/dashboard');
		
		}	        
	});

// END


// DASHBOARD: 

	Route::get('/dashboard', function () {
	
		if(!empty(session('user_type')) && session('user_type')=='vendor'){
			
			return redirect('/vendor/dashboard');
		
		} elseif (!empty(session('user_type')) && session('user_type')=='client') {
			
			//return redirect('account/client');

			return redirect('/client/dashboard');			
		
		}
	        
	});

// END

// CLIENT:

	Route::post('/register/registerClient', 'Auth\AuthController@postRegisterClient');

	Route::get('/verifyClient/{token}/{passcode}', 'Auth\AuthController@verifyClient');

	Route::post('/setCredentialsClient', 'Auth\AuthController@setCredentialsClient');

	Route::get('/account/client', 'Client\ClientController@edit');

	Route::post('/account/client', 'Client\ClientController@update');
	
	Route::post('/client/location', 'Client\ClientController@updateAddress');

	Route::get('/client/dashboard', 'Client\DashboardController@dashboard');
	
	
// END

// VENDOR:
	
	Route::get('/verify/{token}', 'Auth\AuthController@verify');

	Route::post('/setCredentials', 'Auth\AuthController@setCredentials');

	Route::get('/account/vendor', 'Vendor\VendorController@edit');

	Route::post('/account/vendor', 'Vendor\VendorController@update');
	
	Route::post('/vendor/location', 'Vendor\VendorController@updateAddress');

	Route::get('/vendor/dashboard', 'Vendor\DashboardController@dashboard');

// END

// RFP:

	//Route::resource('rfp', 'Rfp\\RfpController');	
	
	Route::get('rfp/create', 'Rfp\RfpController@create');
	
	Route::post('rfp/create', 'Rfp\RfpController@store');

	Route::post('rfp/vendors', 'Rfp\RfpController@listVendors');

	Route::post('rfp/vendors/attach', 'Rfp\RfpController@requestVendors');

	Route::post('rfp/send', 'Rfp\RfpController@sendRequest');
	
	Route::get('/createRfp', function () {
	  
	    return view('client/createRfp');
	
	});



// END

// MASTER TABLES:

	Route::resource('corporation', 'Corporation\CorporationController');

	Route::resource('metroarea', 'Metroarea\\MetroareaController');
	
	Route::resource('color', 'Color\\ColorController');

// END


// OTHERS: 
	
	Route::get('/terms', function () {
	  
	    return view('auth/terms');
	
	});

// END

// EMAIL:

	Route::post('/postman', 'EmailController@postman');	

//


// JSON:

	//industries json:
	
	Route::get('/json/industries', function () {
		
		$industries = \App\Industry::all();
		
		return $industries->sortBy('name')->lists('name','id')->toJson();
	
	});

	//attributes by industry json:
	
	Route::get('/json/industries/{id}/attributes', function ($id) {
		
		$attributes = \App\Attribute::where('industry_id',$id)->get();
		
		return $attributes->sortBy('name')->lists('name','id')->toJson();
	
	});

	//services by industry json:
	
	Route::get('/json/industries/{id}/services', function ($id) {
		
		$services = \App\Service::where('industry_id',$id)->get();
		
		return $services->sortBy('name')->lists('name','id')->toJson();
	
	});

	//countries json:
	
	Route::get('/json/countries', function () {
	
		$countries = \App\Country::all();
	
		return $countries->sortBy('name')->lists('name','id')->toJson();
	
	});

	//states by country json:
	
	Route::get('/json/countries/{id}/states', function ($id) {
	
		$states = \App\State::where('country_id',$id)->get();
	
		return $states->sortBy('name')->lists('name','id')->toJson();
	
	});

	//cities by state json:
	
	Route::get('/json/states/{id}/cities', function ($id) {
	
		$cities = \App\City::where('state_id',$id)->get();
	
		return $cities->sortBy('name')->lists('name','id')->toJson();
		
	});


// TEST AND DEVEL VIEWS: 
	//RFP WIZARD TEMPLATE EXAMPLE
	Route::get('/createRfp', function () {
	  
	    return view('client/createRfp');
	
	});
    
    


    //VENDOR BIDSREQUESTDETAIL
    //EMAIL BIDS REQUEST CLIENT -> VENDOR
    Route::get('/email/clientToVendor', function () {
	  
	    return view('emails/serviceRequestToVendor');
	
	});

    //EMAIL NEW MESSAGE CLIENT <-> VENDOR
    Route::get('/email/newMessage', function () {
	  
	    return view('emails/newMessage');
	
	});

    //EMAIL NEW WORKORDER CLIENT -> VENDOR
    Route::get('/email/workorder', function () {
	  
	    return view('emails/workorder');
	
	});

    //CREATE SUPPORT TICKET
    
    //EMAIL NEW WORKORDER CLIENT -> VENDOR
    Route::get('/email/support', function () {
	  
	    return view('emails/supportUser');
	
	});
    
    //Route::get('rfp/create', 'Rfp\RfpController@create');
    Route::post('support/store', 'Support\SupportController@store');

    //CATEGORIES TABLE JSON:
	Route::get('/json/categories/{table}', function ($table) {
	
		$categories = \App\Categories::where('table',$table)->get();

		//$categories = Categories::where('table',$table)->get();
        
		return $categories->sortBy('label')->lists('label','value')->toJson();
	
    });

    //VENDOR BIDSREQUEST
    Route::get('/rfp/vendor/bidsrequest/{type?}/{status?}', function ($type = 'all', $status = 'all') {
	  
	    return view('vendor/bidsRequest', ['type' => $type, 'status' => $status]);
	
	});

    //BIDS REQUEST VENDOR DATATABLE
    //Route::get('/json/datatable/listRfpVendors', 'Rfp\RfpController@listRfpVendors');
    Route::post('/json/datatable/listRfpVendors', 'Rfp\RfpController@listRfpVendors');

    //VENDOR BIDSREQUEST DETAIL
    Route::get('/rfp/vendor/requestdetail/{id}', 'Rfp\RfpController@rfpVendorsDetails');

    //VENDOR BID AMOUNT
    Route::post('/vendor/bidUpdate', 'Rfp\RfpController@bidUpdate');

    //EMAIL BIDS REQUEST VENDOR -> CLIENT
    Route::get('/email/vendortToClient', function () {
	  
	    return view('emails/setBidAmount');
	
	});
    
    //CLIENT BIDSREQUEST
    Route::get('/rfp/client/bidsrequest/{type?}/{status?}', function ($type = 'all', $status = 'all') {
	  
	    return view('client/bidsRequest',['type' => $type, 'status' => $status]);
	
	});

    //CLIENT BIDS REQUEST DATATABLE 
    Route::get('/json/datatable/listRfpClients/', 'Rfp\RfpController@listRfpClients');
    
    //CLIENT BIDSREQUEST DETAIL
    Route::get('/rfp/client/requestdetail/{id}/{idVendor}', 'Rfp\RfpController@rfpClientsDetails');
    
    
    //CLIENT RFP DATATABLE
    //Route::get('/json/datatable/listRfpClient/{idRfp}', 'Rfp\RfpController@listRfpClient');

    Route::post('/json/datatable/listRfpClient', 'Rfp\RfpController@listRfpClient');



    //CLIENT LIST VENDORS CHILD OF RFP OR SR
    Route::get('/json/datatable/vendorslist/{rfp_id}/', 'Rfp\RfpController@listRfpVendorsBid');
    
    //CLIENT REQUEST DETAIL
    Route::get('/rfp/client/detail/{id}', 'Rfp\RfpController@RfpDetail');

    //CLIENT REQUEST EDIT
    Route::get('/rfp/client/edit/{id}/{step}', 'Rfp\RfpController@edit');

    

    //WORKORDER

    //WORKORDER CREATE
    Route::get('/workorder/create', 'Workorder\WorkorderController@createWorkorder');
    Route::post('/workorder/create', 'Workorder\WorkorderController@createWorkorder');

    //WORKORDER CLIENT VIEW-ALL
    Route::get('/workorder/client/view/{type?}/{status?}', function ($type = 'all', $status = 'all') {
	  
	    return view('workorder/workorderViewClient', ['type' =>$type, 'status'=>$status]);
	
	});

    //WORKORDER LIST CLIENT
    //Route::get('/json/datatable/listWorkorders', 'Workorder\WorkorderController@viewWorkorders');

	Route::post('/json/datatable/listWorkorders', 'Workorder\WorkorderController@viewWorkorders');

    

    //WORKORDER VENDOR VIEW-ALL
    Route::get('/workorder/vendor/view/{type?}/{status?}', function ($type = 'all', $status = 'all') {
	  
	    return view('workorder/workorderViewVendor', ['type' =>$type, 'status' =>$status]);
	
	});
   
    
    //WORKORDER LIST VENDOR
    //Route::get('/json/datatable/listWorkordersVendor', 'Workorder\WorkorderController@viewWorkordersVendor');
	
    Route::post('/json/datatable/listWorkordersVendor', 'Workorder\WorkorderController@viewWorkordersVendor');

    //WORKORDER VIEW-DETAIL-CLIENT
    Route::get('/workorder/client/detail/{id}', 'Workorder\WorkorderController@workorderDetail');

    //WORKORDER VIEW-DETAIL-VENDOR
    Route::get('/workorder/vendor/detail/{id}', 'Workorder\WorkorderController@workorderDetailVendor');

    //WORKORDER UPDATE STATUS
    Route::post('/workorder/update', 'Workorder\WorkorderController@update');

    //WORKORDER PRINT-INVOICE
    Route::get('/workorder/invoice/{id}', 'Workorder\WorkorderController@workorderInvoice');

    //WORKORDER PRINT-INVOICE VENDOR
    Route::get('/workorder/vendor/invoice/{id}', 'Workorder\WorkorderController@workorderInvoiceVendor');

//REALTIME MESSAGING ROUTES:

    //Route::get('/conversation/open/{rfpId}/{vendorId?}', 'Conversation\ConversationController@open');

    Route::post('/conversation/open', 'Conversation\ConversationController@open');

    Route::post('/conversation/auth', 'Conversation\ConversationController@auth_channel');

   	Route::post('/conversation/message', 'Conversation\ConversationController@message');

   	//vendor channel: 1-7-107-31-31-2

   	//client channel: 1-7-107-31-31-2

//Route::get('/rfp/client/requestdetail/{id}/{idVendor}', 'Rfp\RfpController@rfpClientsDetails');


    /*
    Route::get('/bridge', function() {
	    $pusher = App::make('pusher');

	    $pusher->trigger( 'test-channel',
	                      'test-event', 
	                      array('text' => 'Preparing the Pusher Laracon.eu workshop!'));

	    return view('welcome');
	});


	Route::controller('notifications', 'NotificationController');

	*/

	// Route::controller('chat', 'ChatController');



	

	//Route::resource('conversation', '');

	//Route::resource('messages', 'Conversation\\MessagesController');


//CLIENT SETTINGS EDIT VIEW
Route::get('/client/settings', 'Setting\SettingController@edit');

//CLIENT SETTINGS UPDATE VIEW - FOR MESSAGES SMS AND EMAIL
Route::post('/client/settings', 'Setting\SettingController@update');

//CLIENT SETTINGS UPDATE VIEW - FOR SKINS
Route::post('/client/skinSettings', 'Setting\SettingController@updateSkin');


//VENDOR SETTINGS EDIT VIEW
Route::get('/vendor/settings', 'Setting\SettingController@edit');

//VENDOR SETTINGS UPDATE VIEW - FOR MESSAGES SMS AND EMAIL
Route::post('/vendor/settings', 'Setting\SettingController@update');

//VENDOR SETTINGS UPDATE VIEW - FOR SKINS
Route::post('/vendor/skinSettings', 'Setting\SettingController@updateSkin');

//VENDOR SETTINGS UPDATE TAX
Route::post('/vendor/settingsTaxes', 'Setting\SettingController@updateTax');




// ROOT-WITH-SKIN:
	
	Route::get('/', function ()
    { 

		session()->put('skin', "skin-ssolution");
        
        session()->put('logo', '');

		return redirect('/login'); 
	
	});

    
	//dickeys
	Route::get('/dickeys', function () 
    { 

		session()->put('skin', "skin-dickeys");
		
        session()->put('logo', 'logo-dickeys');

		view()->share('skin', session('skin'));
		
		return redirect('/login'); 
	
	});


	//panera
	Route::get('/panera', function () 
    { 

		session()->put('skin', "skin-panera");
        
        session()->put('logo', 'logo-panera');

		view()->share('skin', session('skin'));
		
		return redirect('/login'); 
	
	});


	//chickfila
	Route::get('/chickfila', function () 
    { 

		session()->put('skin', "skin-chickfila");

		view()->share('skin', session('skin'));
		
		return redirect('/login'); 
	
	});

// END ROOT-WITH-SKIN

// VENDOR INVITE VIEW - CLIENT SIDE

// Route::get('/vendors/viewall', function () {
//	  
//	    return view('vendorInvite/vendorInviteAll');
//	
//});


//VENDORS ADD FAVORITES AND INVITE
Route::get('/vendors/viewall/', 'Client\VendorFavoritesController@index');

Route::get('/vendors/viewall/{searchValue}', 'Client\VendorFavoritesController@index');

Route::get('/autocomplete/vendors/', 'Client\VendorFavoritesController@autocomplete');

//CLIENT SET VENDOR FAVORITE
Route::post('/vendors/setfavorite/', 'Client\VendorFavoritesController@attachVendor');

//CLIENT REMOVE VENDOR FAVORITE
Route::post('/vendors/removefavorite/', 'Client\VendorFavoritesController@detachVendor');

//CLIENT INVITE VENDOR
Route::post('/vendors/invite/', 'Client\VendorFavoritesController@inviteVendor');

//VENDORS REMOVE FAVORITES AND VIEW INVITES STATUS
Route::get('/vendors/favorites/', 'Client\VendorFavoritesController@favorites');

Route::get('/vendors/favorites/{searchValue}', 'Client\VendorFavoritesController@favorites');

//INVITATIONS TO FAVORITE
Route::post('/vendors/invtofavorite/', 'Client\VendorFavoritesController@attachInvitationToFavorite');

//CLIENT SIDE - VENDOR INVITATIONS VIEW
Route::get('/vendors/invitations/', 'Client\VendorFavoritesController@indexInvites');

Route::get('/vendors/invitations/{searchValue}', 'Client\VendorFavoritesController@indexInvites');

Route::get('/autocomplete/invites/', 'Client\VendorFavoritesController@autocompleteInvite');

//VENDOR GET MORE INFO
Route::get('/vendors/moreInfo/', 'Vendor\VendorController@vendorProfile');
Route::post('/vendors/moreInfo/', 'Vendor\VendorController@vendorProfile');

// END VENDOR INVITE VIEW - CLIENT SIDE

//CLIENT GET MORE INFO
Route::get('/clients/moreInfo/', 'Client\ClientController@clientProfile');
Route::post('/clients/moreInfo/', 'Client\ClientController@ClientProfile');




//JFARIAS
/*Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);*/


//DASBOARD CLIENT INBOX
Route::post('/json/client/inbox', 'Conversation\ConversationController@listConversationsByClient');

//DASBOARD CLIENT INBOX
Route::post('/json/vendor/inbox', 'Conversation\ConversationController@listConversationsByVendor');



//DELETE COMMON USERS FOR TESTING
Route::get('/delete/common/users', function ()  { 
 
	$usersEmails = [
	 	
 		'tonybassguitar@gmail.com',

 		'jesus.farias@gmail.com',

 		'jesus.lacroix@gmail.com', 

 		'angeldominguezg@gmail.com',

 		'mercurio68@gmail.com', 

 		'jlopez_guillot@hotmail.com',

 		'david@idealjanitorialsystems.com', 	
 		
 		'gonzalez.pedro.jo@gmail.com', 	

 		'juliordc@gail.com',
	];

 	$users = \App\User::whereIn('email', $usersEmails)->get();

 	$countErrors=$countSuccess = 0;
 	$usersCount=$users->count();
 	$resultTotal = false;

 	if(!empty($usersCount)){

 		echo "$usersCount user(s) were found </br>";
 		$resultTotal = true;

 		foreach($users as $user){
		
		 	if(!empty($user->vendors->count())){
		 		foreach($user->vendors as $vendor){
		 			$vendor->forceDelete();
		 		}

		 	}

		 	if(!empty($user->clients->count())){
		 		foreach($user->clients as $client){
		 			$client->forceDelete();
		 		}

		 	}

 			$result=$user->forceDelete();
 			if($result) $countSuccess++;
 			else $countErrors++;
 		}


	}else{

		die('No test user were found');
	}

 		
	die("$countSuccess deleted successfuly, $countErrors Erros found");



	
});