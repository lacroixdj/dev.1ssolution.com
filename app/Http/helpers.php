<?php
    /*
    |--------------------------------------------------------------------------
    | getTextAddress
    |--------------------------------------------------------------------------
    | Get the sms and mms addres from 24-7 api
    | @params $phonenumber
    | @return array mms_addres, sms_address
    | 
    |
    */
    
     function getTextAddress($data){
        //limpiar el string de phone
        $phone = trim($data);
 
        $phone = str_replace(
            array('(', ')', ' ', '-'),
            array('', '', '', ''),
            $phone
        );
    
        $trustedCredentials = config('trusted.credentials');        
    
        if ($phone[0] != "1"){ 
        
            $phone = '1'.$phone;
        
        }
         
        $url = "https://api.data24-7.com/v/2.0?api=".$trustedCredentials['api']."&user=".$trustedCredentials['user']."&pass=".$trustedCredentials['password']."&p1=".$phone;
        $xml = simplexml_load_file($url, 'SimpleXMLElement',LIBXML_NOWARNING);
        $status = $xml->results->result[0]->status;
        $number = $xml->results->result[0]->number;
        $carrier_name = $xml->results->result[0]->carrier_name;
        $carrier_id = $xml->results->result[0]->carrier_id;
       
        $address['sms_address'] = $xml->results->result[0]->sms_address;
        
        $address['mms_address'] = $xml->results->result[0]->mms_address;
			
        if($status != "OK")
        {
            $address['sms_address'] = null;
            
            $address['mms_address'] = null;
        }
        
        return $address;
    
    }

    /*
    |--------------------------------------------------------------------------
    | Check Settings for send email or sms
    |--------------------------------------------------------------------------
    | Description 
    | 
    | @id_setting
    | @id_user
    | return 1 -> on / 0 -> off
    */

    function checkSetting($id_setting, $id_user){
        
        $user = App\User::where('id', $id_user)->firstOrFail();
        
        $user_with_pivot = $user->settings()->where('settings.id','=', $id_setting)->withPivot('status')->firstOrFail();
        
        $status = $user_with_pivot->pivot->status;
                
        return $status;
    }

    /*
    |--------------------------------------------------------------------------
    | Get the setting value 
    |--------------------------------------------------------------------------
    | Description 
    | 
    | @id_setting
    | @id_user
    | return setting_value - string 
    */

    function getSettingValue($id_setting, $id_user){
        
        // if user parent corporation have custom skin-
        
        $user = App\User::where('id', $id_user)->get();
        
        $client = App\Client::where('user_id', $id_user)->get();
        
        $vendor = App\Vendor::where('user_id', $id_user)->get();
        
        //Si es cliente
        if(!$client->isEmpty()){
                         
            $corporation = App\Corporation::where('id', $client[0]->corporation_id)->first();
                                                 
            //Si tiene skin corporativo
            if($corporation->corporation_skin == 1){
                                                                        
                return array(   $corporation->corporation_style_file, 
                                
                                $corporation->corporation_logo_file, 
                                
                                $corporation->name);
                
            }
            //Si no tienen skin corporativo
            else{
                
                $settings = $user[0]->settings()->where('setting_id', $id_setting)->get();
                
                return array($settings[0]->pivot->setting_value, null, null);
            }
        
        }
        //Si es vendor
        else{
                        
            $settings = $user[0]->settings()->where('setting_id', $id_setting)->get();
                
            return array($settings[0]->pivot->setting_value, null, null);
        }        
        
        
        return dd('Error-helper-skin');
    }
