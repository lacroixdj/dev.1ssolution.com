<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conversations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [

        'client_id', 

        'client_userid', 

        'vendor_id',

        'vendor_userid', 
        
        'status',

        'rfp_id',

        'workorder_id',

        'channel',

        'type',

    ];


    public function client(){
        
        return $this->belongsTo(Client::class);
    
    }
    
    public function vendor(){
    
        return $this->belongsTo(Vendor::class);
    
    }

    public function rfp(){
    
        return $this->belongsTo(Rfp::class);
    
    }

    
    public function workorder(){
    
        return $this->belongsTo(Workorder::class);
    
    }
    

    
    public function messages(){
    
        return $this->hasMany(Message::class);
    
    }


    //Get all messages sent between client & vendor 
    //in this conversation from the latest to older
    public function last_messages(){
    
        return $this->hasMany(Message::class)->latest();
    
    }

    
    //Get the last message sent by client in this conversation
    public function last_client_message(){
    
        return $this->hasOne(Message::class)

               ->where('user_id', $this->client_userid)->latest();
    
    }

    //Get all messages sent by client in this conversation from the latest to older
    public function last_client_messages(){
    
        return $this->hasMany(Message::class)

               ->where('user_id', $this->client_userid)->latest();
    
    }


    //Get the last message sent by the vendor in this convesation
    public function last_vendor_message(){
    
        return $this->hasOne(Message::class)

               ->where('user_id', $this->vendor_userid)->latest();
    
    }
    

    //Get all messages sent by the vendor in this convesation from the latest to older
    public function last_vendor_messages(){
    
        return $this->hasMany(Message::class)

               ->has('')

               ->where('user_id', $this->vendor_userid)->latest();
    
    }

       

}
