<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workorder extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'workorders';
    
     /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
     /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [

        'user_id', 'client_id', 'vendor_id', 'industry_id', 'rfp_id',

        'status', 'emergency', 'description', 

        'location', 'bid_amount', 'tax_amount', 'service_date', 

        'type', 'visit', 'details', 'documents',
        
        'question_1', 'question_2', 'vendor_rate', 'close_details',
        
        'close_id', 'closed_date'
    ];
    
    
    //belongs to user 
    public function user(){
        return $this->belongsTo(User::class);
    }

    //belongs to client 
    public function client(){
        return $this->belongsTo(Client::class);
    }
    
    //belongs to  vendors
    public function vendors(){
        return $this->belongsTo(Vendor::class);
    } 

    //belongs to industry 
    public function industry(){
        return $this->belongsTo(Industry::class);
    }

    //belongs to rfp 
    public function rfp(){
        return $this->belongsTo(Rfp::class);
    }
    
   
}
