<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportTickets extends Model
{
    //
    protected $table = 'support_tickets';
    
    protected $fillable = ['user_id',
                           'support_category',
                           'description', 
                           'user_id_attend', 
                           'comment_attend', 
                           'status'];
    
    //RELATIONS
    
    //BELOSGNSTO USER 
    public function user(){
        return $this->belongsTo(User::class);
    }
}
