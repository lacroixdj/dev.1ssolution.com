<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metroarea extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'metroareas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function vendors(){
        return $this->hasMany(Vendor::class);
    }

    public function clients(){
        return $this->hasMany(Client::class);
    }
}
