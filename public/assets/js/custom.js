// inicia los efectos WOW
new WOW().init();

function empty(val){
	
	try {

		if(val==null) return true;
	
		switch(typeof(val)){
		
			case 'number':
				return (val==0);
			
			case 'boolean':
				return (val==false);
			
			case 'string':
				if (val=="" || val.match(/^\s*$/) ) return true;
				else return false;
			
			case 'object':
				return (val.length==0 );
			
			case 'function':
				return false;
			
			case 'undefined':
				return true;
		}

	}catch(e){

		console.log(e);

		return true;
	}
	
}

function XHRFormListener(formId){	

	try{

		if(empty(formId)) throw 'Error in XHRFormListener: formId is empty';

		$(formId).on( "submit", function( event ) {
			event.preventDefault();
			XHRFormSubmmit(this);		 
		});

	}catch(err){
	
		console.log(err);

	}
}

function XHRFormSubmmit(formObject){	
		
	try {

		if(empty(formObject)) throw 'Error in XHRFormSubmmit: formObject is empty';

		if($(formObject).parsley().validate() == true) {
			
			var formData = $(formObject).serialize();
			
			//Mostrar mascara de loading

			$.ajax({

				method: formObject.method,
				url: formObject.action,
				data: formData,
				dataType: 'json',
				encode: true,
				
				success: function(response) {

					// ocultar mascara de loading
					
					try{

						if(!empty(response.callback_func)){

							//window[(response.callback_func)]();
                                                        
                            switch(response.widget_type) {
                                
                                case 'modalMessage':
                                    
                                    showSuccessModal(response.title, response.message, response.text, response.callback_func);
                                    
                                    break;
                                
                                case 'flashMessage':
                                
                                    showSuccessFlashMessage(response.title, response.message, response.text, response.callback_func);
                                    
                                    break;
                                
                                default:
                                    
                                    showSuccessModal(response.title, response.message, response.text, response.callback_func);
                            }
						
                        /*}


						if(!empty(response.wizard_id)){

							handleWizardStep((response.wizard_id));*/

						} else {
                            
                            
                            switch(response.widget_type) {
                                
                                case 'modalMessage':
                                    
                                    showSuccessModal(response.title, response.message, response.text, response.callback_func);
                                    
                                    break;
                                
                                case 'flashMessage':
                                
                                    showSuccessFlashMessage(response.title, response.message, response.text, response.callback_func);
                                    
                                    break;
                                
                                default:
                                    
                                    showSuccessModal(response.title, response.message, response.text, response.callback_func);
                            }
						
						}

						
					}catch(err){

						//showErrorModal('Error', err, null);		
					}
					
				},
				
				error: function(response) {
					
					// ocultar mascara de loading
					
					var title = 'Error';
					
					var errors, message, errors_list = '';

					if(!empty(response.responseJSON)){

						try{

							if(!empty(response.responseJSON.error_message) ){

								message = response.responseJSON.error_message;					

							} else {

								$.each(response.responseJSON, function(key, value) {
									errors_list += '<li>' + key +': '+value+ '</li>'; 
								});

								message = 'Validation errors found';

								errors=errors_list;
							}

						}catch(err){

							message = err;

						} 
							
					} else {

						errors = "";
						message = response.status+' '+response.statusText;
					}

					showErrorModal(title, message, errors);
				}
			});

		} else{
			
			throw "Request errors found";
		}

	}catch(err){
			
			showErrorModal('Error', err, null);
	}
}

function showSuccessModal(title, message, text, callback_func){

	try{

		title = (!empty(title)) ? title : "Success";
			
		message = (!empty(message)) ? message : "Success request.";

		var icon ='<i class="fa fa-check" style="color:green"></i>';		
		
		$('#modal_success_title').html(title); 
		
		$('#modal_success_message').html(icon+message); 
		
		if(!empty(text)) $('#modal_success_text').html(text); 
		
		$('#modal_success').on('hidden.bs.modal',function(){
			
				console.log('limpiando el evento');   
			
				return true;       
	            
	    });

		if(!empty(callback_func)){

			$('#modal_success').on('hidden.bs.modal',function(){
				
				console.log(callback_func);
				
				window[(callback_func)]();       
	            
	        });

		} 
		

		$('#modal_success').modal('show');



		

	}catch(err){

		//alert(err);

		console.log(err);
		
	}
}	

function showErrorModal(title, message, errors_list){

	try{
		
		title = (!empty(title)) ? title : "Error";
		
		message = (!empty(message)) ? message : "Validation errors found";
		
		var icon ='<i class="fa fa-exclamation-triangle"></i> ';
	
		$('#modal_error_title').html(title); 
		
		$('#modal_error_message').html(icon+message); 	
		
		if(!empty(errors_list)) $('#modal_error_list').html(errors_list); 	
		
		$('#modal_error').modal('show');	

	}catch(err){

		//alert(err);

		console.log(err);
	
	}
	
}	

function handleWizardStep(wizard_id) {

	//Esto es una cabra
	//$("#vendorsTable").DataTable().ajax.reload();
	return false;
	//$('#'+wizard_id).wizard('next');
	//$('#'+wizard_id).wizard('selectedItem', { step: 2 });

}


/**
* Esta funcion muestra en una pequeña caja con custom mensaje al usuario,
* para notificar que la transaccion que realizo se proceso correctamente
* - 
* @namespace My.Namespace - N/A
* @method showSuccessFlashMessage
* @title {String} str - Titulo del mensaje
* @message {Object} obj - Texto que va en el mensaje
* @callback_func {requestCallback} callback - Objeto o metodo a ejecutar cuando se cumpla la funcion.
* @return {bool} 
*/
function showSuccessFlashMessage(title, message, text, callback_func){

	try{

		title = (!empty(title)) ? title : "Success";
			
		message = (!empty(message)) ? message : "Success request.";

		var icon ='<i class="fa fa-check" style="color:green"></i>';		
        
        var n = noty({
            text: icon+' '+message,
            
            timeout: 4000,
            
            layout: 'topRight',
            
            theme: 'ssolution',
            
            type: 'success',
            
            animation: {
            
                open: 'animated fadeInRightBig', 
                
                close: 'animated fadeOutRightBig', 
                
                easing: 'animated fadeOutRightBig', 
                
                speed: 200 
            },
            
            callback: {
                
                onShow: function() {
                    
                    if(!empty(callback_func)){
                        
                        console.log(callback_func);
                        
                        window[(callback_func)]();
                    }
                    
                },
                
                afterShow: function() {},
                
                onClose: function() {},
                
                afterClose: function() {},
                
                onCloseClick: function() {},
            },
        });

	}catch(err){

		console.log(err);
		
	}
}	

/*//TO-DO: jfarias
function setCookie(cname, cvalue, exdays) {
	//console.log('setting cookie'+cname);
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + 'path=/';
}*/

/*//TO-DO: jfarias
function getCookie(cname) {   
   	//console.log('getting cookie'+cname);
    var name = cname + "=";
    var ca = document.cookie.split(';');      
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}
*/

/*//TO-DO: jfarias
function removeCookie(cname) {	
	//console.log('removing cookie'+cname);
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/"; 
}
*/

/*//TO-DO: jfarias
function detectSidebarLastState(){
	
	cookie = getCookie('sidebarState');
	console.log(cookie);
	
	if(empty(cookie)){

		console.log('is expanded');

		if ($(window).width() > 767 && ($("body").hasClass('sidebar-collapse'))) {

			console.log('inside if expanded');
          
            $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
          
        }
	
	} else if(cookie=='collapsed'){

		console.log('is collapsed');

		if ($(window).width() > 767) {

			console.log('inside if collapsed');
          
            $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');            
          
        }
	
	}
	
}
*/

/*//TO-DO: jfarias
$( document ).ready(function() {
    //detectSidebarLastState();
})*/

function smartTruncate(str, n){
    if(str.length > n){
    	str = str.substr(0, n-1);
        str = (str.substr(0, str.lastIndexOf(' ')))+'...';
    }
    
    return  str;
};

// Adominguez, Draw the map in the aplication 

function mapMarker(address, mapIdInput, mapCanvas, errorCanvas){

    setTimeout(function(){
         var options = {
                map: mapCanvas
         };
        
         $(mapIdInput).geocomplete(options)

        .bind("geocode:result", function(event, result){

                console.log("Result: " + result.formatted_address);
             
                jQuery(errorCanvas).html('');

                $( mapCanvas ).show();

         })
        .bind("geocode:error", function(event, status){

                console.log("ERROR: " + status);

                $(mapCanvas).hide();

                if($(errorCanvas).has('img')){

                    jQuery(errorCanvas).html('');

                    $(errorCanvas).prepend('<img src="/assets/img/error-map.png" alt="" class="img-responsive img-map-error"/>')   

                }


        })
        .bind("geocode:multiple", function(event, results){

                console.log("Multiple: " + results.length + " results found");
             
                 jQuery(errorCanvas).html('');

                $( mapCanvas ).show();

                //$( ".img-map-error" ).hide();
        });

         $(mapIdInput).val(address).trigger("geocode");  

    }, 200);
}  

// Adominguez, Busca la informacion del vendor y llena las variables del vendor info modal

function findVendorInfo(vendorId, token){
     
$.post( "/vendors/moreInfo", { vendorId: vendorId, _token: token})

    .done(function( data ) {

        //Funcion que inicializa el mapa con la direccion del Vendor

        mapMarker(data.country +','+ data.state +','+ data.city+','+data.address, 
                  '#geocomplete', 
                  '.map_canvas_vendorlist', 
                  '#errorVendorInfoMap');


        var yes_no = ['No', 'Yes'];

        var emergency = yes_no[(data.emergency)];

        //$( "#companyNameInfo" ).text(data.vendorName);

        $( "#companyIndustryInfo" ).text(data.industry);

        $( "#CompanyEmergenciesInfo" ).text(emergency);       

        $( "#vendorContactPhoneInfo" ).text(data.phone);

        $( "#contactEmailInfo" ).text(data.email);

        $( "#contactWebInfo" ).text(data.web);

        $( "#CompanyMetroAreaInfo" ).text(data.metroArea);

        $( "#companyAddressInfo" ).text(data.address);

        $( "#companyCityInfo" ).text(data.city);

        $( "#companyStateInfo" ).text(data.state);

        $( "#companyCountryInfo" ).text(data.country);

        $( "#companyZipInfo" ).text(data.zip); 

        $( "#servicesListInfo" ).text(data.services);       

        $( "#attributesListInfo" ).text(data.attributes); 

        $( "#vendorUserEmailInfo" ).text(data.vendorUserEmail);
    
        $('#vendorRate').rating('update', 3);
    
        //Verifica si el vendor es favorito
        
        if(data.vendorFavorited == 1){
            
            $( "#companyNameInfo" ).text(data.vendorName).prepend('<i class="fa fa-star" data-toggle="tooltip" title="Vendor Favorited!"></i> ');
            
            //$( "#companyNameInfo" ).text(data.vendorName);
            
            $('#vendor-card').removeClass('bg-primary');
            
            $('#vendor-card').addClass('bg-success');
        
        }else{
            
            $( "#companyNameInfo" ).text(data.vendorName);
            
            $('#vendor-card').removeClass('bg-success');
            
            $('#vendor-card').addClass('bg-primary');

            
        }
        
       

    
        // method chaining 
        var ratingValue = $('#vendorRate').rating('update', data.rate).val();
        
        //Show the modal 
        $('#modalVendorInfo').modal('show');

    })

    .fail(function (data) {

        var title = 'Error';

        var errors, message, errors_list = '';

        errors = "";

        if(data.responseJSON){

            message = data.responseJSON.error_message;

        }else{

            message = 'Error looking up vendor';

        }

        showErrorModal(title, message, errors);

    });
}

// Adominguez, Busca la informacion del client y llena las variables del client info modal

function findClientInfo(clientId, token){
     
$.post( "/clients/moreInfo", { clientId: clientId, _token: token})

    .done(function( data ) {

        //Funcion que inicializa el mapa con la direccion del Vendor

        mapMarker(data.country +','+ data.state +','+ data.city+','+data.address, 
                  '#clientInfoGeocomplete', 
                  '.map_canvas_clientlist', 
                  '#errorClientInfoMap');

        $( "#clientNameInfo" ).text(data.clientName);

        $( "#clientContactPhoneInfo" ).text(data.phone);

        $( "#clientContactEmailInfo" ).text(data.email);

        $( "#clientContactWebInfo" ).text(data.web);

        $( "#clientMetroAreaInfo" ).text(data.metroArea);

        $( "#clientAddressInfo" ).text(data.address);

        $( "#clientCityInfo" ).text(data.city);

        $( "#clientStateInfo" ).text(data.state);

        $( "#clientCountryInfo" ).text(data.country);

        $( "#clientZipInfo" ).text(data.zip); 

        $( "#clientUserEmailInfo" ).text(data.clientUserEmail);
        
        //Show the modal 
        $('#modalClientInfo').modal('show');

    })

    .fail(function (data) {

        var title = 'Error';

        var errors, message, errors_list = '';

        errors = "";

        if(data.responseJSON){

            message = data.responseJSON.error_message;

        }else{

            message = 'Error looking up client';

        }

        showErrorModal(title, message, errors);

    });
}

// Global Vendor Rating Settings for vendor cards
// --------------------------------------------------------------------------
// TODO: quitar esto de aqui, y ponerlo en los global js
// --------------------------------------------------------------------------

$(".rating-disabled").rating({min:0, max:5, step:1, size:'vs', displayOnly:true});






      

