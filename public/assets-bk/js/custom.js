// inicia los efectos WOW
new WOW().init();




function empty(val){
	
	if(val==null) return true;
	
	switch(typeof(val)){
		
		case 'number':
			return (val==0);
		
		case 'boolean':
			return (val==false);
		
		case 'string':
			if (val=="" || val.match(/^\s*$/) ) return true;
			else return false;
		
		case 'object':
			return (val.length==0 );
		
		case 'function':
			return false;
		
		case 'undefined':
			return true;
	}
}



function XHRFormListener(formId){	

	try{

		if(empty(formId)) throw 'Error in XHRFormListener: formId is empty';

		$(formId).on( "submit", function( event ) {
			event.preventDefault();
			XHRFormSubmmit(this);		 
		});

	}catch(err){
	
		console.log(err);

	}
}



function XHRFormSubmmit(formObject){	
		
	try {

		if(empty(formObject)) throw 'Error in XHRFormSubmmit: formObject is empty';

		if($(formObject).parsley().validate() == true) {
			
			var formData = $(formObject).serialize();
			
			//Mostrar mascara de loading

			$.ajax({

				method: formObject.method,
				url: formObject.action,
				data: formData,
				dataType: 'json',
				encode: true,
				
				success: function(response) {

					// ocultar mascara de loading
					
					try{

						if(!empty(response.callback_func)){

							//window[(response.callback_func)]();

							showSuccessModal(response.title, response.message, response.text, response.callback_func);
						/*}


						if(!empty(response.wizard_id)){

							handleWizardStep((response.wizard_id));*/

						} else {

							showSuccessModal(response.title, response.message, response.text);
						
						}

						
					}catch(err){

						//showErrorModal('Error', err, null);		
					}
					
				},
				
				error: function(response) {
					
					// ocultar mascara de loading
					
					var title = 'Error';
					
					var errors, message, errors_list = '';

					if(!empty(response.responseJSON)){

						try{

							if(!empty(response.responseJSON.error_message) ){

								message = response.responseJSON.error_message;					

							} else {

								$.each(response.responseJSON, function(key, value) {
									errors_list += '<li>' + key +': '+value+ '</li>'; 
								});

								message = 'Validation errors found';

								errors=errors_list;
							}

						}catch(err){

							message = err;

						} 
							
					} else {

						errors = "";
						message = response.status+' '+response.statusText;
					}

					showErrorModal(title, message, errors);
				}
			});

		} else{
			
			throw "Request errors found";
		}

	}catch(err){
			
			showErrorModal('Error', err, null);
	}
}





function showSuccessModal(title, message, text, callback_func){

	try{

		title = (!empty(title)) ? title : "Success";
			
		message = (!empty(message)) ? message : "Success request.";

		var icon ='<i class="fa fa-check" style="color:green"></i>';		
		
		$('#modal_success_title').html(title); 
		
		$('#modal_success_message').html(icon+message); 
		
		if(!empty(text)) $('#modal_success_text').html(text); 
		
		$('#modal_success').on('hidden.bs.modal',function(){
			
				console.log('limpiando el evento');   
			
				return true;       
	            
	    });

		if(!empty(callback_func)){

			$('#modal_success').on('hidden.bs.modal',function(){
				
				console.log(callback_func);
				
				window[(callback_func)]();       
	            
	        });

		} 
		

		$('#modal_success').modal('show');



		

	}catch(err){

		//alert(err);

		console.log(err);
		
	}
}	




function showErrorModal(title, message, errors_list){

	try{
		
		title = (!empty(title)) ? title : "Error";
		
		message = (!empty(message)) ? message : "Validation errors found";
		
		var icon ='<i class="fa fa-exclamation-triangle"></i>';
	
		$('#modal_error_title').html(title); 
		
		$('#modal_error_message').html(icon+message); 	
		
		if(!empty(errors_list)) $('#modal_error_list').html(errors_list); 	
		
		$('#modal_error').modal('show');	

	}catch(err){

		//alert(err);

		console.log(err);
	
	}
	
}	


function handleWizardStep(wizard_id) {

	//Esto es una cabra
	//$("#vendorsTable").DataTable().ajax.reload();
	return false;
	//$('#'+wizard_id).wizard('next');
	//$('#'+wizard_id).wizard('selectedItem', { step: 2 });

}