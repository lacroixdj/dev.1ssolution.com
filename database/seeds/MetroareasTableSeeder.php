<?php

use Illuminate\Database\Seeder;

class MetroareasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
		$metro_areas = array(
			array('id' => '1','name' => 'Dallas/Ft.Worth'),
			array('id' => '2','name' => 'Austin'),
			array('id' => '3','name' => 'San Antonio'),
			array('id' => '4','name' => 'Houston'),
			array('id' => '5','name' => 'Boston'),
			array('id' => '6','name' => 'Miami'),
			array('id' => '7','name' => 'El Paso')
		);

        DB::table('metroareas')->insert($metro_areas);
    }
}
