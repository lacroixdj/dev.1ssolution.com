<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array(
            array('id' => '1','table' => 'support_category','label' => 'Interface error','value' => 'error_1'),
            array('id' => '2','table' => 'support_category','label' => 'Information error','value' => 'error_2'),
            array('id' => '3','table' => 'support_category','label' => 'Functionality error','value' => 'error_3'),
            array('id' => '4','table' => 'support_category','label' => 'Comunication error','value' => 'error_4'),
            array('id' => '5','table' => 'support_category','label' => 'Other error','value' => 'error_5'),
            array('id' => '6','table' => 'support_status','label' => 'Pending','value' => 'pending'),
            array('id' => '7','table' => 'support_status','label' => 'In Review','value' => 'inreview'),
            array('id' => '8','table' => 'support_status','label' => 'Processing','value' => 'processing'),
            array('id' => '9','table' => 'support_status','label' => 'Closed','value' => 'closed'),
            array('id' => '10','table' => 'workorder_questions','label' => 'Did the Vendor arrive on time?','value' => '1'),
            array('id' => '11','table' => 'workorder_questions','label' => 'Job completed satisfactorily?','value' => '2'),
            array('id' => '12','table' => 'workorder_questions','label' => 'Please rate this request (1-5)','value' => '3'),
            array('id' => '13','table' => 'workorder_questions','label' => 'Please rate this request (1-5)','value' => '4'),
            array('id' => '14','table' => 'workorder_questions_responses','label' => 'Yes','value' => 'yes'),
            array('id' => '15','table' => 'workorder_questions_responses','label' => 'No','value' => 'no'),
            array('id' => '16','table' => 'workorder_questions_rate','label' => '1','value' => '1'),
            array('id' => '17','table' => 'workorder_questions_rate','label' => '2','value' => '2'),
            array('id' => '18','table' => 'workorder_questions_rate','label' => '3','value' => '3'),
            array('id' => '19','table' => 'workorder_questions_rate','label' => '4','value' => '4'),
            array('id' => '20','table' => 'workorder_questions_rate','label' => '5','value' => '5'),
            array('id' => '21','table' => 'interface_skin','label' => 'Blue All','value' => 'skin-ssolution'),
            array('id' => '22','table' => 'interface_skin','label' => 'Blue Light','value' => 'skin-blue-light'),
            array('id' => '23','table' => 'interface_skin','label' => 'Blue Dark','value' => 'skin-blue'),
            array('id' => '24','table' => 'interface_skin','label' => 'Green All','value' => 'skin-green-sidebar'),
            array('id' => '25','table' => 'interface_skin','label' => 'Green Light','value' => 'skin-green-light'),
            array('id' => '26','table' => 'interface_skin','label' => 'Green Dark','value' => 'skin-green'),
            array('id' => '27','table' => 'interface_skin','label' => 'Purple All','value' => 'skin-purple-sidebar'),
            array('id' => '28','table' => 'interface_skin','label' => 'Purple Light','value' => 'skin-purple-light'),
            array('id' => '29','table' => 'interface_skin','label' => 'Purple Dark','value' => 'skin-purple'),
            array('id' => '30','table' => 'interface_skin','label' => 'Red All','value' => 'skin-red-sidebar'),
            array('id' => '31','table' => 'interface_skin','label' => 'Purple Light','value' => 'skin-red-light'),
            array('id' => '32','table' => 'interface_skin','label' => 'Red Dark','value' => 'skin-red'),
            array('id' => '33','table' => 'interface_skin','label' => 'Yellow All','value' => 'skin-yellow-sidebar'),
            array('id' => '34','table' => 'interface_skin','label' => 'Yellow Light','value' => 'skin-yellow-light'),
            array('id' => '35','table' => 'interface_skin','label' => 'Yellow Dark','value' => 'skin-yellow'),
            array('id' => '36','table' => 'vendors_short','label' => 'Name A - Z','value' => 'name-asc'),
            array('id' => '37','table' => 'vendors_short','label' => 'Name Z - A','value' => 'name-desc'),
            array('id' => '38','table' => 'invitations_short','label' => 'Date invitations desc','value' => 'created_at-desc'),
            array('id' => '39','table' => 'invitations_short','label' => 'Date invitations asc','value' => 'created_at-asc'),
            array('id' => '40','table' => 'invitations_short','label' => 'Name A - Z','value' => 'name-asc'),
            array('id' => '41','table' => 'invitations_short','label' => 'Name Z - A','value' => 'name-desc'),
            array('id' => '42','table' => 'invitations_filter','label' => 'Invitations Sent','value' => 'sent'),
            array('id' => '43','table' => 'invitations_filter','label' => 'Invitation accepted','value' => 'joined'),
            array('id' => '44','table' => 'tax_default_value','label' => 'Tax amount default to vendors ','value' => '8.25'),
            
		);

    	DB::table('categories')->insert($categories);

    }
}
