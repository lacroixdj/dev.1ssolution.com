<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $counter;

    public $faker;
    
    public function run() {
        
	    /**	
		 * Model seeder for:
		 * Users
		 *	->Client
		 *		->ClientAddresses
		 *		->Client Settings
         *      ->RFP
         *      ->RFP_Vendors
		 *		->RFP_Services
		*/

    	// Users:

        $this->counter=0;

        $this->faker = \Faker\Factory::create();

    	factory(App\User::class, 100)->create()->each(function($user) {

            $user->email='sanchezanto@aafes.com';
            //$user->email='angeldominguezg@gmail.com';
            //$user->email='jesus.lacroix@gmail.com';
            
            $aux = 'client'.$this->counter;

            $user->email=$aux.'@1ssolution.com';
            
            $user->passwordplain = $aux;
            
            $user->password= bcrypt($aux);
            
            $user->sms_address='2149063897@txt.att.net';
            
            $user->mms_address='2149063897@mms.att.net';
            
            $user->phone='(214) 906-3897';
            
            $user->save();

            $this->counter++;
            
            //Client Settings
            
            $settings = App\Setting::all();
            
            foreach($settings as $setting) {     
                
                $user->settings()->attach($setting->id);   
            
            }
            
            $setting_skin =  $user->settings()->where('setting_id', 3)->get();
            
            //$skin = \DB::table('categories')->where('table', 'interface_skin')->get();
                        
            //$skinRandom =  array_rand($skin,1);
                          
            //$setting_skin[0]->pivot->setting_value =  $skin[$skinRandom]->value;
            
            $setting_skin[0]->pivot->setting_value =  'skin-blue';
            
            $result =  $setting_skin[0]->pivot->save();

            // Clients:

        	$client = $user->clients()->save(

        		factory(App\Client::class)->make()

        	);

			// Client addresses:

            $client_address = factory(App\ClientAddress::class)->make();

			$client->addresses()->save($client_address); 

            $rfp = false;
            
            
            //Adominguez -> crear 50 invitaciones por client
            for($i=0; $i<50; $i++){
                
                $client_invite = factory(App\Invite::class)->make();
                
                $client_invite->user_id = $client->user_id;
                
                if($client_invite->status == 'joined'){
                     
                    $client_invite->email = 'vendor'.$i.'@1ssolution.com';
                
                }
                
                $client_invite->save();
            }
            //fin creacion de invitaciones
            

            for($i=0; $i<50; $i++){

                $rfp = factory(App\Rfp::class)->make();

                $rfp->user_id = $user->id;

                $rfp->client_id = $client->id;

                $rfp->number = $client->id;

                $rfp->location = $client_address->address.', '.$client_address->city->name.', '.$client_address->city->state->name;

                $rfp->save();

                $vendors_ids = [];

                $services_ids = [];

                $vendors = \App\Vendor::where('industry_id', $rfp->industry_id)

                            ->inRandomOrder()

                            ->take(3)

                            ->get();

                
                if($rfp->type=='RFP' && (($vendors->count())>=3)){

                        foreach ($vendors as $vendor) {
                                               
                            $vendors_ids [] = $vendor->id;

                            $services_ids = array_merge($services_ids, ($vendor->services->lists('id')->toArray()));       

                        }
    
                } elseif(!empty($vendors[0])) {

                        $vendors_ids []  = $vendors[0]->id;

                        $services_ids = array_merge($services_ids, ( $vendors[0]->services->lists('id')->toArray()));
                
                } else {

                        $rfp->status = 'open';

                        $rfp->save();
                }

                
                if (!empty($vendors_ids) && count($vendors_ids)) $rfp->vendors()->sync($vendors_ids);                    
                
                if (!empty($services_ids) && count($services_ids)) {

                        $services_ids = array_unique ($services_ids, SORT_NUMERIC);

                        $rfp->services()->sync($services_ids);

                }



                switch ($rfp->status) {

                    case 'open':
                    case 'sent':
                            
                        foreach ($rfp->vendors as $vendor) {

                            if($rfp->status == 'sent') $vendor->pivot->status = 'invited';
                            else $vendor->pivot->status = $rfp->status;

                            $vendor->pivot->save();
                        
                        }
      
                    break;

                    case 'bid':
                    case 'workorder':
                    case 'closed':
                    
                        $vendor = $rfp->vendors->random();
                        $vendor->pivot->status = $rfp->status;
                        $vendor->pivot->bid_amount = $this->faker->randomFloat(2, 100 , 1000);
                        $vendor->pivot->save();

                        if($rfp->status=='workorder' || $rfp->status=='closed'){

                            
                            $workorder = factory(App\Workorder::class)->make();
                            
                            $workorder->user_id = $rfp->user_id;

                            $workorder->client_id = $rfp->client_id;

                            $workorder->vendor_id = $vendor->id;

                            $workorder->industry_id =  $rfp->industry_id;
                            
                            $workorder->rfp_id =  $rfp->id;

                            $workorder->emergency =  $rfp->emergency;

                            $workorder->description =  $rfp->description;
                            
                            $workorder->location =  $rfp->location;

                            $workorder->service_date =  $rfp->service_date;

                            $workorder->type =  $rfp->type;

                            $workorder->visit =  $rfp->visit;
                            
                            $workorder->details =  $rfp->details;
                            
                            $workorder->documents =  $rfp->documents;

                            $workorder->status =  'open';

                            if($rfp->status=='closed') {

                                $workorder->status =  'closed';

                                $workorder->question_1 =  $this->faker->realText(25);
                                
                                $workorder->question_2 =  $this->faker->realText(25);

                                $workorder->vendor_rate =  $this->faker->numberBetween(1, 5); 

                                $workorder->close_details =  $this->faker->realText(100);

                            }

                            $workorder->save();

                            $workorder = false;

                        }

                    break;

                }


                $rfp = false;
            }

        	    	
    	});

    	// End model seeder.

	}

}
