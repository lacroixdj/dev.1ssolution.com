<?php

use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
		$industries = array(
			  array('id' => '1','name' => 'Janitorial'),
			  array('id' => '2','name' => 'HVAC'),
			  array('id' => '3','name' => 'Glass'),
			  array('id' => '4','name' => 'Landscaping'),
			  array('id' => '5','name' => 'Plumbing'),
			  array('id' => '6','name' => 'Roofing'),
			  array('id' => '7','name' => 'Supplies'),
			  array('id' => '8','name' => 'Movers/Reloc'),
			  array('id' => '9','name' => 'Electrical'),
			  array('id' => '10','name' => 'Pest Control'),
			  array('id' => '11','name' => 'Vending'),
			  array('id' => '12','name' => 'Waste Management'),
			  array('id' => '13','name' => 'Parking Lot'),
			  array('id' => '14','name' => 'Storage'),
			  array('id' => '15','name' => 'Signs')
		);

    	DB::table('industries')->insert($industries);



    }
}
