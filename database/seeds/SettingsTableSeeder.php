<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $settings = array(
            array('id' => '1','name' => 'Receive email messages?','group' => 'messages','status' => '1'),
            array('id' => '2','name' => 'Receive sms messages?','group' => 'messages','status' => '1'),
            array('id' => '3','name' => 'Skin selector.','group' => 'skin','status' => '1'),
            array('id' => '4','name' => 'Amount','group' => 'vendor-tax','status' => '1'),             
		);

    	DB::table('settings')->insert($settings);
    }
}
