<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

		//
		//From legacy database `1ss_db`
		//
		// `1ss_db`.`industries_attributes`
		$attributes = array(
		  array('industry_id' => '1','name' => ucfirst( 'Jan Licensed')),
		  array('industry_id' => '1','name' => ucfirst( 'Jan Bonded')),
		  array('industry_id' => '1','name' => ucfirst( 'Jan BBB Memberfslkfsldkfjdslfj')),
		  array('industry_id' => '4','name' => ucfirst( 'Landscaping Certified')),
		  array('industry_id' => '4','name' => ucfirst( 'Landscaping BBB Member')),
		  array('industry_id' => '4','name' => ucfirst( 'Tree removal Certification')),
		  array('industry_id' => '7','name' => ucfirst( 'Supplier bonded')),
		  array('industry_id' => '7','name' => ucfirst( 'Supplier BBB')),
		  array('industry_id' => '8','name' => ucfirst( 'Movers insured')),
		  array('industry_id' => '8','name' => ucfirst( 'Movers bonded')),
		  array('industry_id' => '2','name' => ucfirst( 'Maint/Repair')),
		  array('industry_id' => '2','name' => ucfirst( 'Install')),
		  array('industry_id' => '3','name' => ucfirst( 'Glass Bonded')),
		  array('industry_id' => '3','name' => ucfirst( 'Glass Certification')),
		  array('industry_id' => '3','name' => ucfirst( 'Glass Licensing')),
		  array('industry_id' => '5','name' => ucfirst( 'Plumbers Certification')),
		  array('industry_id' => '5','name' => ucfirst( 'Plumbers Bonded')),
		  array('industry_id' => '6','name' => ucfirst( 'Roofers  Warranty')),
		  array('industry_id' => '7','name' => ucfirst( 'Supplier Insurance depot Certified')),
		  array('industry_id' => '5','name' => ucfirst( 'Plumbers Licensed')),
		  array('industry_id' => '6','name' => ucfirst( 'Roofers Licensing')),
		  array('industry_id' => '6','name' => ucfirst( 'Roofers NEN Certified')),
		  array('industry_id' => '8','name' => ucfirst( 'Movers BBB')),
		  array('industry_id' => '10','name' => ucfirst( 'BBB')),
		  array('industry_id' => '9','name' => ucfirst( 'BBB')),
		  array('industry_id' => '9','name' => ucfirst( 'Bonded')),
		  array('industry_id' => '11','name' => ucfirst( 'vendor licensed')),
		  array('industry_id' => '11','name' => ucfirst( 'BBB approved')),
		  array('industry_id' => '12','name' => ucfirst( 'BBB approved')),
		  array('industry_id' => '12','name' => ucfirst( 'Waste disposal certified')),
		  array('industry_id' => '12','name' => ucfirst( 'Bonded')),
		  array('industry_id' => '13','name' => ucfirst( 'BBB Certified')),
		  array('industry_id' => '13','name' => ucfirst( 'Licensed'))
		);

		DB::table('attributes')->insert($attributes);
    }
}
