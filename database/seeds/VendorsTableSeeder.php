<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $counter;


    public function run() {
        
	    /**	
		 * Model seeder for:
		 * Users
		 *	->Vendors
		 *		->VendorAddresses
		 *		->VendorSerices
		 *		->Vendor Attributes
		*/
	    //
    	// Users:
	    $this->counter=0;
        


    	factory(App\User::class, 100)->create()->each(function($user) {

    		//User changes for testing    		
    		//$user->email='tonybassguitar@gmail.com';
    		//$user->email='dominguez.punk@gmail.com';
            //$user->email='jesus.farias@gmail.com';

            $aux = 'vendor'.$this->counter;

            $user->email=$aux.'@1ssolution.com';
            
            $user->passwordplain = $aux;
            
            $user->password= bcrypt($aux);

            $user->sms_address='2149063897@txt.att.net';

            $user->mms_address='2149063897@mms.att.net';

            $user->phone='(214) 906-3897';

        	$user->save();

            $this->counter++;
    		

    		// Vendors:   		
    		$vendor = $user->vendors()->save(
        		factory(App\Vendor::class)->make()
        	);

			// Vendors addresses:
			$vendor->addresses()->save(

				factory(App\VendorAddresses::class)->make()

			);
            
            $settings = App\Setting::all();
            
            //Vendor Settings   
            foreach($settings as $setting)
            {     
                $user->settings()->attach($setting->id);   
                                          
            }
            
            //Skin
            
            $setting_skin =  $user->settings()->where('setting_id', 3)->get();
            
            $skin = \DB::table('categories')->where('table', 'interface_skin')->get();
            
            $skinRandom =  array_rand($skin,1);
                          
            $setting_skin[0]->pivot->setting_value =  $skin[$skinRandom]->value;
            
            $result =  $setting_skin[0]->pivot->save();
            
            //Tax amount
		                  
            $category_tax_default = App\Categories::where('table','=','tax_default_value')->first();
            
            $category_tax_default = $category_tax_default->value;
            
            $setting_tax=  $user->settings()->where('setting_id', 4)->get();
                                                      
            $setting_tax[0]->pivot->setting_value = $category_tax_default;
    
            $result =  $setting_tax[0]->pivot->save();
            
            // Services:

			$services = null;
			
			$services = \DB::table('services')->where('industry_id', $vendor->industry_id)->get();
       	
        	$services_ids = [];

        	if(count($services)>=3){

	        	$keys = array_rand($services, 3);

	        	foreach ($keys as $key => $value) {

	        		try {
	        			
	        			if( !empty($services[$value]->id) ){

	        				$services_ids [] = $services[$value]->id;

	        			}

	        		} catch (Exception $e) {
	        			
	        			continue;	
	        		
	        		}	        		

	        	}

	
			} elseif(!empty($services[0])) {

        		$services_ids [0]  = $services[0]->id;

        	}

        	if (!empty($services_ids) && count($services_ids)) $vendor->services()->sync($services_ids);

        	// Attributes:

        	$attributes = \DB::table('attributes')
        		
        		->where('industry_id', $vendor->industry_id)
        		
        		->get();

        	$attributes_ids = [];

        	if(count($attributes)>=2){

	        	$att_keys = array_rand($attributes, 2);

	        	foreach ($att_keys as $key => $value) {

	        		try {
	        			
	        			if( !empty($attributes[$value]->id) ){

	        				$attributes_ids [] = $attributes[$value]->id;

	        			}

	        		} catch (Exception $e) {
	        			
	        			continue;	
	        		
	        		}	        		

	        	}
       	
			
			} elseif(!empty($attributes[0])) {

        		$attributes_ids [0]  = $attributes[0]->id;

        	}

        	if(!empty($attributes_ids && count($attributes_ids))) $vendor->attributes()->sync($attributes_ids);	
        	    	
    	});

    	// End model seeder.

	}   
	
}
