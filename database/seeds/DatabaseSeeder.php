<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //industries
        $this->call(IndustriesTableSeeder::class); 
        
        //attributes
        $this->call(AttributesTableSeeder::class);
        
        //services
        $this->call(ServicesTableSeeder::class);
        
        //caegories
        $this->call(CategoriesTableSeeder::class);

        //countries
        $this->call(CountriesTableSeeder::class);

        //states
        $this->call(StatesTableSeeder::class);

        //cities
        $this->call(CitiesTableSeeder::class); 

        //metroareas
        $this->call(MetroareasTableSeeder::class);
        
        //corporations
        $this->call(CorporationTableSeeder::class);

        //settings
        $this->call(SettingsTableSeeder::class);

        //vendors
        $this->call(VendorsTableSeeder::class);

        //clients
        $this->call(ClientsTableSeeder::class);
        

        
        
        
        
    }
}
