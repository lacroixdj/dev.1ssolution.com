<?php

use Illuminate\Database\Seeder;

class CorporationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $corporation = array(
            array('name' => 'Walmart SE Division','passcode' => '123456', 'phone'=>'(000) 000-0000', 'email'=>'contact@walmart.com', 'url'=>'www.walmart.com','corporation_type' => 'CORP', 'corporation_skin' => 0, 'corporation_style_file' => '', 'corporation_logo_file'=>'' ),
            array('name' => 'Target','passcode' => '23456', 'phone'=>'(000) 000-0000', 'email'=>'contact@target.com', 'url'=>'www.target.com', 'corporation_type' => 'TENANT', 'corporation_skin' => 0, 'corporation_style_file' => '', 'corporation_logo_file'=>''),
            array('name' => 'Starbucks','passcode' => '34567', 'phone'=>'(000) 000-0000', 'email'=>'contact@starbucks.com', 'url'=>'www.starbucks.com', 'corporation_type' => 'CORP', 'corporation_skin' => 0, 'corporation_style_file' => '', 'corporation_logo_file'=>''),
            array('name' => 'Dickey\'s','passcode' => 'dickeys','phone' => '(000) 000-0000','email' => 'contact-test@dickeys.com','url' => 'www.dickeys.com','corporation_type' => 'CORP', 'corporation_skin' => 1, 'corporation_style_file' => 'skin-dickeys', 'corporation_logo_file'=>'logo-dickeys'),
            array('name' => 'Panera Bread','passcode' => 'panera','phone' => '(000) 000-0000','email' => 'contact-test@panerabread.com','url' => 'www.panerabread.com','corporation_type' => 'CORP', 'corporation_skin' => 1, 'corporation_style_file' => 'skin-panera', 'corporation_logo_file'=>'logo-panera')
        );
        
        DB::table('corporations')->insert($corporation);
    }
}