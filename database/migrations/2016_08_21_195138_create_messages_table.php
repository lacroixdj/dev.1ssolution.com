<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function(Blueprint $table) {
            
            $table->increments('id');
            $table->integer('conversation_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->string('channel')->index();
            $table->string('socket_id')->index();
            $table->integer('status')->index()->default(0);
            $table->mediumtext('text');
            $table->timestamps();
            $table->softDeletes();

        $table->foreign('conversation_id')
            ->references('id')
            ->on('conversations')
            ->onUpdate('cascade')  
            ->onDelete('restrict');

        $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')  
            ->onDelete('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
