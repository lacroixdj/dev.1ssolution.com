<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('workorders', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('client_id')->unsigned()->index();       
            $table->integer('vendor_id')->unsigned()->index();      
            $table->integer('industry_id')->unsigned()->index();
            $table->integer('rfp_id')->unsigned()->index();
            $table->string('status')->default('open')->index();
            $table->boolean('emergency')->default(0)->nullable();
            $table->string('description');
            $table->mediumText('location');
            $table->decimal('bid_amount', 12, 2)->default(0.00);
            $table->decimal('tax_amount', 12, 2)->default(0.00);
            $table->date('service_date');
            $table->string('type');
            $table->boolean('visit')->default(0)->nullable();
            $table->mediumText('details');
            $table->mediumText('documents')->nullable();
            $table->string('question_1')->nullable();
            $table->string('question_2')->nullable();
            $table->integer('vendor_rate')->nullable();
            $table->string('close_details')->nullable();
            $table->integer('close_id')->unsigned()->index()->nullable();
            $table->date('closed_date')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');

            $table->foreign('client_id')
                    ->references('id')
                    ->on('clients')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
            
            $table->foreign('vendor_id')
                    ->references('id')
                    ->on('vendors')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
            
            $table->foreign('rfp_id')
                    ->references('id')
                    ->on('rfps')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->foreign('industry_id')
                    ->references('id')
                    ->on('industries')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workorders');
    }
}
