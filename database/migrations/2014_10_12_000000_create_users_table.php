<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->string('name');
            $table->string('lastname');
            $table->string('phone');
            $table->string('email')->unique();
            //$table->string('email');
            $table->string('sms_address')->nullable();
            $table->string('mms_address')->nullable();
            $table->string('password');
            $table->string('passwordplain')->nullable();
            $table->rememberToken();
            $table->string('verification_code', 500);
            $table->boolean('verified');
            $table->boolean('haslogged')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
