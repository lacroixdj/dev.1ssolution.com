<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRfpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfps', function(Blueprint $table) {

            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();

            $table->integer('client_id')->unsigned()->index();           

            $table->integer('industry_id')->unsigned()->index();

            $table->string('status')->default('open')->index();

            $table->integer('number');            

            $table->string('description');

            $table->boolean('emergency')->default(0)->nullable();

            $table->mediumText('location');

            $table->timestamp('service_date');

            $table->timestamp('response_date');

            $table->string('type');

            $table->boolean('visit')->default(0)->nullable();

            $table->mediumText('details');

            $table->mediumText('documents')->nullable();

            $table->timestamps();

            $table->softDeletes();

            $table->foreign('user_id')
                    
                    ->references('id')
                    
                    ->on('users')
                    
                    ->onUpdate('cascade')                        
                    
                    ->onDelete('restrict');

            $table->foreign('client_id')
                    
                    ->references('id')
                    
                    ->on('clients')
                    
                    ->onUpdate('cascade')
                    
                    ->onDelete('restrict');

            $table->foreign('industry_id')
                    
                    ->references('id')
                    
                    ->on('industries')
                    
                    ->onUpdate('cascade')
                    
                    ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfps');
    }
}
