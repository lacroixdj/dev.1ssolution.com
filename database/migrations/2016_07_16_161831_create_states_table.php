<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->integer('country_id')->unsigned()->index();
            $table->string('name');
            $table->string('shortname')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onUpdate('cascade')                        
                ->onDelete('restrict');
                


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('states');
    }
}
