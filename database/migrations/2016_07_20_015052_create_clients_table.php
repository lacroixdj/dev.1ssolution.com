<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->integer('user_id')->unsigned()->index();  
            $table->integer('corporation_id')->unsigned()->index(); 
            $table->integer('metroarea_id')->unsigned()->index();
            $table->string('name');
            $table->string('phone');
            $table->string('email')->unique();
            $table->string('sms_address')->nullable();
            $table->string('mms_address')->nullable();
            $table->string('url')->nullable();            
            $table->timestamps();
            $table->softDeletes();
         
         
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
              
            $table->foreign('corporation_id')
                    ->references('id')
                    ->on('corporations')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
             
            $table->foreign('metroarea_id')
                    ->references('id')
                    ->on('metroareas')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
         
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('clients');
    }
}
