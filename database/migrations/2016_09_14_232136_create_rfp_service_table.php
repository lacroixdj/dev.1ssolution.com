<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfpServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfp_service', function (Blueprint $table) {
            $table->integer('rfp_id')->unsigned()->index();
            $table->integer('service_id')->unsigned()->index();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('rfp_id')
                    ->references('id')
                    ->on('rfps')
                    ->onUpdate('cascade')      
                    ->onDelete('restrict');
                
            $table->foreign('service_id')
                    ->references('id')
                    ->on('services')
                    ->onUpdate('cascade')  
                    ->onDelete('restrict');
                    

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfp_service');
    }
}
