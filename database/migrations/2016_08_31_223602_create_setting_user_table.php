<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_user', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('setting_id')->unsigned()->index();
            $table->boolean('status')->default(1)->index(); 
            $table->string('setting_value')->default(null);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
            
            $table->foreign('setting_id')
                    ->references('id')
                    ->on('settings')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting_user');
    }
}
