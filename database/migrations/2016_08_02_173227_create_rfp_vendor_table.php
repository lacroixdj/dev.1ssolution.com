<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfpVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        
        Schema::create('rfp_vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rfp_id')->unsigned()->index();
            $table->integer('vendor_id')->unsigned()->index();
            $table->decimal('bid_amount', 12, 2)->default(0.00);
            $table->string('status')->default('open')->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('rfp_id')
                    ->references('id')
                    ->on('rfps')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
                
            $table->foreign('vendor_id')
                    ->references('id')
                    ->on('vendors')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfp_vendor');
    }
}
