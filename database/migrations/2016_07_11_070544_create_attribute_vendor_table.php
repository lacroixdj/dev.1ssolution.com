<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_vendor', function (Blueprint $table) {
            
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('vendor_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('attribute_id')
                    ->references('id')
                    ->on('attributes')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
                
            $table->foreign('vendor_id')
                    ->references('id')
                    ->on('vendors')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
                
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_vendor');
    }
}
