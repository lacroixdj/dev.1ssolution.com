<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up(){

        Schema::create('vendor_addresses', function (Blueprint $table) {
            
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->integer('vendor_id')->unsigned()->index();
            $table->integer('city_id')->unsigned()->index();
            $table->string('address');
            $table->string('address2')->nullable();
            $table->string('zip');
            $table->string('phone');
            $table->boolean('main_address')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onUpdate('cascade')                      
                ->onDelete('restrict');
                
            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendor_addresses');
    }
}
