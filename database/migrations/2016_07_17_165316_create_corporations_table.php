<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorporationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporations', function(Blueprint $table) {
            
            $table->increments('id');
            
            $table->string('name');
            
            $table->string('passcode');
            
            $table->string('phone');
            
            $table->string('email');
            
            $table->string('url');
            
            $table->string('corporation_type');
            
            $table->boolean('corporation_skin'); // I/O On or Of 
            
            $table->string('corporation_style_file'); //CSS File Name
            
            $table->string('corporation_logo_file'); //Logo File Name
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('corporations');
    }
}
