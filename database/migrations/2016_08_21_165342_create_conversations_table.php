<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::create('conversations', function(Blueprint $table) {
            
            $table->increments('id');
            $table->integer('client_id')->unsigned()->index();
            $table->integer('client_userid')->unsigned()->index();
            $table->integer('vendor_id')->unsigned()->index();
            $table->integer('vendor_userid')->unsigned()->index();
            $table->integer('status')->index()->default(0);
            $table->integer('rfp_id')->unsigned()->index();            
            $table->integer('workorder_id')->unsigned()->index()->nullable();
            $table->string('channel')->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onUpdate('cascade')  
                ->onDelete('restrict');

            $table->foreign('client_userid')
                ->references('user_id')
                ->on('clients')
                ->onUpdate('cascade')  
                ->onDelete('restrict');

            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onUpdate('cascade')  
                ->onDelete('restrict');

            $table->foreign('vendor_userid')
                ->references('user_id')
                ->on('vendors')
                ->onUpdate('cascade')  
                ->onDelete('restrict');

            $table->foreign('rfp_id')
                ->references('id')
                ->on('rfps')
                ->onUpdate('cascade')  
                ->onDelete('restrict');
            
            
            $table->foreign('workorder_id')
                ->references('id')
                ->on('workorders')
                ->onUpdate('cascade')  
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conversations');
    }
}
