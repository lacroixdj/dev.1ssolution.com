<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->integer('user_id')->unsigned()->index();            
            $table->integer('industry_id')->unsigned()->index();
            $table->integer('metroarea_id')->unsigned()->index();
            $table->string('name');
            $table->string('phone');
            $table->string('email')->unique();
            $table->string('sms_address')->nullable();
            $table->string('mms_address')->nullable();
            $table->string('url')->nullable();            
            $table->boolean('emergency')->default(0)->nullable();
            $table->integer('rate')->default(0)->nullable();;
            $table->timestamps();
            $table->softDeletes();

            //Became part of vendor_addresses table            
                //$table->string('address');
                //$table->string('city');
                //$table->string('state');
                //$table->string('zip');
                //$table->string('phone');

            //@/elseif ((Auth::user()->has('clients')))
            
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
                
                $table->foreign('industry_id')
                    ->references('id')
                    ->on('industries')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

                $table->foreign('metroarea_id')
                    ->references('id')
                    ->on('metroareas')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vendors');
    }
}
