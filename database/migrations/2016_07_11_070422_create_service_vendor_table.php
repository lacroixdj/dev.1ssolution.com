<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_vendor', function (Blueprint $table) {
           
            $table->integer('service_id')->unsigned()->index();
            $table->integer('vendor_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('service_id')
                    ->references('id')
                    ->on('services')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
                
            $table->foreign('vendor_id')
                    ->references('id')
                    ->on('vendors')
                    ->onUpdate('cascade')                        
                    ->onDelete('restrict');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('service_vendor');
    }
}
