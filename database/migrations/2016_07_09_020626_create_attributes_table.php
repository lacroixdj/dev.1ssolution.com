<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
           $table->increments('id');
            $table->uuid('uuid')->index();
            $table->integer('industry_id')->unsigned()->index();
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('industry_id')
                    ->references('id')
                    ->on('industries')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attributes');
    }
}
