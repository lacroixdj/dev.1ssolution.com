<?php

use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// Users: 

$factory->define(App\User::class, function (Faker\Generator $faker) {
    
    return [

        'uuid' => $faker->uuid,
        
        'name' => $faker->firstName,
        
        'lastname' => $faker->lastname,
        
        'phone' => $faker->phoneNumber,
        
        //'email' => $faker->safeEmail,

        'email' => $faker->email,
        
        'password' => bcrypt('qwerty'),
        
        'remember_token' => str_random(10),
        
        'verification_code' => str_random(30),
        
        'verified' => 1,
        
        'haslogged' => 0
    
    ];
});


// Vendors:

$factory->define(App\Vendor::class, function (Faker\Generator $faker) {
    
    $industries = \DB::table('industries')->get();
    
    $metroareas = \DB::table('metroareas')->get();
    
    $rates = [0,1,2,3,4,5];
    
    return [
    
        'uuid' => $faker->uuid,
    
        'industry_id' => $faker->randomElement($industries)->id,
    
        'metroarea_id' => $faker->randomElement($metroareas)->id,
    
        'name' => $faker->company.' '.$faker->companySuffix,
    
        'phone' => $faker->phoneNumber,
    
        'email' => $faker->companyEmail,
    
        'url' => 'www.'.$faker->domainName,
    
        'emergency' => $faker->boolean,
        
        'rate' => $faker->randomElement($rates),
    
    ];
});


// Vendors Addresses:

$factory->define(App\VendorAddresses::class, function (Faker\Generator $faker) {
    	
    // Default country_id is 231 (US)

	$states = DB::table('states')

				->where('country_id', 231)
    
            	->whereExists(function ($query) {
    
                	$query->select(DB::raw(1))
    
                      ->from('cities')
    
                      ->whereRaw('cities.state_id = states.id');
    
            	})->get();

	$state_id = $faker->randomElement($states)->id;

    $cities = \DB::table('cities')
    
        ->where('state_id', $state_id)
    
        ->get();

    $city_id = $faker->randomElement($cities)->id;
    
    return [
        
        'uuid' => $faker->uuid,
        
        'city_id' => $city_id,
        
        //'address' => $faker->address,

        'address' => $faker->streetAddress,
        
        'address2' => $faker->secondaryAddress,
        
        'phone' => $faker->phoneNumber,
        
        'zip' => $faker->postcode,
        
        'main_address' => 1,
    
    ];
});


// Client:

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    
    $corporations = \DB::table('corporations')->get();
    
    $metroareas = \DB::table('metroareas')->get();
    
    return [
    
        'uuid' => $faker->uuid,
    
        'corporation_id' => $faker->randomElement($corporations)->id,
        
        'metroarea_id' => $faker->randomElement($metroareas)->id,
    
        'name' => $faker->company.' '.$faker->companySuffix,
    
        'phone' => $faker->phoneNumber,
    
        'email' => $faker->companyEmail,
    
        'url' => 'www.'.$faker->domainName

    ];
});


// Clients Addresses:

$factory->define(App\ClientAddress::class, function (Faker\Generator $faker) {
    	
    // Default country_id is 231 (US)

	$states = DB::table('states')

				->where('country_id', 231)
    	
            	->whereExists(function ($query) {
    
                	$query->select(DB::raw(1))
    
                      ->from('cities')
    
                      ->whereRaw('cities.state_id = states.id');
    
            	})->get();

	$state_id = $faker->randomElement($states)->id;

    $cities = \DB::table('cities')
    
        ->where('state_id', $state_id)
    
        ->get();

    $city_id = $faker->randomElement($cities)->id;
    
    return [
        
        'uuid' => $faker->uuid,
        
        'city_id' => $city_id,
        
        'address' => $faker->address,
        
        'address2' => $faker->secondaryAddress,
        
        'phone' => $faker->phoneNumber,
                
        'zip' => $faker->postcode,
        
        'main_address' => 1,    
    ];
});



$factory->define(App\Rfp::class, function (Faker\Generator $faker) {
    
    $industries = \DB::table('industries')->get();
    
    $statuses = ['open', 'sent', 'bid', 'workorder', 'closed'];

    $types = ['RFP', 'SR'];

    $fakeDate = $faker->dateTimeThisMonth('now');

    $response_date = Carbon::instance($fakeDate);

    $service_date =  Carbon::instance($fakeDate)->addWeek();

    return [
    
        //id generated by db

        //userid generated by relationship / seeder

        //client_id generated by relationship
   
        'industry_id' => $faker->randomElement($industries)->id,
    
        'status' => $faker->randomElement($statuses),
        
        'type' => $faker->randomElement($types),
    
        //'number' generated by mutator model
    
        'description' => $faker->realText(50),

        //'location' generated from the client location
    
        'emergency' => $faker->boolean, 

        'visit' => $faker->boolean,

        'details' => $faker->realText(200),

        'documents' => $faker->realText(50),

        'response_date' => $response_date,       

        'service_date' => $service_date,
    
    ];

});



//Workorder factory 

$factory->define(App\Workorder::class, function (Faker\Generator $faker) {
    
    //$industries = \DB::table('industries')->get();
    
    $statuses = ['open', 'closed'];

    //$metroareas = \DB::table('metroareas')->get();

    return [
    
        //id generated by db

        //user_id generated by relationship / seeder

        //client_id taken from the rfp

        //vendor_id taken from the pivot rfp_vendor

        //industry_id taken from the rfp

        //rfp_id taken from the rfp

        //emergency taken from the rfp, 

        //description taken from the rfp,

        //service_date taken from the rfp

        //type taken from the rfp

        //visit taken from the rfp

        //details taken from the rfp
        
        //documents taken from the rfp

        'status' => $faker->randomElement($statuses),

        //'question_1' => its filled from seeder,

        //'question_2' => its filled from seeder,

        //'vendor_rate' => its filled from seeder,

        //'close_details' => its filled from seeder,
        
        //'close_id' => its filled from seeder,

        //'closed_date' => its filled from seeder,

    ];

});


// Invites: 

$factory->define(App\Invite::class, function (Faker\Generator $faker) {
    
    $statuses = ['sent', 'joined', 'invitation-removed'];
    
    $fakeDate = $faker->dateTimeThisMonth('now');
    
    $created_date = Carbon::instance($fakeDate);
    
    return [
        
        'name' => $faker->firstName.' '.$faker->lastname,
                
        'phone' => $faker->phoneNumber,

        'email' => $faker->email,
        
        'type' => 'client-to-vendor',
        
        'status' => $faker->randomElement($statuses),
        
        'verification_code' => str_random(30),
        
        'created_at' => $created_date, 
        
        'updated_at' => $created_date,
    
    ];
});

