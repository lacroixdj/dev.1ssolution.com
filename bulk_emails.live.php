<?php

/**
	 * BULK EMAIL ACCOUNT CREATION FOR TENSTING PURPOSES.
	 * Author: Jesus Farias Lacroix <jesus.farias@gmail.com>
	 * Date: Oct 11, 2016.
*/

//Require the LivePHP CPANEL API
require_once "/usr/local/cpanel/php/cpanel.php";


//This function create 100 vendor emails account
//vendor0@1ssolution.com, 
//vendor1@1ssolution.com,
//vendor2@1ssolution.com,
//...
//vendor99@1ssolution.com
function create_vendors_emails($cpanel){

	$vendors_emails = [];

	for($i=0; $i<100; $i++){

		$user = $pass = 'vendor'.$i;

		$new_email = $cpanel->uapi(
	    	
	    	'Email', 'add_pop',
	    
	    	array(
	        	
	        	'email'           => $user,
		        
		        'password'        => $pass,
		        
		        'quota'           => '500',
		        
		        'domain'          => '1ssolution.com',
		        
		        'skip_update_db'  => '0',
	        )
		);

		$vendors_emails [] = $new_email;

		$new_email = false;

	}

	echo "<pre>";
	print_r($vendors_emails);
	echo "</pre>";

}



//This function create 100 client emails account
//client0@1ssolution.com, 
//client1@1ssolution.com,
//client2@1ssolution.com,
//...
//client99@1ssolution.com
function create_client_emails($cpanel){

	$clients_emails = [];

	for($i=0; $i<100; $i++){

		$user = $pass = 'client'.$i;

		$new_email = $cpanel->uapi(
	    	
	    	'Email', 'add_pop',
	    
	    	array(
	        	
	        	'email'           => $user,
		        
		        'password'        => $pass,
		        
		        'quota'           => '500',
		        
		        'domain'          => '1ssolution.com',
		        
		        'skip_update_db'  => '0',
	        )
		);

		$clients_emails [] = $new_email;

		$new_email = false;

	}

	echo "<pre>";
	print_r($clients_emails);
	echo "</pre>";

}

function debug($cpanel){

	$get_userdata = $cpanel->uapi(
	  
	    'DomainInfo', 'domains_data',
	  
	    array(
	  
	        'format'    => 'hash',
	    )
	);

	echo "<pre>";
	print_r($get_userdata);
	echo "</pre>";
}


// Connect to cPanel - only do this once.
// CPANEL Object instance;
$cpanel = new CPANEL();

//debug($cpanel);
create_vendors_emails($cpanel);
create_client_emails($cpanel);


?>