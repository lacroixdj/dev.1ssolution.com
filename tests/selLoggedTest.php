<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;

class selLoggedTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'jlopez_guillot@hotmail.com';
		// $pass 	= 'qwerty1';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}
    



	/**
     * Client Log In With Bad Email.
     	(Cliente con email invalido hace Sig In)
		Comprobamos que estamos en pagina login texto y url
		comprobamoos que exista el formulario
		llenamos el formulario con email errado
		enviamos el formulario
		verificamso el mensaje de error con email invalido
		verificamos que seguimos en la pagina de login texto y url
     *	selLoggedTest::testClientLogInWithBadEmail
     * @return void
     */
    public function testClientLogInWithBadEmail()
    {
		$this->makeLogin('email@mail.com', 'qwerty1');
		// Verificamos que el mensaje de validacion aparezca
		$this->assertEquals('These credentials do not match our records.', $this->byCssSelector('.help-block')->text());
		// Refrescamos la pagina para hacer nueva prueba
		$this->refresh();
		$this->timeouts()->implicitWait(2000);
		// Verificamos que el title sea apropiado
        $this->assertEquals('One Seamless Solution', $this->title());
        // Verificamos que el mensaje indique que haga login en la pagina
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
    }
    

	

	/**
     * Client Log In With Bad Password.
     	(Cliente con password invalido hage login)
		Comprobamos que estamos en pagina login texto y url
		comprobamoos que exista el formulario
		llenamos el formulario con password errado
		enviamos el formulario
		verificamso el mensaje de error con email invalido
		verificamos que seguimos en la pagina de login texto y url
     * @return void
     */
    public function testClientLogInWithBadPassword()
    {
		$this->makeLogin('client0@1ssolution.com', '123456789');
		// Verificamos que el mensaje de validacion aparezca
		$this->assertEquals('These credentials do not match our records.', $this->byCssSelector('.help-block')->text());
		// Refrescamos la pagina para hacer nueva prueba
		$this->refresh();
		$this->timeouts()->implicitWait(2000);
		// Verificamos que el title sea apropiado
        $this->assertEquals('One Seamless Solution', $this->title());
        // Verificamos que el mensaje indique que haga login en la pagina
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
    }	

	

	/**
     * A basic test example.
     	(Cliente con datos correcto hace login)
		Comprobamos que estamos en pagina login texto y url
		comprobamoos que exista el formulario
		llenamos el formulario con datos correctos
		enviamos el formulario
		verificamos que llegamos a la url esperada
		verificamos que estamos en la pagina indicada por textos dentro de la pagina
		Hacemos clicl en logout
		verificamos que llegamos a la pagina de login texto y url
     *
     * @return void
     */
    public function testClientLogInWithGoodCredentials()
    {
    	$this->makeLogin('client0@1ssolution.com', 'client0');
		$this->assertEquals($this->baseUrl.'/client/dashboard', $this->url());
		$this->assertEquals('Dashboard', $this->byCssSelector("h1")->text());
		$this->assertEquals('Requests for Proposals', $this->byCssSelector("h3.box-title")->text());
// $this->byXPath("//section[@class='content']//h3")->text(); //Service Requests
// $this->byXPath("//section[@class='content']//h3")->text(); //Last messages
		$this->makeLogout();		
		$this->timeouts()->implicitWait(3000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());		
    }
    	
	

	/**
     * Vendor Log In With Bad Email.
     	(Vendor con email invalido hace Sig In)
		Comprobamos que estamos en pagina login texto y url
		comprobamoos que exista el formulario
		llenamos el formulario con email errado
		enviamos el formulario
		verificamso el mensaje de error con email invalido
		verificamos que seguimos en la pagina de login texto y url
     *
     * @return void
     */
    public function testVendorLogInWithBadEmail()
    {
        $this->makeLogin('email@mail.com', 'vendor0');
		// Verificamos que el mensaje de validacion aparezca
		$this->assertEquals('These credentials do not match our records.', $this->byCssSelector('.help-block')->text());
		// Refrescamos la pagina para hacer nueva prueba
		$this->refresh();
		$this->timeouts()->implicitWait(2000);
		// Verificamos que el title sea apropiado
        $this->assertEquals('One Seamless Solution', $this->title());
        // Verificamos que el mensaje indique que haga login en la pagina
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
    }
    


	/**
     * Vendor Log In With Bad Password.
     	(Vendor con password invalido hage login)
		Comprobamos que estamos en pagina login texto y url
		comprobamoos que exista el formulario
		llenamos el formulario con password errado
		enviamos el formulario
		verificamso el mensaje de error con email invalido
		verificamos que seguimos en la pagina de login texto y url
     *
     * @return void
     */
    public function testVendorLogInWithBadPassword()
    {
		$this->makeLogin('vendor0@1ssolution.com', '123456');
		// Verificamos que el mensaje de validacion aparezca
		$this->assertEquals('These credentials do not match our records.', $this->byCssSelector('.help-block')->text());
		// Refrescamos la pagina para hacer nueva prueba
		$this->refresh();
		$this->timeouts()->implicitWait(2000);
		// Verificamos que el title sea apropiado
        $this->assertEquals('One Seamless Solution', $this->title());
        // Verificamos que el mensaje indique que haga login en la pagina
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
    }	
	


	/**
     * Vendor Log In With Good Credentials.
     	(Vendor con datos correcto hace login)
		Comprobamos que estamos en pagina login texto y url
		comprobamoos que exista el formulario
		llenamos el formulario con datos correctos
		enviamos el formulario
		verificamos que llegamos a la url esperada
		verificamos que estamos en la pagina indicada pr textos dentro de la pagina
		Hacemos clicl en logout
		verificamos que llegamos a la pagina de login texto y url
     *
     * @return void
     */
    public function testVendorLogInWithGoodCredentials()
    {
    	$this->makeLogin('vendor0@1ssolution.com', 'vendor0');    	
		$this->assertEquals($this->baseUrl.'/vendor/dashboard', $this->url());		
        $this->assertEquals('Vendor Dashboard | One Seamless Solution', $this->title());
		$this->assertEquals('Dashboard', $this->byCssSelector("h1")->text());
		$this->assertEquals('Requests for Proposals', $this->byCssSelector("h3.box-title")->text());
		$this->makeLogout();		
		$this->timeouts()->implicitWait(3000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());
    }	
}
