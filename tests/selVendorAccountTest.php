<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;

class selVendorAccountTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'jlopez_guillot@hotmail.com';
		// $pass 	= 'qwerty1';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}


	protected function formatPhone($phone){
		$phoneExplode = explode('-', $phone);
		$n = count($phoneExplode);
		if($n > 3){
			return '('.$phoneExplode[0].') '.$phoneExplode[1].'-'.$phoneExplode[2].' '.$phoneExplode[3];
		} else {
			return '('.$phoneExplode[0].') '.$phoneExplode[1].'-'.$phoneExplode[2];
		}
    }

    

    
	/**
     * Vendor in Account modify Vendor Account Company Info.
     (Verificamo Account -> Vendor account -> Company Info.)
		-Vendor con datos correcto hace login
		comprobamos la url, title y textos en esta url (http://dev.1ssolution.com/account/vendor)
		comprobamos el title
		comprobamos campos para Company Info.
		comprobamos que el campo industry este llenos y sea redonly
		comprobamos que los campos para form Company Info. esten llenos		
		limpiamos el formulario
		le damos al boton cancel y verificamos que se vuelval a cargar todos los campos	
		limpiamos el formulario
		llenamos el formulario con nueva informacion
		enviamos el formulario
		verificamos el modal window tituo y mensaje
		cerramos el modal window
		volvemos a verificar que la informacion que habiamos enviado este impresa
     *	selVendorAccountTest::testVendorInAccountModifyVendorAccountCompanyInfo
     * @return void
     */
    public function testVendorInAccountModifyVendorAccountCompanyInfo()
    {
        $faker = Faker::create();
        $this->makeLogin('vendor1@1ssolution.com', 'vendor1');
        $this->byLinkText("Account")->click();
		$this->byLinkText("Vendor account")->click();
		$this->assertEquals($this->baseUrl.'/account/vendor', $this->url());
        $this->assertEquals('Vendor User Info | One Seamless Solution', $this->title());
		$this->assertEquals('Account Company Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		$this->assertEquals('Company Info.', $this->byXPath("//div/div[1]/section[2]/div[1]/div/div/div/h3")->text());
		

		$this->assertEquals($this->baseUrl.'/account/vendor', $this->byCssSelector('form')->attribute('action'));
		$this->assertEquals('true', $this->byId('industry_id')->attribute('readonly'));
		//url and emergency are not required
		$inputFields = array('_token','name','phone','email');
		$selectFields = array('industry_id' => 'required','services[]' => 'required','attributess[]' => 'not required','metroarea_id' => 'required');
		foreach ($inputFields as $f) {
			$v = $this->byName($f)->value();
			$this->assertTrue(!empty($v));
		}
		foreach ($selectFields as $f => $m) {
			$v = $this->byXPath("//select[@name='$f']/option")->text();
			if($m == "required"){ $this->assertTrue(!empty($v)); }
		}

$this->byId("name")->clear();
$this->byId("name")->value($faker->company.' '.$faker->companySuffix);

$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[3]/div/button")->click();
$this->byXPath("//form[@id='vendor_company_edit']//button[.='Deselect All']")->click();

$this->byXPath("//select[@id='services']/option[@value='104']")->click();
$this->byXPath("//select[@id='services']/option[@value='107']")->click();
$this->byXPath("//select[@id='services']/option[@value='93']")->click();
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[3]/div/button")->click();
// $this->byXPath("//form[@id='vendor_company_edit']//button[.='Deselect All']")->click();
// $this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[3]/div/button")->click();
// $this->byXPath("//form[@id='vendor_company_edit']//button[.='Select All']")->click();
// $this->byXPath("//select[@id='services']/option[@value='5']")->click();
// $this->byXPath("//select[@id='services']/option[@value='4']")->click();
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[3]/div/button")->click();


$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[4]/div/button")->click();
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[4]/div/div/div[2]/div/button[2]")->click(); //deselect
$this->byXPath("//select[@id='attributes']/option[@value='13']")->click();
$this->byXPath("//select[@id='attributes']/option[@value='14']")->click();
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[4]/div/button")->click();
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[4]/div/div/div[2]/div/button[2]")->click(); //deselect
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[4]/div/div/div[2]/div/button[1]")->click();
$this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[1]/div[4]/div/button")->click();

$this->byXPath("//select[@id='metroarea_id']//option[7]")->click();

$this->byId("phone")->clear();
$this->byId("phone")->value("(646) 874-5478");

$this->byId("email")->clear();
$this->byId("email")->value($faker->companyEmail);

$this->byId("url")->clear();
$this->byId("url")->value('www.'.$faker->domainName);

$this->byCssSelector("ins.iCheck-helper")->click();
$this->byCssSelector("ins.iCheck-helper")->click();


		$this->assertEquals('Cancel', $this->byXPath("//div/div[1]/section[2]/div[1]/div/div/form/div[1]/div[2]/div[5]/button")->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byXPath("//div/div[1]/section[2]/div[1]/div/div/form/div[1]/div[2]/div[5]/button")->attribute('onclick')
			);
		$this->assertEquals('Update Company', $this->byXPath("//form[@id='vendor_company_edit']/div[1]/div[2]/div[5]/input")->value());
// 		// $this->keys(Keys::ENTER);
		$this->byId('vendor_company_edit')->submit();

		sleep(10);
		$mTitle 	= $this->byId("modal_success_title")->text();
		$mMessage= $this->byId('modal_success_message')->text();
		$mText 	= $this->byId('modal_success_text')->text();
		$this->assertEquals('Company information', $mTitle);
		$this->assertEquals('Success! You have updated your company information', $mMessage);
		$this->assertEquals('Background verification process would be applied', $mText);
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();
		// sleep(1);
		// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());

		// foreach ($inputFields as $f => $v) {
		// 	$this->assertEquals($v, $this->byName($f)->value());
		// }

		// foreach ($selectFields as $f => $v) {
		// 	$this->assertEquals($v, $this->byXPath("//select[@name='$f']/option[@value='$v']")->value());
		// }
	
		$this->makeLogout();
    }

	
	
	/**
     * Vendor In Account Modify Vendor Account Company Location.
     (Verificamo Account -> Vendor account -> Company Location.)
		comprobamos que los campos para form Company Location. esten llenos		
		limpiamos el formulario
		le damos al boton cancel y verificamos que se vuelva a cargar todos los campos	
		limpiamos el formulario
		llenamos el formulario con nueva informacion
		enviamos el formulario
		verificamos el modal window titulo y mensaje
		cerramos el modal window
		volvemos a verificar que la informacion que habiamos enviado este impresa
     *	selVendorAccountTest::testVendorInAccountModifyVendorAccountCompanyLocation
     * @return void
     */
    public function testVendorInAccountModifyVendorAccountCompanyLocation()
    {
        $faker = Faker::create();
        $this->makeLogin('vendor1@1ssolution.com', 'vendor1');
        $this->byLinkText("Account")->click();
		$this->byLinkText("Vendor account")->click();
		$this->assertEquals($this->baseUrl.'/account/vendor', $this->url());
        $this->assertEquals('Vendor User Info | One Seamless Solution', $this->title());
		$this->assertEquals('Account Company Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		$this->assertEquals('Company Location.', $this->byXPath("//div/div[1]/section[2]/div[2]/div/div/div/h3")->text());
		http://dev.1ssolution.com/account/vendor
		$this->assertEquals($this->baseUrl.'/vendor/location', $this->byCssSelector('form#vendor_company_location_edit')->attribute('action'));		
		$inputFields = array('_token','address','address2','phone','zip');
		$selectFields = array('country','state','city');
		// $country = $this->byXPath("//select[@name='country']/option")->text();
		foreach ($inputFields as $f) {
			$v = $this->byName($f)->value();
			$this->assertTrue(!empty($v));
		}
		foreach ($selectFields as $f) {
			$v = $this->byXPath("//select[@name='$f']/option")->text();
			$this->assertTrue(!empty($v));
		}

		$address 	= $faker->streetAddress;
		$address2 	= $faker->secondaryAddress;
		$phone 		= '(151) 646-8741';
		$zip 		= '13223-954';

		$inputFields = array(
					'address' 	=> $address,
					'address2' 	=> $address2,
					'phone' 	=> $phone,
					'zip' 		=> $zip
					);
		foreach ($inputFields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}
// 3948	Missouri
// 45426	Camdenton
// 3930 Florida
// 43618 Cape Coral
		$selectFields = array(
					'state' => '3930',
					'city' 	=> '43618'
					);
		foreach ($selectFields as $f => $v) {
			// $this->byName($f)->clear();
			$this->byXPath("//select[@name='$f']/option[@value='$v']")->click();
		}

		$this->assertEquals('Cancel', $this->byXPath("//div/div[1]/section[2]/div[2]/div/div/form/div[1]/div[2]/div[4]/button")->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byXPath("//div/div[1]/section[2]/div[2]/div/div/form/div[1]/div[2]/div[4]/button")->attribute('onclick')
			);
		$this->assertEquals('Update Location', $this->byXPath("//form[@id='vendor_company_location_edit']/div[1]/div[2]/div[4]/input")->value());
		// $this->keys(Keys::ENTER);
		$this->byId("vendor_company_location_edit")->submit();

		sleep(5);
		$title 	= $this->byId("modal_success_title")->text();
		$message= $this->byId('modal_success_message')->text();
		$this->assertEquals('Company Location', $title);
		$this->assertEquals('Success! You have updated your company location', $message);		
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();
		// sleep(1);
		// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());

		foreach ($inputFields as $f => $v) {
			// if($this->byName($f)->text() == 'phone'){
			// 	$this->assertEquals(formatPhone($this->byName($f)->text()), $this->byName($f)->value());
			// } else {
				$this->assertEquals($v, $this->byName($f)->value());
			// }
		}

		foreach ($selectFields as $f => $v) {
			$this->assertEquals($v, $this->byXPath("//select[@name='$f']/option[@value='$v']")->value());
		}
	
		$this->makeLogout();
    }

		
		
	
	/**
     * Vendor User Account Modify User.
     (Verificamos Account -> User account -> User)
		hacemos clic en User account
		verificamo que llegamos a la pagina deseada url, title y textos
		Verificamos que el formulario para User contenga los campos apropiados
		verificamso que el campo email sea redonly
		limpiaos todos los campos 
		hacemos click en boton Cancel
		verificamo que llegamos a la pagina deseada url, title y textos
		Verificamos que el formulario para User contenga los campos apropiados
		verificamso que el campo email sea redonly
		limpiaos todos los campos 
		enviamos el formulario
		verificamos mensajes de validacion
		llenamos todos los camos con datos nuevos
		enviamos el formulario
		verificamos modal window por titulo y mensaje
		cerramos modal haciendo click en ok
		verificamos que los datos que habiamos enviados este presente en los campos del formulario
     *	selVendorAccountTest::testVendorUserAccountModifyUser
     * @return void
     */
    public function testVendorUserAccountModifyUser()
    {
        $faker = Faker::create();
        $this->makeLogin('vendor1@1ssolution.com', 'vendor1');
        $this->byLinkText("Account")->click();
		$this->byLinkText("User account")->click();
		$this->assertEquals($this->baseUrl.'/account/user', $this->url());
        $this->assertEquals('Vendor User Info | One Seamless Solution', $this->title());
		$this->assertEquals('User Account User Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		
		$this->assertEquals($this->baseUrl.'/account/user', $this->byId('user_edit')->attribute('action'));
		$this->assertEquals('true', $this->byName('email')->attribute('readonly'));
		$fields = array('_token','email','phone','name','lastname');
		foreach ($fields as $f) {
			$v = $this->byName($f)->value();
			$this->assertTrue(!empty($v));
		}
		$phone 		= '(457) 646-9874';
		$name 		= $faker->firstName;
		$lastname 	= $faker->lastname;
		$fields = array(
			'phone' 	=> $phone,
			'name' 		=> $name,
			'lastname' 	=> $lastname,
			);
		foreach ($fields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}

		$this->assertEquals('Cancel', $this->byXPath("//form[@id='user_edit']//button")->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byXPath("//form[@id='user_edit']//button")->attribute('onclick')
			);
		$this->assertEquals('Update User', $this->byCssSelector('.btn.btn-success.btn-flat')->value());
		// $this->keys(Keys::ENTER);
		$this->byId("user_edit")->submit();
		// $this->byXPath("//form[@id='user_edit']/div[1]/div[2]/div[3]/input")->click();
		// $this->timeouts()->implicitWait(5000);

		sleep(10);
		$title 	= $this->byId("modal_success_title")->text();
		$message= $this->byId('modal_success_message')->text();
		$this->assertEquals('User information', $title);
		$this->assertEquals('Success! You have updated your user information', $message);		
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();
		// sleep(1);
		// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());

		foreach ($fields as $f => $v) {
			if($this->byName($f)->text() == 'phone'){				
				$this->assertEquals(formatPhone($this->byName($f)->text()), $this->byName($f)->value());
			} else {
				$this->assertEquals($v, $this->byName($f)->value());
			}
		}
	
		$this->makeLogout();
	}

		
	
	/**
     * Vendor User Account Change Password.
     (Verificamos Account -> User account -> Change Password)
		hacemos clic en User account
		verificamo que llegamos a la pagina deseada url, title y textos
		Verificamos que el formulario para Change Password contenga los campos apropiados
		enviamos el formulario
		verificamos mensajes de validacion
		llenamos todos los campos con datos nuevos
		enviamos el formulario
		verificamos modal window por titulo y mensaje
		cerramos modal haciendo click en ok
		verificamos que los datos que habiamos enviados esten presente en los campos del formulario
     *	selVendorAccountTest::testVendorUserAccountChangePassword
     * @return void
     */
    public function testVendorUserAccountChangePassword()
    {
        $this->makeLogin('vendor1@1ssolution.com', 'vendor1');

        $this->byLinkText("Account")->click();
		$this->byLinkText("User account")->click();
		$this->assertEquals($this->baseUrl.'/account/user', $this->url());
        $this->assertEquals('Vendor User Info | One Seamless Solution', $this->title());
		$this->assertEquals('User Account User Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		$this->assertEquals('Change password', $this->byXPath("//div/div[1]/section[2]/div[2]/div/div/div/h3")->text());
		$this->assertEquals($this->baseUrl.'/user/passwd', $this->byId('user_passwd_edit')->attribute('action'));

		$password 			= '123456';
		$current_password 	= '123456';
		$fields = array(
			'password' 	=> $password,
			'password_confirmation' => $password,
			'current_password' 	=> $current_password
			);
		foreach ($fields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}

		$this->assertEquals('Cancel', $this->byXPath("//div[@id='pwd-container']//button")->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byXPath("//div[@id='pwd-container']//button")->attribute('onclick')
			);
		$this->assertEquals('Update Password', $this->byId('update_change_password')->value());
		// $this->keys(Keys::ENTER);

		$this->byId("update_change_password")->submit();
		// $this->byXPath("//form[@id='user_edit']/div[1]/div[2]/div[3]/input")->click();
		// $this->timeouts()->implicitWait(5000);
		sleep(10);
		$title 	= $this->byId("modal_error_title")->text();
		$message= $this->byId('modal_error_message')->text();
		$this->assertEquals('Error', $title);
		$this->assertEquals('The current password does not match', $message);		
		$this->byXPath("//div[@id='modal_error']//button[.='Ok ']")->click();

		$this->byName("password")->clear();
		$textVerdict = $this->byCssSelector("span.password-verdict")->text();
		$this->assertTrue(empty($textVerdict), $this->byCssSelector("span.password-verdict")->text());
		$this->byName("password")->value("!QwertY_147852@"); 
		$this->assertEquals('Very Strong', $this->byCssSelector("span.password-verdict")->text());

		$password 			= 'vendor1';
		$current_password 	= 'vendor1';
		$fields = array(
			'password' 	=> $password,
			'password_confirmation' => $password,
			'current_password' 	=> $current_password
			);
		foreach ($fields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}
		$this->byId("update_change_password")->submit();

		sleep(5);
		$title 	= $this->byId("modal_success_title")->text();
		$message= $this->byId('modal_success_message')->text();
		$this->assertEquals('User password', $title);
		$this->assertEquals('Success! You have updated your password', $message);		
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();
		// sleep(1);
		// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());

		$this->makeLogout();
    }	
}
