<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Queue;
use Faker\Factory as Faker;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;


class selRegisterTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'jlopez_guillot@hotmail.com';
		// $pass 	= 'qwerty1';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals($this->baseUrl.'/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}
	


    protected function consultData(){
    	$client = App\User::where([
                                    ['email', '=', 'vendor1@1ssolution.com'],
                                    ['passwordplain', '=','vendor1'],
                                ])->first();
    	$this->seeInDatabase('users', [
            'id'            => $client['attributes']['id'],
            'uuid'          => $client['attributes']['uuid'],
            'name'          => $client['attributes']['name'],
            'lastname'      => $client['attributes']['lastname'],
            'phone'         => $client['attributes']['phone'],
            'sms_address'   => $client['attributes']['sms_address'],
            'mms_address'   => $client['attributes']['mms_address'],
            'password'      => $client['attributes']['password'],
            'passwordplain' => $client['attributes']['passwordplain'],
            'remember_token'=> $client['attributes']['remember_token'],
            'verification_code' => $client['attributes']['verification_code'],
            'verified'      => $client['attributes']['verified'],
            'haslogged'     => $client['attributes']['haslogged']
        ]);
    	return print_r($client);
    }


    /**
     * Home Page.
     	comprobar que nos redirija a la direccion correcta
		comprobar el title de la pagina
		comprobar texto de entrada a la pagina
		comprobar la existencia del formulario de entrada
		comprobar existencia de enlace de registro
		comprobar existencia de enlace a olvido contrasenia
     * 	selRegisterTest::
     * @return void
     */
	public function testHomePage(){
		$faker = Faker::create();
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);

 		$fields = array(
		 			'email' 	=> $faker->email,
		 			'password' 	=> $faker->word
		 			);
		foreach ($fields as $k => $v) {
			$this->byName($k)->value($v);
			$f = $this->byName($k)->value();
			$this->assertTrue(!empty($f));
		}
	}


	/**
     * Create New Vendor Accunt
		comprobar texto de pagina de registro de nueva cuenta
		comprobar que exista el enlace I already have a membership y su enlace
		******** comprobar la existencia del enlace terms (new window)
		comprobar los campos del formulario para Vendor
		llenar los campos del formulario
		comprobar que el correo no este en uso
		comprobar que el check este marcado para reconoer los terms
		enviar el formulario
		esperar por mensaje
		verificar el mensaje (titulo y mensaje)
		verificar el redirect a pagina principal de login
		selRegisterTest::testCreateNewVendorAccount
     * 
     * @return void
     */
	public function testCreateNewVendorAccount(){
		$faker = Faker::create();
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
        $this->byLinkText("Register New Account")->click();
        sleep(7);
        $this->assertEquals('Register new account', $this->byCssSelector(".login-box-msg")->text());
        $this->assertEquals('active', $this->byCssSelector("li#tab1")->attribute('class'));
        $this->assertEquals('Vendor Account', $this->byXPath("//li[@id='tab1']//a")->text());
		$this->byName('name')->value($faker->name);
        $this->byName('email')->value("vendor0@1ssolution.com");  
        $this->byCssSelector("ins.iCheck-helper")->click(); 
        $this->byXPath("//div[@id='tab_1']//button[.='Register Vendor']")->click();  
		sleep(7);
		$this->assertEquals('The email has already been taken.', $this->byXPath("//div/div[2]/div/div/div[1]/form/div[2]/span[2]/strong")->text());		
        $this->byName('name')->clear();
        $this->byName('email')->clear();  
        $this->byName('name')->value($faker->name);
        $this->byName('email')->value($faker->email);        
        $this->byCssSelector("ins.iCheck-helper")->click(); 
        $this->byXPath("//div[@id='tab_1']//button[.='Register Vendor']")->click();      
        // $this->byLinkText('Register Vendor');
        sleep(7);
        $this->assertEquals('Account Registration', $this->byCssSelector("h4.modal-title")->text()); 
		$this->assertEquals(' Your account was registered successfully!', $this->byCssSelector("h3.text-left")->text());
		$this->assertEquals('We have sent you an email with activation instructions, please proceed to activate your account.', $this->byCssSelector("h4.text-justify")->text()); 		 
	    $this->byXPath("//div[@class='modal-content']//button[.='Ok']")->click();
	    sleep(7);
		$this->assertEquals('http://dev.1ssolution.com/login', $this->url());
	}


	/**
     * Activate New Vendor Account.
		(Completar proceso de activacion de cuenta como Vendor)
		???????????????????????????????????????
		hay un token que no tenemos como emular.
     * 	selRegisterTest::testActivateNewVendorAccount
     * @return void
     */
	public function testActivateNewVendorAccount(){
		$this->assertTrue(true);
	}



	/**
     * Create New Client Account
     	(Probar Registrar Nueva cuenta como Client)
		comprobar texto de pagina de registro de nueva cuenta
		hacer click en pestania Client
		comprobar que exista el enlace I already have a membership y su enlace
		******** comprobar la existencia del enlace terms (new window) 
		comprobar los campos del formulario para Client
		llenar los campos del formulario
		comprobar que el correo no este en uso
		comprobar que el pass code no sea invalido
		comprobar que el check este marcado para reconoer los terms
		enviar el formulario
		esperar por mensaje
		verificar el mensaje (titulo y mensaje)
		verificar el redirect a pagina principal de login
     * 	selRegisterTest::testCreateNewClientAccount
     * @return void
     */
	public function testCreateNewClientAccount(){

		$faker = Faker::create();
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
        $this->byLinkText("Register New Account")->click();
        sleep(10);
        $this->assertEquals('Register new account', $this->byCssSelector(".login-box-msg")->text());
        $this->byLinkText("Client Account")->click();
        $this->assertEquals('active', $this->byCssSelector("li#tab2")->attribute('class'));
        $this->assertEquals('Client Account', $this->byXPath("//li[@id='tab2']//a")->text());
        $this->byName('client-name')->value($faker->name);
        $this->byName('client-email')->value("jlopez_guillot@hotmail.com");  
        $this->byName('passcode')->value('12345678');    
        $this->byXPath("//form[@id='client']/div[4]/div[1]/div/label/div/ins")->click();
        $this->byXPath("//div[@id='tab_2']//button[.='Register Client']")->click();    
		sleep(7);
		$this->assertEquals('The client-email has already been taken.', $this->byXPath("//form[@id='client']/div[2]/span[2]/strong")->text());
		$this->assertEquals('The selected passcode is invalid.', $this->byXPath("//form[@id='client']/div[3]/span[2]/strong")->text());
        $this->byName('client-name')->clear();
        $this->byName('client-email')->clear();  
        $this->byName('passcode')->clear();
        $this->byName('client-name')->value($faker->name);
        $this->byName('client-email')->value($faker->email);    
        $this->byName('passcode')->value('123456');     
        // $this->byCssSelector("ins.iCheck-helper")->click(); 
        $this->byXPath("//form[@id='client']/div[4]/div[1]/div/label/div/ins")->click();
        $this->byXPath("//div[@id='tab_2']//button[.='Register Client']")->click();      
        // $this->byLinkText('Register Vendor');
        sleep(7);
        $this->assertEquals('Account Registration', $this->byCssSelector("h4.modal-title")->text()); 
		$this->assertEquals(' Your account was registered successfully!', $this->byCssSelector("h3.text-left")->text());
		$this->assertEquals('We have sent you an email with activation instructions, please proceed to activate your account.', $this->byCssSelector("h4.text-justify")->text()); 		 
	    $this->byXPath("//div[@class='modal-content']//button[.='Ok']")->click();
	    sleep(7);
		$this->assertEquals('http://dev.1ssolution.com/login', $this->url());

	}



	/**
     * Activate New Client Account.
		(Completar proceso de activacion de cuenta como Client)
		???????????????????????????????????????
		hay un token que no tenemos como emular.
     * 	selRegisterTest::testActivateNewClientAccount
     * @return void
     */
	public function testActivateNewClientAccount(){
		$this->assertTrue(true);
	}



	/**
     * Password Recovery
     	(Probar he olvidado mi contrasenia)
		comprobar texto de pagina de registro de nueva cuenta
		hacer click en I forgot my password
		verificar que llegamos a la pantalla de reset con el texto y url
		verificamos que exista el vinculo back to login
		probamos el vinculo back to login
		hacer click en I forgot my password
		verificamos que el formulario tenga los campos requeridos
		llenamos los campos
		verificamos que el correo no exista en base de datos
		********* verificamos que el captcha este marcado 
		enviamos el formulario
		verificamos la pagina que la pagina que devuelve sea validad comprobando el texto que imprime (We have e-mailed your password reset link!)
		probamos el vinculo back to login
		comprobams que volvemos a pagina login por texto y url
     * 	selRegisterTest::testPasswordRecovery
     * @return void
     */
	public function testPasswordRecovery(){
		$faker = Faker::create();
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
        $this->byLinkText("I forgot my password")->click();
        sleep(10);
        $this->assertEquals('Enter your email or username below and we will send you a link to reset your password', $this->byCssSelector(".login-box-msg")->text());
        $this->byName('email')->value($faker->email);   
        $this->execute(array('script' => "alert('Hay un recaptcha de seguridad. Solo un humano puede activarlo. Activelo!! Tienes 30 segundos. De lo contrario la prueba fallara');", 'args' => array()));
        sleep(20); 
        // $this->byCssSelector('div.recaptcha-checkbox-checkmark')->click();
        // $this->byCssSelector('button[type="submit"]')->click();
        // Send Password Reset Link
		sleep(10);
		$this->assertEquals("We can't find a user with that e-mail address." , $this->byXPath("//span[@class='help-block']/strong"));
		$this->byName('email')->clear();
        $this->byName('email')->value('jlopez_guillot@hotmail.com');   
        $this->execute(array('script' => "alert('Hay un recaptcha de seguridad. Solo un humano puede activarlo. Activelo!! Tienes 30 segundos. De lo contrario la prueba fallara');", 'args' => array()));
        sleep(30); 
        // recaptcha debe activarse a mano
        // $this->byCssSelector('div.recaptcha-checkbox-checkmark')->click();
        $this->byCssSelector("button.btn")->click();
        sleep(10);
        
	}



	/**
     * Landing Page.
     	(Completar proceso de cambio de contrasenia)
		???????????????????????????????????????
		hay un token que no tenemos como emular.
     * 	selRegisterTest::testCompletePasswordRecovery
     * @return void
     */
	public function testCompletePasswordRecovery(){
		$this->assertTrue(true);
	}


	/**
     * Read Terms And Conditions.
     	Verificar que la pagina de terminos y condiciones existe
     	Verificar su title
     	Verificar titulo dentro de la pagina
     	verificar vinculo de salida
     * 	selRegisterTest::testReadTermsAndConditions
     * @return void
     */
	public function testReadTermsAndConditions(){
		$this->url($this->baseUrl.'/terms');
		$this->assertEquals('One Seamless Solution', $this->title()); //Corregir Terms and Conditions
        $this->assertEquals($this->baseUrl.'/terms', $this->url());
		$this->assertContains('Vendor Master Terms and Conditions', $this->byCssSelector("h3.login-box-msg")->text());
		$this->assertEquals('Back to register.', $this->byCssSelector("a.text-center.padding-l-r-30.padding-t-b-30")->text());
		$this->assertEquals('window.close()', $this->byCssSelector("a.text-center.padding-l-r-30.padding-t-b-30")->attribute('onclick'));
		$this->assertEquals($this->baseUrl.'/register', $this->byCssSelector("a.text-center.padding-l-r-30.padding-t-b-30")->attribute('href'));
		$this->byCssSelector("a.text-center.padding-l-r-30.padding-t-b-30")->click();
	}

}
