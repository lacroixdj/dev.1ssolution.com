<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;


class selVendorSettingTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'jlopez_guillot@hotmail.com';
		// $pass 	= 'qwerty1';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}


    
	/**
     *  Vendor Modify Messeges Setting.
     (Vendor entra a Setting -> Messeges)
		Verificamos que entramos a la url title y textos esperados
		hacemos uncheck en receive email messages
		verificamos modal window titulo y mensaje
		hacemos click en ok
		hacemos check en receive email messages
		verificamos modal window titulo y mensaje
		hacemos click en ok
		hacemos uncheck en receive sms messages
		verificamos modal window titulo y mensaje
		hacemos click en ok
		hacemos check en receive sms messages
		verificamos modal window titulo y mensaje
		hacemos click en ok
     *	selVendorSettingTest::testVendorModifyMessegesSetting
     * @return void
     */
    public function testVendorModifyMessegesSetting()
    {
        $this->makeLogin('vendor1@1ssolution.com', 'vendor1');	
		$this->byLinkText("Settings")->click();	
		sleep(5);
        $this->assertEquals($this->baseUrl.'/vendor/settings', $this->url());
        $this->assertEquals('Vendor User Settings | One Seamless Solution', $this->title());
		$this->assertEquals('Vendor User Settings', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		$this->assertEquals('Notification Control', $this->byCssSelector("h3.page-header")->text());
		$this->byName('emailMessages')->click();
		sleep(1);
		$this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());
		sleep(5);
		$this->byName('smsMessages')->click();
		sleep(1);
		$this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());
		$this->makeLogout();
    }


	
	/**
     *  Vendor Modify Appaerance Setting.
     (Vendor entra a Setting -> Appaerance)
		Verificamos que estamos en la pagina esperada textos, url, title
		Hacemos click en boton preview
		Verificamos que cambien los colores del new template
		hacemos cick en select
		verificamos modal window titulo y mensaje
		hacemos click en ok
     *	selVendorSettingTest::testVendorModifyAppaeranceSetting
     * @return void
     */
    public function testVendorModifyAppaeranceSetting()
    {
        $this->makeLogin('vendor1@1ssolution.com', 'vendor1');
		$this->byLinkText("Settings")->click();
		sleep(5);
        $this->assertEquals($this->baseUrl.'/vendor/settings', $this->url());
        $this->assertEquals('Vendor User Settings | One Seamless Solution', $this->title());
		$this->assertEquals('Vendor User Settings', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		$this->assertEquals('Notification Control', $this->byCssSelector("h3.page-header")->text());
		$this->byLinkText("Appearance")->click();
		$this->assertEquals('Custom Appearance', $this->byXPath("//div[@id='tab_2-1']//h3")->text());
		$cssOrigen = $this->byXPath('//head/link[5]')->attribute('href');
		// echo $cssOrigen;
		$this->byXPath("//form[@id='theme_upd']/div[2]/div/div[1]/div/div/button[1]")->click();
		$this->byXPath("//form[@id='theme_upd']/div[4]/div/div[1]/div/div/button[1]")->click();
		$this->byXPath("//form[@id='theme_upd']/div[4]/div/div[2]/div/div/button[1]")->click();
		$this->byXPath("//form[@id='theme_upd']/div[4]/div/div[2]/div/div/button[2]")->click();
		sleep(1);
		$this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());
		$cssNew = $this->byXPath('//head/link[5]')->attribute('href');
		// echo $cssNew;
		// $this->assertFalse($cssOrigen == $cssNew);
		$this->makeLogout();
    }
}
