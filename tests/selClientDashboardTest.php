<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;

class selClientDashboardTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'client0@1ssolution.com';
		// $pass 	= 'client0';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}




    /**
     * Client Dashboard.
     (Verificamos Dashboar de Cliente)
		Hacemos click en enlace Dashboard
		Verificamos url, title y textos en la pagina
		Verificamos que el dashboard sea para cliente.
		Verificamos enlaces de cada grafico
		Verificamos que cada grafico del dashboard valla a la url que
     * selClientDashboardTest::testClientDashboard
     * @return void
     */
    public function testClientDashboard()
    {
        $this->makeLogin('client0@1ssolution.com', 'client0');
        sleep(7);
		$this->assertEquals('Client Dashboard | One Seamless Solution', $this->title());
		$this->assertEquals('http://dev.1ssolution.com/client/dashboard', $this->url());
		$this->assertEquals('Dashboard', $this->byXPath("//div/div[1]/section[1]/h1")->text());

		$this->assertEquals('Requests for Proposals', $this->byXPath("//div/div[1]/section[2]/div[1]/div[1]/div/div[1]/h3")->text());
		$this->assertEquals('Service Requests', $this->byXPath("//div/div[1]/section[2]/div[1]/div[2]/div/div[1]/h3")->text());
		$this->assertEquals('  Inbox', $this->byXPath("//div/div[1]/section[2]/div[2]/div[1]/div/div[1]/h3")->text());
		$this->assertEquals(' Messages', $this->byXPath("//div/div[1]/section[2]/div[2]/div[2]/div/div[1]/h3")->text());


		$consultWiged = array(
							array(
								"url" 		=> "/rfp/client/bidsrequest/rfp/open", 
								"status" 	=> "open",
								),
							array(
								"url" 		=> "/rfp/client/bidsrequest/rfp/sent", 
								"status" 	=> "sent", 
								),
							array(
								"url" 		=> "/rfp/client/bidsrequest/rfp/bid", 
								"status" 	=> "bid", 
								),
							array(
								"url" 		=> "/workorder/client/view/rfp/open", 
								"status" 	=> "workorder", 
								),
							array(
								"url" 		=> "/workorder/client/view/rfp/closed", 
								"status" 	=> "closed", 
								),
							array(
								"url" 		=> "/rfp/client/bidsrequest/sr/open", 
								"status" 	=> "open",
								),
							array(
								"url" 		=> "/rfp/client/bidsrequest/sr/sent", 
								"status" 	=> "sent", 
								),
							array(
								"url" 		=> "/rfp/client/bidsrequest/sr/bid", 
								"status" 	=> "bid", 
								),
							array(
								"url" 		=> "/workorder/client/view/sr/open", 
								"status" 	=> "workorder", 
								),
							array(
								"url" 		=> "/workorder/client/view/sr/closed", 
								"status" 	=> "closed", 
								)
							);
		$count = count($consultWiged);
		for($i=0;$i<$count;$i++){
			// print_r($consultWiged[$i]); die();
			$url 		= $consultWiged[$i]["url"];
			$status 	= $consultWiged[$i]["status"];
			$urlArray 	= explode("/", $url);
			$type 		= $urlArray[4];
			$this->byCssSelector("div#$type-$status")->click();
			sleep(3);
			$this->assertEquals($this->baseUrl.$url, $this->url());
			if($status == "workorder" || $status == "closed" ){
				$status = ($status == "workorder")?"open":"closed";
				$tablaF1Col5 = $this->byXPath("//div/div[1]/section[2]/div/div/div/div[2]/div/table/tbody/tr[1]/td[5]")->text();
				// $this->assertEquals($status, strtolower($tablaF1Col5));
			} else {			
				$tablaF1Col2 = $this->byXPath("//div/div[1]/section[2]/div/div/div/div[3]/div[1]/table/tbody/tr[1]/td[2]")->text();
				$tablaF1Col7 = $this->byXPath("//div/div[1]/section[2]/div/div/div/div[3]/div[1]/table/tbody/tr[1]/td[7]")->text();
				
				// $this->assertEquals($type, strtolower($tablaF1Col2));
				// $this->assertEquals($status, strtolower($tablaF1Col7));
			}			
			$this->byLinkText("Dashboard")->click();	
			sleep(3);
		}

		$text1 = $this->byXPath("//table[@id='pendingClientResponses']/tbody/tr[1]/td[1]")->text();
		$text2 = $this->byXPath("//table[@id='pendingClientResponses']/tbody/tr[1]/td[2]")->text();

		$this->byXPath("//table[@id='pendingClientResponses']/tbody/tr[1]/td[1]/a")->click();
		sleep(3);
		$this->byXPath("//div[@id='modalVendor']//button[.='Close']")->click();

		$this->byXPath("//table[@id='pendingClientResponses']/tbody/tr[1]/td[2]/a")->click();
		sleep(10);
		$this->byXPath("//div[@id='modalRFP']//button[.='Close']")->click();

		$this->makeLogout();		
		$this->timeouts()->implicitWait(3000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());
    }
}
