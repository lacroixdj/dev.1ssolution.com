<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;


class selClientRequestBidsTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'client0@1ssolution.com';
		// $pass 	= 'client0';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}

    
	/**
     * Client Make New Request.
     (Client hace new request )
		Hacemos click en Request/Bids
		hacemos click en New Request
		verificamos que estamos en pagina new request/bids url, title, textos
		verificamos campos de formulario existan
		hacemos click en next
		verificamos validacion
		llenamos el formulaio con datos (Janitorian )
		hacemos click en ok
		verificamos modal con title y messege
		revisamos que llegamos a la pagina desada url, title, textos
		hacemos click en next
		Verificamos modal por titulo y mensaje
		verificamos validacion de formulario
		llenamos el formulario (Nikolaus - Miami)
		Verificamos que aparesca un vendor
		Hacemos click en el check del vendor
		Verificamos que aparesca como selected vendor
		hacemos click en info del vendor
		Verificamos que aparesca modal con los datos del vendor
		hacemos click en cerrar modal
		hacemos click en clear selected
		verificamos que se deselecciones el vendor y desaparesca el vendor selected
		hacemos click en check
		verificamso que aparescan los datos del vendor en selected vendor
		hacemos click en next
		verificamos el modal por su title y mensaje
		hacemos cick en ok
		verificamos que caimos en la pagina indicada verifiando textos en la misma
		hacemos c lick en complete
		verificamso el modal por su titulo y mensaje
		hacemos click en ok del modal
		verificamos que hayamos caido en la pagina de list request
     *	selClientRequestBidsTest::testClientNewRequest
     * @return void
     */
    public function testClientNewRequest()
    {
		$faker = Faker::create();
        $this->makeLogin('client0@1ssolution.com', 'client0');

        $this->byLinkText("Requests / Bids")->click();
		$this->byLinkText("New request")->click();
		$this->assertEquals($this->baseUrl.'/rfp/create', $this->url());
        $this->assertEquals('Create RFP | One Seamless Solution', $this->title());
		$this->assertEquals('RFP Create', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		$this->assertEquals('1New Request', $this->byXPath("//ul[@class='steps']/li[1]")->text());
		

		$this->byXPath("//select[@id='industry_id']//option[5]")->click();
		$this->byId("description")->clear();
		$this->byId("description")->value("Necesidad de servicio de janitorial");

		$this->byId("location")->clear();
		$this->byId("location")->value($faker->streetAddress);

		$dia1 = date('d', strtotime('+1 day', strtotime(date('Y-m-j'))));
		$dia5 = date('d', strtotime('+5 day', strtotime(date('Y-m-j'))));

		$this->byId("response_date")->click();
		$this->byXPath("//div[@class='datepicker-days']//td[.='$dia1']")->click();
		$this->byId("service_date")->click();
		$this->byXPath("//div[@class='datepicker-days']//td[.='$dia5']")->click();

		$this->byXPath("//form[@id='rfp_create']/div[6]/div[1]/div/label[3]/div/ins")->click();
		$this->byXPath("//form[@id='rfp_create']/div[6]/div[2]/div/label[3]/div/ins")->click();
		$this->byXPath("//form[@id='rfp_create']/div[6]/div[3]/div/label[3]/div/ins")->click();

		$this->byId("details")->clear();
		$this->byId("details")->value("Servicio interno 24 horas");
		$this->byId("documents")->clear();
		$this->byId("documents")->value("Documentos de salud");
		$this->byId("btnNext1")->click();
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();


//Paso 2

$this->byId("mySearchTextField")->click();
$this->byId("mySearchTextField")->value("niko");

$this->byXPath("//div[@id='serviceContainer']/div/button")->click();
$this->byLinkText("Healthcare facility cleaning")->click();
$this->byXPath("//select[@id='services']/option[@value='61']")->click();

$this->byLinkText("Vacuuming and carpet care")->click();

$this->byXPath("//select[@id='services']/option[@value='66']")->click();
$this->byLinkText("Trash removal")->click();
$this->byXPath("//select[@id='services']/option[@value='65']")->click();
$this->byLinkText("Porter Services")->click();
$this->byXPath("//select[@id='services']/option[@value='68']")->click();
$this->byLinkText("Emergency cleaning/restoration services")->click();
$this->byXPath("//select[@id='services']/option[@value='70']")->click();
$this->byLinkText("Ceiling and wall cleaning")->click();
$this->byXPath("//select[@id='services']/option[@value='72']")->click();
$this->byXPath("//div[@id='serviceContainer']/div/button")->click();
$this->byXPath("//select[@id='metroarea']//option[7]")->click();
$this->byXPath("//div[@class='step-content']/div[2]/div[2]/div[2]/div/div/button")->click();
$this->byXPath("//div[@class='step-content']/div[2]/div[2]/div[2]/div/div/div/div[2]/div/button[1]")->click();
$this->byXPath("//select[@id='attributes']/option[@value='1']")->click();
$this->byXPath("//select[@id='attributes']/option[@value='2']")->click();
$this->byXPath("//select[@id='attributes']/option[@value='3']")->click();
$this->byXPath("//div[@class='step-content']/div[2]/div[2]/div[2]/div/div/button")->click();
$this->byName("vendors_selected[]")->click();
$this->byCssSelector("i.fa.fa-list-alt")->click();
$this->byXPath("//div[@id='modalVendor']//button[.='Close']")->click();
$this->byId("clearVendorsSelection")->click();
$this->byName("vendors_selected[]")->click();
$this->byId("btnNext2")->click();
$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();

// paso 3
$this->byId("btnComplete")->click();
$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();

// vuelve a list
$this->byXPath("//table[@id='rfp']/tbody/tr[1]/td[10]/a/i")->click();
$this->byLinkText("No, edit fields")->click();
$this->byXPath("//select[@id='industry_id']//option[6]")->click();
$this->byId("btnNext1")->click();
$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();

$this->byXPath("//select[@id='metroarea']//option[7]")->click();
$this->byXPath("//select[@id='metroarea']//option[2]")->click();
$this->byXPath("//div[@id='serviceContainer']/div/button")->click();
$this->byXPath("//div[@id='serviceContainer']//button[.='Select All']")->click();
$this->byXPath("//select[@id='services']/option[@value='119']")->click();
$this->byXPath("//select[@id='services']/option[@value='120']")->click();
$this->byXPath("//select[@id='services']/option[@value='121']")->click();
$this->byXPath("//select[@id='services']/option[@value='122']")->click();
$this->byXPath("//select[@id='services']/option[@value='123']")->click();
$this->byXPath("//select[@id='services']/option[@value='124']")->click();
$this->byXPath("//select[@id='services']/option[@value='125']")->click();
$this->byXPath("//select[@id='services']/option[@value='126']")->click();
$this->byXPath("//select[@id='services']/option[@value='127']")->click();
$this->byXPath("//select[@id='services']/option[@value='128']")->click();
$this->byXPath("//select[@id='services']/option[@value='129']")->click();
$this->byXPath("//select[@id='services']/option[@value='130']")->click();
$this->byXPath("//select[@id='services']/option[@value='131']")->click();
$this->byXPath("//select[@id='services']/option[@value='132']")->click();
$this->byXPath("//select[@id='services']/option[@value='133']")->click();
$this->byXPath("//select[@id='services']/option[@value='134']")->click();
$this->byXPath("//select[@id='services']/option[@value='135']")->click();

$this->byXPath("//div[@id='serviceContainer']/div/button")->click();
$this->byXPath("//select[@id='metroarea']//option[3]")->click();
$this->byXPath("//select[@id='metroarea']//option[4]")->click();
$this->byXPath("//select[@id='metroarea']//option[5]")->click();
$this->byXPath("//select[@id='metroarea']//option[6]")->click();
$this->byXPath("//select[@id='metroarea']//option[7]")->click();
$this->byXPath("//select[@id='metroarea']//option[8]")->click();

$this->byXPath("//div[@class='step-content']/div[2]/div[2]/div[2]/div/div/button")->click();
$this->byXPath("//div[@class='step-content']/div[2]/div[2]/div[2]/div/div/div/div[2]/div/button[1]")->click();
$this->byXPath("//select[@id='attributes']/option[@value='4']")->click();
$this->byXPath("//select[@id='attributes']/option[@value='5']")->click();
$this->byXPath("//select[@id='attributes']/option[@value='6']")->click();

$this->byXPath("//div[@class='step-content']/div[2]/div[2]/div[2]/div/div/button")->click();
$this->byId("btnNext2")->click();
$this->byXPath("//div[@id='modal_error']//button[.='Ok ']")->click();

$this->byId("draft-button2")->click();
$this->byXPath("//div[@id='modal_error']//button[.='Ok ']")->click();
$this->byId("btnBack")->click();
$this->byId("draft-button")->click();
$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();











		$this->makeLogout();

    }
		
		
	
	/**
     * Client New Request Using Seva And Exit.
     (Client hace new request (Save and Exit))
     *
     * @return void
     */
    public function testClientRequestNewUsingSevaAndExit()
    {
		$faker = Faker::create();
        $this->assertTrue(true);
    }
	
	
	/**
     * A Client View All Request.
     (Client entra en Request/Bids -> View all)
		***** Dudas con comportamiento de Vendor y Request Action
     *	selClientRequestBidsTest::testClientViewAllRequest
     * @return void
     */
    public function testClientViewAllRequest()
    {
		$faker = Faker::create();
		$this->makeLogin('client0@1ssolution.com', 'client0');

        $this->byLinkText("Requests / Bids")->click();
		$this->byLinkText("View all")->click();
		$this->assertEquals($this->baseUrl.'/rfp/client/bidsrequest', $this->url());
        $this->assertEquals('Bids request | One Seamless Solution', $this->title());
		$this->assertEquals('Bids request', $this->byXPath("//div/div[1]/section[1]/h1")->text());



	$this->byXPath("//div[@id='rfp_wrapper']/div[2]/div[2]/button")->click();
    // clickElement
    $this->byLinkText("RFP")->click();
    // setElementSelected
    $element = $this->byXPath("//div[@id='rfp_wrapper']//select[.='AllSRRFP']//option[3]");
    if (!$element->selected()) {
      $element->click();
    }
    // clickElement
    $this->byXPath("//div[@id='rfp_wrapper']/div[2]/div[1]/button")->click();
    // clickElement
    $this->byLinkText("Workorder")->click();
    // setElementSelected
    $element = $this->byXPath("//div[@id='rfp_wrapper']//select[.='AllOpenSentBidWorkorderClosed']//option[5]");
    if (!$element->selected()) {
      $element->click();
    }
    // clickElement
    $this->byXPath("//table[@id='rfp']//td[.='Workorder']")->click();
    // clickElement
    $this->byXPath("//table[@id='rfp']//td[.='RFP']")->click();
    // clickElement
    // $this->byLinkText("3/1")->click();
    // clickElement
    $this->byXPath("//table[@id='VendorBidsRequest']/tbody/tr[1]/td[5]/a/i")->click();
    // clickElement
    $this->byXPath("//div[@id='modalVendor']//button[.='Close']")->click();
    // clickElement
    $this->byXPath("//table[@id='VendorBidsRequest']/tbody/tr[1]/td[6]/a/i")->click();
    // clickElement
    $this->byLinkText("More Info.")->click();
    // clickElement
    $this->byLinkText("Request / Bids")->click();
    // clickElement
    $this->byXPath("//div[@id='rfp_wrapper']/div[2]/div[1]/button")->click();
    // clickElement
    $this->byLinkText("Workorder")->click();
    // setElementSelected
    $element = $this->byXPath("//div[@id='rfp_wrapper']//select[.='AllOpenSentBidWorkorderClosed']//option[5]");
    if (!$element->selected()) {
      $element->click();
    }
    // clickElement
    $this->byXPath("//div[@id='rfp_wrapper']/div[2]/div[2]/button")->click();
    // clickElement
    $this->byLinkText("RFP")->click();
    // setElementSelected
    $element = $this->byXPath("//div[@id='rfp_wrapper']//select[.='AllSRRFP']//option[3]");
    if (!$element->selected()) {
      $element->click();
    }
    // clickElement
    $this->byXPath("//table[@id='rfp']/tbody/tr[1]/td[10]/a/i")->click();
    // clickElement
    $this->byLinkText("Request / Bids")->click();
    // clickElement
    $this->byXPath("//div[@id='rfp_wrapper']/div[2]/div[1]/button")->click();
    // clickElement
    $this->byLinkText("Workorder")->click();
    // setElementSelected
    $element = $this->byXPath("//div[@id='rfp_wrapper']//select[.='AllOpenSentBidWorkorderClosed']//option[5]");
    if (!$element->selected()) {
      $element->click();
    }
    // clickElement
    $this->byXPath("//div[@id='rfp_wrapper']/div[2]/div[2]/button")->click();
    // clickElement
    $this->byLinkText("RFP")->click();
    // setElementSelected
    $element = $this->byXPath("//div[@id='rfp_wrapper']//select[.='AllSRRFP']//option[3]");
    if (!$element->selected()) {
      $element->click();
    }
    // clickElement
    $this->byLinkText("3/1")->click();
    // clickElement
    $this->byXPath("//table[@id='VendorBidsRequest']//td[.='Open']")->click();
    // clickElement
    $this->byXPath("//table[@id='VendorBidsRequest']/tbody/tr[1]/td[2]")->click();
    // clickElement
    $this->byId("reloadRfpTable")->click();

		

		$this->makeLogout();
    }
}
