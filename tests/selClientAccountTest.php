<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

use Laracasts\Integrated\Extensions\Selenium;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;


class selClientAccountTest extends PHPUnit_Extensions_Selenium2TestCase
{
    use DatabaseTransactions;
    use Laravel;
    
    protected $baseUrl = 'http://dev.1ssolution.com';

    public function setUp()
    {
        parent::setUp();

        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->baseUrl);   

    }


	public static function setUpBeforeClass(){
	    self::shareSession(true);
        // DB::beginTransaction();
        // $this->app['db']->beginTransaction();
	}


    public function tearDown(){
		parent::tearDown();
	    // $this->stop();
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilTimeout(0);
		PHPUnit_Extensions_Selenium2TestCase::setDefaultWaitUntilSleepInterval(500);
	}

	public static function tearDownAfterClass()
    {
        // parent::tearDownAfterClass();        
	    // $this->app['db']->rollBack(); 
	    // DB::rollBack();
    }



	protected function makeLogin($email, $pass){
		// $email 	= 'client0@1ssolution.com';
		// $pass 	= 'client0';	
		$this->url($this->baseUrl.'/login');
        $this->assertEquals('Sign in to start your session', $this->byCssSelector('.login-box-msg')->text());
        $this->assertEquals('http://dev.1ssolution.com/login', $this->url());
		// Llenamos el formulario con datos correcto
 		$form = $this->byCssSelector('form');
 		$formAction = $form->attribute('action');
 		$this->assertEquals($this->baseUrl.'/login', $formAction);
		$this->byName('email')->value($email);
		$this->byName('password')->value($pass);
		$this->byCssSelector("ins.iCheck-helper")->click();
		// enviamos el formulario
		// $this->byId('login-form')->submit();
		$form->submit();
		$this->timeouts()->implicitWait(5000);
	}



	protected function makeLogout(){
		//Salimos del sistema
		$this->byLinkText("Logout")->click();
		$this->timeouts()->implicitWait(5000);
		// Verificamos que llegamos a la pagina principal
		$this->assertEquals($this->baseUrl.'/login', $this->url());	
	}


	protected function formatPhone($phone){
		$phoneExplode = explode('-', $phone);
		$n = count($phoneExplode);
		if($n > 3){
			return '('.$phoneExplode[0].') '.$phoneExplode[1].'-'.$phoneExplode[2].' '.$phoneExplode[3];
		} else {
			return '('.$phoneExplode[0].') '.$phoneExplode[1].'-'.$phoneExplode[2];
		}
    }
    




	/**
     * Verify Client Data Form.
     (Verificamo datos de cuenta de Cliente)
		Cliente con datos correcto hace login
		comprobamos la url, title y textos en esta url (http://dev.1ssolution.com/account/client)
		comprobamos el title
		comprobamos campos para Parent Corporation, Client Info 
		comprobamos que los campos esten llenos y sean redonly
		comprobamos los campos en client facility location
		limpiamos el formulario
		llenamos el formulario con nueva location
		enviamos el formulario
		verificamos el modal window tituo y mensaje
		cerramos el modal window
		volvemos a verificar que la informacion que habiamos enviado este impresa
     * 	selClientAccountTest::testVerifyClientDataForm
     * @return void
     */
    public function testVerifyClientDataForm()
    {
		$faker = Faker::create();
        $this->makeLogin('client0@1ssolution.com', 'client0');
        $this->byLinkText("Account")->click();
		$this->byLinkText("Client account")->click();
		$this->assertEquals($this->baseUrl.'/account/client', $this->url());
        $this->assertEquals('Client User Info | One Seamless Solution', $this->title());
		$this->assertEquals('Account Company Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		
		$readOnlyFields = array('corporationName', 'corporationPhone', 'corporationEmail', 'corporationUrl', 'clientName', 'clientPhone', 'clientEmail', 'clientUrl');
		foreach ($readOnlyFields as $key) {
			$this->assertEquals('true', $this->byId($key)->attribute('readonly'));
		}

		$this->assertEquals($this->baseUrl.'/client/location', $this->byCssSelector('form')->attribute('action'));
		
		$inputFields = array('_token','address','address2','phone','zip');
		$selectFields = array('country','state','city');
		// $country = $this->byXPath("//select[@name='country']/option")->text();
		foreach ($inputFields as $f) {
			$v = $this->byName($f)->value();
			$this->assertTrue(!empty($v));
		}
		foreach ($selectFields as $f) {
			$v = $this->byXPath("//select[@name='$f']/option")->text();
			$this->assertTrue(!empty($v));
		}

		$address 	= $faker->streetAddress;
		$address2 	= $faker->secondaryAddress;
		$phone 		= '(151) 646-8741';
		$zip 		= '13223-954';

		$inputFields = array(
					'address' 	=> $address,
					'address2' 	=> $address2,
					'phone' 	=> $phone,
					'zip' 		=> $zip
					);
		foreach ($inputFields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}
// 3948	Missouri
// 45426	Camdenton
// 3930 Florida
// 43618 Cape Coral
		$selectFields = array(
					'state' => '3930',
					'city' 	=> '43618'
					);
		foreach ($selectFields as $f => $v) {
			// $this->byName($f)->clear();
			$this->byXPath("//select[@name='$f']/option[@value='$v']")->click();
		}

		$this->assertEquals('Cancel', $this->byCssSelector('button')->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byCssSelector('button')->attribute('onclick')
			);
		$this->assertEquals('Update Location', $this->byCssSelector('.btn.btn-success.btn-flat')->value());
		// $this->keys(Keys::ENTER);
		$this->byCssSelector('.btn.btn-success.btn-flat')->submit();

		sleep(10);
		$title 	= $this->byId("modal_success_title")->text();
		$message= $this->byId('modal_success_message')->text();
		$this->assertEquals('Facility Location', $title);
		$this->assertEquals('Success! You have updated your facility location', $message);		
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();

		// sleep(1);
		// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());


		foreach ($inputFields as $f => $v) {
			if($this->byName($f)->text() == 'phone'){
				$this->assertEquals(formatPhone($this->byName($f)->text()), $this->byName($f)->value());
			} else {
				$this->assertEquals($v, $this->byName($f)->value());
			}
		}

		foreach ($selectFields as $f => $v) {
			$this->assertEquals($v, $this->byXPath("//select[@name='$f']/option[@value='$v']")->value());
		}
	
		$this->makeLogout();
    }
    
		
	
	/**
     * A basic test example.
     (Verificamos datos de cuenta de usuario)
		hacemos clic en User account
		verificamo que llegamos a la pagina deseada url, title y textos
		Verificamos que el formulario para User contenga los campos apropiados
		verificamso que el campo email sea redonly
		limpiaos todos los campos 
		hacemos click en boton Cancel
		verificamo que llegamos a la pagina deseada url, title y textos
		Verificamos que el formulario para User contenga los campos apropiados
		verificamso que el campo email sea redonly
		limpiaos todos los campos 
		enviamos el formulario
		verificamos mensajes de validacion
		llenamos todos los camos con datos nuevos
		enviamos el formulario
		verificamos modal window por titulo y mensaje
		cerramos modal haciendo click en ok
		verificamos que los datos que habiamos enviados este presente en los campos del formulario
     *	selClientAccountTest::testVerifyUserDataForm
     * @return void
     */
    public function testVerifyUserDataForm()
    {
		$faker = Faker::create();
        $this->makeLogin('client0@1ssolution.com', 'client0');
        $this->byLinkText("Account")->click();
		$this->byLinkText("User account")->click();
		$this->assertEquals($this->baseUrl.'/account/user', $this->url());
        $this->assertEquals('Vendor User Info | One Seamless Solution', $this->title());
		$this->assertEquals('User Account User Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());
		
		$this->assertEquals($this->baseUrl.'/account/user', $this->byId('user_edit')->attribute('action'));
		$this->assertEquals('true', $this->byName('email')->attribute('readonly'));
		$fields = array('_token','email','phone','name','lastname');
		foreach ($fields as $f) {
			$v = $this->byName($f)->value();
			$this->assertTrue(!empty($v));
		}
		$phone 		= '(151) 646-8741';
		$name 		= $faker->firstName;
		$lastname 	= $faker->lastname;
		$fields = array(
			'phone' 	=> $phone,
			'name' 		=> $name,
			'lastname' 	=> $lastname,
			);
		foreach ($fields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}

		$this->assertEquals('Cancel', $this->byXPath("//form[@id='user_edit']//button")->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byXPath("//form[@id='user_edit']//button")->attribute('onclick')
			);
		$this->assertEquals('Update User', $this->byCssSelector('.btn.btn-success.btn-flat')->value());
		// $this->keys(Keys::ENTER);
		$this->byId("user_edit")->submit();
		// $this->byXPath("//form[@id='user_edit']/div[1]/div[2]/div[3]/input")->click();
		// $this->timeouts()->implicitWait(5000);

		sleep(10);
		$title 	= $this->byId("modal_success_title")->text();
		$message= $this->byId('modal_success_message')->text();
		$this->assertEquals('User information', $title);
		$this->assertEquals('Success! You have updated your user information', $message);		
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();

		// sleep(1);
		// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());

		foreach ($fields as $f => $v) {
			if($this->byName($f)->text() == 'phone'){				
				$this->assertEquals(formatPhone($this->byName($f)->text()), $this->byName($f)->value());
			} else {
				$this->assertEquals($v, $this->byName($f)->value());
			}
		}
	
		$this->makeLogout();
    }


	
	/**
     * Verify User Change Password.
     (Verificamos datos para Change Password)
		hacemos clic en User account
		verificamo que llegamos a la pagina deseada url, title y textos
		Verificamos que el formulario para Change Password contenga los campos apropiados
		enviamos el formulario
		verificamos mensajes de validacion
		llenamos todos los campos con datos nuevos
		enviamos el formulario
		verificamos modal window por titulo y mensaje
		cerramos modal haciendo click en ok
		verificamos que los datos que habiamos enviados esten presente en los campos del formulario
     *	selClientAccountTest::testVerifyUserChangePassword
     * @return void
     */
    public function testVerifyUserChangePassword()
    {
		$this->makeLogin('client0@1ssolution.com', 'client0');
        $this->byLinkText("Account")->click();
		$this->byLinkText("User account")->click();
		$this->assertEquals($this->baseUrl.'/account/user', $this->url());
        $this->assertEquals('Vendor User Info | One Seamless Solution', $this->title());
		$this->assertEquals('User Account User Information', $this->byXPath("//div/div[1]/section[1]/h1")->text());		
		$this->assertEquals('Change password', $this->byXPath("//div/div[1]/section[2]/div[2]/div/div/div/h3")->text());
		$this->assertEquals($this->baseUrl.'/user/passwd', $this->byId('user_passwd_edit')->attribute('action'));

		$password 			= '123456';
		$current_password 	= '123456';
		$fields = array(
			'password' 	=> $password,
			'password_confirmation' => $password,
			'current_password' 	=> $current_password
			);
		foreach ($fields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}

		$this->assertEquals('Cancel', $this->byXPath("//div[@id='pwd-container']//button")->text());
		$this->assertEquals(
			'window.location.reload()', 
			$this->byXPath("//div[@id='pwd-container']//button")->attribute('onclick')
			);
		$this->assertEquals('Update Password', $this->byId('update_change_password')->value());
		// $this->keys(Keys::ENTER);

		$this->byId("update_change_password")->submit();
		// $this->byXPath("//form[@id='user_edit']/div[1]/div[2]/div[3]/input")->click();
		// $this->timeouts()->implicitWait(5000);
		sleep(10);
		$title 	= $this->byId("modal_error_title")->text();
		$message= $this->byId('modal_error_message')->text();
		$this->assertEquals('Error', $title);
		$this->assertEquals('The current password does not match', $message);		
		$this->byXPath("//div[@id='modal_error']//button[.='Ok ']")->click();

		$this->byName("password")->clear();
		$textVerdict = $this->byCssSelector("span.password-verdict")->text();
		$this->assertTrue(empty($textVerdict), $this->byCssSelector("span.password-verdict")->text());
		$this->byName("password")->value("!QwertY_147852@"); 
		$this->assertEquals('Very Strong', $this->byCssSelector("span.password-verdict")->text());

		$password 			= 'client0';
		$current_password 	= 'client0';
		$fields = array(
			'password' 	=> $password,
			'password_confirmation' => $password,
			'current_password' 	=> $current_password
			);
		foreach ($fields as $f => $v) {
			$this->byName($f)->clear();
			$this->byName($f)->value($v); 
		}
		$this->byId("update_change_password")->submit();
		sleep(5);
		$title 	= $this->byId("modal_success_title")->text();
		$message= $this->byId('modal_success_message')->text();
		$this->assertEquals('User password', $title);
		$this->assertEquals('Success! You have updated your password', $message);		
		$this->byXPath("//div[@id='modal_success']//button[.='Ok ']")->click();
// sleep(1);
// $this->assertEquals('Update successfully', $this->byXPath("//span[@class='noty_text']")->text());

		$this->makeLogout();
    }	
}
